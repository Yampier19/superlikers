<?php

namespace App\Exports;

use App\Models\Factura;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FacturaExport implements FromCollection, WithHeadings
{

    public function headings():array{
        return [
            'id_factura',
            'programa',
            'referencia',
            'cdc',
            'folio',
            'mesero',
            'campaign',
            'state',
            'moderation',
            'envio',
            'photo',
            'fecha impresa',
            'lectura',
            'tarifario',
            'fecha registrada SL',
            'fecha lectura PM',
            'nombre',
            'email',
        ];
    }

    public function __construct(string $fechaIni, string $fechaFin, string $tipo)
    {
        $this->fechaIni = $fechaIni;
        $this->fechaFin = $fechaFin;
        $this->tipo = $tipo;
    }

    public function collection()
    {
        if($this->tipo == "lectura"){
            $facturas = DB::table('facturas')
                            ->join('participante', 'participante.id_participante', '=', 'facturas.id_factura')
                            ->select('id_factura', 'facturas.programa', 'referencia', 'cdc', 'folio', 'mesero', 'campaign', 
                                    'state', 'moderation', 'envio', 'photo', 'registro', 'lectura', 'tarifario',
                                    'user_upload', 'facturas.created_at', 'name', 'email')
                            ->whereDate('facturas.created_at', '>=', $this->fechaIni)
                            ->whereDate('facturas.created_at', '<=', $this->fechaFin)
                            ->get();
        }
        else{
            $facturas = DB::table('facturas')
                            ->join('participante', 'participante.id_participante', '=', 'facturas.id_factura')
                            ->select('id_factura', 'facturas.programa', 'referencia', 'cdc', 'folio', 'mesero', 'campaign', 
                                    'state', 'moderation', 'envio', 'photo', 'registro', 'lectura', 'tarifario',
                                    'user_upload', 'facturas.created_at', 'name', 'email')
                            ->whereDate('user_upload', '>=', $this->fechaIni)
                            ->whereDate('user_upload', '<=', $this->fechaFin)
                            ->get();
        }

        return $facturas;
    }
}
