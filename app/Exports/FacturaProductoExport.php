<?php

namespace App\Exports;

use App\Models\Factura;
use App\Models\FacturaProducto;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FacturaProductoExport implements FromCollection, WithHeadings
{

    public function headings():array{
        return [
            'referencia',
            'price',
            'quantity',
            'provider',
            'line',
            'formato',
            'FK_id_factura',
        ];
    }

    public function __construct(string $fechaIni, string $fechaFin, string $tipo)
    {
        $this->fechaIni = $fechaIni;
        $this->fechaFin = $fechaFin;
        $this->tipo = $tipo;
    }

    public function collection()
    {
        if($this->tipo == "lectura"){
            $facturas = DB::table('factura_producto')
                            ->join('facturas', 'facturas.id_factura', '=', 'factura_producto.FK_id_factura')
                            ->selectRaw('factura_producto.referencia , round(price, 2) as price, quantity, provider, line, formato, FK_id_factura')
                            ->whereDate('facturas.created_at', '>=', $this->fechaIni)
                            ->whereDate('facturas.created_at', '<=', $this->fechaFin)
                            ->get();
        }
        else{
            $facturas = DB::table('factura_producto')
                            ->join('facturas', 'facturas.id_factura', '=', 'factura_producto.FK_id_factura')
                            ->selectRaw('factura_producto.referencia , round(price, 2) as price, quantity, provider, line, formato, FK_id_factura')
                            ->whereDate('user_upload', '>=', $this->fechaIni)
                            ->whereDate('user_upload', '<=', $this->fechaFin)
                            ->get();
        }

        return $facturas;
    }
}
