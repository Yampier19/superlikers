<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

//Exports
use App\Exports\FacturaExport;
use App\Exports\FacturaProductoExport;

class FacturaMultipleExport implements WithMultipleSheets
{
    public function __construct(string $fechaIni, string $fechaFin, string $tipo)
    {
        $this->fechaIni = $fechaIni;
        $this->fechaFin = $fechaFin;
        $this->tipo = $tipo;
    }

    public function sheets(): array
    {
        return [
            'Factura Información' => new FacturaExport($this->fechaIni, $this->fechaFin, $this->tipo),
            'Factura Productos' => new FacturaProductoExport($this->fechaIni, $this->fechaFin, $this->tipo),
        ];
    }
}
