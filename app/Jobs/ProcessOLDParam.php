<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;
use Illuminate\Support\Facades\DB;

//Modelos
use App\Models\FacturaVieja;
use App\Models\FacturaViejaProducto;

//Helper
use App\Helper\FacturaIA2;
use Illuminate\Support\Facades\Storage;

class ProcessOLDParam implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $factura;

    public function __construct(FacturaVieja $factura)
    {
        $this->factura = $factura->withoutRelations(); //modelo de la factura en la BD
    }

    //buscamos la primera factura con no param y posterior a ello la enviamos a paramterizar y al OCR
    public function handle()
    {
        ini_set('max_execution_time', 600); //10 minutos
        DB::beginTransaction();
        try{
            $factura = $this->factura;
            
            //Almaceno la foto
            if(!$factura->lectura)
                $archivo = $this->saveFactura($factura->photo);
            else
                $archivo = true;
            
            if($factura->campaign != "ti")
                return;

            if($archivo){
                //La envio a leer
                if(!$factura->lectura){
                    $values = FacturaIA2::instance()->readFactura($factura->photo, $archivo);
                    $values->reparam = "no";
                    try{ unlink($archivo); }
                    catch(\Exception $e){}
                }
                else{
                    $values = new \stdClass;
                    $values->contenido = $factura->lectura;
                    $values->reparam = "si";
                }

                if(isset($values->error)){
                    /* return response()->json([
                        'status' => 'error',
                        'message' => 'Error interno en el proceso de lectura del archivo',
                        'error message' => $values->error,
                        "codigo" => 503,
                    ], 503); */
                    $factura->lectura = "";
                    $factura->mesero = "No parametrizo";
                    $factura->folio = "No parametrizo";
                    //$factura->state = "rechazado";
                    $factura->save();
                    DB::commit();
                    return;
                }

                if($values){
                    $factura->lectura = $values->contenido;

                    //La envio a parametrizar
                    $values = FacturaIA2::instance()->parametrizar($values);
                    
                    if(isset($values->error)){
                        $factura->lectura = "";
                        $factura->mesero = "No parametrizo";
                        $factura->folio = "No parametrizo";
                        //$factura->state = "fallo";
                        $factura->save();
                        DB::commit();
                    }

                    //Ahora manejamos los datos evaluados
                        $factura->cdc = $values->restaurante;
                        $factura->campaign = $values->category; //mes de la factura
                        $factura->folio = $values->referencia;
                        $factura->mesero = $values->mesero;
                        $factura->registro = $values->fecha_factura;

                    //Grabamos los productos Pernod recibidos
                        foreach ($values->productos as $key => $producto) {
                            $pernod = new FacturaViejaProducto();
                            $pernod->referencia = $producto["referencia"];
                            $pernod->price = $producto["precio"];
                            $pernod->quantity = $producto["cantidad"];
                            $pernod->provider = "Pernod Ricard";//$producto["proveedor"];
                            $pernod->line = $producto["linea"];
                            $pernod->formato = $producto["formato"];
                            $pernod->FK_id_factura = $factura["id_factura"];
                            $pernod->save();
                        }
                    
                    //Hacemos save de todo
                    $factura->save();

                    DB::commit();
                }
                else{
                    $factura->lectura = "";
                    $factura->mesero = "No parametrizo";
                    $factura->folio = "No parametrizo";
                    //$factura->state = "sin texto";
                    $factura->save();
                    DB::commit();
                }
            }
            else{
                $factura->lectura = null;
                $factura->mesero = "No parametrizo";
                $factura->folio = "No parametrizo";
                //$factura->state = "no param";
                $factura->save();
                DB::commit();
            }
        }catch(\Exception $e){
            DB::rollback();
            throw new \Exception($e->getMessage(), 1);
        }
    }

    //Almacenar la factura para enviar al OCR
    public function saveFactura($link){
        
        $url = $link;
        
        try{
            $content = file_get_contents($url);
            $name = substr($url, strrpos($url, '/') + 1);

            //El archivo esta grabado
            $archivo = storage_path().'/app/public/invoices/'.$name;
            file_put_contents($archivo, $content);

            return $archivo;
        }
        catch(\Exception $e){
            //return $e->getCode()." | ".$e->getMessage();
            return null;
        }
    }

    //Manejando el failed del job
    public function failed(\Exception $e)
    {
        \Illuminate\Support\Facades\Log::debug($e->getMessage());
    }
}
