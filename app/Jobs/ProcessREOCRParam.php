<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;
use Illuminate\Support\Facades\DB;

//Modelos
use App\Models\Factura;
use App\Models\FacturaVieja;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;

//Helper
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;
use Illuminate\Support\Facades\Storage;

class ProcessREOCRParam implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $factura;

    public function __construct(FacturaVieja $factura)
    {
        $this->factura = $factura->withoutRelations(); //modelo de la factura en la BD
    }

    //buscamos la primera factura con no param y posterior a ello la enviamos a paramterizar y al OCR
    public function handle()
    {
        DB::beginTransaction();
        try{
            $factura = $this->factura;

            $values = new \stdClass;
            $values->contenido = $factura->lectura;
            $values->upload = $factura->user_upload;
            $values->contenido = $factura->lectura;
            $values->id_factura = $factura->id_factura;
            $values->reparam = "no";
            //echo $factura->id_factura;
            
            if($values){
                $factura->lectura = $values->contenido;

                //La envio a parametrizar
                $values = FacturaIA2::instance()->parametrizar($values);
                
                if(isset($values->error)){
                    //$factura->lectura = "";
                    //$factura->mesero = "No parametrizo";
                    //$factura->folio = "No parametrizo";
                    $factura->state = "rechazado";
                    $factura->save();
                    DB::commit();
                }

                //Ahora manejamos los datos evaluados
                    //$factura->cdc = $values->restaurante;
                    if(date("m",strtotime($values->fecha_factura)) != "04")
                        $factura->campaign = $values->category; //mes de la factura
                    //$factura->folio = $values->referencia;
                    //$factura->mesero = $values->mesero;
                    $factura->state = $values->estado;
                    if(date("m",strtotime($values->fecha_factura)) != "04")
                        $factura->registro = $values->fecha_factura;

                //Grabamos los productos Pernod recibidos
                    /* foreach ($values->productos as $key => $producto) {
                        $pernod = new FacturaProducto();
                        $pernod->referencia = $producto["referencia"];
                        $pernod->price = $producto["precio"];
                        $pernod->quantity = $producto["cantidad"];
                        $pernod->provider = $producto["proveedor"];
                        $pernod->line = $producto["linea"];
                        $pernod->FK_id_factura = $factura["id_factura"];
                        $pernod->save();
                    } */
                
                //Hacemos save de todo
                $factura->save();

                DB::commit();
            }
            else{
                $factura->lectura = "";
                $factura->mesero = "No parametrizo";
                $factura->folio = "No parametrizo";
                $factura->state = "rechazado";
                $factura->save();
                DB::commit();
            }
            
        }catch(\Exception $e){
            DB::rollback();
            throw new \Exception($e->getMessage(), 1);
        }
    }
}
