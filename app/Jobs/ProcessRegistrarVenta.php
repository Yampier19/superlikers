<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;

//Helper
use App\Helper\FacturaIA;
use Illuminate\Support\Facades\Storage;

class ProcessRegistrarVenta implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $factura;

    public function __construct(Factura $factura)
    {
        $this->factura = $factura; //modelo de la factura en la BD
    }

    public function handle()
    {
        DB::beginTransaction();
        try{
            $factura = $this->factura;

            //cambio el estado interno como enviado
            
                $factura->estado_int = "enviado";
            
            
            if($factura->state != "rechazado"){
                //Seteamos los datos para enviar al endpoint de registrar factura
                $url = "https://api.superlikers.com/v1/retail/buy";

                $data = [
                    'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                    'campaign' => 'ti',//$factura->campaign,
                    'category' => 'invoice',
                    'distinct_id' => $factura->participante->uid, //"uid - correo": del usuario participante o mesero,
                    'ref' => $factura->referencia, //referencia de la factura en superlikers
                    'date' => strtotime($factura->registro), ///TIEMPO EN UNIX
                    'products' => [],
                    'properties' => [
                        'tarifario' => $factura->tarifario,
                        'folio' => $factura->folio,
                        'url_photo' => $factura->photo,
                    ]
                ];
                

                //Productos Pernod
                foreach ($factura->productos as $producto) {
                    $pernot = array(
                        'ref' => $producto->referencia, //codigo referencia del producto
                        'provider' => $producto->provider, 
                        'line' => $producto->line,
                        'price' => ($producto->price / $producto->quantity),
                        'quantity' => $producto->quantity,
                    );
                    array_push($data['products'], $pernot);
                }

                //Productos Extras
                foreach ($factura->extras as $producto) {
                    $extra = array(
                        'ref' => $producto->nombre, //nombre del producto
                        'provider' => 'adicional', 
                        'price' => 0,
                        'quantity' => 0,
                    );
                    array_push($data['products'], $extra);
                }

                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                ])->post($url, $data);

                if($response->successful()){
                    $data = $response->object();
                    //SE REGISTRO LA VENTA
                    if($data->ok){
                        
                        if($factura->state == "aceptado"){
                            $url = "https://api.superlikers.com/v1/entries/accept";

                            $send = [
                                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                                'campaign' => 'ti',
                                'id' => $factura->referencia, //identificador de la actividad - factura en superlikers
                            ];

                            $response = Http::withHeaders([
                                'Content-Type' => 'application/json',
                            ])->post($url, $send);
                            
                            if($response->successful()){
                                $data = $response->object();
                                //Revisamos si fue exitoso para actualizar la factura
                                if($data->ok){
                                    $factura->moderation = "accepted";
                                }
                                else{
                                    if($data->code_error == 41){
                                        $factura->moderation = "accepted";
                                    }
                                }
                            }
                            else{
                                //AQUI SE EJECUTAN LOS CODE ERROR, EN ESTE CASO ES CUANDO EL CÓDIGO ES 63 PORQUE ESTA REGISTRADO
                                $data = $response->object();
                                //Vemos si el error es porque ya se cargo
                                if($data->code_error == 41){
                                    $factura->moderation = "accepted";
                                }
                            }
                        }

                         //CAMBIO EL ESTADO INTERNO
                        $factura->estado_int = "enviado";
                        $factura->envio = 1;
                        $factura->save();
                        DB::commit();
                    }
                    else{
                        //Vemos si el error es porque ya se cargo
                        if($data->code_error == 63){
                            $factura->envio = 1;
                            $factura->estado_int = "enviado";
                            $factura->moderation = "accepted";
                            $factura->save();
                            DB::commit();
                        }
                        else
                            return;
                    }
                }
                else if($response->clientError()){
                    //AQUI SE EJECUTAN LOS CODE ERROR, EN ESTE CASO ES CUANDO EL CÓDIGO ES 63 PORQUE ESTA REGISTRADO
                    $data = $response->object();
                    //Vemos si el error es porque ya se cargo
                    if($data->code_error == 63){
                        $factura->envio = 1;
                        $factura->estado_int = "enviado";
                        $factura->moderation = "accepted";
                        $factura->save();
                        DB::commit();
                    }
                    elseif($data->code_error == 20){
                        $factura->envio = 1;
                        $factura->estado_int = "enviado";
                        $factura->moderation = "accepted";
                        $factura->save();
                        DB::commit();
                    }
                    else{
                        DB::rollback();
                        throw new \Exception($data->code_error, 1);
                    }
                }
                else{
                    DB::rollback();
                    throw new \Exception('Error interno del servidor de Superlikers', 1);
                }
            }
            else{
                //la envio a rechazar no más y ya
                $url = "https://api.superlikers.com/v1/entries/reject";

                $data = [
                    'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                    'campaign' => 'ti',
                    'id' => $factura->referencia, //identificador de la actividad - factura en superlikers
                ];

                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                ])->post($url, $data);
                
                if($response->successful()){
                    $data = $response->object();
                    //Revisamos si fue exitoso para actualizar la factura
                    if($data->ok){
                        $factura->envio = 1;
                        $factura->estado_int = "enviado";
                        $factura->moderation = "rejected";
                        $factura->save();
                        DB::commit();
                    }
                    else{
                        if($data->code_error == 41){
                            $factura->envio = 1;
                            $factura->estado_int = "enviado";
                            $factura->moderation = "rejected";
                            $factura->save();
                            DB::commit();
                        }
                        else
                            return;
                    }
                }
                else if($response->clientError()){
                    $data = $response->object();
                    if($data->code_error == 41){
                        $factura->envio = 1;
                        $factura->estado_int = "enviado";
                        $factura->moderation = "rejected";
                        $factura->save();
                        DB::commit();
                    }
                    else{
                        DB::rollback();
                        throw new \Exception('No se pudo hacer la conexión con la API de Superlikers, intentelo más tarde', 1);
                    }
                }
                else{
                    DB::rollback();
                    throw new \Exception('Error interno del servidor de Superlikers', 1);
                }
            }

            

        }catch(\Exception $e){
            DB::rollback();
            throw new \Exception($e->getMessage()." | ".$e->getLine(), 1);
        }
    }
}
