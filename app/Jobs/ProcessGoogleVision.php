<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

//Modelos
use App\Models\ContentRead;

//Helper
use App\Helper\FacturaIA2;
use Illuminate\Support\Facades\Storage;

class ProcessGoogleVision implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file;

    public function __construct($file = null)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = storage_path().'/app/public/invoices/comprimida.jpg';
        //$file = Storage::disk('public')->get('invoices/16202673156869073921503274056703.jpg');
        
        $values = FacturaIA2::instance()->read500($file);
        
        $lectura = new ContentRead();
        $lectura->id_archivo = rand();
        $lectura->archivo = 'comprimida.jpg';

        if($values){
            if(isset($values->error)){
                $lectura->contenido = "Error en el proceso de lectura en google vision";
            }
            else{
                $lectura->contenido = $values->contenido;
            }
        }
        else{
            $lectura->contenido = "No pudo leer nada el google vision";
        }
        
        $lectura->save();
    }
}
