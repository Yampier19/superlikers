<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;

//Modelos
use App\Models\Participante;
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;

//Jobs
use App\Jobs\ProcessGoogleVision;
use App\Jobs\ProcessOCRParam;
use App\Jobs\ProcessREOCRParam;
use App\Jobs\ProcessRegistrarVenta;
use App\Jobs\ProcessOLDParam;

//Helper
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;
use App\Models\FacturaVieja;
use App\Models\FacturaViejaProducto;
use Illuminate\Support\Facades\Storage;

class FacturaViejaController extends Controller
{
    public $mensajes_error = [
        'required' => 'El dato archivo es requerido (procura enviarlo bajo este nombre)',
        'file' => 'El dato debe llegar como formato file (archivo)',
        'mimes' => 'El archivo debe llegar solo en formato png, jpg, jpeg o pdf',
        'archivo.max' => 'El archivo no puede ser mayor a 2Mb',
    ];

    public $validaciones = [
        'archivo' => 'required|file|mimes:jpg,jpeg,png,pdf',
    ];

    //PROCESO YA NORMAL DEL FLUJO COMPLETO
        //CONSULTA DE LAS ENTRADAS ALMACENADAS EN SUPERLIKERS
        public function entries(Request $request){
            
            $url = "https://api.superlikers.com/v1/entries/index";

            //Validamos que hayan enviado la cantidad que se desea sacar
            if(!$request->cantidad){
                return response()->json([
                    'status' => 'warning',
                    'message' => "Porfavor envia el dato cantidad para saber cuantas facturas debemos sacar de superlikers",
                    'dato' => "cantidad"
                ], 200);
            }

            $cantidad = $request->cantidad;
            $almacenadas = 0;

            //jaureguilucio6962@gmail.com
            //karenespinosa26429@gmail.com
            //albertopagola78@gmail.com

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                '_type' => 'UploadPhoto',
                'category' => 'invoice',
                //'before' => '1639116538.149',
                //'after' => '1638381897.451',
                //'limit' => 150,
                //'hide_undo' => true,
                //'distinct_id' => 'jaureguilucio6962@gmail.com',
                'date_filter' => [
                    'sdate' => '2021-09-02',//date('Y-m-d'), //dia DE HOY
                    'edate' => '2021-12-01', //dia SIGUIENTE
                ],
            ]);
            
            $data = $response->object();
            return $data;
        }

        //LA CONSULTA DE LA API DE ENTRIES DE SUPERLIKERS Y ALMACENADO DE LA INFORMACIÓN DE LA MISMA
        public function retrieveEntries(Request $request){
            
            $url = "https://api.superlikers.com/v1/entries/index";
            
            $urlparticipant = "https://api.superlikers.com/v1/participants/info";

            //Validamos que hayan enviado la cantidad que se desea sacar
            if(!$request->cantidad){
                return response()->json([
                    'status' => 'warning',
                    'message' => "Porfavor envia el dato cantidad para saber cuantas facturas debemos sacar de superlikers",
                    'dato' => "cantidad"
                ], 200);
            }

            $cantidad = $request->cantidad;
            $almacenadas = 0;

            //jaureguilucio6962@gmail.com
            //karenespinosa26429@gmail.com
            //albertopagola78@gmail.com

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                '_type' => 'UploadPhoto',
                'category' => 'invoice',
                //'before' => '1639116538.149',
                //'after' => '1639155405.472',
                //'limit' => 150,
                //'hide_undo' => true,
                //'distinct_id' => 'albertopagola78@gmail.com',
                'date_filter' => [
                    'sdate' => '2022-03-31',//date('Y-m-d'), //dia DE HOY
                    'edate' => '2022-03-31', //dia SIGUIENTE
                ],
            ]);
            
            //Evaluamos los resultados
            if($response->successful()){
                $data = $response->object();
                
                if($data->ok == "true"){

                    //Este es el primer lote de 100 cuando ya me devuelva entries en [] es que ya no hay más de hoy
                    $data = $data->data; //tomo el objeto con la información;
                    
                    while (count($data->entries) && $almacenadas < $cantidad){
                        $next_page = $data->next_page_token;
                        $entradas = $data->entries;

                        //Recorremos el objeto de todas las entradas y las almacenamos
                        foreach ($entradas as $key => $entrada) {
                            if($almacenadas >=  $cantidad)
                                break;

                            //revisamos si no esta duplicada para hcaerla varias veces
                            if(!FacturaVieja::where('referencia', $entrada->id)->first()){

                                //Guardamos la Factura
                                $factura = new FacturaVieja();
                                $factura->referencia = $entrada->id;
                                $factura->email = $entrada->participant->email;
                                $factura->campaign = 'ti';
                                $factura->moderation = $entrada->moderation;
                                $factura->photo = $entrada->photo_url;
                                $factura->user_upload = $entrada->created_at;
                                $factura->save();

                                $almacenadas++;
                            }
                        }

                        //Vuelvo a consultar para buscar otra pagina
                            $response = Http::withHeaders([
                                'Content-Type' => 'application/json',
                            ])->post($url, [
                                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                                'campaign' => 'ti',
                                '_type' => 'UploadPhoto',
                                'category' => 'invoice',
                                //'before' => '1639116538.149',
                                'after' => $next_page,
                                //'limit' => 150,
                                //'hide_undo' => true,
                                //'distinct_id' => 'albertopagola78@gmail.com',
                                'date_filter' => [
                                    'sdate' => '2022-03-31',//date('Y-m-d'), //dia DE HOY
                                    'edate' => '2022-03-31', //dia SIGUIENTE
                                ],
                            ]);

                            try{
                                $data = $response->object();
                                $data = $data->data;
                            }  
                            catch(\Exception $e){
                                $data->entries = [];
                            }
                    }
                   
                    return response()->json([
                        'status' => 'success',
                        'message' => "Factura y Participantes almacenados de forma exitosa (".$almacenadas."), porfavor envielas al OCR",
                        'disponible' => true,
                    ], 200);
                }
                else{
                    return response()->json([
                        'status' => 'warning',
                        'message' => $data->message,
                        'codigo' => $data->code_error
                    ], 200);
                }
            }
            else if($response->clientError()){
                $data = $response->object();
                return response()->json([
                    'status' => 'error',
                    'message' => $data->message,
                    'codigo' => $data->code_error
                ], 400);
            }
            else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error en el Servidor de la API de Superlikers, intentelo más tarde',
                ], 503);
            }
        }

        //ENVIAR FACTURAS EN ESTADO NO PARAM A OCR Y PARAMETRIZAR
        public function sendOCRParam(){
            //date('Y-m-d')
            try{
                $facturas = FacturaVieja::where('campaign', 'ti')->where('id_factura', '>', 21631)->
                                    whereDate('user_upload', '>=', "2022-02-01")->
                                    whereNull('lectura')->whereDate('created_at', '>=', "2022-04-16")->get();
                
                $cantidad = 0;

                foreach ($facturas as $key => $factura) {
                    //CREAMOS EL CRON JOB
                    ProcessOLDParam::dispatch($factura)->onQueue('paramOldJob');

                    $cantidad++;
                    if($cantidad > 11000){
                        break;
                    }
                }

                //Disparamos el Job con el Artisan
                //$exitCode = Artisan::call('queue:work', ['--queue' => 'paramJob']);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Facturas enviadas a parametrizar exitosamente, se ira haciendo el proceso poco a poco internamente',
                    'cantidad' => $cantidad,
                ], 200);

            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso antes de inicar la lectura ocr y parametrización',
                    'error message' => $e->getMessage(),
                    'codigo' => 503,
                ], 503);
            }
        }

        //RE-ENVIAR FACTURA A PARAMETRIZAR
        public function paramInvoice($id){
            DB::beginTransaction();
            try{
                $factura = FacturaVieja::find($id);

                //ELIMINAMOS LOS PRODUCTOS PERNOD
                FacturaViejaProducto::where('FK_id_factura', $factura->id_factura)->delete();

                //Almaceno la foto si hace falta
                if(!$factura->lectura)
                    $archivo = $this->saveFactura($factura->photo);
                else
                    $archivo = true;
                
                if($archivo){
                    //La envio a leer
                    if(!$factura->lectura){
                        $values = FacturaIA2::instance()->readFactura($factura->photo, $archivo);
                        $values->reparam = "no";
                        try{ unlink($archivo); }
                        catch(\Exception $e){}
                    }
                    else{
                        $values = new \stdClass;
                        $values->contenido = $factura->lectura;
                        $values->reparam = "si";
                    }

                    if(isset($values->error)){
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Error interno en el proceso de lectura del archivo',
                            'error message' => $values->error,
                            "codigo" => 503,
                        ], 503);
                    }
                    
                    if($values){
                        $factura->lectura = $values->contenido;

                        //La envio a parametrizar
                        $values = FacturaIA2::instance()->parametrizar($values);
                        
                        if(isset($values->error)){
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Error interno en el proceso de parametrización',
                                'error message' => $values->error,
                                "codigo" => 503,
                            ], 503);
                        }
                        
                        //Ahora manejamos los datos evaluados
                            $factura->cdc = $values->restaurante;
                            $factura->campaign = $values->category; //mes de la factura
                            $factura->folio = $values->referencia;
                            $factura->mesero = $values->mesero;
                            $factura->registro = $values->fecha_factura;

                        //Grabamos los productos Pernod recibidos
                            foreach ($values->productos as $key => $producto) {
                                $pernod = new FacturaViejaProducto();
                                $pernod->referencia = $producto["referencia"];
                                $pernod->price = $producto["precio"];
                                $pernod->quantity = $producto["cantidad"];
                                $pernod->provider = "Pernod Ricard";//$producto["proveedor"];
                                $pernod->line = $producto["linea"];
                                $pernod->formato = $producto["formato"];
                                $pernod->FK_id_factura = $factura["id_factura"];
                                $pernod->save();
                            }
                        
                        //Hacemos save de todo
                        $factura->save();

                        DB::commit();
                        return response()->json([
                            'status' => 'success',
                            'message' => 'Factura leida exitosamente, revisa el objeto y el ID de la misma',
                            'id' => $factura->id_factura,
                            'lectura' => $factura->lectura,
                            'productos' => $factura->productos,
                            'extras' => $factura->extras,
                            'codigo' => 200,
                        ], 200);
                    }
                    else{
                        $factura->lectura = "";
                        $factura->save();

                        return response()->json([
                            'status' => 'success',
                            'message' => 'El lector no pudo captar ningun caracter',
                            'id' => $factura->id_factura,
                            'lectura' => $factura->lectura,
                            'codigo' => 203,
                        ], 203);
                    }
                }
            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso de lectura y posterior envio a parametrizar',
                    'error message' => $e->getMessage()." | ".$e->getLine(),
                    'codigo' => 503,
                ], 503);
            }

        }

        public function saveFactura($link){
            
            $url = $link;
            
            try{
                $content = file_get_contents($url);
                $name = substr($url, strrpos($url, '/') + 1);

                //El archivo esta grabado
                $archivo = storage_path().'/app/public/invoices/'.$name;
                file_put_contents($archivo, $content);

                return $archivo;
            }
            catch(\Exception $e){
                //return $e->getCode()." | ".$e->getMessage();
                return null;
            }
        }
    //FIN PROCESO YA NORMAL DEL FLUJO COMPLETO

    //RE ENVIAR FACTURAS EN ESTADO NO PARAM O PENDIENTE A PARAMETRIZAR CON LECTURA
    public function reSendOCRParam(){

        try{
            $facturas = FacturaVieja::whereDate('user_upload', '>=', "2022-03-01")->
                                      whereDate('user_upload', '<', "2022-04-01")->
                                      //whereDate('registro', '>', "2022-04-01")->
                                      /* where('campaign', '')-> */orderBy('user_upload', 'asc')->get();
            
            $cantidad = 0;
            
            foreach ($facturas as $key => $factura) {
                
                //CREAMOS EL CRON JOB
                ProcessREOCRParam::dispatch($factura)->onQueue('reParamJob');

                $cantidad++;
                if($cantidad > 20000){
                    break;
                }
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Facturas enviadas a re-parametrizar exitosamente, se ira haciendo el proceso poco a poco internamente',
                'cantidad' => $cantidad,
            ], 200);

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json([
                'status' => 'error',
                'message' => 'Error interno en el proceso antes de inicar la lectura ocr y parametrización',
                'error message' => $e->getMessage(),
                'codigo' => 503,
            ], 503);
        }
    }
}
