<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;
use App\Models\Jobs;
use App\Models\FailJobs;

//Helper
use App\Helper\Notificacion;
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;

//Jobs
use App\Jobs\ProcessGoogleVision;
use App\Jobs\ProcessOCRParam;
use App\Jobs\ProcessREOCRParam;
use App\Jobs\ProcessRegistrarVenta;

class ParticipanteController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
        'before' => 'La fecha de inicio debe ser ajuro igual o menor al mes/año actual',
        'after' => 'La fecha de inicio debe ser ajuro mayor al mes/año actual',
        'duplicate' => 'Tienes elementos seleccionados duplicados en la marca, procura dejar uno solo',
    ];

    ////////////////////////////////////////////////VISTAS
    
    public function index(){  
        $usuarios = Participante::selectRaw('name, email, COUNT(*) AS total')
                                ->orderBy('total', 'desc')->groupBy('email')->get()->take(5);
        
        return view('usuarios.index', compact('usuarios'));
    }

    public function registrados(){
        $usuarios = Participante::selectRaw('name, id, uid, email, COUNT(*) AS total')->groupBy('email')->
                                  orderBy('name', 'desc')->paginate(200);//get();
        
        return view('usuarios.list_register', compact('usuarios'));
    }

    public function facturas($email = null, $filtro = null, $filtro2 = null){

        $correos = Participante::select('email')->groupBy('email')->get();

        if($email){

            if($filtro || $filtro2){
                if (strtotime($filtro)){
                    $date = $filtro;
                    $state = $filtro2;
                }
                else{
                    $date = $filtro2;
                    $state = $filtro;
                }
    
                if($date && $state){
                    $facturas = Factura::join('participante', 'participante.id_participante', '=', 'facturas.FK_id_participante')->
                                    where('participante.email', $email)->
                                    where('facturas.created_at', '>=', $date)->
                                    where('facturas.state', $state)->orderBy('id_factura', 'desc')->paginate(200);//get();
                }elseif($date){
                    $facturas = Factura::join('participante', 'participante.id_participante', '=', 'facturas.FK_id_participante')->
                                    where('participante.email', $email)->
                                    where('facturas.created_at', '>=', $date)->orderBy('id_factura', 'desc')->paginate(200);//get();
                }elseif($state){
                    $facturas = Factura::join('participante', 'participante.id_participante', '=', 'facturas.FK_id_participante')->
                                    where('participante.email', $email)->
                                    where('facturas.state', $state)->orderBy('id_factura', 'desc')->paginate(200);//get();
                }
            }
            else
                $facturas = Factura::join('participante', 'participante.id_participante', '=', 'facturas.FK_id_participante')
                                    ->where('participante.email', $email)->orderBy('id_factura', 'desc')->paginate(200);//get();
        }
        else{
            $facturas = [];
            $filtro = null;
            $filtro2 = null;
        }
        
        return view('usuarios.list_tracking', compact('facturas', 'correos', 'email', 'filtro', 'filtro2'));
    }

    /////////////////////////////////////////////////FIN VISTAS
}
