<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;
use App\Models\Jobs;
use App\Models\FailJobs;

//Helper
use App\Helper\Notificacion;
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;

//Jobs
use App\Jobs\ProcessGoogleVision;
use App\Jobs\ProcessOCRParam;
use App\Jobs\ProcessREOCRParam;
use App\Jobs\ProcessRegistrarVenta;

//Exportables
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\FacturaExport;
use App\Exports\FacturaProductoExport;
use App\Exports\FacturaMultipleExport;

class ReportController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'date' => 'El dato debe ser enviado en formato de fecha (Y-m-d) por ejemplo: 2022-05-18',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    public function index(){
        return view('reportes.index');
    }

    public function exportData(Request $request){

        $validate = Validator::make($request->all(), [
            //VALIDACIONES
            'factInicial' => 'required|date',
            'factFinal' => 'nullable|date', 
            'tipo' => 'required|string',
        ], $this->mensajes_error);

        if($validate->fails()){
            return Redirect::back()->withErrors($validate)->withInput();
        }

        if(!$request->factFinal){
            $request->factFinal = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));
        }
        else{
            $dateMin = strtotime($request->factInicial);
            $dateMax = strtotime($request->factFinal);
            if($dateMin >= $dateMax){
                return Redirect::back()->with('errores', 'Debes enviar una fecha máxima mayor a la fecha menor registrada. Intentalo de nuevo');
            }
        }

        return Excel::download(new FacturaMultipleExport($request->factInicial, $request->factFinal, $request->tipo), 'facturas.xlsx');
    }
}
