<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

//Modelos
use App\Models\ContentRead;

//Jobs
use App\Jobs\ProcessGoogleVision;

//Helper
use App\Helper\FacturaIA;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ParametricController extends Controller
{
    public $mensajes_error = [
        'required' => 'El dato archivo es requerido (procura enviarlo bajo este nombre)',
        'file' => 'El dato debe llegar como formato file (archivo)',
        'mimes' => 'El archivo debe llegar solo en formato png, jpg, jpeg o pdf',
        'archivo.max' => 'El archivo no puede ser mayor a 2Mb',
    ];

    public $validaciones = [
        'archivo' => 'required|file|mimes:jpg,jpeg,png,pdf',
    ];

    //COMPRESOR DE IMAGEN
    public function imagecompress($image){
        try{
            $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save();
            $img->destroy();
            return true;
        }
        catch(\Exception $e){
            return false;
        }
    }

    //ENVIAR FACTURA AL LECTOR PARA ALMACENAR EL CONTENIDO LEIDO
    public function invoiceLector(Request $request){
        DB::beginTransaction();
        try{

            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if($validate->fails()){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                    'codigo' => 400,
                ], 400);
            }
            
            //Creamos el Modelo del contenido para almacenar la data
            $lectura = new ContentRead();
            $lectura->id_archivo = rand();
            $lectura->archivo = null;

            if($request->hasFile('archivo')){
                $archivo = $request->file('archivo')->store('invoices', 'public');
                //COMPRESOR DE IMAGENES - Creo que los de superlikers ya las comprimen
                /* if(!$this->imagecompress($archivo)){
                    Storage::delete(['public/'.$lectura->archivo]);
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Error interno en el proceso de compresión de factura',
                        "codigo" => 503,
                    ], 503);
                } */
            }

            $lectura->archivo = $archivo;

            $values = FacturaIA::instance()->read($request->file('archivo'));

            if(isset($values->error)){
                Storage::delete(['public/'.$lectura->archivo]);
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso de lectura del archivo',
                    'error message' => $values->error,
                    "codigo" => 503,
                ], 503);
            }

            if($values){
                $lectura->contenido = $values->contenido;
                $lectura->save();

                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' => 'Factura leida exitosamente, revisa el objeto y el ID de la misma',
                    'id' => $lectura->id,
                    'lectura' => $lectura,
                    'codigo' => 200,
                ], 200);
            }
            else{
                $lectura->contenido = "";
                $lectura->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'El lector no pudo captar ningun caracter',
                    'id' => $lectura->id,
                    'lectura' => $lectura,
                    'codigo' => 203,
                ], 203);
            }

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            Storage::delete(['public/'.$lectura->archivo]);
            return response()->json([
                'status' => 'error',
                'message' => 'Error interno en el proceso antes de inicar la lectura ocr',
                'error message' => $e->getMessage(),
                'codigo' => 503,
            ], 503);
        }

    }

    //VER TODAS LAS FACTURAS CARGADAS Y DEVOLVER EL OBJETO
    public function facturas(){
        $lecturas = ContentRead::all();

        return response()->json([
            'status' => 'success',
            'message' => 'Todas las facturas almacenadas',
            'factura' => $lecturas,
            'codigo' => 200,
        ], 200);
    }

    //VER LA INFORMACIÓN DE UNA FACTURA ENVIADA A LECTURA
    public function factura($id = null){

        try{
            if(!$id){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error envia un ID de factura',
                    'codigo' => 503,
                ], 503);
            }

            if(!is_numeric($id)){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error envia un ID de factura valido (número entero)',
                    'codigo' => 503,
                ], 503);
            }

            $lectura = ContentRead::find($id);

            if(!$lectura){
                return response()->json([
                    'status' => 'error',
                    'message' => 'El ID enviado no posee una factura asociada a la misma',
                    'codigo' => 503,
                ], 503);
            }

            $lectura->archivo = basename($lectura->archivo);

            return response()->json([
                'status' => 'success',
                'message' => 'La Factura se encontro de forma exitosa',
                'factura' => $lectura,
                'codigo' => 200,
            ], 200);
        }
        catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
                'codigo' => 500,
            ], 500);
        }

    }

    //RETORNAR ARCHIVO DE FACTURA
    public function getImage($filename = null){
        
        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if($filename){
            $exist = Storage::disk('public')->exists('invoices/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                return response()->json([
                    'message' => 'El documento de la factura no fue encontrado',
                    'codigo' => 503,
                ], 503);
            else
                return response()->file(storage_path('app/public/invoices/'.$filename));
        }
        else
            return response()->json([
                'message' => 'Por favor envia un nombre de archivo para hacer la busqueda dentro del sistema',
                'codigo' => 503,
            ], 503);
    }

    //ENVIAR A PARAMETRIZAR UNA FACTURA
    public function parametrizar($id = null){

        try{
            if(!$id){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error envia un ID de factura',
                    'codigo' => 503,
                ], 503);
            }

            if(!is_numeric($id)){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error envia un ID de factura valido (número entero)',
                    'codigo' => 503,
                ], 503);
            }

            $lectura = ContentRead::find($id);

            if(!$lectura){
                return response()->json([
                    'status' => 'error',
                    'message' => 'El ID enviado no posee una factura asociada a la misma',
                    'codigo' => 503,
                ], 503);
            }

            //ENVIO A PARAMETRIZACIÓN
            $values = FacturaIA::instance()->parametrizar($lectura);

            if(isset($values->error)){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso de parametrización',
                    'error message' => $values->error,
                    "codigo" => 503,
                ], 503);
            }

            if($values){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Contenido Parametrizado',
                    'parametrizacion' => $values,
                    'codigo' => 200,
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => 'El lector no pudo captar ningun caracter',
                    'ocr' => 'La factura no pudo leerse ya que el archivo no contiene caracteres legibles',
                    'codigo' => 203,
                ], 203);
            }
        }
        catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
                'codigo' => 500,
            ], 500);
        }

    }
}
