<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;
use App\Models\Jobs;
use App\Models\FailJobs;

//Helper
use App\Helper\Notificacion;
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;

//Jobs
use App\Jobs\ProcessGoogleVision;
use App\Jobs\ProcessOCRParam;
use App\Jobs\ProcessREOCRParam;
use App\Jobs\ProcessRegistrarVenta;

class FacturaPanelController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    ////////////////////////////////////////////////VISTAS

    public function index(){
        ini_set('max_execution_time', '36000');
        $facturas = Factura::orderBy('id_factura', 'desc')->take(5)->get();
        return view('facturas.index', compact('facturas'));
    }

    public function listado($filtro = null, $filtro2 = null){
        ini_set('max_execution_time', '36000');
        if($filtro || $filtro2){
            if (strtotime($filtro)){
                $date = $filtro;
                $state = $filtro2;
            }
            else{
                $date = $filtro2;
                $state = $filtro;
            }

            if($date && $state)
                $facturas = Factura::where('created_at', '>=', $date)->where('state', $state)->orderBy('id_factura', 'desc')->paginate(200);//get();
            elseif($date)
                $facturas = Factura::where('created_at', '>=', $date)->orderBy('id_factura', 'desc')->paginate(200);//get();
            elseif($state)
                $facturas = Factura::where('state', $state)->orderBy('id_factura', 'desc')->paginate(200);//get();
        }
        else
            $facturas = Factura::orderBy('id_factura', 'desc')->paginate(200);//get();

        return view('facturas.listado', compact('facturas', 'filtro', 'filtro2'));
    }

    public function ocr(){
        ini_set('max_execution_time', '36000');
        $facturas = Factura::where('state', 'no param')->where('campaign', 'ti')->orderBy('id_factura', 'desc')->paginate(200);//get();
        $jobs = Jobs::where('queue', 'paramJob')->get();

        return view('facturas.list_ocr', compact('facturas', 'jobs'));
    }

    public function enviadas(){
        ini_set('max_execution_time', '36000');
        $facturas = Factura::where('envio', '1')->orderBy('id_factura', 'desc')->paginate(200);//get();
        return view('facturas.list_enviadas', compact('facturas'));
    }

    public function no_enviadas($filtro = null){
        ini_set('max_execution_time', '36000');
        if($filtro){
            $facturas = Factura::where('envio', '0')->
                                where('state', '<>', 'pendiente')->
                                where('state','<>','pendiente superlikers')->
                                where('state','<>','no pendiente')->
                                where('state','<>','folio repetido')->
                                where('state','<>','folio duplicado')->
                                where('state','<>','sin texto')->
                                where('state','<>','no factura')->
                                where('state','<>','link roto')->
                                where('state','<>','no param')->
                                where('created_at', '>=', $filtro)->
                                orderBy('id_factura', 'desc')->paginate(200);//get();
        }
        else{
            //Tomamos la fecha de cuando se subio la ultima factura
            $fecha = Factura::where('envio', '1')->orderBy('id_factura', 'desc')->first();
            $fecha = date("Y-m-d", strtotime($fecha->created_at));

            $facturas = Factura::where('envio', '0')->
                                where('state', '<>', 'pendiente')->
                                where('state','<>','pendiente superlikers')->
                                where('state','<>','no pendiente')->
                                where('state','<>','folio repetido')->
                                where('state','<>','folio duplicado')->
                                where('state','<>','sin texto')->
                                where('state','<>','link roto')->
                                where('state','<>','no param')->
                                where('state','<>','no factura')->
                                where('created_at', '>=', $fecha)->
                                orderBy('id_factura', 'desc')->paginate(200);//get();
        }

        $jobs = Jobs::where('queue', 'registrarInv')->get();

        return view('facturas.list_noenviadas', compact('facturas', 'jobs', 'filtro'));
    }

    public function pendientes($filtro = null){
        ini_set('max_execution_time', '36000');
        if($filtro)
            $facturas = Factura::where('state', '!=', 'aceptado')->where('created_at', '>=', $filtro)->orderBy('id_factura', 'desc')->paginate(200);//get();
        else
            $facturas = Factura::where('state', '!=', 'aceptado')->orderBy('id_factura', 'desc')->paginate(200);//get();

        return view('facturas.list_pendientes', compact('facturas', 'filtro'));
    }

    public function aceptadas($filtro = null){

        if($filtro)
            $facturas = Factura::where('state', 'aceptado')->where('created_at', '>=', $filtro)->orderBy('id_factura', 'desc')->paginate(200);//get();
        else
            $facturas = Factura::where('state', 'aceptado')->orderBy('id_factura', 'desc')->paginate(200);//get();

        return view('facturas.list_aceptados', compact('facturas', 'filtro'));
    }

    public function cdc_facturas($cdc, $filtro = null, $filtro2 = null){
        ini_set('max_execution_time', '36000');
        if($filtro || $filtro2){
            if (strtotime($filtro)){
                $date = $filtro;
                $state = $filtro2;
            }
            else{
                $date = $filtro2;
                $state = $filtro;
            }

            if($date && $state)
                $facturas = Factura::where('cdc', $cdc)->where('created_at', '>=', $date)->where('state', $state)->orderBy('id_factura', 'desc')->paginate(200);//get();
            elseif($date)
                $facturas = Factura::where('cdc', $cdc)->where('created_at', '>=', $date)->orderBy('id_factura', 'desc')->paginate(200);//get();
            elseif($state)
                $facturas = Factura::where('cdc', $cdc)->where('state', $state)->orderBy('id_factura', 'desc')->paginate(200);//get();
        }
        else
            $facturas = Factura::where('cdc', $cdc)->orderBy('id_factura', 'desc')->orderBy('id_factura', 'desc')->paginate(200);//get();

        return view('facturas.list_cdc', compact('facturas', 'cdc', 'filtro', 'filtro2'));
    }

    public function editar($id_factura){
        ini_set('max_execution_time', '36000');
        $factura = Factura::find($id_factura);
        $restaurantes = Restaurantes::orderBy('nombre', 'desc')->get();
        $marcas = Productos::all();

        if(!$factura)
            return Redirect::back()->with('edit', 'La factura seleccionada a editar no pudo ser encontrada');

        return view('facturas.edit', compact('factura', 'restaurantes', 'marcas'));
    }

    /////////////////////////////////////////////////FIN VISTAS

    /////////////////////////////////////////////////CRUD

    public function parametrizar($id_factura){
        ini_set('max_execution_time', '36000');
        DB::beginTransaction();
        try{
            $factura = Factura::find($id_factura);

            //ELIMINAMOS SUS PRODUCTOS EXTRAS
            FacturaExtra::where('FK_id_factura', $factura->id_factura)->delete();

            //ELIMINAMOS LOS PRODUCTOS PERNOD
            FacturaProducto::where('FK_id_factura', $factura->id_factura)->delete();

            //Almaceno la foto si hace falta
            if(!$factura->lectura)
                $archivo = $this->saveFactura($factura->photo);
            else
                $archivo = true;

            if($archivo){
                //La envio a leer
                if(!$factura->lectura){
                    $values = FacturaIA::instance()->readFactura($factura->photo, $archivo);
                    $values->reparam = "no";
                    try{ unlink($archivo); }
                    catch(\Exception $e){}
                }
                else{
                    $values = new \stdClass;
                    $values->contenido = $factura->lectura;
                    if($factura->folio != "N/A" && $factura->folio != "No parametrizo")
                        $values->reparam = "si";
                    else
                        $values->reparam = "no";
                }

                if(isset($values->error)){
                    return Redirect::back()->with('delete', 'Error en el proceso de lectura del archivo / '.$values->error);
                }

                if($values){
                    $factura->lectura = $values->contenido;

                    //La envio a parametrizar
                    $values = FacturaIA::instance()->parametrizar($values);

                    if(isset($values->error)){
                        return Redirect::back()->with('delete', 'Error en el proceso de parametrización / '.$values->error);
                    }

                    //Ahora manejamos los datos evaluados
                        $factura->cdc = $values->restaurante;
                        $factura->campaign = $values->category; //mes de la factura
                        $factura->folio = $values->referencia;
                        $factura->mesero = $values->mesero;
                        $factura->state = $values->reparam = "no" ? $values->estado : "";
                        $factura->registro = $values->fecha_factura;
                        $factura->tarifario = $values->tarifario;

                    //Grabamos los productos Pernod recibidos
                        foreach ($values->productos as $key => $producto) {
                            $pernod = new FacturaProducto();
                            $pernod->referencia = $producto["referencia"];
                            $pernod->price = $producto["precio"];
                            $pernod->quantity = $producto["cantidad"];
                            $pernod->provider = "Pernod Ricard";//$producto["proveedor"];
                            $pernod->line = $producto["linea"];
                            $pernod->FK_id_factura = $factura["id_factura"];
                            $pernod->save();
                        }

                    //Grabamos los producos extras de la factura
                        foreach ($values->extras as $key => $producto) {
                            $extra = new FacturaExtra();
                            $extra->nombre = $producto;
                            $extra->FK_id_factura = $factura["id_factura"];
                            $extra->save();
                        }

                    //Hacemos save de todo
                    $factura->save();

                    DB::commit();
                    return Redirect::back()->with('param', 'Facturas actualizada y parametrizada exitosamente.');
                }
                else{
                    $factura->lectura = "";
                    $factura->mesero = "No parametrizo";
                    $factura->folio = "No parametrizo";
                    $factura->state = "sin texto";
                    $factura->save();
                    DB::commit();

                    return Redirect::back()->with('param', 'Facturas actualizada exitosamente a estado imagen sin textos.');
                }
            }
            else{
                $factura->lectura = "";
                $factura->mesero = "No parametrizo";
                $factura->folio = "No parametrizo";
                $factura->state = "link roto";
                $factura->save();
                DB::commit();
                return Redirect::back()->with('param', 'Facturas actualizada exitosamente a estado link roto.');
            }
        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error al parametrizar: '.$e->getMessage());
        }
    }

    //Traer lote de facturas de la API superlikers
    public function retrieveInvoices(Request $request){
        ini_set('max_execution_time', '36000');
        $validate = Validator::make($request->all(), [
            'cantidad' => 'required|numeric|min:0|max:1000',
        ], $this->mensajes_error);

        if($validate->fails()){
            return Redirect::back()->withErrors($validate)->withInput();
        }

        $url = "https://api.superlikers.com/v1/entries/index";

        $urlparticipant = "https://api.superlikers.com/v1/participants/info";

        $cantidad = $request->cantidad;
        $almacenadas = 0;

        //Ubicamos la fecha de la ultima factura tomada por el dato user_upload y lo utilizamos para la busqueda
        $fecha = Factura::orderBy('id_factura', 'desc')->first();
        $fecha = date("Y-m-d", strtotime($fecha->user_upload));

        DB::beginTransaction();
        try{
            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                '_type' => 'UploadPhoto',
                'category' => 'invoice',
                //'before' => '1639116538.149',
                //'after' => '1639155405.472',
                //'limit' => 150,
                //'hide_undo' => true,
                //'distinct_id' => 'julianclavijof@gmail.com',
                'date_filter' => [
                    'sdate' => $fecha,//date('Y-m-d', strtotime("-1 week")), //dia DE HOY
                    'edate' => date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day')), //dia SIGUIENTE
                ],
            ]);

            //Evaluamos los resultados
            if($response->successful()){
                $data = $response->object();

                if($data->ok == "true"){

                    //Este es el primer lote de 100 cuando ya me devuelva entries en [] es que ya no hay más de hoy
                    $data = $data->data; //tomo el objeto con la información;

                    while (count($data->entries) && $almacenadas < $cantidad){
                        $next_page = $data->next_page_token;
                        $entradas = $data->entries;

                        //Recorremos el objeto de todas las entradas y las almacenamos
                        foreach ($entradas as $key => $entrada) {
                            if($almacenadas >=  $cantidad)
                                break;

                            if($entrada->moderation == "pending"){
                                //revisamos si no esta duplicada para hcaerla varias veces
                                if(!Factura::where('referencia', $entrada->id)->first()){

                                    //Recogemos la información del participante para saber a que programa es la factura
                                    /* $response = Http::withHeaders([
                                        'Content-Type' => 'application/json',
                                    ])->get($urlparticipant, [
                                        'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                                        'campaign' => 'ti',
                                        'distinct_id' => $entrada->participant->email,
                                    ]);

                                    $programa = "pc";
                                    $datos = $response->object();

                                    if($datos->ok == "true"){
                                        if(isset($datos->object->challenges) && !empty($datos->object->challenges))
                                            $programa = $datos->object->challenges[0]; //tipo de factura
                                    }
                                    else
                                        $programa = "N/A"; */

                                    //Primero grabamos el participantes
                                    $participante = new Participante();
                                    //$participante->programa	= $programa;
                                    $participante->name = $entrada->participant->name;
                                    $participante->id = $entrada->participant->id;
                                    $participante->uid = $entrada->participant->uid;
                                    $participante->email = $entrada->participant->email;
                                    $participante->save();

                                    //Guardamos la Factura
                                    $factura = new Factura();
                                    //$factura->programa	= $programa;
                                    $factura->referencia = $entrada->id;
                                    $factura->campaign = 'ti';
                                    $factura->moderation = $entrada->moderation;
                                    $factura->photo = $entrada->photo_url;
                                    $factura->FK_id_participante = $participante->id_participante;
                                    $factura->user_upload = $entrada->created_at;
                                    $factura->save();

                                    $almacenadas++;
                                }
                            }
                        }

                        //Vuelvo a consultar para buscar otra pagina
                            $response = Http::withHeaders([
                                'Content-Type' => 'application/json',
                            ])->post($url, [
                                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                                'campaign' => 'ti',
                                '_type' => 'UploadPhoto',
                                'category' => 'invoice',
                                //'before' => '1639116538.149',
                                'after' => $next_page,
                                //'limit' => 150,
                                //'hide_undo' => true,
                                //'distinct_id' => 'julianclavijof@gmail.com',
                                'date_filter' => [
                                    'sdate' => $fecha,//date('Y-m-d', strtotime("-1 week")), //dia DE HOY
                                    'edate' => date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day')), //dia SIGUIENTE
                                ],
                            ]);

                            try{
                                $data = $response->object();
                                $data = $data->data;
                            }
                            catch(\Exception $e){
                                $data->entries = [];
                            }
                    }
                    DB::commit();
                    return Redirect::back()->with('create', 'Factura y Participantes almacenados de forma exitosa ('.$almacenadas.'), porfavor envielas al OCR');
                }
                else{
                    return Redirect::back()->with('edit', 'Fallo interno de superlikers, mensaje: '.$data->message.' | '.$data->code_error);
                }
            }
            else if($response->clientError()){
                $data = $response->object();
                return Redirect::back()->with('edit', 'Fallo interno de superlikers, mensaje: '.$data->message.' | '.$data->code_error);
            }
            else{
                return Redirect::back()->with('delete', 'Error en el Servidor de la API de Superlikers, intentelo más tarde');
            }
        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error en el proceso de solicitar lotes: '.$e->getMessage());
        }
    }

    //Enviar lote de facturas a parametrizar
    public function sendParam(){
        ini_set('max_execution_time', '36000');
        try{
            $facturas = Factura::where('state', 'no param')->
                                whereNull('lectura')->whereDate('created_at', '>=', date('Y-m-d', strtotime("-1 week")))->get();

            $cantidad = 0;

            if($facturas->isEmpty())
                return Redirect::back()->with('edit', 'No hay facturas a espera para parametrizar, intentelo en otro momento');

            foreach ($facturas as $key => $factura) {
                //CREAMOS EL CRON JOB
                ProcessOCRParam::dispatch($factura)->onQueue('paramJob');

                $cantidad++;
                if($cantidad > 1000){
                    break;
                }
            }

            //Disparamos el Job con el Artisan
            //$exitCode = Artisan::call('queue:work', ['--queue' => 'paramJob']);

            return Redirect::back()->with('create', 'Facturas enviadas a parametrizar exitosamente, se ira haciendo el proceso poco a poco internamente, iras viendo que en esta pantalla cuando vayas actualizando encontraras menos facturas, las mismas las puedes encontrar en la tabla de listado completo de facturas');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error interno en el proceso antes de inicar la lectura ocr y parametrización, MENSAJE: '.$e->getMessage());
        }
    }

    //Enviar lote de facturas a registrar en superlkers
    public function sendInvoice(Request $request){

        ini_set('max_execution_time', '36000');
        $validate = Validator::make($request->all(), [
            //VALIDACIONES
            'factInicial' => 'required|date',
            'factFinal' => 'nullable|date',
            'tipo' => 'required|string',
        ], $this->mensajes_error);

        if($validate->fails()){
            return Redirect::back()->withErrors($validate)->withInput();
        }

        if(!$request->factFinal){
            $request->factFinal = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));
        }
        else{
            $dateMin = strtotime($request->factInicial);
            $dateMax = strtotime($request->factFinal);
            if($dateMin >= $dateMax){
                return Redirect::back()->with('delete', 'Debes enviar una fecha máxima mayor a la fecha menor registrada. Intentalo de nuevo');
            }
        }

        try{
            if($request->tipo == "lectura"){
                $facturas = Factura::where('envio', 0)
                                    ->where('moderation', 'pending')
                                    ->where('state','<>','pendiente')->where('state','<>','pendiente superlikers')
                                    ->whereDate('created_at', '>=', date($request->factInicial))
                                    ->whereDate('created_at', '<=', date($request->factFinal))
                                    ->get();
                //traer las facturasa pendientes para cambiarles el estado interno pero no enviarlas
                $facturasPen = Factura::where('envio', 0)
                                    ->where('moderation', 'pending')
                                    ->where('state','pendiente')
                                    ->whereDate('created_at', '>=', date($request->factInicial))
                                    ->whereDate('created_at', '<=', date($request->factFinal))
                                    ->get();
                

            }
            else{
                $facturas = Factura::where('envio', 0)
                                    ->where('moderation', 'pending')
                                    ->where('state','<>','pendiente')->where('state','<>','pendiente superlikers')
                                    ->whereDate('user_upload', '>=', date($request->factInicial))
                                    ->whereDate('user_upload', '<=', date($request->factFinal))
                                    ->get();
                //traer las facturasa pendientes para cambiarles el estado interno pero no enviarlas
                $facturasPen = Factura::where('envio', 0)
                                    ->where('moderation', 'pending')
                                    ->where('state','pendiente')
                                    ->whereDate('user_upload', '>=', date($request->factInicial))
                                    ->whereDate('user_upload', '<=', date($request->factFinal))
                                    ->get();

            }

            $cantidad = 0;

            if($facturas->isEmpty())
                return Redirect::back()->with('edit', 'No hay facturas a espera para registrar, intentelo en otro momento');

            foreach ($facturas as $key => $factura) {
                //CREAMOS EL CRON JOB
                if(!$factura->productos->isEmpty() || $factura->state == "rechazado"){
                    ProcessRegistrarVenta::dispatch($factura)->onQueue('registrarInv');
                    $cantidad++;
                }

                if($cantidad > 2000){
                    break;
                }
            }

            //cambio el estado interno de las facturas pendientes 
            foreach ($facturasPen as $key => $pendientes) {
                $pendientes->estado_int = 'enviado';
                $pendientes->save();

            }

            return Redirect::back()->with('create', 'Facturas enviadas a registrar exitosamente, se ira haciendo el proceso poco a poco internamente, iras viendo que en esta pantalla cuando vayas actualizando encontraras menos facturas, las mismas las puedes encontrar en la tabla de listado completo de facturas');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error interno en el proceso de enviar facturas a registrar en superlikers, MENSAJE: '.$e->getMessage());
        }
    }

    //Productos de la Factura
    public function productos($id_factura = null){
        ini_set('max_execution_time', '36000');
        try{
            $factura = Factura::find($id_factura);

            if(!$factura)
                return response()->json([
                    'message' => "La factura no pudo ser encontrada"
                ]);

            $pernots = $factura->productos;
            $extras = $factura->extras;

            return response()->json([
                'pernots' => $pernots,
                'extras' => $extras,
                'message' => "Factura encontrada exitosamente"
            ]);
        }
        catch(\Exception $e){
            return response()->json([
                'message' => "Error al consultar productos: ".$e->getMessage()
            ]);
        }
    }

    //Update de una Factura
    public function update(Request $request, $id_factura){
        ini_set('max_execution_time', '36000');
        //return $request->all();

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'cdc' => 'required|string|max:255',
                'ciudad' => 'required|string|max:255',
                'folio' => 'required|string|max:255',
                'mesero' => 'required|string|max:255',
                'state' => 'required|string|max:255',
                'registro' => 'required|date',
                'hora' => 'required',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $factura = Factura::find($id_factura);

            if(!$factura)
                return redirect()->route('facturas.index')->with('delete', 'La factura seleccionada no pudo ser encontrada por lo que no pudo ser editada');

            $data = $request->all();

            if($request->has('selectPernod')){
                if($request->selectPernod == 1){
                    if(!($request->has('pernod') || $request->has('extra')))
                        return Redirect::back()->with('error', 'Debes hacer edición de la factura con al menos un producto, ya sea de la marca Pernod o uno extra');
                    elseif(!$request->has('pernod'))
                        $data["state"] = "pendiente";
                }
            }

            //REVISAR SI EL FOLIO SELECCIONADO YA EXISTE Y PONERLO EN FOLIO REPETIDO
            $check_fact = Factura::where('cdc', $data["cdc"])->
                                where('folio', $data["folio"])->
                                where('id_factura', '<', $id_factura)->first();
                                //whereDate('registro', date("Y-m-d", strtotime($data["registro"])))->first();

            if($check_fact && $request->folio != "N/A"){
                $data["state"] = "rechazado";
            }

            $data["registro"] = $data["registro"]." ".$data["hora"];

            //AJUSTE DE LA CAMPAÑA DE ACUERDO AL MES REGISTRADO
                $meses = array(
                    "01" => "Enero", "02" => "Febrero", "03" => "Marzo", "04" => "Abril",
                    "05" => "Mayo", "06" => "Junio", "07" => "Julio", "08" => "Agosto",
                    "09" => "Septiembre", "10" => "Octubre", "11" => "Noviembre", "12" => "Diciembre",
                );
                $month = date("m",strtotime($data["registro"]));

                try{
                    $data["campaign"] = $meses[$month];
                }catch(\Exception $e){
                    $month = date('m');
                    $data["campaign"] = $meses[$month];
                }

            //Sacamos el tarifario que fue de acuerdo a la fecha impresa
                $tarifa = array(
                    "01" => "ene_feb", "02" => "ene_feb",
                    "03" => "mar_abr", "04" => "mar_abr",
                    "05" => "may_jun", "06" => "may_jun",
                    "07" => "jul_ago", "08" => "jul_ago",
                    "09" => "sep_oct", "10" => "sep_oct",
                    "11" => "nov_dic", "12" => "nov_dic",
                );
                $mes_fact = date("m", strtotime($data["registro"]));

                try{
                    $data["tarifario"] = $tarifa[$mes_fact];
                }catch(\Exception $e){
                    $mes_fact = date('m');
                    $data["tarifario"] = $tarifa[$mes_fact];
                }

            $factura->update($data);

            //PRODUCTOS
            if($request->has('selectPernod')){
                if($request->selectPernod == 1){

                    /* ELIMINAR LOS PRODUCTOS EXTRA Y DE LA MARCA */
                        FacturaProducto::where('FK_id_factura', $factura->id_factura)->delete();
                        FacturaExtra::where('FK_id_factura', $factura->id_factura)->delete();
                    /* FIN ELIMINAR LOS PRODUCTOS EXTRA Y DE LA MARCA */

                    /* PRODUCTOS PERNOD */
                        if($request->has('pernod')){
                            foreach($request->pernod as $pernod){
                                $per_pro = Productos::where('referencia', $pernod["ref"])->first();
                                if($per_pro)
                                    $linea = $per_pro->line;
                                else
                                    $linea = "VODKA";

                                //Evaluamos si el producto pertenece al tarifario actual
                                $tar_mes = date('m');
                                if(intval($mes_fact)%2 == 0)
                                    $mes_fact = "0". intval($mes_fact - 1);

                                $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$mes_fact.'-01'))->
                                                            where('FK_id_marca', $per_pro->id_productos)->
                                                            where('ciudad',$request->ciudad)->first();

                                //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
                                if(!$tarifario_mes)
                                    $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$mes_fact.'-01'))->
                                                                where('FK_id_marca', $per_pro->id_productos)->
                                                                where('ciudad',$request->ciudad)->first();

                                if(!$tarifario_mes)
                                    continue;

                                //Sacamos la cantidad de acuerdo al tarifario si es botella
                                $quantity = $this->transforQuantity($pernod["cantidad"], $pernod["forma"], $request->ciudad, $per_pro->id_productos, $mes_fact);

                                $producto = new FacturaProducto();
                                $producto->referencia = $pernod["ref"];
                                $producto->price = $pernod["precio"];
                                $producto->quantity = $quantity;//$pernod["cantidad"];
                                $producto->provider = "Pernod Ricard";
                                $producto->line = $linea;
                                $producto->formato = $pernod["forma"];
                                $producto->FK_id_factura = $factura->id_factura;
                                $producto->save();
                            }
                        }
                    /* FIN PRODUCTOS PERNOD */

                    /* PRODUCTOS EXTRAS */
                        if($request->has('extra')){
                            foreach($request->extra as $extra){
                                //GRABAMOS PREGUNTA
                                $producto = new FacturaExtra();
                                $producto->nombre = $extra;
                                $producto->FK_id_factura = $factura->id_factura;
                                $producto->save();
                            }
                        }
                    /* FIN PRODUCTOS EXTRAS */
                }
            }
            //FIN ALMACENAMIENTOS DE PRODUCTOS

            //NOTIFICACIÓN
            /* $data = [
                "nombre" => "Update de Cuestionario",
                "tipo" => "Update",
                "descripcion" => "Edición de Cuestionario: ".$cuestionario->titulo,
            ];

            Notificacion::instance()->store($data, "trivia"); */

            if(count($factura->productos) <= 0 && $factura->state != "pendiente"){
                //AJUSTE QUE SI NO TENGO NADA DE PERNOD RECHAZADO DE UNA
                $factura->state = "rechazado";
                $factura->save();
            }

            DB::commit();
            return Redirect::back()->with('edit', 'Factura editada de forma exitosa!!, de acuerdo al estado que hayas seleccionado saldra en un apartado diferente del panel de facturas');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Editar Factura, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    public function transforQuantity($cantidad, $formato, $ciudad, $id_producto, $month){
        ini_set('max_execution_time', '36000');
        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){

            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$id_producto)
                                        ->where('ciudad',$ciudad)->first();

            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");

                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01");
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }

                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            $date = strtotime($multiplicador->mes);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes);

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($month && $mes >= $date){
                                    if($month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidad *= $multiplicador;
            }
        }

        if($formato == "jarra"){
            $cantidad *= 3;
        }

        return $cantidad;
    }

    /////////////////////////////////////////////////FIN CRUD

    /////////////////////////////////////////////////ALMACENAR FACTURA
    public function saveFactura($link){
        ini_set('max_execution_time', '36000');
        $url = $link;

        try{
            $content = file_get_contents($url);
            $name = substr($url, strrpos($url, '/') + 1);

            //El archivo esta grabado
            $archivo = storage_path().'/app/public/invoices/'.$name;
            file_put_contents($archivo, $content);

            return $archivo;
        }
        catch(\Exception $e){
            //return $e->getCode()." | ".$e->getMessage();
            return null;
        }
    }

    public function searchFolio($id){
        $folios = Factura::where('folio',$id)->get();

        return view('nuevas_vistas/searchFolio', compact('folios','id'));
    }
}
