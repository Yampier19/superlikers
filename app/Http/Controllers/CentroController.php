<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;

//Helper
use App\Helper\Notificacion;

class CentroController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    public function index()
    {  
        $restaurantes = Restaurantes::orderBy('nombre', 'desc')->get();
        $facturas = Factura::whereNotNull('cdc')->groupBy('cdc')->
                            selectRaw('count(cdc) as total, cdc')->get();
        
        //Tomamos las ciudades del tarifario
        $ciudades = Tarifario::select('ciudad')->groupBy('ciudad')->get();

        return view('consumos.index', compact('restaurantes', 'facturas', 'ciudades'));
    }

    public function editar($id_cdc){
        $restaurante = Restaurantes::find($id_cdc);

        if(!$restaurante)
            return Redirect::back()->with('edit', 'El Centro de Consumo seleccionado a editar no pudo ser encontrado');
        
        //Tomamos las ciudades del tarifario
        $ciudades = Tarifario::select('ciudad')->groupBy('ciudad')->get();

        return view('consumos.edit', compact('restaurante', 'ciudades'));
    }

    /////////////////////////////////////////////////CRUD

    //Store de un cdc 
    public function store(Request $request){
        //return $request->all();
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'ciudad' => 'required|string|max:255', 
                'nombre' => 'required|string|max:255', 
                'tipo' => 'required|string|max:255', 
                'estado' => 'required',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $new = new Restaurantes();
            $new->nombre = strtoupper($request->nombre);
            $new->ciudad = strtoupper($request->ciudad);
            $new->tipo = $request->tipo;
            $new->activo = $request->estado;

            $new->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Centro de Consumo ('.$new->nombre.') agregado exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Agregar CDC, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Update de un cdc
    public function update(Request $request, $id_cdc){
        //return $request->all();
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'ciudad' => 'required|string|max:255', 
                'tipo' => 'required|string|max:255', 
                'estado' => 'required',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $restaurante = Restaurantes::find($id_cdc);
            if(!$restaurante)
                return Redirect::back()->with('error', 'El Centro de Consumo seleccionado no pudo ser encontrado por lo que no pudo ser editado');

            //UPDATE DE DATOS ACTUALES
            $restaurante->ciudad = strtoupper($request->ciudad);
            $restaurante->activo = $request->estado;
            $restaurante->tipo = $request->tipo;

            $restaurante->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Centro de Consumo editado exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Editar CDC, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Recoger imagen del usuario
    public function getImage($filename = null){

        if($filename){
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('usericon.jpg');
            else
                $file = Storage::disk('public')->get('uploads/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('usericon.jpg');

        return new Response($file, 200);
    }
}
