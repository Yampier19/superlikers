<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\FacturaProducto;

use App\Models\Factura;

class CommentsController extends Controller
{
    function addComents($fecha){
        ini_set('max_execution_time', '36000');
        $facturas = Factura::where('created_at','>=',$fecha)->where('coments', NULL)->get();

        foreach($facturas as $factura){

            //Verifica si coments esta null
            if($factura->coments == NULL){
                $factura->coments = " ";
            }

            //Busca el CDC
            if($factura->cdc == NULL || $factura->cdc == 'N/A'|| $factura->cdc == 'No parametrizo'){

                $factura->coments = $factura->coments."+ No se encontro CDC";

            }

            //Revisa que el folio no este null o no parametrizado
            if($factura->folio == NULL || $factura->folio == "No parametrizo" ||  $factura->folio == "N/A" ){
                $factura->coments = $factura->coments."+ No se encontro Folio";
            }
            //Verifica que el folio sea diferente a null o a no parametrizo o a N/A
            else{
            //Consulta si ya existe el folio en el centro de consumo
            $folios = Factura::where('folio',$factura->folio)->where('cdc',$factura->cdc)->get();

            //Cuenta si la consulta trae mas de un resutado

            if(count($folios,COUNT_RECURSIVE) >= 2){
                $factura->coments = $factura->coments."+ Folio Repetido";
            }
            }


            //Formatea Las fechas como entero
            $fechaSubida = strtotime($factura->user_upload);
            $fehcaCreacion = strtotime($factura->created_at);

                //Revisa que el mes de la fecha de subida y el mes actual sean diferentes
            if(date("m", $fechaSubida) < date("m", $fehcaCreacion)){
                //revisa que el dia actual es superior a  10
                if(date("d") > 10 ){
                    $factura->coments = $factura->coments."+ La fecha de subida es superior a 10 dias";
                }
            }

            //La fecha de la factura no puede ser null
            if($factura->registro == NULL || $factura->registro == "No parametrizo" ||  $factura->registro == "N/A"){
                $factura->coments = $factura->coments."+ No se encontro fecha Impresa";
            }


            //Verifica quela factura contenga productos de pernod
            $productos = FacturaProducto::where('FK_id_factura',$factura->id_factura)->count();

            if($productos <= 0){
                $factura->coments = $factura->coments."+ No hay productos validos en la factura";
            }

            //Guarda y si no sabias que esto guarda no toques por que no sabes  :)


            $factura->save();

            $mostrar = str_replace('+', '<br>-', $factura->coments);

            echo $mostrar;
            echo "<br>----------------";
        }
        echo "<br>********************************************<br>";
        echo "<br><h5>Termino</h5>";
    }
}
