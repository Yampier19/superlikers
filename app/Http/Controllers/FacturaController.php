<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;

//Modelos
use App\Models\Participante;
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;

//Jobs
use App\Jobs\ProcessGoogleVision;
use App\Jobs\ProcessOCRParam;
use App\Jobs\ProcessREOCRParam;
use App\Jobs\ProcessRegistrarVenta;

//Helper
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;
use Illuminate\Support\Facades\Storage;

class FacturaController extends Controller
{
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'file' => 'El dato debe llegar como formato file (archivo)',
        'mimes' => 'El archivo debe llegar solo en formato png, jpg, jpeg o pdf',
        'archivo.max' => 'El archivo no puede ser mayor a 2Mb',
    ];

    public $validaciones = [
        'archivo' => 'required|file|mimes:jpg,jpeg,png,pdf',
    ];

    //PROCESO YA NORMAL DEL FLUJO COMPLETO
        //CONSULTA DE LAS ENTRADAS ALMACENADAS EN SUPERLIKERS
        public function entries(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com/v1/entries/index";

            //Validamos que hayan enviado la cantidad que se desea sacar
            if(!$request->cantidad){
                return response()->json([
                    'status' => 'warning',
                    'message' => "Porfavor envia el dato cantidad para saber cuantas facturas debemos sacar de superlikers",
                    'dato' => "cantidad"
                ], 200);
            }

            $cantidad = $request->cantidad;
            $almacenadas = 0;

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                '_type' => 'UploadPhoto',
                'category' => 'invoice',
                //'before' => '1639116538.149',
                //'after' => '1649602751.906',
                //'limit' => 150,
                //'hide_undo' => true,
                //'distinct_id' => 'julianclavijof@gmail.com',
                'date_filter' => [
                    'sdate' => '2022-04-27',//date('Y-m-d'), //dia DE HOY
                    'edate' => date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day')), //dia SIGUIENTE
                ],
            ]);

            $data = $response->object();
            return $data;
        }

        //RETAIL FACTURA EN SUPERLIKERS
        public function retailInvoice(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com/v1/retail/buy";

            $data = [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                'category' => 'invoice',
                'distinct_id' => 'angeljovaniarana.91@gmail.com', //"uid - correo": del usuario participante o mesero,
                'ref' => '61b788c3f4a1b7225770fa24', //referencia de la factura en superlikers
                'date' => strtotime("2021-12-11 17:46:08"), ///TIEMPO EN UNIX
                'products' => [
                    [
                        'ref' => 9878, //codigo referencia del producto
                        'provider' => 'Pernot',
                        'line' => 'GINEBRA',
                        'price' => 125.000,
                        'quantity' => 1,
                    ],
                ],
                'properties' => [
                    'tarifario' => 'nov_dic',
                    'folio' => '456153',
                    'url_photo' => 'https://super-likers.s3.amazonaws.com/.../IMG_20220507_002605.jpg',
                ],
            ];

            /* $factura = Factura::find(110);

            $data = [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                'category' => 'invoice',
                'distinct_id' => $factura->participante->uid, //"uid - correo": del usuario participante o mesero,
                'ref' => $factura->referencia, //referencia de la factura en superlikers
                'date' => $factura->registro,
                'products' => []
            ];

            foreach ($factura->productos as $producto) {
                $pernot = array(
                    'ref' => $producto->referencia, //codigo referencia del producto
                    'provider' => $producto->provider,
                    'line' => $producto->line,
                    'price' => $producto->price,
                    'quantity' => $producto->quantity,
                );
                array_push($data['products'], $pernot);
            } */

            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, $data);

            $data = $response->object();
            return $data;
        }

        //ACEPTAR - RECHAZAR O DEJAR EN PENDIENTE UNA FACTURA
        public function changeStateInvoice(Request $request){
            ini_set('max_execution_time', '36000');
            //Ruta de Acepatada
            $url = "https://api.superlikers.com/v1/entries/accept";

            $data = [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                'id' => '61b2673bf4a1b763544e7c75', //identificador de la actividad - factura en superlikers
            ];

            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, $data);

            $data = $response->object();
            return $data;
        }

        //LA CONSULTA DE LA API DE ENTRIES DE SUPERLIKERS Y ALMACENADO DE LA INFORMACIÓN DE LA MISMA
        public function retrieveEntries(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com/v1/entries/index";

            $urlparticipant = "https://api.superlikers.com/v1/participants/info";

            //Validamos que hayan enviado la cantidad que se desea sacar
            if(!$request->cantidad){
                return response()->json([
                    'status' => 'warning',
                    'message' => "Porfavor envia el dato cantidad para saber cuantas facturas debemos sacar de superlikers",
                    'dato' => "cantidad"
                ], 200);
            }

            $cantidad = $request->cantidad;
            $almacenadas = 0;

            //Ubicamos la fecha de la ultima factura tomada por el dato user_upload y lo utilizamos para la busqueda
            $fecha = Factura::orderBy('id_factura', 'desc')->first();
            $fecha = date("Y-m-d", strtotime($fecha->user_upload));

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                '_type' => 'UploadPhoto',
                'category' => 'invoice',
                //'before' => '1639116538.149',
                //'after' => '1639155405.472',
                //'limit' => 150,
                //'hide_undo' => true,
                //'distinct_id' => 'julianclavijof@gmail.com',
                'date_filter' => [
                    'sdate' => $fecha,//date('Y-m-d'), //dia DE HOY
                    'edate' => date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day')), //dia SIGUIENTE
                ],
            ]);

            //Evaluamos los resultados
            if($response->successful()){
                $data = $response->object();

                if($data->ok == "true"){

                    //Este es el primer lote de 100 cuando ya me devuelva entries en [] es que ya no hay más de hoy
                    $data = $data->data; //tomo el objeto con la información;

                    while (count($data->entries) && $almacenadas < $cantidad){
                        $next_page = $data->next_page_token;
                        $entradas = $data->entries;

                        //Recorremos el objeto de todas las entradas y las almacenamos
                        foreach ($entradas as $key => $entrada) {
                            if($almacenadas >=  $cantidad)
                                break;

                            if($entrada->moderation == "pending"){
                                //revisamos si no esta duplicada para hcaerla varias veces
                                if(!Factura::where('referencia', $entrada->id)->first()){

                                    //Recogemos la información del participante para saber a que programa es la factura
                                    /* $response = Http::withHeaders([
                                        'Content-Type' => 'application/json',
                                    ])->get($urlparticipant, [
                                        'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                                        'campaign' => 'ti',
                                        'distinct_id' => $entrada->participant->email,
                                    ]);

                                    $programa = "pc";
                                    $datos = $response->object();

                                    if($datos->ok == "true"){
                                        if(isset($datos->object->challenges) && !empty($datos->object->challenges))
                                            $programa = $datos->object->challenges[0]; //tipo de factura
                                    }
                                    else
                                        $programa = "N/A"; */

                                    //Primero grabamos el participantes
                                    $participante = new Participante();
                                    //$participante->programa	= $programa;
                                    $participante->name = $entrada->participant->name;
                                    $participante->id = $entrada->participant->id;
                                    $participante->uid = $entrada->participant->uid;
                                    $participante->email = $entrada->participant->email;
                                    $participante->save();

                                    //Guardamos la Factura
                                    $factura = new Factura();
                                    //$factura->programa	= $programa;
                                    $factura->referencia = $entrada->id;
                                    $factura->campaign = 'ti';
                                    $factura->moderation = $entrada->moderation;
                                    $factura->photo = $entrada->photo_url;
                                    $factura->FK_id_participante = $participante->id_participante;
                                    $factura->user_upload = $entrada->created_at;
                                    $factura->save();

                                    $almacenadas++;
                                }
                            }
                        }

                        //Vuelvo a consultar para buscar otra pagina
                            $response = Http::withHeaders([
                                'Content-Type' => 'application/json',
                            ])->post($url, [
                                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                                'campaign' => 'ti',
                                '_type' => 'UploadPhoto',
                                'category' => 'invoice',
                                //'before' => '1639116538.149',
                                'after' => $next_page,
                                //'limit' => 150,
                                //'hide_undo' => true,
                                //'distinct_id' => 'julianclavijof@gmail.com',
                                'date_filter' => [
                                    'sdate' => $fecha,//date('Y-m-d'), //dia DE HOY
                                    'edate' => date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day')), //dia SIGUIENTE
                                ],
                            ]);

                            try{
                                $data = $response->object();
                                $data = $data->data;
                            }
                            catch(\Exception $e){
                                $data->entries = [];
                            }
                    }

                    return response()->json([
                        'status' => 'success',
                        'message' => "Factura y Participantes almacenados de forma exitosa (".$almacenadas."), porfavor envielas al OCR",
                        'disponible' => true,
                    ], 200);
                }
                else{
                    return response()->json([
                        'status' => 'warning',
                        'message' => $data->message,
                        'codigo' => $data->code_error
                    ], 200);
                }
            }
            else if($response->clientError()){
                $data = $response->object();
                return response()->json([
                    'status' => 'error',
                    'message' => $data->message,
                    'codigo' => $data->code_error
                ], 400);
            }
            else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error en el Servidor de la API de Superlikers, intentelo más tarde',
                ], 503);
            }
        }

        //ENVIAR FACTURAS EN ESTADO NO PARAM A OCR Y PARAMETRIZAR
        public function sendOCRParam(){
            ini_set('max_execution_time', '36000');
            try{
                $facturas = Factura::where('state', 'no param')->
                                    whereNull('lectura')->whereDate('created_at', '>=', date('Y-m-d'))->get();

                $cantidad = 0;

                foreach ($facturas as $key => $factura) {
                    //CREAMOS EL CRON JOB
                    ProcessOCRParam::dispatch($factura)->onQueue('paramJob');

                    $cantidad++;
                    if($cantidad > 1000){
                        break;
                    }
                }

                //Disparamos el Job con el Artisan
                //$exitCode = Artisan::call('queue:work', ['--queue' => 'paramJob']);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Facturas enviadas a parametrizar exitosamente, se ira haciendo el proceso poco a poco internamente',
                    'cantidad' => $cantidad,
                ], 200);

            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso antes de inicar la lectura ocr y parametrización',
                    'error message' => $e->getMessage(),
                    'codigo' => 503,
                ], 503);
            }
        }

        //RE ENVIAR FACTURAS EN ESTADO NO PARAM O PENDIENTE A PARAMETRIZAR CON LECTURA
        public function reSendOCRParam(){
            ini_set('max_execution_time', '36000');
            try{
                $facturas = Factura::where('state', 'pendiente')->where('envio', 0)->get();

                $cantidad = 0;

                foreach ($facturas as $key => $factura) {

                    //ELIMINAMOS LOS PRODUCTOS PERNOD
                    FacturaProducto::where('FK_id_factura', $factura->id_factura)->delete();

                    if($factura->productos->isEmpty()){
                        //ELIMINAMOS SUS PRODUCTOS EXTRAS
                        FacturaExtra::where('FK_id_factura', $factura->id_factura)->delete();

                        //CREAMOS EL CRON JOB
                        ProcessREOCRParam::dispatch($factura)->onQueue('reParamJob');

                        $cantidad++;
                        if($cantidad > 600){
                            break;
                        }
                    }
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Facturas enviadas a re-parametrizar exitosamente, se ira haciendo el proceso poco a poco internamente',
                    'cantidad' => $cantidad,
                ], 200);

            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso antes de inicar la lectura ocr y parametrización',
                    'error message' => $e->getMessage(),
                    'codigo' => 503,
                ], 503);
            }
        }

        //ENVIAR FACTURAS A SUPERLIKERS EN RETAIL Y CAMBIAR SU ESTADO
        public function sendInvoice(Request $request){
            ini_set('max_execution_time', '36000');
            $validate = Validator::make($request->all(), [
                //VALIDACIONES
                'fecha_min' => 'required|date',
                'fecha_max' => 'nullable|date',
                'tipo' => 'required|string',
            ], $this->mensajes_error);

            if($validate->fails()){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors()
                ], 203);
            }

            if($request->tipo != "lectura" && $request->tipo != "usuario")
                return response()->json([
                    'status' => 'warning',
                    'message' => 'Por favor envia el dato (tipo) de manera correcta, "lectura" para filtrar por fecha en que se hizo la lectura o "usuario" para filtrar por fecha en que subieron las facturas',
                ], 203);

            if(!$request->fecha_max){
                $request->fecha_max = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));
            }
            else{
                $dateMin = strtotime($request->fecha_min);
                $dateMax = strtotime($request->fecha_max);
                if($dateMin >= $dateMax){
                    return response()->json([
                        'status' => 'warning',
                        'message' => 'Debes enviar una fecha máxima mayor a la fecha menor registrada. Intentalo de nuevo',
                    ], 203);
                }
            }

            try{
                if($request->tipo == "lectura"){
                    $facturas = Factura::where('envio', 0)
                                        ->where('moderation', 'pending')
                                        ->where('state','<>','pendiente')->where('state','<>','pendiente superlikers')
                                        ->whereDate('created_at', '>=', date($request->fecha_min))
                                        ->whereDate('created_at', '<=', date($request->fecha_max))
                                        ->get();
                }
                else{
                    $facturas = Factura::where('envio', 0)
                                        ->where('moderation', 'pending')
                                        ->where('state','<>','pendiente')->where('state','<>','pendiente superlikers')
                                        ->whereDate('user_upload', '>=', date($request->fecha_min))
                                        ->whereDate('user_upload', '<=', date($request->fecha_max))
                                        ->get();
                }

                $cantidad = 0;

                foreach ($facturas as $key => $factura) {
                    //CREAMOS EL CRON JOB
                    if(!$factura->productos->isEmpty() || $factura->state == "rechazado"){
                        ProcessRegistrarVenta::dispatch($factura)->onQueue('registrarInv');
                        $cantidad++;
                    }

                    if($cantidad > 2000){
                        break;
                    }
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Facturas enviadas para registrar exitosamente, se ira haciendo el proceso poco a poco internamente',
                    'cantidad' => $cantidad,
                ], 200);

            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso antes de inicar al envio de retail a superlikers',
                    'error message' => $e->getMessage(),
                    'codigo' => 503,
                ], 503);
            }
        }

        //RE-ENVIAR FACTURA A PARAMETRIZAR
        public function paramInvoice($id){
            ini_set('max_execution_time', '36000');
            DB::beginTransaction();
            try{
                $factura = Factura::find($id);

                //ELIMINAMOS SUS PRODUCTOS EXTRAS
                FacturaExtra::where('FK_id_factura', $factura->id_factura)->delete();

                //ELIMINAMOS LOS PRODUCTOS PERNOD
                FacturaProducto::where('FK_id_factura', $factura->id_factura)->delete();

                //Almaceno la foto si hace falta
                if(!$factura->lectura)
                    $archivo = $this->saveFactura($factura->photo);
                else
                    $archivo = true;

                if($archivo){
                    //La envio a leer
                    if(!$factura->lectura){
                        $values = FacturaIA::instance()->readFactura($factura->photo, $archivo);
                        $values->upload = $factura->user_upload;
                        $values->reparam = "no";
                        try{ unlink($archivo); }
                        catch(\Exception $e){}
                    }
                    else{
                        $values = new \stdClass;
                        $values->contenido = $factura->lectura;
                        $values->upload = $factura->user_upload;
                        $values->reparam = "si";
                    }

                    if(isset($values->error)){
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Error interno en el proceso de lectura del archivo',
                            'error message' => $values->error,
                            "codigo" => 503,
                        ], 503);
                    }

                    if($values){
                        $factura->lectura = $values->contenido;

                        //La envio a parametrizar
                        $values = FacturaIA::instance()->parametrizar($values);

                        if(isset($values->error)){
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Error interno en el proceso de parametrización',
                                'error message' => $values->error,
                                "codigo" => 503,
                            ], 503);
                        }

                        //Ahora manejamos los datos evaluados
                            $factura->cdc = $values->restaurante;
                            $factura->campaign = $values->category; //mes de la factura
                            $factura->folio = $values->referencia;
                            $factura->mesero = $values->mesero;
                            $factura->state = $values->estado;
                            $factura->registro = $values->fecha_factura;

                        //Grabamos los productos Pernod recibidos
                            foreach ($values->productos as $key => $producto) {
                                $pernod = new FacturaProducto();
                                $pernod->referencia = $producto["referencia"];
                                $pernod->price = $producto["precio"];
                                $pernod->quantity = $producto["cantidad"];
                                $pernod->provider = "Pernod Ricard";//$producto["proveedor"];
                                $pernod->line = $producto["linea"];
                                $pernod->formato = $producto["formato"];
                                $pernod->FK_id_factura = $factura["id_factura"];
                                $pernod->save();
                            }

                        //Grabamos los producos extras de la factura
                            foreach ($values->extras as $key => $producto) {
                                $extra = new FacturaExtra();
                                $extra->nombre = $producto;
                                $extra->FK_id_factura = $factura["id_factura"];
                                $extra->save();
                            }

                        //Hacemos save de todo
                        $factura->save();

                        DB::commit();
                        return response()->json([
                            'status' => 'success',
                            'message' => 'Factura leida exitosamente, revisa el objeto y el ID de la misma',
                            'id' => $factura->id_factura,
                            'lectura' => $factura->lectura,
                            'productos' => $factura->productos,
                            'extras' => $factura->extras,
                            'codigo' => 200,
                        ], 200);
                    }
                    else{
                        $factura->lectura = "";
                        $factura->save();

                        return response()->json([
                            'status' => 'success',
                            'message' => 'El lector no pudo captar ningun caracter',
                            'id' => $factura->id_factura,
                            'lectura' => $factura->lectura,
                            'codigo' => 203,
                        ], 203);
                    }
                }
            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso de lectura y posterior envio a parametrizar',
                    'error message' => $e->getMessage()." | ".$e->getLine(),
                    'codigo' => 503,
                ], 503);
            }

        }

        //Enviar una factura individual a Superlikers
        public function registrarInvoice(Request $request){
            ini_set('max_execution_time', '36000');
            DB::beginTransaction();
            try{
                $factura = Factura::find($request->id);

                if(!$factura)
                    return response()->json([
                        'status' => 'warning',
                        'message' => 'La Factura no pudo ser encontrada, por el ID enviado',
                        "codigo" => 200,
                    ], 200);

                if($factura->envio)
                    return response()->json([
                        'status' => 'warning',
                        'message' => 'La Factura ya fue cargada en la API de superlikers',
                        "codigo" => 200,
                    ], 200);

                if($factura->state != "rechazado"){
                    //Seteamos los datos para enviar al endpoint de registrar factura
                    $url = "https://api.superlikers.com/v1/retail/buy";

                    $data = [
                        'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                        'campaign' => 'ti',
                        'category' => 'invoice',
                        'distinct_id' => $factura->participante->uid, //"uid - correo": del usuario participante o mesero,
                        'ref' => $factura->referencia, //referencia de la factura en superlikers
                        'date' => strtotime($factura->registro), ///TIEMPO EN UNIX
                        'products' => [],
                        'properties' => [
                            'tarifario' => $factura->tarifario,
                            'folio' => $factura->folio,
                            'url_photo' => $factura->photo,
                        ]
                    ];

                    //Productos Pernod
                    foreach ($factura->productos as $producto) {
                        $pernot = array(
                            'ref' => $producto->referencia, //codigo referencia del producto
                            'provider' => $producto->provider,
                            'line' => $producto->line,
                            'price' => ($producto->price / $producto->quantity),
                            'quantity' => $producto->quantity,
                        );
                        array_push($data['products'], $pernot);
                    }

                    //Productos Extras
                    foreach ($factura->extras as $producto) {
                        $extra = array(
                            'ref' => $producto->nombre, //nombre del producto
                            'provider' => 'adicional',
                            'price' => 0,
                            'quantity' => 0,
                        );
                        array_push($data['products'], $extra);
                    }

                    $response = Http::withHeaders([
                        'Content-Type' => 'application/json',
                    ])->post($url, $data);

                    $resultado = $response->object();

                    if($response->successful()){
                        $data = $response->object();
                        //SE REGISTRO LA VENTA
                        if($data->ok){

                            if($factura->state == "aceptado"){
                                $url = "https://api.superlikers.com/v1/entries/accept";

                                $send = [
                                    'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                                    'campaign' => 'ti',
                                    'id' => $factura->referencia, //identificador de la actividad - factura en superlikers
                                ];

                                $response = Http::withHeaders([
                                    'Content-Type' => 'application/json',
                                ])->post($url, $send);
                            }

                            if($response->successful()){
                                $data = $response->object();
                                //Revisamos si fue exitoso para actualizar la factura
                                if($data->ok){
                                    $factura->moderation = "accepted";
                                }
                                else{
                                    if($data->code_error == 41){
                                        $factura->moderation = "accepted";
                                    }
                                }
                            }

                            $factura->envio = 1;
                            $factura->save();
                            DB::commit();

                            return response()->json([
                                'status' => 'success',
                                'message' => 'Facturada enviada de manera exitosa a superlikers',
                                "codigo" => 200,
                                'resultado' => $resultado,
                            ], 200);
                        }
                        else{
                            //Vemos si el error es porque ya se cargo
                            if($data->code_error == 63){
                                $factura->envio = 1;
                                $factura->moderation = "accepted";
                                $factura->save();
                                DB::commit();

                                return response()->json([
                                    'status' => 'success',
                                    'message' => 'La factura ya fue registrada anteriormente por otro usuario de superlikers',
                                    "codigo" => 200,
                                ], 200);
                            }
                            else
                                return response()->json([
                                    'status' => 'error',
                                    'message' => 'La factura no pudo ser procesada por superlikers',
                                    'resultado' => $data->message,
                                    "codigo" => $data->code_error,
                                ], 200);
                        }
                    }
                    else if($response->clientError()){
                        DB::rollback();
                        return response()->json([
                            'status' => 'warning',
                            'message' => 'La API de superlikers no esta disponible en este momento',
                            "codigo" => 200,
                        ], 200);
                    }
                    else{
                        DB::rollback();
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Error interno del servidor de Superlikers',
                            "codigo" => 200,
                        ], 200);
                    }
                }
                else{
                    //la envio a rechazar no más y ya
                    $url = "https://api.superlikers.com/v1/entries/reject";

                    $data = [
                        'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                        'campaign' => 'ti',
                        'id' => $factura->referencia, //identificador de la actividad - factura en superlikers
                    ];

                    $response = Http::withHeaders([
                        'Content-Type' => 'application/json',
                    ])->post($url, $data);

                    if($response->successful()){
                        $data = $response->object();
                        //Revisamos si fue exitoso para actualizar la factura
                        if($data->ok){
                            $factura->envio = 1;
                            $factura->moderation = "rejected";
                            $factura->save();
                            DB::commit();

                            return response()->json([
                                'status' => 'success',
                                'message' => 'Facturada enviada de manera exitosa a superlikers',
                                "codigo" => 200,
                            ], 200);
                        }
                        else{
                            if($data->code_error == 41){
                                $factura->envio = 1;
                                $factura->moderation = "accepted";
                                $factura->save();
                                DB::commit();

                                return response()->json([
                                    'status' => 'success',
                                    'message' => 'La factura ya fue procesada por superlikers anteriormente',
                                    "codigo" => 200,
                                ], 200);
                            }
                            else
                                return response()->json([
                                    'status' => 'error',
                                    'message' => 'La factura no pudo ser procesada por superlikers',
                                    'resultado' => $data->message,
                                    "codigo" => $data->code_error,
                                ], 200);
                        }
                    }
                    else if($response->clientError()){
                        DB::rollback();
                        return response()->json([
                            'status' => 'warning',
                            'message' => 'La API de superlikers no esta disponible en este momento',
                            "codigo" => 200,
                        ], 200);
                    }
                    else{
                        DB::rollback();
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Error interno del servidor de Superlikers',
                            "codigo" => 200,
                        ], 200);
                    }
                }

            }catch(\Exception $e){
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' => 'Fallo del servidor: '.$e->getMessage(),
                    "codigo" => 200,
                ], 200);
            }
        }

    //FIN PROCESO YA NORMAL DEL FLUJO COMPLETO

    //RUTAS DE TESTING
        //TESTING DE PASAR FACTURA AL OCR Y LUEGO A PARAMETRIZAR
        public function testOCRParam(){
            ini_set('max_execution_time', '36000');
            DB::beginTransaction();
            try{
                $factura = Factura::where('state', 'no param')->first();

                //Almaceno la foto
                $archivo = $this->saveFactura($factura->photo);

                if($archivo){
                    //La envio a leer
                    $values = FacturaIA::instance()->readFactura($factura->photo, $archivo);

                    if(isset($values->error)){
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Error interno en el proceso de lectura del archivo',
                            'error message' => $values->error,
                            "codigo" => 503,
                        ], 503);
                    }

                    if($values){
                        $factura->lectura = $values->contenido;
                        $values->reparam = "no";

                        //La envio a parametrizar
                        $values = FacturaIA::instance()->parametrizar($values);

                        if(isset($values->error)){
                            return response()->json([
                                'status' => 'error',
                                'message' => 'Error interno en el proceso de parametrización',
                                'error message' => $values->error,
                                "codigo" => 503,
                            ], 503);
                        }

                        //Ahora manejamos los datos evaluados
                            $factura->folio = $values->referencia;
                            $factura->mesero = $values->mesero;
                            $factura->state = $values->estado;
                            $factura->registro = $values->fecha_factura;

                        //Grabamos los productos Pernod recibidos
                            foreach ($values->productos as $key => $producto) {
                                $pernod = new FacturaProducto();
                                $pernod->referencia = $producto["referencia"];
                                $pernod->price = $producto["precio"];
                                $pernod->quantity = $producto["cantidad"];
                                $pernod->provider = $producto["proveedor"];
                                $pernod->line = $producto["linea"];
                                $pernod->FK_id_factura = $factura["id_factura"];
                                $pernod->save();
                            }

                        //Grabamos los producos extras de la factura
                            foreach ($values->extras as $key => $producto) {
                                $extra = new FacturaExtra();
                                $extra->nombre = $producto;
                                $extra->FK_id_factura = $factura["id_factura"];
                                $extra->save();
                            }

                        //Hacemos save de todo
                        $factura->save();

                        DB::commit();
                        return response()->json([
                            'status' => 'success',
                            'message' => 'Factura leida exitosamente, revisa el objeto y el ID de la misma',
                            'id' => $factura->id_factura,
                            'lectura' => $factura->lectura,
                            'productos' => $factura->productos,
                            'extras' => $factura->extras,
                            'codigo' => 200,
                        ], 200);
                    }
                    else{
                        $factura->lectura = "";
                        $factura->mesero = "No parametrizo";
                        $factura->folio = "No parametrizo";
                        $factura->state = "rechazado";
                        $factura->save();

                        return response()->json([
                            'status' => 'success',
                            'message' => 'El lector no pudo captar ningun caracter',
                            'id' => $factura->id_factura,
                            'lectura' => $factura->lectura,
                            'codigo' => 203,
                        ], 203);
                    }
                }
            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso de lectura y posterior envio a parametrizar',
                    'error message' => $e->getMessage()." | ".$e->getLine(),
                    'codigo' => 503,
                ], 503);
            }

        }

        //TEST ALMACENAR FACTURA PARA ENVIAR AL OCR
        public function saveFactura($link){
            ini_set('max_execution_time', '36000');
            $url = $link;

            try{
                $content = file_get_contents($url);
                $name = substr($url, strrpos($url, '/') + 1);

                //El archivo esta grabado
                $archivo = storage_path().'/app/public/invoices/'.$name;
                file_put_contents($archivo, $content);

                return $archivo;
            }
            catch(\Exception $e){
                //return $e->getCode()." | ".$e->getMessage();
                return null;
            }
        }

        //TESTEAR EL LECTOR DE IMAGENES O PDF
        public function invoiceLector(Request $request){

            ini_set('max_execution_time', '36000');
            try{
                //PROCESO DE VALIDAR SI TIENE PERMISOS EL USUARIO
                $user = "superlikelogin";
                $token = "h4oHMTTV76";

                $header = $request->header();

                //Revisamos si tienen permisos de ingresar
                if(! ($header["authorization"][0] == $user && $header["token"][0] == $token) ){
                    return response()->json([
                        'status' => 'unauthenticated',
                        'message' => 'Unauthenticated',
                        'codigo' => 401
                    ], 401);
                }

                $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

                if($validate->fails()){
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Error formato de datos recibidos',
                        'errores' => $validate->errors(),
                        'codigo' => 400,
                    ], 400);
                }

                $values = FacturaIA2::instance()->read($request->file('archivo'), strtoupper('DENTAL 83'));

                if(isset($values->error)){
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Error interno en el proceso de lectura del archivo',
                        'error message' => $values->error,
                        "codigo" => 503,
                    ], 503);
                }

                if($values){
                    $contenido = $values->contenido;
                    unset($values->contenido);

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Factura leida exitosamente',
                        'ocr' => $contenido,
                        'valores' => $values,
                        'codigo' => 200,
                    ], 200);
                }
                else{
                    return response()->json([
                        'status' => 'success',
                        'message' => 'El lector no pudo captar ningun caracter',
                        'ocr' => 'La factura no pudo leerse ya que el archivo no contiene caracteres legibles',
                        'codigo' => 203,
                    ], 203);
                }

            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso antes de inicar la lectura ocr',
                    'error message' => $e->getMessage(),
                    'codigo' => 503,
                ], 503);
            }

        }

        //TESTEAR EL LECTOR DE IMAGENES PARA LAS 1500 VECES
        public function invoiceLector500(Request $request){
            ini_set('max_execution_time', '36000');

            try{
                //PROCESO DE VALIDAR SI TIENE PERMISOS EL USUARIO
                $user = "superlikelogin";
                $token = "h4oHMTTV76";

                $header = $request->header();

                //Revisamos si tienen permisos de ingresar
                if(! ($header["authorization"][0] == $user && $header["token"][0] == $token) ){
                    return response()->json([
                        'status' => 'unauthenticated',
                        'message' => 'Unauthenticated',
                        'codigo' => 401
                    ], 401);
                }

                $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

                if($validate->fails()){
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Error formato de datos recibidos',
                        'errores' => $validate->errors(),
                        'codigo' => 400,
                    ], 400);
                }

                $cantidad = $request->cantidad;
                $success = 0;
                $fail = 0;

                for ($i=0; $i < $cantidad; $i++) {
                    //EN TEORIA TENEMOS QUE GRABAR EL FILE Y SE PASA SOLO LA RUTA
                    ProcessGoogleVision::dispatch()->onQueue('googleVision');
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Proceso finalizado',
                    'cantidad' => $cantidad,
                ], 200);

            }catch(\Exception $e){
                DB::rollback();
                //Enviamos a una vista con un mensaje de error
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error interno en el proceso antes de inicar la lectura ocr',
                    'error message' => $e->getMessage(),
                    'codigo' => 503,
                ], 503);
            }

        }

        //TESTING DE LEER LINK DE INVOICE Y ALMACENAR LA MISMA
        public function saveinvoice(Request $request){
            ini_set('max_execution_time', '36000');
            $url = $request->invoice;

            try{
                $content = file_get_contents($url);
                $name = substr($url, strrpos($url, '/') + 1);

                //El archivo esta grabado
                $archivo = storage_path().'/app/public/invoices/'.$name;
                file_put_contents($archivo, $content);

                return $archivo;
            }
            catch(\Exception $e){
                return $e->getCode()." | ".$e->getMessage();
            }
        }
    //FIN RUTAS TESTING

    //PRUEBA DE LAS RUTAS DE DEVOLUCIONES
        //CONSULTA DE LAS ENTRADAS ALMACENADAS EN SUPERLIKERS
        public function entriesDev(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com/v1/entries/index";

            //Validamos que hayan enviado la cantidad que se desea sacar
            if(!$request->cantidad){
                return response()->json([
                    'status' => 'warning',
                    'message' => "Porfavor envia el dato cantidad para saber cuantas facturas debemos sacar de superlikers",
                    'dato' => "cantidad"
                ], 200);
            }

            $cantidad = $request->cantidad;
            $almacenadas = 0;

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                '_type' => 'UploadPhoto',
                'category' => 'invoice',
                //'before' => '1639116538.149',
                //'after' => '1640199596.837',
                //'limit' => 150,
                //'hide_undo' => true,
                'distinct_id' => 'julianclavijof@gmail.com',
                'date_filter' => [
                    'sdate' => '2021-12-01',//date('Y-m-d'), //dia DE HOY
                    'edate' => date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day')), //dia SIGUIENTE
                ],
            ]);

            $data = $response->object();
            return $data;
        }

        //FACTURA INDIVIDUAL A VER
        public function individual(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com/v1/entries/index";

            $referencia = "61b2673af4a1b763544e7c72";

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                '_type' => 'UploadPhoto',
                'category' => 'invoice',
                //'before' => '1639116538.149',
                //'after' => '1648776213.972',
                //'limit' => 150,
                //'hide_undo' => true,
                'distinct_id' => 'julianclavijof@gmail.com',
                'date_filter' => [
                    'sdate' => '2021-12-01',//date('Y-m-d'), //dia DE HOY
                    'edate' => date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day')), //dia SIGUIENTE
                ],
            ]);

            $data = $response->object();
            return $data;
        }

        //DEVOLUCIÓN
        public function devolucion(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com//v1/retail/refund";

            $referencia = "61b2673af4a1b763544e7c73";

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                'distinct_id' => 'julianclavijof@gmail.com',
                'ref' => $referencia,
                'products' => [
                    //'sku' => 'quantity',
                    '9881' => 1,
                    //'9879' => 3,
                ],
            ]);

            $data = $response->object();
            return $data;
        }

        //RE-ENVIO POST DEVOLUCIÓN
        public function recarga(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com/v1/retail/buy";

            $data = [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',//'ti',
                'category' => 'invoice',
                'distinct_id' => 'julianclavijof@gmail.com', //"uid - correo": del usuario participante o mesero,
                'ref' => '61b2673af4a1b763544e7c73-1', //referencia de la factura en superlikers
                'date' => strtotime("2021-05-04 21:29:00"),
                'products' => [
                    [
                        'ref' => 9881, //codigo referencia del producto
                        'provider' => 'Pernod Ricard',
                        'line' => 'WHISKY',
                        'price' => 220.00,
                        'quantity' => 1,
                    ],
                    [
                        'ref' => 'TOSTADA DE ATUN', //nombre de producto adicional
                        'provider' => 'adicional',
                        'price' => 0,
                        'quantity' => 0,
                    ],
                ]
            ];

            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($url, $data);

            $data = $response->object();
            return $data;
        }
    //FIN PRUEBA RUTAS DE LAS DEVOLUCIONES

    //PRUEBA DE SACAR LA INFORMACIÓN DEL PARTICIPANTE PARA SABER QUE PROGRAMA ES LA FACTURA
        public function participanteData(Request $request){
            ini_set('max_execution_time', '36000');
            $url = "https://api.superlikers.com/v1/participants/info";

            //Esto solamente se trae 100 busquedas
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->get($url, [
                'api_key' => '8d351cbff604b9f5f99cf150afaa8aef',
                'campaign' => 'ti',
                'distinct_id' => 'alfredorozco1999@gmail.com',
            ]);

            //OPCIONES
            //alfredorozco1999@gmail.com pernod_off
            //saulantonio2307@outlook.es pernod challengue
            //pirur99@gmail.com pernod challenge

            $programa = "N/A";

            $data = $response->object();
            //return $data;
            if($data->ok == "true"){

                if(isset($data->object->challenges) && !empty($data->object->challenges))
                    $programa = $data->object->challenges[0]; //tipo de factura
                else
                    $programa = "Participante existe pero no se le reconocio programa";
            }
            else{
                if($data->code_error = 20)
                    $programa = "Participante no existe";
                elseif($data->code_error = 23)
                    $programa = "Participante ha sido baneado";
                else
                    $programa = "No se pudo encontrar algo";
            }

            return $programa;
        }
    //ES DECIR CHALLENGE O OFF
}
