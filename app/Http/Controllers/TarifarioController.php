<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;
use App\Models\Jobs;
use App\Models\FailJobs;

//Helper
use App\Helper\Notificacion;
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;

//Jobs
use App\Jobs\ProcessGoogleVision;
use App\Jobs\ProcessOCRParam;
use App\Jobs\ProcessREOCRParam;
use App\Jobs\ProcessRegistrarVenta;

class TarifarioController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
        'before' => 'La fecha de inicio debe ser ajuro igual o menor al mes/año actual',
        'after' => 'La fecha de inicio debe ser ajuro mayor o igual al mes/año actual',
        'duplicate' => 'Tienes elementos seleccionados duplicados en la marca, procura dejar uno solo',
    ];

    ////////////////////////////////////////////////VISTAS

    public function index($filtro = null){
        ini_set('max_execution_time', '36000');
        if($filtro)
            $tarifarios = Tarifario::where('ciudad', $filtro)->orderBy('ciudad', 'desc')->get();
        else
            $tarifarios = Tarifario::orderBy('ciudad', 'desc')->get();

        $ciudades = Tarifario::select('ciudad')->groupBy('ciudad')->get();
        $marcas = Productos::all();

        return view('tarifarios.index', compact('tarifarios', 'ciudades', 'marcas', 'filtro'));
    }

    public function editar($id_tarifario){
        $tarifario = Tarifario::find($id_tarifario);
        $marcas = Productos::all();

        if(!$tarifario)
            return Redirect::back()->with('edit', 'El Tarifario seleccionado a editar no pudo ser encontrado');

        //Evaluar si el tarifario antiguo que posiblemente sea el actual se pueda editar
        $editable = true;

        if($tarifario->antmes){
            //Evaluamos en que dia estamos del mes actual
            $dia = date("d");

            //Mes Actual o Anterior para ajustar;
                $mes = date("Y-m-01");
                if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                    $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                }

            //Validamos la fecha de la factura actual para ver si la rechazamos o no
                $mes = strtotime($mes);
                $date = strtotime($tarifario->mes);

                if($mes >= $date){
                    $editable = false;
                }
        }

        return view('tarifarios.edit', compact('tarifario', 'marcas', 'editable'));
    }

    public function editarall($ciudad){
        $tarifarios = Tarifario::where('ciudad', $ciudad)->get();
        $marcas = Productos::all();

        if($tarifarios->isEmpty())
            return Redirect::back()->with('edit', 'La ciudad seleccionada no posee tarifarios actualmente');

        return view('tarifarios.edit_all', compact('tarifarios', 'marcas', 'ciudad'));
    }

    /////////////////////////////////////////////////FIN VISTAS

    /////////////////////////////////////////////////CRUD

    //Store de un tarifario individual
    public function store(Request $request){
        //return $request->all();

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'ciudad' => 'required|string|max:255',
                'marca' => 'required',
                'modificador' => 'required|numeric|min:1',
                'mes' => 'required|date|before:today',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $tarifarios = Tarifario::where('ciudad', $request->ciudad)->get();
            $default = false;

            if(!$tarifarios->isEmpty()){
                foreach($tarifarios as $tarifario){
                    if($tarifario->FK_id_marca == $request->marca){
                        $default = true;
                        break;
                    }
                }
            }

            if($default)
                return Redirect::back()->with('error', 'Ya existe la ciudad con la marca seleccionada con un valor de tarifario, puedes editar el mismo ubicandolo en la tabla');

            //SACAMOS LA CATEGORIA DE LA MARCA SELECCIONADA
            switch ($request->marca) {
                case 1: $categoria = "VODKA"; break;
                case 2: $categoria = "VODKA"; break;
                case 6: $categoria = "WHISKY"; break;
                case 7: $categoria = "WHISKY"; break;
                case 8: $categoria = "WHISKY"; break;
                case 9: $categoria = "WHISKY"; break;
                case 10: $categoria = "RON"; break;
                case 11: $categoria = "COÑAC"; break;
                case 12: $categoria = "WHISKY"; break;
                case 13: $categoria = "TEQUILA"; break;
                case 14: $categoria = "WHISKY"; break;
                case 15: $categoria = "WHISKY"; break;
                case 16: $categoria = "COÑAC"; break;
                case 17: $categoria = "TEQUILA"; break;
                case 18: $categoria = "GINEBRA"; break;
                case 19: $categoria = "GINEBRA"; break;
                case 20: $categoria = "GINEBRA"; break;
                case 21: $categoria = "GINEBRA"; break;
                default: $categoria = "WHISKY"; break;
            }

            //PASO TODAS LAS VALIDACIONES AHORA SI ANEXAMOS EL NUEVO TARIFARIO DE LA CIUDAD / MARCA
            $new = new Tarifario();
            $new->ciudad = $request->ciudad;
            $new->categoria = $categoria;
            $new->copaxbotella = $request->modificador;
            $new->FK_id_marca = $request->marca;
            $new->mes = $request->mes."-01";

            $new->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Tarifario agregado exitosamente a la ciudad ('.$request->ciudad.') para la marca seleccionada...');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Agregar Tarifario, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Update de un tarifario individual
    public function update(Request $request, $id_tarifario){
        //return $request->all();

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'marca' => 'required',
                'modActual' => 'required|numeric|min:1',
                'modAntiguo' => 'required|numeric|min:0',
                'mes' => 'required|date',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            if($request->has('selectPernod')){
                if($request->selectPernod == 1){
                    $validate = Validator::make($request->all(), [
                        'modNuevo' => 'required|numeric|min:1',
                        'mesNuevo' => 'required|date|after:today',
                    ], $this->mensajes_error);

                    if($validate->fails()){
                        return Redirect::back()->withErrors($validate)->withInput();
                    }
                }
            }

            $tarifa = Tarifario::find($id_tarifario);
            if(!$tarifa)
                return Redirect::back()->with('error', 'El Tarifario seleccionado no pudo ser encontrado por lo que no pudo ser editado');


            if($request->marca != $tarifa->FK_id_marca){
                $tarifarios = Tarifario::where('ciudad', $tarifa->ciudad)->get();
                $default = false;

                if(!$tarifarios->isEmpty()){
                    foreach($tarifarios as $tarifario){
                        if($tarifario->FK_id_marca == $request->marca){
                            $default = true;
                            break;
                        }
                    }
                }

                if($default)
                    return Redirect::back()->with('error', 'Ya existe la ciudad con la marca seleccionada con un valor de tarifario, puedes editar el mismo ubicandolo en la tabla en el apartado de panel');
            }

            //SACAMOS LA CATEGORIA DE LA MARCA SELECCIONADA
            switch ($request->marca) {
                case 1: $categoria = "VODKA"; break;
                case 2: $categoria = "VODKA"; break;
                case 6: $categoria = "WHISKY"; break;
                case 7: $categoria = "WHISKY"; break;
                case 8: $categoria = "WHISKY"; break;
                case 9: $categoria = "WHISKY"; break;
                case 10: $categoria = "RON"; break;
                case 11: $categoria = "COÑAC"; break;
                case 12: $categoria = "WHISKY"; break;
                case 13: $categoria = "TEQUILA"; break;
                case 14: $categoria = "WHISKY"; break;
                case 15: $categoria = "WHISKY"; break;
                case 16: $categoria = "COÑAC"; break;
                case 17: $categoria = "TEQUILA"; break;
                case 18: $categoria = "GINEBRA"; break;
                case 19: $categoria = "GINEBRA"; break;
                case 20: $categoria = "GINEBRA"; break;
                case 21: $categoria = "GINEBRA"; break;
                default: $categoria = "WHISKY"; break;
            }

            //PASO TODAS LAS VALIDACIONES AHORA SI HACEMOS UPDATE
            /* $new = new Tarifario();
            $new->ciudad = $request->ciudad;
            $new->categoria = $categoria;
            $new->copaxbotella = $request->modificador;
            $new->FK_id_marca = $request->marca;
            $new->mes = $request->mes."-01"; */

            if($request->has('selectPernod')){
                if($request->selectPernod == 1){
                    //TENEMOS NUEVO TARIFARIO
                    $tarifa->categoria = $categoria;
                    $tarifa->FK_id_marca = $request->marca;

                    //new tarifario en copaxbotella y mes en que se hara efectivo
                    $tarifa->copaxbotella = $request->modNuevo;
                    $tarifa->mes = $request->mesNuevo."-01";

                    //tomamos el tarifario anterior que estaba como actual y lo grabamos en los anteriores
                    $tarifa->antcopaxbotella = $request->modActual;
                    $tarifa->antmes = $request->mes."-01";
                }
                else{
                    //UPDATE DE DATOS ACTUALES
                    $tarifa->categoria = $categoria;
                    $tarifa->FK_id_marca = $request->marca;

                    //new tarifario en copaxbotella y mes en que se hara efectivo
                    $tarifa->copaxbotella = $request->modActual;
                    $tarifa->mes = $request->mes."-01";

                    //tomamos el tarifario anterior que estaba como actual y lo grabamos en los anteriores
                    $tarifa->antcopaxbotella = $request->modAntiguo;
                }
            }

            $tarifa->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Tarifario editado exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Editar Tarifario, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Update de un tarifario individual
    public function updateall(Request $request){
        //return $request->all();

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'ciudad' => 'required',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            if($request->has('selectPernod')){
                if($request->selectPernod == 1){
                    $validate = Validator::make($request->all(), [
                        'pernod.*.ref' => 'required|distinct',
                        'pernod.*.multi' => 'required|numeric|min:1',
                        'pernod.*.mes' => 'required|date',//'required|date|after:today',
                    ], $this->mensajes_error);

                    if($validate->fails()){
                        return Redirect::back()->withErrors($validate)->withInput();
                    }
                }
            }

            //PASO TODAS LAS VALIDACIONES AHORA SI HACEMOS UPDATE
            if($request->has('selectPernod')){
                if($request->selectPernod == 1){

                    /* ELIMINAR Y HACER STORE DE LO VIEJO TODO EL TARIFARIO DE LA CIUDAD */
                        $tarifarios = Tarifario::where('ciudad', $request->ciudad)->get();
                    /* FIN ELIMINAR TODO EL TARIFARIO DE LA CIUDAD */

                    if($request->has('pernod')){
                        foreach($request->pernod as $pernod){

                            $trf = Tarifario::where('FK_id_marca', $pernod["ref"])->where('ciudad', $request->ciudad)->first();

                            if($trf){
                                //TARIFARIO NUEVO EN UN CASO YA EXISTENTE DE MARCA
                                //tomamos el tarifario anterior que estaba como actual y lo grabamos en los anteriores
                                $trf->antcopaxbotella = $trf->copaxbotella;
                                $trf->antmes = $trf->mes;

                                //new tarifario en copaxbotella y mes en que se hara efectivo
                                $trf->copaxbotella = $pernod["multi"];
                                $trf->mes = $pernod["mes"]."-01";
                                $trf->save();
                            }
                            else{
                                //TARIFARIO NUEVO EN UN CASO NO EXISTENTE DE MARCA

                                //Ubicamos el tipo de linea que maneja para grabrlo en el tarifario
                                $producto = Productos::find($pernod["ref"]);
                                if($producto)
                                    $categoria = $producto->line;
                                else
                                    $categoria = "WHISKY";

                                $new = new Tarifario();
                                $new->ciudad = $request->ciudad;
                                $new->categoria = $categoria;
                                $new->copaxbotella = $pernod["multi"];
                                $new->FK_id_marca = $pernod["ref"];
                                $new->mes = $pernod["mes"]."-01";
                                $new->save();
                            }
                        }
                    }
                }
            }
            //FIN ALMACENAMIENTOS DE PREGUNTAS

            DB::commit();
            return Redirect::back()->with('edit', 'Tarifario editado exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Editar Tarifario, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Eliminar Tarifario
    public function delete($id){

        DB::beginTransaction();
        try{

            $tarifario = Tarifario::find($id);

            if(!$tarifario)
                return Redirect::back()->with('error', 'El Tarifario a eliminar no pudo ser encontrado');

            $tarifario->delete();

            DB::commit();
            return Redirect::back()->with('edit', 'Tarifario eliminado de manera exitosa en la ciudad y marca seleciconada');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al eliminar tarifario, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    /////////////////////////////////////////////////FIN CRUD
}
