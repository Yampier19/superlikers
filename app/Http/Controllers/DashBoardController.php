<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;

//Helper
use App\Helper\Notificacion;

class DashBoardController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    public function index()
    {
    //     $facturas = Factura::orderBy('id_factura', 'desc')->take(15)->get();
    //     $totalFacturas=Factura::count();
    //     $totalFacturasAceptadas=Factura::where('state', 'aceptado')->count();
    //     $totalFacturasRechazadas=Factura::where('state', 'pendiente')->count();
    //     //$invoices = Factura::orderBy('id_factura', 'desc')->paginate('15');
    //     $productos = FacturaProducto::sum("quantity");

    //     return view('portal.index', compact('facturas','productos', "totalFacturas","totalFacturasAceptadas","totalFacturasRechazadas"));
    // 
        ini_set('max_execution_time', '36000');
        $facturas = Factura::orderBy('id_factura', 'desc')->take(5)->get();
        return view('facturas.index', compact('facturas'));
    }

    //Recoger imagen del usuario
    public function getImage($filename = null){

        if($filename){
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('usericon.jpg');
            else
                $file = Storage::disk('public')->get('uploads/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('usericon.jpg');

        return new Response($file, 200);
    }
}
