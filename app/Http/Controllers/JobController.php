<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Artisan;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;
use App\Models\Jobs;
use App\Models\FailJobs;

//Helper
use App\Helper\Notificacion;
use App\Helper\FacturaIA;
use App\Helper\FacturaIA2;

//Jobs
use App\Jobs\ProcessGoogleVision;
use App\Jobs\ProcessOCRParam;
use App\Jobs\ProcessREOCRParam;
use App\Jobs\ProcessRegistrarVenta;

class JobController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    ////////////////////////////////////////////////VISTAS
    
    public function index(){  
        $jobs = Jobs::orderBy('created_at', 'desc')->get();
        $fail = FailJobs::orderBy('failed_at', 'desc')->get();

        return view('jobs.index', compact('jobs', 'fail'));
    }

    public function pending($filtro = null){

        if($filtro){
            $jobs = Jobs::where('queue', $filtro)->get();
        }
        else
            $jobs = Jobs::orderBy('created_at', 'desc')->get();
        
        return view('jobs.list_work', compact('jobs', 'filtro'));
    }

    public function failed($filtro = null){

        if($filtro){
            $fails = FailJobs::where('queue', $filtro)->get();
        }
        else
            $fails = FailJobs::orderBy('failed_at', 'desc')->get();
        
        return view('jobs.list_fail', compact('fails', 'filtro'));
    }

    /////////////////////////////////////////////////FIN VISTAS

    /////////////////////////////////////////////////CRUD
    
    //Recuperar un failed job
    public function restoreJob($id){
        
        try{
            $fail = FailJobs::find($id);
            
            //Almaceno la foto si hace falta
            if(!$fail->uuid)
                return Redirect::back()->with('param', 'Failed Job no pudo ser encontrado');

            Artisan::call('queue:retry '.$fail->uuid);

            return Redirect::back()->with('param', 'Failed Job recuperado exitosamente.');
        }catch(\Exception $e){
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error al recuperar Job: '.$e->getMessage());
        }
    }

    //Recuperar todos los failed jobs
    public function restoreAll($id){
        
        try{
            Artisan::call('queue:retry all');

            return Redirect::back()->with('param', 'Jobs recuperados exitosamente.');
        }catch(\Exception $e){
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error al recuperar todos los Job: '.$e->getMessage());
        }
    }

    //Eliminar Job
    public function deleteJob($id){
        
        DB::beginTransaction();
        try{

            $job = Jobs::find($id);

            if(!$job)
                return Redirect::back()->with('delete', 'El Job a eliminar no pudo ser encontrado');
            
            if($job->attempts > 0)
                return Redirect::back()->with('delete', 'El Job a eliminar no puede ser eliminado ya que se esta ejecutando en segundo plano');

            $job->delete();

            DB::commit();
            return Redirect::back()->with('edit', 'Job eliminado de manera exitosa');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al eliminar Job, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Eliminar Failed Job
    public function deleteFailed($id){
        
        DB::beginTransaction();
        try{

            $fail = FailJobs::find($id);

            if(!$fail)
                return Redirect::back()->with('delete', 'El Failed Job a eliminar no pudo ser encontrado');
            
            $fail->delete();

            DB::commit();
            return Redirect::back()->with('edit', 'Failed Job eliminado de manera exitosa');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al eliminar Job, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    /////////////////////////////////////////////////FIN CRUD
}
