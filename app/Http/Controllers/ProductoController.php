<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

//Modelos
use App\Models\Factura;
use App\Models\FacturaProducto;
use App\Models\FacturaExtra;
use App\Models\Participante;
use App\Models\Productos;
use App\Models\Restaurantes;
use App\Models\Tarifario;

//Helper
use App\Helper\Notificacion;

class ProductoController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado en otra marca',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    public function index()
    {  
        $productos = Productos::all();

        //Tomamos las lineas de productos
        $lineas = Productos::select('line')->groupBy('line')->get();

        return view('productos.index', compact('productos', 'lineas'));
    }

    public function editar($id_producto){
        $producto = Productos::find($id_producto);

        if(!$producto)
            return Redirect::back()->with('edit', 'El Producto seleccionado a editar no pudo ser encontrado');

        //Tomamos las lineas de productos
        $lineas = Productos::select('line')->groupBy('line')->get();

        return view('productos.edit', compact('producto', 'lineas'));
    }

    /////////////////////////////////////////////////CRUD

    //Store de un producto 
    public function store(Request $request){
        //return $request->all();
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'nombre' => 'required|string|max:255', 
                'referencia' => 'required|numeric|unique:productos,referencia', 
                'line' => 'required|string|max:255', 
                'estado' => 'required',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $new = new Productos();
            $new->marca = strtoupper($request->nombre);
            $new->referencia = strtoupper($request->referencia);
            $new->line = strtoupper($request->line);
            $new->activo = $request->estado;

            $new->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Producto ('.$new->marca.') agregado exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Agregar Producto, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Update de un producto
    public function update(Request $request, $id_producto){
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'referencia' => 'required|numeric|unique:productos,referencia,'.$id_producto.',id_productos', 
                'line' => 'required|string|max:255', 
                'estado' => 'required',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $producto = Productos::find($id_producto);
            if(!$producto)
                return Redirect::back()->with('error', 'El Producto seleccionado no pudo ser encontrado por lo que no pudo ser editado');
            
            //REVISAMOS SI YA EXISTE EL SKU

            //UPDATE DE DATOS ACTUALES
            $producto->referencia = $request->referencia;
            $producto->line = strtoupper($request->line);
            $producto->activo = $request->estado;

            $producto->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Producto editado exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Editar Producto, mensaje: '.$e->getMessage().' | Linea: '.$e->getCode());
        }
    }

    //Recoger imagen del usuario
    public function getImage($filename = null){

        if($filename){
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('usericon.jpg');
            else
                $file = Storage::disk('public')->get('uploads/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('usericon.jpg');

        return new Response($file, 200);
    }
}
