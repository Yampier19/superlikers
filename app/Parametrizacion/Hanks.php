<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos;
use App\Models\Tarifario;

class Hanks
{

    public $state = "aceptado";
    public $referencia = "";
    public $mesero = "";
    public $date = "";
    public $horas = "";
    public $month = "";
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new Hanks();
    }

    //Sacamos la referencia del folio
    public function referenciaFolio($content){
        $folio = null;
        
        if(str_contains($content, 'RDEN') || str_contains($content, 'ODEN')){

            if(str_contains($content, 'RDEN'))
                $ref_posicion = stripos($content, 'RDEN') + strlen('RDEN:');
            else
                $ref_posicion = stripos($content, 'ODEN') + strlen('ODEN:');
            
            $texto = substr($content, $ref_posicion);
            $new_text = $texto;
            $texto = trim(substr($texto, 0, stripos($texto,"\n")));

            if(preg_match('/\d+/', $texto, $matches)){
                $folio = $matches[0];
            }
            else{

                if(str_contains($texto, "."))
                    $texto = trim(substr($texto, stripos($texto,".") + 1));
                else{
                    $palabras = trim(preg_replace("/[^A-Za-z: ?]/", "", $texto));
                    $texto = str_replace($palabras, "", $texto);
                }

                $texto = trim(str_replace(".", "", $texto));

                if(empty($texto)){
                    $texto = trim(substr($new_text, stripos($texto,"\n") + 1));
                    $texto = trim(substr($texto, 0, stripos($texto,"\n")));
                    $palabras = trim(preg_replace("/[^A-Za-z: ?]/", "", $texto));
                    $texto = str_replace($palabras, "", $texto);
                }

                $minr = 7;
                while($minr > 2) {
                    $folio = trim(substr($texto, 0, $minr));
                    if(is_numeric($folio)){
                        break; 
                    }
                    $minr--;
                }
            }

            //Si no posee solo números quiere decir que hubo un fallo
            if (!is_numeric($folio))
                $this->state = "pendiente";
        }
        else
            $folio = "N/A";

        $this->referencia = $folio;

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){

        $mesero = null;

        if(str_contains($content, 'MESER')){
            $pos_mesero = stripos($content, 'MESER') + strlen('MESERO');
            $mesero = substr($content, $pos_mesero);
            //$mesero = substr($mesero, stripos($mesero, "\n") + 1);
            
            $revision = trim(substr($mesero, 0, stripos($mesero, "\n")));
            //Limpieza por si lo que lee no es un producto
            $revision = preg_replace('/[$]/', '', $revision);
            $revision = preg_replace('/[,]/', '', $revision);
            $revision = preg_replace('/[.]/', '', $revision);
            $revision = trim($revision);

            while(strlen($revision) <= 4 || is_numeric($revision) || str_contains($revision, "MESER") || str_contains($revision, "RDEN") || str_contains($revision, "MESA") || str_contains($revision, "ESTAC") || str_contains($revision, "PERSON") || substr_count($revision, "/") > 1){
                $mesero = trim(substr($mesero, stripos($mesero, "\n") + 1));
                
                $revision = trim(substr($mesero, 0, stripos($mesero, "\n")));

                //Limpieza por si lo que lee no es un producto
                $revision = preg_replace('/[$]/', '', $revision);
                $revision = preg_replace('/[,]/', '', $revision);
                $revision = preg_replace('/[.]/', '', $revision);
                $revision = trim($revision);

                if(empty($revision))
                    break;
            }

            //Limpiamos números o textos cortos intercalados
            $mesero = trim(preg_replace("/[^A-Za-z ?]/", "", $revision)); //Me quedo solo con el texto
        }
        else
            $mesero = "N/A";

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";
        
        $this->mesero = $mesero;

        return $mesero;
    }

    //Sacamos la fecha con hora
    public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:i:s');

        //Segmento donde esta la linea de la fecha
            $date_texto = $content;
            $fecha_texto = $date_texto;
            $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

            while(substr_count($revision, "/") < 2){
                $fecha_texto = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);
                $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

                if(empty($revision))
                    break;
            }
            
            //aqui abajo ya tengo la linea de la fecha ejemplo: d/m/Y
            $this->date = $revision;
       
        //Sacamos la fecha
            $texto = trim(preg_replace("/[^A-Za-z: ?]/", "", $revision));
            $palabras = explode(" ", $texto);

            foreach($palabras as $palabra)
                $revision = trim(str_replace($palabra, "", $revision));

            if(str_contains($revision, " "))
                $revision = substr($revision, 0, stripos($revision, " "));
            
            $fecha = str_replace("/", "-", $revision);
            $fecha = trim(preg_replace("/[^0-9- ?]/", "", $fecha));
            $fecha = str_replace("-", "/", $fecha);
            
        //Sacamos la hora de la misma
            $horas = trim($date_texto);
            $revision = trim(substr($horas, 0, stripos($horas, "\n")));
            
            while(substr_count($revision, ":") < 2){
                $date_texto = substr($date_texto, stripos($date_texto, "\n") + 1);
                $revision = trim(substr($date_texto, 0, stripos($date_texto, "\n")));

                if(empty($revision))
                    break;
            }
            
            $this->horas = $revision;

            if(str_contains($revision, $fecha)){
                $revision = substr($revision, stripos($revision, $fecha));
                $revision = trim(str_replace($fecha, "", $revision));
            }
                
            $horas = $revision;
            
            if(str_contains($horas, "PM") || str_contains($horas, "P.M") || str_contains($horas, "P. M")){
                if(str_contains($horas, " "))
                    $horas = substr($horas, 0, stripos($horas, " "));

                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas));
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour != "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i:s");
                    }
                    catch(\Exception $e){}
                }
            }
            else{
                if(str_contains($horas, " "))
                    $horas = substr($horas, 0, stripos($horas, " "));

                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas)); 
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour == "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i:s");
                    }
                    catch(\Exception $e){}
                }
            }

            //Revisamos la hora para que no tire fallo con la fecha
            $check_hora = DateTime::createFromFormat('H:i:s', $horas);
            if($check_hora && $check_hora->format('H:i:s') == $horas)
                $horas = date("H:i:s", strtotime($horas));
            else
                $horas = date("H:i:s");
            
        //Formateamos la fecha
            $minf = 15;
            $fecha_texto = $fecha;
            while($minf > 6) {
                $fecha = substr($fecha_texto, 0, $minf);

                $fecha_check = str_replace('/', '', $fecha);
                if(is_numeric($fecha_check)){
                    break; 
                }
                $minf--;
            }

            //Correccion de dia y mes
            $dia = substr($fecha, 0, stripos($fecha, "/"));
            strlen($dia) < 2 ? $dia = "0".$dia : "";

            $mes = substr($fecha, stripos($fecha, "/") + 1, strrpos($fecha, "/") - stripos($fecha, "/") - 1);
            strlen($mes) < 2 ? $mes = "0".$mes : "";

            $fecha = $dia."/".$mes.substr($fecha, strrpos($fecha, "/"));

            $fecha = $fecha." ".$horas;
            
        //Corrección y validación de la fecha
            $fecha = str_replace('/', '-', $fecha);
            $chech_date = DateTime::createFromFormat('d-m-Y H:i:s', $fecha);
            if($chech_date && $chech_date->format('d-m-Y H:i:s') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

            //valido si la fecha es superorio a la actual
            $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
            $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
            //COMPARO LAS FECHAS
            if ($fecha > $DateAndTime){
                //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
                $fecha = $DateAndTime;
                $this->month = null;
                $this->state = "pendiente";
            }
            $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
            
        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        //marca pues graba en teoria el nombre real que maneja en la factura
        //linea es que tipo de licor es
        //formato es si lo maneja como botella o como copa

        switch ($marca) {
            case "ABSOLUT BLUE": 
                $referencia = array(
                    array(
                        "marca" => "MARTINI ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "TINI ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ALFONSO XIII",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "APPLE TINI",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COSMOPOLITAN",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "POP MARTINI",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BLUE HAWAIIAN",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VODKA FRESH",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MAZAPAN T",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MANGOTINI\n",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VON ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VCD ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VOD ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "YOD ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "V0D ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VOD ABSOLUT. AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VDO ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "D ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "D ABSOLUT. AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT . AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "1 . ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "1 ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "2 ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "3 ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "4 ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BLOODY MARY",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BLOODY MAR",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CLAMATO PREP",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "HH2X1 ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VODKA PINK",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT PUNCH",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOT ABOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT ABSOLUT. AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT . ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT. ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT EXTRAKT": 
                $referencia = array(
                    array(
                        "marca" => "FOREST CRUSH",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "XTRAKT APPLE",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "PASSION EXTRAKT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "BEEFEATER DRY": 
                $referencia = array(
                    array(
                        "marca" => "GIN BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MARTINI BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "HH2X7 BEEFEATER-HH",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "HH2X1 BEEFEATER-HH",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MART BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "GIN FRUTOS ROJOS",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "TORONJA AND GIN",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "NEGRONY",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "NEGRONI",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CATRINA",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CATRIN",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MANGO COLADO",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "GIN TONIC",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "RAMOS GIN F",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOT BEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT. BEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER PINK": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER BLACKBERRY": 
                $referencia = array(
                    array(
                        "marca" => "GIN BEEFEATER BLACKBER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 TEQUILA": 
                $referencia = array(
                    array(
                        "marca" => "BOT. CHIVAS 13",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "CHIVAS 13 TEQ",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS 13 T",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "WHISKY TROPICAL",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOURBON CINNAMON",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOT CHIVAS. EXTRA",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT CHIVAS EXTRA",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "T CHIVAS. E",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "T CHIVAS E",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "CHIVAS 13\n",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 SHERRY": 
                $referencia = array(
                    array(
                        "marca" => "CHIVAS REGAL SHERRY",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS REGEL SHERRY",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS REGAL S",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS REGEL S",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS XV": 
                $referencia = array(
                    array(
                        "marca" => "CHIVAS XV PUNCH",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS X PUNCH",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS XY PUNCH",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS XV P",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS XY P",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS X P",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS X",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "THE GLENLIVET FOUNDERS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "WHI GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "WHT GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOT GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "OT GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT .GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT. GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "HAVANA 7": 
                $referencia = array(
                    array(
                        "marca" => "RON HAVANA 7 AÑOS",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "MARTELL VSOP": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                );
                break;
            case "BALLATINE'S FINEST": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ALTOS PLATA": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "TEQ ALTOS BCO",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "Q ALTOS BCO",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "TO ALTOS BICO",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => " ALTOS B",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "THE GLENLIVET 12": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 18": 
                $referencia = array(
                    array(
                        "marca" => "BOT WHI CHIVAS 18",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "T WHI CHIVAS 18",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT CHIVAS 18",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT. CHIVAS 18",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "CHIVAS . 18",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS .18",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS. 18",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "MARTELL BS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT WATERMELON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "JAMESON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
            break;
        }

        return $referencia;
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();
        
        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto
            
            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);
            
            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {
                    
                    $cantidad = 1;
                    $contenido = $content;

                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto["marca"];
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO
                    
                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["nombre"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO
                    
                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){

                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);

                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 7);

                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 7);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }

                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);

                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                if(empty($linea_marca))
                                    break;

                                $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            };

                        //REVISAMOS QUE NO SEA UN NOMBRE DE MARCA CON >
                            $revisar = substr($linea_marca, 0, stripos($linea_marca, "\n"));
                            if(str_contains($revisar, ">"))
                                break;

                        //CANTIDAD DEL PRODUCTO
                            if($cantidad > 1){
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $new_content);
                            }
                            else
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $content);
                        
                        //PRECIO DEL PRODUCTO
                            if($cantidad > 1){
                                $precio = $this->productoPrecio($linea_marca, $content, $new_content);
                            }
                            else
                                $precio = $this->productoPrecio($linea_marca, $content, $content);
                            
                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => substr($linea_marca, 0, stripos($linea_marca, "\n")),//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );
                        array_push($check_product, $data);
                        array_push($return_productos, $data);
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $formato, $bdproducto, $ciudad, $content, $contenido){
        $cantidades = 0;
        $minc = 6;
        $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));

        $check_line = substr($linea_cantidad, 0, 4);

        //Si la cantidad no esta en la misma linea debemos ubicarlo
        if(!preg_match('/\d+/', $check_line, $matches)){
            $fila = 0;
            $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto
            
            if(str_contains($contenido, 'PERSO')){
                $ref_inicio = 'PERSO';
            }
            elseif(str_contains($contenido, 'MESA')){
                $ref_inicio = 'MESA';
            }
            elseif(str_contains($contenido, 'RDEN')){
                $ref_inicio = 'RDEN';
            }
            else
                $ref_inicio = 'ESTACI';
            
            //Evaluamos la posición del producto para luego sacar la del precio
                //texto a partir del producto hacia atras
                if(!str_contains($contenido, $producto))
                    $producto = substr($producto, 0, strrpos($producto, " "));

                $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                
                for($j = 0; $j < 60; $j++){
                    //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                    $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                    
                    //Limpieza por si lo que lee no es un producto
                    $revision = preg_replace('/[$]/', '', $revision);
                    $revision = preg_replace('/[,]/', '', $revision);
                    $revision = preg_replace('/[.]/', '', $revision);
                    $revision = preg_replace('/[@]/', '', $revision);
                    $revision = trim($revision);

                    //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                    while(strlen($revision) <= 5 || is_numeric($revision) || str_contains($revision, $ref_inicio) || str_contains($revision, "ESTACI") || str_contains($revision, "RDEN:") || str_contains($revision, "THIEMAN") || str_contains($revision, "(415) ") || str_contains($revision, "ITACO") || str_contains($revision, "PERSO") || str_contains($revision, "PER SOF") || str_contains($revision, "MESA") || str_contains($revision, "> NO CHA") || str_contains($revision, ") JUGO N") || str_contains($revision, "GOUDA") || str_contains($revision, "JITOMATE") || str_contains($revision, "MIX: CON") || str_contains($revision, "TIPO: ") || str_contains($revision, "SALSA: ") || str_contains($revision, "> ABIER") || str_contains($revision, "> PURE") || str_contains($revision, "> FILE") || str_contains($revision, "MESER") || str_contains($revision, "SAN MIGU") || str_contains($revision, "> VERDE M") || str_contains($revision, "CER VIC") || str_contains($revision, "CON SAL") || str_contains($revision, "MOJ) RE") || str_contains($revision, ") AGUA M") || str_contains($revision, ") ÁGUA M") || str_contains($revision, "VOD UP") || str_contains($revision, ") COCA ZERO") || str_contains($revision, ") SERV") || str_contains($revision, "> CLAMA") || str_contains($revision, "> CITRI") || str_contains($revision, "PUEST") /* || str_contains($revision, "GINGER") */ || str_contains($revision, "> CAZAD") || str_contains($revision, "DERECH") || str_contains($revision, "DIVORC") || str_contains($revision, "COCA Y") || str_contains($revision, "S SAL") || str_contains($revision, "ADERE")/*  || str_contains($revision, "CEBOLLA") */ || str_contains($revision, "TERM") || str_contains($revision, "ROCAS") || str_contains($revision, "> BEEF") || str_contains($revision, "MESA") /* || str_contains($revision, ") PAL") */ || str_contains($revision, "HIELO") || str_contains($revision, "S PLATO") || (str_contains($revision, "COCA L") && $revision != "1 COCA LIGHT" ) || str_contains($revision, "> FRU") || str_contains($revision, "HORA") || str_contains($revision, "MARACU") || str_contains($revision, "SABOR") || (str_contains($revision, "GUAR") && !str_contains($revision, "GUACAM")) || str_contains($revision, "> Y MANZANA") || str_contains($revision, "QUESO:") || str_contains($revision, ") ARIBA") || str_contains($revision, ") COCA") || str_contains($revision, "MART) S") || str_contains($revision, "> KINA") || str_contains($revision, "> EN ") || str_contains($revision, ") JUGO ") || str_contains($revision, "> NATUR") || str_contains($revision, "> ESPI") || str_contains($revision, "> GIN") || str_contains($revision, "> TON") || str_contains($revision, "> MIN") || str_contains($revision, "> YA") || str_contains($revision, "BIEN DORADA") || str_contains($revision, ") AGU") || str_contains($revision, "> MRTINI") || str_contains($revision, "RON: BA") || str_contains($revision, "RG) FR") || str_contains($revision, "> VASO") || str_contains($revision, "> NEG") || str_contains($revision, "EQ) BAN") || str_contains($revision, "P/SODA") || str_contains($revision, "VIP ") || str_contains($revision, "> PO") || str_contains($revision, "> BI") || str_contains($revision, "> LE") || str_contains($revision, "> CHE") || str_contains($revision, "> SHE") || str_contains($revision, "> TON") || str_contains($revision, "> 3 CU") || str_contains($revision, "> MAN") || str_contains($revision, ">") || str_contains($revision, "EQ) P") || str_contains($revision, ") SPRI") || str_contains($revision, "CER PACI") || str_contains($revision, "REGALO D") || str_contains($revision, "999)") || str_contains($revision, "HUV") || str_contains($revision, ") MAN") || str_contains($revision, "R) CHE") /* || str_contains($revision, "CER MOD") */ || $revision == "TONIC FRUTOS ROJOS" || $revision == "CER CORONA" || str_contains($revision, "RON: M") || str_contains($revision, " AZUCAR") || str_contains($revision, "RT) E") || str_contains($revision, "ER) MI") || str_contains($revision, "RT) RE") || str_contains($revision, ") PUE") || str_contains($revision, "ASIENTO") || str_contains($revision, "ER) CI") || str_contains($revision, ") GIN") || str_contains($revision, "(MOJ") || str_contains($revision, "(.MOJ") || str_contains($revision, "TEQ UP:") || $revision == "NO CEBOLLA"){
                                  
                        if(str_contains($revision, $ref_inicio))
                            break;

                        if(str_contains($revision, "1 +"))
                            break;

                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);

                        if(empty($revision) && substr_count($all_text, "\n") < 1)
                            break;
                    }

                    //Revision de que venga V.TRIP y arreglar el espaciado
                    /* if(str_contains($revision, "AGUA")){
                        $checking = substr($all_text, 0, strrpos($all_text,"\n"));
                        $revision = substr($checking, strrpos($checking,"\n") + 1);

                        if(str_contains($revision, "V.TRIP")){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        }
                    } */

                    //Validamos que no contenga la palabra clave de TOTAL ya que 
                    //a partir de ahí empiezan los productos
                    if(!str_contains($revision, $ref_inicio)){
                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        
                        //Otra limpieza por si hay un salto de linea intercalado
                        if(str_contains(substr($all_text, -2), "\n"))
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                        $fila++;
                    }
                    else
                        break;
                }
            //Fin evaluar la fila del producto
            
            //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                //Generalmente a partir de donde sale la REF_FOLIO es que estan los valores de los precios
                //en estas facturas y tomamos el texto a partir de ahí hasta el final
                //$all_text = substr($content, stripos($content, $ref_inicio));
                $all_text = substr($content, strrpos($content, $ref_inicio));
                $all_text = substr($all_text, stripos($all_text, "\n") + 1);
                //$all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                
                for($j = 1; $j < 60; $j++){
                    //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                    //if($j > 1)
                        $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                    
                    //Revisamos la linea
                    $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                    if(str_contains($revision, " ")){
                        $revision = trim(substr($revision, 0, stripos($revision, " ")));
                    }

                    $revision = str_replace('$', 'AAA', $revision);
                    $revision = str_replace(',', '', $revision);
                    $revision = str_replace('|', '', $revision);

                    //Convalidamos que no hayan saltos de linea a travesados
                    while(str_contains(substr($revision, 0, 2), "\n") || !is_numeric($revision) || strlen($revision) >= 3){
                        /* if(str_contains($revision, " "))
                            $all_text = trim(substr($all_text, stripos($all_text, " ")));
                        else
                             */$all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                        
                        //Re-Revisamos la linea
                        $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                        if(str_contains($revision, " ")){
                            $revision = trim(substr($revision, 0, stripos($revision, " ")));
                        }

                        $revision = str_replace('$', 'AAA', $revision);
                        $revision = str_replace(',', '', $revision);
                        $revision = str_replace('|', '', $revision);

                        if(empty($all_text))
                            break;
                    }

                    //Evaluamos si ya estamos en la misma linea para culminar
                    if($fila == $j){
                        break;
                    }
                }
            //Fin evaluación posición del precio
            
            $linea_cantidad = substr($all_text, 0, stripos($all_text,"\n"));
        }

        while($minc > 0) {
            $cantidades = trim(substr($linea_cantidad, 0, $minc));
            if(is_numeric($cantidades)){
                break; 
            }
            $minc--;
        }

        if(!is_numeric($cantidades)){ 
            $cantidades = 1;
            $this->state = "pendiente";
        }

        if($cantidades >= 50){
            $this->state = "pendiente";
        }

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();
            
            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");
                        
                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01"); 
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }
                        
                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes); 

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }

        return $cantidades;
    }

    //Proceso de sacar precio del Producto
    public function productoPrecio($linea_marca, $content, $contenido){
        //Revisemos si el precio esta en la misma linea del producto
        $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
        $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
        $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea_producto;
        //Arriba ya tenemos la linea del producto con su precio si hipoteticamente lo tiene
        
        //Arreglo y toma del espacio donde aparece el precio digitado
            $check_line = preg_replace('/[,]/', '1', $linea_producto);
            $check_line = preg_replace('/[.]/', '', $check_line);
            
            //Reviso que exista un secuencia de 4 números consecutivos d{4,}
            preg_match('!\d{4,}!', $check_line, $matches);
            $inline = true;
            
            //Revisamos si dos lineas por debajo de $linea_producto hay mas precios
                $ultimo_pro = false;
                //Linea 1
                $linea1 = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                $linea1 = substr($linea1, 0, stripos($linea1, "\n"));

                //Linea 2
                $linea2 = substr($linea_marca, stripos($linea_marca, $linea1) + strlen($linea1) + 1);
                $linea2 = substr($linea2, 0, stripos($linea2, "\n"));

                //Ultimas dos lineas
                $linea_ultimo = $linea1.$linea2;
                $linea_ultimo = trim(str_replace(".", "", $linea_ultimo));
                $linea_ultimo = trim(str_replace(",", "", $linea_ultimo));
                
                if(is_numeric($linea_ultimo))
                    $ultimo_pro = true;
                
            //$matches[0] -> hubo coincidencia | is_numeric(que esa coincidencia sea numerica)
            //$matches[0] >= 4 que sea mayor a 4 digitos
            if(isset($matches[0]) && is_numeric($matches[0]) && strlen(trim($matches[0])) >= 4 && $matches[0] != "0500" && !str_contains($matches[0], "1111") && $matches[0] != "1800" && !$ultimo_pro){
                //TIENE EL PRECIO EN LA MISMA LINEA
                if(str_contains($linea_producto, "$")){
                    $linea_precio = substr($linea_producto, stripos($linea_producto,"$") + 1);
                    $inline = false;
                }
                else
                    $linea_precio = preg_replace('/[,]/', '', $linea_producto);
            }
            else{
                //NO TIENE EL PRECIO EN LA MISMA LINEA POR LO QUE DEBEMOS UBICARLO
                $inline = false;
                $fila = 0;
                $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto

                if(str_contains($contenido, 'PERSO')){
                    $ref_inicio = 'PERSO';
                }
                elseif(str_contains($contenido, 'MESA')){
                    $ref_inicio = 'MESA';
                }
                elseif(str_contains($contenido, 'RDEN')){
                    $ref_inicio = 'RDEN';
                }
                else
                    $ref_inicio = 'ESTACI';

                //Evaluamos la posición del producto para luego sacar la del precio
                    //texto a partir del producto hacia atras
                    //Arreglo de $producto
                    if(!str_contains($contenido, $producto)){
                        if(str_contains($producto, ">"))
                            $producto = substr($linea_marca, 0 , stripos($linea_marca, ">"));
                        else
                            $producto = substr($producto, 0, strlen($producto) - 5);
                    }
                    
                    $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                    
                    for($j = 0; $j < 60; $j++){
                        //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        
                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);
    
                        //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                        while(strlen($revision) <= 5 || is_numeric($revision) || str_contains($revision, $ref_inicio) || str_contains($revision, "ESTACI") || str_contains($revision, "RDEN:") || str_contains($revision, "THIEMAN") || str_contains($revision, "(415) ") || str_contains($revision, "ITACO") || str_contains($revision, "PERSO") || str_contains($revision, "PER SOF") || str_contains($revision, "MESA") || str_contains($revision, "> NO CHA") || str_contains($revision, ") JUGO N") || str_contains($revision, "GOUDA") || str_contains($revision, "JITOMATE") || str_contains($revision, "MIX: CON") || str_contains($revision, "TIPO: ") || str_contains($revision, "SALSA: ") || str_contains($revision, "> ABIER") || str_contains($revision, "> PURE") || str_contains($revision, "> FILE") || str_contains($revision, "MESER") || str_contains($revision, "SAN MIGU") || str_contains($revision, "> VERDE M") || str_contains($revision, "CER VIC") || str_contains($revision, "CON SAL") || str_contains($revision, "MOJ) RE") || str_contains($revision, ") AGUA M") || str_contains($revision, ") ÁGUA M") || str_contains($revision, "VOD UP") || str_contains($revision, ") COCA ZERO") || str_contains($revision, ") SERV") || str_contains($revision, "> CLAMA") || str_contains($revision, "> CITRI") || str_contains($revision, "PUEST") || str_contains($revision, "> CAZAD") || str_contains($revision, "DERECH") || str_contains($revision, "DIVORC") || str_contains($revision, "COCA Y") || str_contains($revision, "S SAL") || str_contains($revision, "ADERE")/*  || str_contains($revision, "CEBOLLA") */ || str_contains($revision, "TERM") || str_contains($revision, "ROCAS") || str_contains($revision, "> BEEF") || str_contains($revision, "MESA") /* || str_contains($revision, ") PAL") */ || str_contains($revision, "HIELO") || str_contains($revision, "S PLATO") || (str_contains($revision, "COCA L") && $revision != "1 COCA LIGHT" ) || str_contains($revision, "> FRU") || str_contains($revision, "HORA") || str_contains($revision, "MARACU") || str_contains($revision, ") SABOR") || (str_contains($revision, "GUAR") && !str_contains($revision, "GUACAM")) || str_contains($revision, "> Y MANZANA") || str_contains($revision, "QUESO:") || str_contains($revision, ") ARIBA") || str_contains($revision, ") COCA") || str_contains($revision, "MART) S") || str_contains($revision, "> KINA") || str_contains($revision, "> EN ") || str_contains($revision, ") JUGO ") || str_contains($revision, "> NATUR") || str_contains($revision, "> GIN") || str_contains($revision, "> ESPI") || str_contains($revision, "> TON") || str_contains($revision, "> MIN") || str_contains($revision, "> YA") || str_contains($revision, "BIEN DORADA") || str_contains($revision, "> MRTINI") || str_contains($revision, ") AGU") || str_contains($revision, "RON: BA") || str_contains($revision, "RG) FR") || str_contains($revision, "> VASO") || str_contains($revision, "> NEG") || str_contains($revision, "EQ) BAN") || str_contains($revision, "P/SODA") || str_contains($revision, "VIP ") || str_contains($revision, "> PO") || str_contains($revision, "> BI") || str_contains($revision, "> LE") || str_contains($revision, "> CHE") || str_contains($revision, "> SHE") || str_contains($revision, "> TON") || str_contains($revision, "> 3 CU") || str_contains($revision, "> MAN") || str_contains($revision, ">") || str_contains($revision, "EQ) P") || str_contains($revision, ") SPRI") || str_contains($revision, "CER PACI") || str_contains($revision, "REGALO D") || str_contains($revision, "999)") || str_contains($revision, "HUV") || str_contains($revision, ") MAN") || str_contains($revision, "R) CHE") /* || str_contains($revision, "CER MOD") */ || $revision == "TONIC FRUTOS ROJOS" || $revision == "CER CORONA" || str_contains($revision, "RON: M") || str_contains($revision, " AZUCAR") || str_contains($revision, "RT) E") || str_contains($revision, "ER) MI") || str_contains($revision, "RT) RE") || str_contains($revision, ") PUE") || str_contains($revision, "ASIENTO") || str_contains($revision, "ER) CI") || str_contains($revision, ") GIN") || str_contains($revision, "RI) TAM") || str_contains($revision, "(MOJ") || str_contains($revision, "(.MOJ") || str_contains($revision, "TEQ UP:") || $revision == "NO CEBOLLA"){
                     
                            if(str_contains($revision, $ref_inicio))
                                break;

                            if(str_contains($revision, "1 +"))
                                break;
    
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);
    
                            //Limpieza por si lo que lee no es un producto
                            $revision = preg_replace('/[$]/', '', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[.]/', '', $revision);
                            $revision = preg_replace('/[@]/', '', $revision);
                            $revision = trim($revision);
    
                            if(empty($revision) && substr_count($all_text, "\n") < 1)
                                break;
                        }
                        
                        //Revision de que venga V.TRIP y arreglar el espaciado
                        /* if(str_contains($revision, "AGUA")){
                            $checking = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($checking, strrpos($checking,"\n") + 1);
    
                            if(str_contains($revision, "V.TRIP")){
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                                $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                            }
                        } */
                        
                        //echo $revision."\n";
                        //Validamos que no contenga la palabra clave de TOTAL ya que 
                        //a partir de ahí empiezan los productos
                        if(!str_contains($revision, $ref_inicio)){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            
                            //Otra limpieza por si hay un salto de linea intercalado
                            if(str_contains(substr($all_text, -2), "\n"))
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
    
                            $fila++;
                        }
                        else
                            break;
                    }
                    //echo "\n\n";
                    //return $fila;
                //Fin evaluar la fila del producto

                //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                    //Generalmente a partir de IMPORTE es que estan los valores de los precios
                    //en estas facturas y tomamos el texto a partir de ahí hasta el final

                    $all_text = substr($content, strrpos($content, $ref_inicio));
                    $all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                    //return $all_text;

                    for($j = 1; $j < 60; $j++){
                        //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                        if($j > 1)
                            $all_text = substr($all_text, stripos($all_text, "\n") + 1);

                        //Revisamos la linea
                        $revision = substr($all_text, 0, stripos($all_text, "\n"));
                        //si tiene +dos puntos consecutivos
                        if(str_contains($revision,".."))
                            continue;

                        if(str_contains($revision, " ")){
                            if(substr_count($revision, " ") > 1)
                                $revision = trim(substr($revision, 0, stripos($revision, " ")));
                            else{
                                $revision = trim(substr($revision, stripos($revision, " ")));
                            }
                        }

                        if(substr_count($revision, ".") > 1)
                            $revision = preg_replace('/[.]/', '111', $revision);

                        $revision = preg_replace('/[.]/', '111111', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[$]/', '111111', $revision);

                        //Convalidamos que no hayan saltos de linea a travesados
                        while(str_contains(trim(substr($revision, 0, 3)), "\n") || !is_numeric($revision) || strlen($revision) <= 7){
                            if(str_contains($revision, " "))
                                $all_text = trim(substr($all_text, stripos($all_text, " ")));
                            else
                                $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));

                            //Re-Revisamos la linea
                            $revision = substr($all_text, 0, stripos($all_text, "\n"));
                            //si tiene +dos puntos consecutivos
                            if(str_contains($revision,".."))
                                continue;

                            if(str_contains($revision, " ")){
                                if(substr_count($revision, " ") > 1)
                                    $revision = trim(substr($revision, 0, stripos($revision, " ")));
                                else{
                                    $revision = trim(substr($revision, stripos($revision, " ")));
                                }
                            }

                            if(substr_count($revision, ".") > 1)
                                $revision = preg_replace('/[.]/', '111', $revision);

                            $revision = preg_replace('/[.]/', '111111', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[$]/', '111111', $revision);
                            
                            if(empty($all_text))
                                break;
                        }
                        //echo $revision."\n";
                        //Evaluamos si ya estamos en la misma linea para culminar
                        if($fila == $j){
                            break;
                        }
                    }
                //Fin evaluación posición del precio
                //return $all_text;
                $linea_precio = trim(substr($all_text, 0, stripos($all_text,"\n")));
                if(str_contains($linea_precio, "$"))
                    $linea_precio = trim(substr($linea_precio, stripos($linea_precio,"$") + 1));
            }
        //Fin arreglo y toma del espacio donde aparece el precio digitado
        
        $precio = 0;
        $minp = 14;
        
        if($inline){
            $out = true;
            while($out){
                $revision = trim(substr($linea_precio, 0, stripos($linea_precio, " ")));
                $linea_precio = trim(substr($linea_precio, stripos($linea_precio, " ")));
                
                if(str_contains($revision, ".")){
                    if(preg_match('/^([1-9]\d*|0)(\.\d+)?$/', $revision, $matches)){
                        $linea_precio = $matches[0];
                        $out = false;
                    }
                }
                
                if(!strpos($linea_precio, ' '))
                    $out = false;
            }
        }
        
        while($minp >= 2) {
            $precio = substr($linea_precio, 0, $minp);
            $precio = preg_replace('/[,]/', '', $precio);
            $precio = preg_replace('/[(]/', '', $precio);
            $precio = preg_replace('/[)]/', '', $precio);

            if(is_numeric($precio)){
                break;
            }
            $minp--;
        }

        if(!is_numeric($precio)){ 
            $precio = 0;
            $this->state = "pendiente";
        }

        return $precio;
    }

    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = $content;
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);
        
        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            if(str_contains($productos, "ESTACI"))
                $productos = substr($productos, stripos($productos, 'ESTACI'));

            if(str_contains($productos, "RDEN"))
                $productos = substr($productos, stripos($productos, 'RDEN'));

            if(str_contains($productos, "MESA"))
                $productos = substr($productos, stripos($productos, 'MESA'));

            if(str_contains($productos, "PERSO"))
                $productos = substr($productos, stripos($productos, 'PERSO'));

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, "SUBTOTAL"))
                $productos = substr($productos, 0, strrpos($productos, 'SUBTOTAL'));

            if(str_contains($productos, "TOTAL"))
                $productos = substr($productos, 0, stripos($productos, 'TOTAL'));
        //Fin limpiado

        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(!is_numeric($revision)){
                
                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break; 
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){ 
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad
                
                //Eliminamos el precio si esta en la misma linea
                    $precio = 0;
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    //Elimina los precios intercalados
                    while(preg_match('!\d{4,}!', $revision, $matches)){
                        $precio = $matches[0];
                        $revision = trim(preg_replace('/['.$precio.']/', '', $revision));
                    }

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){ 
                        $revision = str_replace($precio, '', $revision);
                    }
                //Fin eliminido el precio
                
                $nombre = trim($revision);

                //Limpiamos texto basura
                if(strlen($nombre) <= 4)
                    $nombre = "";
            
                if(str_contains($nombre, "WWW"))
                    $nombre = "";

                if($nombre && !str_contains($nombre, "IMPORTE") && !str_contains($nombre, "> ") && !str_contains($nombre, ") ") && !str_contains($nombre, "()") && !str_contains($nombre, "ORDE") && !str_contains($nombre, "TOTAL") && !str_contains($nombre, "CUENTA") && !str_contains($nombre, "/"))
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){
        
        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);
        
        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);
        
        //Sacamos el mesero
        $values->mesero = $this->mesero($content);
        
        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos($content, $values->ciudad);
        
        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        $values->extras = $this->productosExtra($content);
        
        //Sacamos del vector de productos extras los productos pernod
        $values->extras = $this->limpiandoExtra($values->productos, $values->extras);
        
        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
