<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos;
use App\Models\Tarifario;

class BrunoQRO
{

    public $state = "aceptado";
    public $referencia = "";
    public $mesero = "";
    public $date = "";
    public $horas = "";
    public $month = "";
    public $cantidad = 1;
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new BrunoQRO();
    }

    //Sacamos la referencia del folio
    public function referenciaFolio($content){
        $folio = null;
        
        if(str_contains($content, "TIQUET")){
            $ref_posicion = stripos($content, 'TIQUET') + strlen('TIQUET');
            $texto = substr($content, $ref_posicion);
            $texto = substr($texto, stripos($texto,"/") + 1);
        }
        else{
            $ref_posicion = stripos($content, '001/') + strlen('001/');
            $texto = substr($content, $ref_posicion);
        }
        
        if(preg_match('/\d+/', $texto, $matches)){
            $folio = $matches[0];
        }
        else{

            if(str_contains($texto, ":"))
                $texto = trim(substr($texto, stripos($texto,":") + 1));
            elseif(str_contains($texto, "."))
                $texto = trim(substr($texto, stripos($texto,".") + 1));

            $texto = trim(str_replace(".", "", $texto));

            $minr = 7;
            while($minr > 2) {
                $folio = trim(substr($texto, 0, $minr));
                if(is_numeric($folio)){
                    break; 
                }
                $minr--;
            }
        }

        //Si no posee solo números quiere decir que hubo un fallo
        if (!is_numeric($folio))
            $this->state = "pendiente";

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){

        $mesero = null;

        if(str_contains($content, 'ATEND')){
            $pos_mesero = stripos($content, 'ATEND') + strlen('ATEND');
            $mesero = substr($content, $pos_mesero);
            $mesero = substr($mesero, 0, stripos($mesero, "\n"));
            
            if(str_contains($mesero, ":"))
                $mesero = trim(substr($mesero, stripos($mesero, ":") + 1));
            else
                $mesero = trim(substr($mesero, stripos($mesero, " ")));

            //Limpiamos números o textos cortos intercalados
            $mesero = trim(preg_replace("/[^A-Za-z ?]/", "", $mesero)); //Me quedo solo con el texto
        }
        else
            $mesero = "N/A";

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";

        return $mesero;
    }

    //Sacamos la fecha con hora
    public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:i:s');

        if(str_contains($content, "FECHA"))
            $referencia = "FECHA";
        elseif(str_contains($content, "FECIA"))
            $referencia = "FECIA";
        elseif(str_contains($content, "FECA"))
            $referencia = "FECA";
        else
            $referencia = "FEC";

        //Segmento donde esta la linea de la fecha
            $fecha_texto = substr($content, stripos($content, $referencia));
            $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

            while(substr_count($revision, "/") < 1){
                $fecha_texto = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);
                $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

                if(empty($revision))
                    break;
            }
            
            //aqui abajo ya tengo la linea de la fecha ejemplo: FECHA d/m/Y
            $this->date = $revision;
       
        //Sacamos la fecha
            $texto = trim(preg_replace("/[^A-Za-z:. ?]/", "", $revision));
            $revision = trim(str_replace($texto, "", $revision));
            
            if(str_contains($revision, " "))
                $revision = substr($revision, 0, stripos($revision, " "));

            $fecha = str_replace("/", "-", $revision);
            $fecha = trim(preg_replace("/[^0-9- ?]/", "", $fecha));
            $fecha = str_replace("-", "/", $fecha);

        //Sacamos la hora de la misma
            $horas = trim($fecha_texto);
            $revision = trim(substr($horas, 0, stripos($horas, "\n")));

            while(substr_count($revision, ":") < 2){
                $fecha_texto = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);
                $revision = trim(substr($fecha_texto, 0, stripos($fecha_texto, "\n")));

                if(empty($revision))
                    break;
            }

            if(str_contains($revision, $fecha))
                $revision = trim(str_replace($fecha, "", $revision));

            $horas = $revision;
            $this->horas = $horas;
            
            if(str_contains($horas, " "))
                $horas = trim(substr($horas, 0, stripos($horas, " ")));
                
            $horas = trim(preg_replace("/[^0-9: ?]/", "", $horas)); 

            //Revisamos la hora para que no tire fallo con la fecha
                $check_hora = DateTime::createFromFormat('H:i:s', $horas);
                if($check_hora && $check_hora->format('H:i:s') == $horas)
                    $horas = date("H:i:s", strtotime($horas));
                else
                    $horas = date("H:i:s");
            
        //Formateamos la fecha
            $fecha = $fecha." ".$horas;
            
        //Corrección y validación de la fecha
            $fecha = str_replace('/', '-', $fecha);
            $chech_date = DateTime::createFromFormat('d-m-Y H:i:s', $fecha);
            if($chech_date && $chech_date->format('d-m-Y H:i:s') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

            //valido si la fecha es superorio a la actual
            $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
            $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
            //COMPARO LAS FECHAS
            if ($fecha > $DateAndTime){
                //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
                $fecha = $DateAndTime;
                $this->month = null;
                $this->state = "pendiente";
            }
            $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
            
        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        //marca pues graba en teoria el nombre real que maneja en la factura
        //linea es que tipo de licor es
        //formato es si lo maneja como botella o como copa

        switch ($marca) {
            case "ABSOLUT BLUE": 
                $referencia = array(
                    array(
                        "marca" => "BOT. 1LT ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT 1LT ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "COPA ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "PA ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BOT, ABSOLUT A",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BOT. ABSOLUT A",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA 1 1/2 x1 ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT EXTRAKT": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "BEEFEATER DRY": 
                $referencia = array(
                    array(
                        "marca" => "COPA BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA 1 1/2X1 BEEFEAT",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA 1 1/2 x1 BEEFEAT",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MARTINI BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER PINK": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "BEEFEATER BLACKBERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BEEFEATER BLACK",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 TEQUILA": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA CHIVAS REGAL 13",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 SHERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "CHIVAS XV": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "THE GLENLIVET FOUNDERS": 
                $referencia = array(
                    array(
                        "marca" => "COPA GLENLIVET FOUND",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA GLENLIVET F",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "HAVANA 7": 
                $referencia = array(
                    array(
                        "marca" => "BOT.700ML HAVANA",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT. 700ML HAVANA",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "MARTELL VSOP": 
                $referencia = array(
                    array(
                        "marca" => "COPA MARTELL VSOP",
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOT. 700ML MARTELL V",
                        "linea" => "COÑAC",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT 700ML MARTELL V",
                        "linea" => "COÑAC",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                );
                break;
            case "BALLATINE'S FINEST": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "ALTOS PLATA": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "THE GLENLIVET 12": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 18": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "MARTELL BS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT WATERMELON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "JAMESON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
            break;
        }

        return $referencia;
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();
        
        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto
            
            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);
            
            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {
                    
                    $cantidad = 1;
                    $contenido = $content;

                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto["marca"];
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO
                    
                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["nombre"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO
                    
                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){

                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);

                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 7);

                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 7);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }

                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);

                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                if(empty($linea_marca))
                                    break;

                                $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            };

                        //CANTIDAD DEL PRODUCTO
                            if($cantidad > 1){
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $new_content);
                            }
                            else
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $content);
                        
                        //PRECIO DEL PRODUCTO
                            if($cantidad > 1){
                                $precio = $this->productoPrecio($linea_marca, $content, $new_content);
                            }
                            else
                                $precio = $this->productoPrecio($linea_marca, $content, $content);
                            
                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => substr($linea_marca, 0, stripos($linea_marca, "\n")),//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio*$this->cantidad,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );
                        array_push($check_product, $data);
                        array_push($return_productos, $data);
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $formato, $bdproducto, $ciudad, $content, $contenido){
        $cantidades = 0;
        $minc = 6;
        $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));

        $check_line = substr($linea_cantidad, 0, 4);

        //Si la cantidad no esta en la misma linea debemos ubicarlo
        if(!preg_match('/\d+/', $check_line, $matches)){
            $fila = 0;
            $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto
            
            if(str_contains($contenido, 'MPORT')){
                $ref_inicio = 'MPORT';
            }
            elseif(str_contains($contenido, 'SCRIPC')){
                $ref_inicio = 'SCRIPC';
            }
            elseif(str_contains($contenido, 'OMENSA')){
                $ref_inicio = 'OMENSA';
            }
            else
                $ref_inicio = 'PVP';
            
            //Evaluamos la posición del producto para luego sacar la del precio
                //texto a partir del producto hacia atras
                if(!str_contains($contenido, $producto))
                    $producto = substr($producto, 0, strrpos($producto, " "));

                $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                
                for($j = 0; $j < 60; $j++){
                    //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                    $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                    
                    //Limpieza por si lo que lee no es un producto
                    $revision = preg_replace('/[$]/', '', $revision);
                    $revision = preg_replace('/[,]/', '', $revision);
                    $revision = preg_replace('/[.]/', '', $revision);
                    $revision = preg_replace('/[@]/', '', $revision);
                    $revision = trim($revision);

                    //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                    while(strlen($revision) <= 5 || is_numeric($revision) || str_contains($revision, $ref_inicio) || str_contains($revision, "OMENSA") || str_contains($revision, "SCRIPC") || str_contains($revision, "MPORT") || $revision == "AGUA MINERAL" || str_contains($revision, "ALBERTO") || str_contains($revision, "TRES") || str_contains($revision, "SELLADO")){
                        
                        if(str_contains($revision, $ref_inicio))
                            break;

                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);

                        if(empty($revision))
                            break;
                    }

                    //Validamos que no contenga la palabra clave de TOTAL ya que 
                    //a partir de ahí empiezan los productos
                    if(!str_contains($revision, $ref_inicio)){
                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        
                        //Otra limpieza por si hay un salto de linea intercalado
                        if(str_contains(substr($all_text, -2), "\n"))
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                        $fila++;
                    }
                    else
                        break;
                }
            //Fin evaluar la fila del producto
            
            //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                //Generalmente a partir de donde sale la REF_FOLIO es que estan los valores de los precios
                //en estas facturas y tomamos el texto a partir de ahí hasta el final
                $all_text = substr($content, stripos($content, $ref_inicio));
                $all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                
                for($j = 1; $j < 60; $j++){
                    //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                    if($j > 1)
                        $all_text = substr($all_text, stripos($all_text, "\n") + 1);
                    
                    //Revisamos la linea
                    $revision = substr($all_text, 0, stripos($all_text, "\n"));
                    if(str_contains($revision, " ")){
                        if(substr_count($revision, " ") > 1)
                            $revision = trim(substr($revision, 0, stripos($revision, " ")));
                        else
                            $revision = trim(substr($revision, stripos($revision, " ")));
                    }

                    $revision = str_replace('$', 'AAA', $revision);
                    $revision = str_replace(',', '', $revision);
                    $revision = str_replace('|', '', $revision);

                    //Convalidamos que no hayan saltos de linea a travesados
                    while(str_contains(substr($revision, 0, 2), "\n") || !is_numeric($revision) || strlen($revision) >= 3){
                        if(str_contains($revision, " "))
                            $all_text = trim(substr($all_text, stripos($all_text, " ")));
                        else
                            $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                        
                        //Re-Revisamos la linea
                        $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                        if(str_contains($revision, " ")){
                            if(substr_count($revision, " ") > 1)
                                $revision = trim(substr($revision, 0, stripos($revision, " ")));
                            else{
                                $revision = trim(substr($revision, stripos($revision, " ")));
                            }
                        }

                        $revision = str_replace('$', 'AAA', $revision);
                        $revision = str_replace(',', '', $revision);
                        $revision = str_replace('|', '', $revision);

                        if(empty($all_text))
                            break;
                    }

                    //Evaluamos si ya estamos en la misma linea para culminar
                    if($fila == $j){
                        break;
                    }
                }
            //Fin evaluación posición del precio
            
            $linea_cantidad = substr($all_text, 0, stripos($all_text,"\n"));
        }

        while($minc > 0) {
            $cantidades = trim(substr($linea_cantidad, 0, $minc));
            if(is_numeric($cantidades)){
                break; 
            }
            $minc--;
        }

        if(!is_numeric($cantidades)){ 
            $cantidades = 1;
            $this->state = "pendiente";
        }

        if($cantidades >= 50){
            $this->state = "pendiente";
        }

        $this->cantidad = $cantidades;

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();
            
            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");
                        
                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01"); 
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }
                        
                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes); 

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }
        
        return $cantidades;
    }

    //Proceso de sacar precio del Producto
    public function productoPrecio($linea_marca, $content, $contenido){
        //Revisemos si el precio esta en la misma linea del producto
        $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
        $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
        $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea_producto;
        //Arriba ya tenemos la linea del producto con su precio si hipoteticamente lo tiene
        
        //Arreglo y toma del espacio donde aparece el precio digitado
            $check_line = preg_replace('/[,]/', '1', $linea_producto);
            $check_line = preg_replace('/[.]/', '', $check_line);
            
            //Reviso que exista un secuencia de 4 números consecutivos d{4,}
            preg_match('!\d{4,}!', $check_line, $matches);
            $inline = true;
            
            //$matches[0] -> hubo coincidencia | is_numeric(que esa coincidencia sea numerica)
            //$matches[0] >= 4 que sea mayor a 4 digitos
            if(isset($matches[0]) && is_numeric($matches[0]) && strlen(trim($matches[0])) >= 4 && $matches[0] != "0500"){
                //TIENE EL PRECIO EN LA MISMA LINEA
                if(str_contains($linea_marca, "$")){
                    $linea_precio = substr($linea_marca, stripos($linea_marca,"$") + 1);
                    $inline = false;
                }
                else
                    $linea_precio = preg_replace('/[,]/', '', $linea_producto);
            }
            else{
                //NO TIENE EL PRECIO EN LA MISMA LINEA POR LO QUE DEBEMOS UBICARLO
                $inline = false;
                $fila = 0;
                $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto

                if(str_contains($contenido, 'MPORT')){
                    $ref_inicio = 'MPORT';
                }
                elseif(str_contains($contenido, 'SCRIPC')){
                    $ref_inicio = 'SCRIPC';
                }
                elseif(str_contains($contenido, 'OMENSA')){
                    $ref_inicio = 'OMENSA';
                }
                else
                    $ref_inicio = 'PVP';

                //Evaluamos la posición del producto para luego sacar la del precio
                    //texto a partir del producto hacia atras
                    //Arreglo de $producto
                    if(!str_contains($contenido, $producto))
                        $producto = substr($producto, 0, strlen($producto) - 5);

                    $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                    
                    for($j = 0; $j < 60; $j++){
                        //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        
                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);
    
                        //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                        while(strlen($revision) <= 5 || is_numeric(str_replace(" ", "", $revision)) || str_contains($revision, $ref_inicio) || str_contains($revision, "OMENSA") || str_contains($revision, "SCRIPC") || str_contains($revision, "MPORT") || $revision == "AGUA MINERAL" || str_contains($revision, "ALBERTO") || str_contains($revision, "TRES") || str_contains($revision, "SELLADO") || str_contains($revision, "PAN CHIC")){
                            
                            if(str_contains($revision, $ref_inicio))
                                break;
    
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);
    
                            //Limpieza por si lo que lee no es un producto
                            $revision = preg_replace('/[$]/', '', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[.]/', '', $revision);
                            $revision = preg_replace('/[@]/', '', $revision);
                            $revision = trim($revision);
    
                            if(empty($revision))
                                break;
                        }
                        
                        //echo $revision."\n";
                        //Validamos que no contenga la palabra clave de TOTAL ya que 
                        //a partir de ahí empiezan los productos
                        if(!str_contains($revision, $ref_inicio)){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            
                            //Otra limpieza por si hay un salto de linea intercalado
                            if(str_contains(substr($all_text, -2), "\n"))
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
    
                            $fila++;
                        }
                        else
                            break;
                    }
                //Fin evaluar la fila del producto

                //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                    //Generalmente a partir de IMPORTE es que estan los valores de los precios
                    //en estas facturas y tomamos el texto a partir de ahí hasta el final

                    $all_text = substr($content, strrpos($content, $ref_inicio));
                    $all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                    
                    for($j = 1; $j < 60; $j++){
                        //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                        if($j > 1)
                            $all_text = substr($all_text, stripos($all_text, "\n") + 1);

                        //Revisamos la linea
                        $revision = substr($all_text, 0, stripos($all_text, "\n"));
                        if(str_contains($revision, " ")){
                            if(substr_count($revision, " ") > 1)
                                $revision = trim(substr($revision, 0, stripos($revision, " ")));
                            else{
                                $revision = trim(substr($revision, stripos($revision, " ")));
                            }
                        }

                        $revision = preg_replace('/[.]/', '111', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[$]/', '', $revision);

                        //Convalidamos que no hayan saltos de linea a travesados
                        while(str_contains(trim(substr($revision, 0, 3)), "\n") || !is_numeric($revision) || strlen($revision) <= 5){
                            if(str_contains($revision, " "))
                                $all_text = trim(substr($all_text, stripos($all_text, " ")));
                            else
                                $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));

                            //Re-Revisamos la linea
                            $revision = substr($all_text, 0, stripos($all_text, "\n"));
                            if(str_contains($revision, " ")){
                                if(substr_count($revision, " ") > 1)
                                    $revision = trim(substr($revision, 0, stripos($revision, " ")));
                                else{
                                    $revision = trim(substr($revision, stripos($revision, " ")));
                                }
                            }
                            
                            $revision = preg_replace('/[.]/', '111', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[$]/', '', $revision);
                            
                            if(empty($all_text))
                                break;
                        }
                        
                        //Evaluamos si ya estamos en la misma linea para culminar
                        if($fila == $j){
                            break;
                        }
                    }
                //Fin evaluación posición del precio
                
                $linea_precio = trim(substr($all_text, 0, stripos($all_text,"\n")));
                if(str_contains($linea_precio, "$"))
                    $linea_precio = trim(substr($all_text, stripos($all_text,"$") + 1));
            }
        //Fin arreglo y toma del espacio donde aparece el precio digitado
        
        $precio = 0;
        $minp = 14;
        
        if($inline){
            $out = true;
            while($out){
                $revision = trim(substr($linea_precio, 0, stripos($linea_precio, " ")));
                $linea_precio = trim(substr($linea_precio, stripos($linea_precio, " ")));
                
                if(str_contains($revision, ".")){
                    if(preg_match('/^([1-9]\d*|0)(\.\d+)?$/', $revision, $matches)){
                        $linea_precio = $matches[0];
                        $out = false;
                    }
                }

                if(!strpos($linea_precio, ' '))
                    $out = false;
            }
        }

        while($minp >= 2) {
            $precio = substr($linea_precio, 0, $minp);
            $precio = preg_replace('/[,]/', '', $precio);
            if(is_numeric($precio)){
                break;
            }
            $minp--;
        }

        if(!is_numeric($precio)){ 
            $precio = 0;
            $this->state = "pendiente";
        }

        return $precio;
    }

    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = substr($content, stripos($content, $this->referencia));
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);
        
        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            if(str_contains($productos, "MESA"))
                $productos = substr($productos, stripos($productos, 'MESA'));

            if(str_contains($productos, "COMENSALES"))
                $productos = substr($productos, stripos($productos, 'COMENSALES'));

            if(str_contains($productos, "DESCRIPCI"))
                $productos = substr($productos, stripos($productos, 'DESCRIPCI'));

            if(str_contains($productos, "IMPORTE"))
                $productos = substr($productos, stripos($productos, 'IMPORTE'));

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, "TOTAL"))
                $productos = substr($productos, 0, strrpos($productos, 'TOTAL'));
        //Fin limpiado

        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(!is_numeric($revision)){
                
                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break; 
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){ 
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad
                
                //Eliminamos el precio si esta en la misma linea
                    $precio = 0;
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    //Elimina los precios intercalados
                    while(preg_match('!\d{4,}!', $revision, $matches)){
                        $precio = $matches[0];
                        $revision = trim(preg_replace('/['.$precio.']/', '', $revision));
                    }

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){ 
                        $revision = preg_replace('/['.$precio.']/', '', $revision);
                    }
                //Fin eliminido el precio
                
                $nombre = trim($revision);

                //Limpiamos texto basura
                if(strlen($nombre) <= 4)
                    $nombre = "";
            
                if(str_contains($nombre, "WWW"))
                    $nombre = "";

                if($nombre && !str_contains($nombre, "TOTAL"))
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){
        
        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);
        
        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);
        
        //Sacamos el mesero
        $values->mesero = $this->mesero($content);
        
        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos($content, $values->ciudad);
        
        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        $values->extras = $this->productosExtra($content);
        
        //Sacamos del vector de productos extras los productos pernod
        $values->extras = $this->limpiandoExtra($values->productos, $values->extras);
        
        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
