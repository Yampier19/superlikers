<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos;
use App\Models\Tarifario;

class TabernaLeon
{

    public $state = "aceptado";
    public $date = "";
    public $month = "";
    public $cantidad = 1;
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new TabernaLeon();
    }

    //Sacamos la referencia del folio
    public function referenciaFolio($content){
        $folio = null;

        if(str_contains($content, 'CHK'))
            $ref_posicion = stripos($content, 'CHK') + strlen('CHK');
        elseif(str_contains($content, 'SHK'))
            $ref_posicion = stripos($content, 'SHK') + strlen('SHK');
        elseif(str_contains($content, 'FHK'))
            $ref_posicion = stripos($content, 'FHK') + strlen('FHK');
        else
            $ref_posicion = stripos($content, 'BHK') + strlen('BHK');

        $texto = substr($content, $ref_posicion);
        $texto = trim(substr($texto, 0, stripos($texto,"\n")));

        if(preg_match('/\d+/', $texto, $matches)){
            $folio = $matches[0];
        }
        else{

            if(str_contains($texto, "."))
                $texto = trim(substr($texto, stripos($texto,".") + 1));
            else{
                $palabras = trim(preg_replace("/[^A-Za-z: ?]/", "", $texto));
                $texto = str_replace($palabras, "", $texto);
            }

            $texto = trim(str_replace(".", "", $texto));

            $minr = 7;
            while($minr > 2) {
                $folio = trim(substr($texto, 0, $minr));
                if(is_numeric($folio)){
                    break;
                }
                $minr--;
            }
        }

        //Si no posee solo números quiere decir que hubo un fallo
        if (!is_numeric($folio))
            $this->state = "pendiente";

        $this->referencia = $folio;

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){

        $mesero = null;

        //Me paro en un punto inicial para ir buscando a partir de ahí hacia abajo el mesero
        $pos_revision = stripos($content, '3986 EXPEDIDO');
        $revision = str_replace('(', '', substr($content, $pos_revision));
        $revision = str_replace('.', '', $revision);
        $revision = str_replace(',', '1', $revision);

        //Tomo la primera linea siguiente
        $revision = substr($revision, stripos($revision, "\n") + 1);

        //Tomo la primera palabra que puede ser la secuencia de 3 números que estoy buscando
        $check = substr($revision, 0, stripos($revision, " "));

        while(!preg_match('/^[0-9]{3}$/', $check)){

            if(is_numeric($check) && strlen($check) <= 2)
                break;

            //Tomo la primera linea siguiente
            $revision = substr($revision, stripos($revision, "\n") + 1);

            //recogo primera palabra que puede ser la secuencia de 3 números que estoy buscando y paso al while
            $check = substr($revision, 0, stripos($revision, " "));

            if(empty($check))
                break;
        }

        $mesero = substr($revision, 0, stripos($revision, "\n"));
        $mesero = substr($mesero, stripos($mesero, " "));

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";

        return trim($mesero);
    }

    //Sacamos la fecha con hora
    public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:m:s');

        //Segmento donde esta la linea de la fecha
        //$fecha_pos = stripos($content, $referencia) + strlen($referencia) + 1;
        $fecha_texto = substr($content, stripos($content, $referencia));
        $fecha_texto = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);

        //Correccion por si tiene el GST
        $revision = trim(substr($fecha_texto, 0 ,stripos($fecha_texto, "\n")));
        if(str_contains($revision, "GST")){
            $check = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);

            if(strlen(trim(substr($check, 0 ,stripos($check, "\n")))) <= 3)
                $this->date = trim(substr($fecha_texto, 0 ,stripos($fecha_texto, "\n")));
            else
                $this->date = trim(substr($check, 0 ,stripos($check, "\n")));

            //aqui abajo ya tengo la linea de la fecha ejemplo: May23'21 01:55 PM
            $fecha_texto = trim(substr($check, 0 ,stripos($check, "\n")));
        }
        else{
            //aqui abajo ya tengo la linea de la fecha ejemplo: May23'21 01:55 PM
            $check = trim(substr($fecha_texto, 0 ,stripos($fecha_texto, "\n")));

            if(strlen($check) < 5){
                $fecha_texto = trim(substr($fecha_texto, stripos($fecha_texto, "\n") + 1));
                $fecha_texto = trim(substr($fecha_texto, 0 ,stripos($fecha_texto, "\n")));
            }
            else
                $fecha_texto = $check;

            $this->date = $fecha_texto;
        }

        if(strlen($fecha_texto) <= 5)
            $fecha_texto = "ENE15'21 01:41PM";
        //Debemos sacar mes dia y año

        //Mes
            $mes = substr($fecha_texto,0,3); //Porque son solo 3 caracteres
            //Arreglo el mes por si lee L como 1 o la O como 0
            $mes = str_replace('1', 'L', $mes);
            $mes = str_replace('0', 'O', $mes);

        //Dia
            //Revisamos si hay mas de un espacio intercalado
            $cantidad = 0;
            if(preg_match_all("/\\s/i", $fecha_texto, $macthes))
                $cantidad = count($macthes[0]);

            if($cantidad > 1){
                $dia = trim(substr($fecha_texto, stripos($fecha_texto, " ")));
                $dia = substr($dia, 0, 2);
            }
            else
                $dia = substr($fecha_texto, 3, 2);

        //Año
            $year = substr($fecha_texto, stripos($fecha_texto, $dia));

            if(substr_count($year, " ") > 1 && !str_contains($year, "'")){
                $year = trim(substr($year, stripos($year," ")));
                $year = substr($year, 0, stripos($year," "));
            }
            else
                $year = substr($year, 0, stripos($year," "));
            $year = substr($year, -2);

        //Sacamos la hora de la misma
            $horas = trim(substr($fecha_texto, stripos($fecha_texto, " ")));

            if(str_contains($horas, " "))
                $horas = trim(substr($horas, stripos($horas, " ")));

            if(str_contains($horas, "PM") || str_contains($horas, "FM")){
                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas));
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour != "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i");
                    }
                    catch(\Exception $e){}
                }
            }
            else{
                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas)); //Le estoy eliminando pues el PM o AM
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour == "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i");
                    }
                    catch(\Exception $e){}
                }
            }

            //Revisamos la hora para que no tire fallo con la fecha
            $check_hora = DateTime::createFromFormat('H:i', $horas);
            if($check_hora && $check_hora->format('H:i') == $horas)
                $horas = date("H:i", strtotime($horas));
            else
                $horas = date("H:i");

        //Formateamos la fecha
            $mes = ucfirst(strtolower($mes)); //Deja la fecha con sus tres letras pero la primera en mayus
            $year = "20".$year; //arreglamos el año

            if($year > 2023)
                $year = date('Y');

            $fecha = $dia."-".$mes."-".$year." ".$horas;

        //Corrección y validación de la fecha
            $chech_date = DateTime::createFromFormat('d-M-Y H:i', $fecha);
            if($chech_date && $chech_date->format('d-M-Y H:i') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

        $this->date = substr($this->date, 0, 8);

        
        //valido si la fecha es superorio a la actual
        $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
        $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
        //COMPARO LAS FECHAS
        if ($fecha > $DateAndTime){
            //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
            $fecha = $DateAndTime;
            $this->month = null;
            $this->state = "pendiente";
        }
        $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
        

        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        //marca pues graba en teoria el nombre real que maneja en la factura
        //linea es que tipo de licor es
        //formato es si lo maneja como botella o como copa

        switch ($marca) {
            case "ABSOLUT BLUE":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MARTINI ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT EXTRAKT":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "BEEFEATER DRY":
                $referencia = array(
                    array(
                        "marca" => "BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MARTINI BEEFEATE",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MARTINI\nBEEFEATE",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MARTINI MAZAPAN",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                     array(
                        "marca" => "MARTINI LYCHES",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MART.BEEFE.GIBSO",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CASABLANCA",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "NEGRONI",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "BEEFEATER PINK":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    )
                );
                break;
            case "BEEFEATER BLACKBERRY":
                $referencia = array(
                    array(
                        "marca" => "BEEFEATERBLA",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 13 TEQUILA":
                $referencia = array(
                    array(
                        "marca" => "CHIVAS 13TEQUILA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 13 SHERRY":
                $referencia = array(
                    array(
                        "marca" => "CHIVAS12SHERRY",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS13SHERRY",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS 13SHERRY",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS 13 SHERRY",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS XV":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => " CHIVAS 15 ATOS",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS 15 ATOS",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS 15 A",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "THE GLENLIVET FOUNDERS":
                $referencia = array(
                    array(
                        "marca" => "THE 2 GLENLIVETRESERF",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "GLENLIVETRESERR",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => " GLENLIVETRESERF",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "GLENLIVETRESERF",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "HAVANA 7":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    )
                );
                break;
            case "MARTELL VSOP":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "BALLATINE'S FINEST":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "ALTOS PLATA":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "THE GLENLIVET 12":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "GLENLIVET 12A",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "CHIVAS 18":
                $referencia = array(
                    array(
                        "marca" => "CHIVAS18ATOS",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS 18ATOS",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS18A+-OS",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "MARTELL BS":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47":
                $referencia = array(
                    array(
                        "marca" => "MONKEY",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    )
                );
                break;
            case "ABSOLUT WATERMELON":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "JAMESON":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
            break;
        }

        return $referencia;
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();

            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();

            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();

        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto

            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);

            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {

                    $cantidad = 1;
                    $contenido = $content;

                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto["marca"];
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO

                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["referencia"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO

                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){

                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 6;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);

                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 6);

                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 6);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 6;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }

                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                if(empty($linea_marca))
                                    break;

                                $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            };

                        //CANTIDAD DEL PRODUCTO
                            $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad);

                        //PRECIO DEL PRODUCTO
                            if($cantidad > 1){
                                $precio = $this->productoPrecio($linea_marca, $content, $new_content);
                            }
                            else
                                $precio = $this->productoPrecio($linea_marca, $content, $content);

                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => substr($linea_marca, 0, stripos($linea_marca, "\n")),//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio*$this->cantidad,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );
                        array_push($check_product, $data);
                        array_push($return_productos, $data);
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $formato, $bdproducto, $ciudad){
        $cantidades = 0;
        $minc = 6;
        $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));

        while($minc > 0) {
            $cantidades = trim(substr($linea_cantidad, 0, $minc));
            $cantidades = trim(str_replace("-", "", $cantidades));
            $cantidades = trim(str_replace("*", "", $cantidades));
            $cantidades = trim(str_replace("+", "", $cantidades));
            $cantidades = trim(str_replace("#", "", $cantidades));
            $cantidades = trim(str_replace("•", "", $cantidades));
            $cantidades = trim(str_replace("Á", "", $cantidades));
            $cantidades = trim(str_replace("$", "", $cantidades));
            $cantidades = trim(str_replace("DO", "", $cantidades));
            $cantidades = trim(str_replace("SY", "", $cantidades));
            $cantidades = trim(str_replace("OF", "", $cantidades));
            $cantidades = trim(str_replace("X ", "", $cantidades));
            $cantidades = trim(str_replace("X", "", $cantidades));
            $cantidades = trim(str_replace("A ", "", $cantidades));

            if(is_numeric($cantidades)){
                break;
            }
            $minc--;
        }

        if(!is_numeric($cantidades)){
            $cantidades = 1;
            $this->state = "pendiente";
        }

        if($cantidades >= 50){
            $this->state = "pendiente";
        }

        if($cantidades < 0){
            $cantidades*=-1;
        }

        $this->cantidad = $cantidades;

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();

            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");

                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01");
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }

                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes);

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }

        return $cantidades;
    }

    //Proceso de sacar precio del Producto
    public function productoPrecio($linea_marca, $content, $contenido){
        //Revisemos si el precio esta en la misma linea del producto
        $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
        $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
        if(strlen($linea_producto) <= 3){
            $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
            $linea_producto = substr($linea_producto, stripos($linea_producto, "\n") + 1);
            $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
        }
        $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea_producto;
        //Arriba ya tenemos la linea del producto con su precio si hipoteticamente lo tiene

        //Arreglo y toma del espacio donde aparece el precio digitado
            $check_line = preg_replace('/[,]/', '1', $linea_producto);
            $check_line = preg_replace('/[.]/', '1', $check_line);

            //Reviso que exista un secuencia de 4 números consecutivos d{4,}
            preg_match('!\d{4,}!', $check_line, $matches);
            $inline = true;

            //Revisamos si dos lineas por debajo de $linea_producto hay mas precios
                $ultimo_pro = false;
                //Linea 1
                $linea1 = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                $linea1 = substr($linea1, 0, stripos($linea1, "\n"));

                //Linea 2
                $linea2 = substr($linea_marca, stripos($linea_marca, $linea1) + strlen($linea1) + 1);
                $linea2 = substr($linea2, 0, stripos($linea2, "\n"));

                //Ultimas dos lineas
                $linea_ultimo = $linea1.$linea2;
                $linea_ultimo = trim(str_replace(".", "", $linea_ultimo));
                $linea_ultimo = trim(str_replace(",", "", $linea_ultimo));

                if(is_numeric($linea_ultimo))
                    $ultimo_pro = true;

            //$matches[0] -> hubo coincidencia | is_numeric(que esa coincidencia sea numerica)
            //$matches[0] >= 4 que sea mayor a 4 digitos
            if(isset($matches[0]) && is_numeric($matches[0]) && strlen(trim($matches[0])) >= 4 && !$ultimo_pro){
                //TIENE EL PRECIO EN LA MISMA LINEA
                //$linea_precio = substr($linea_marca, stripos($linea_marca,"$") + 1);
                $linea_precio = preg_replace('/[,]/', '', $linea_producto);
            }
            else{
                //NO TIENE EL PRECIO EN LA MISMA LINEA POR LO QUE DEBEMOS UBICARLO
                $inline = false;
                $fila = 0;
                $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto

                //Evaluamos la posición del producto para luego sacar la del precio
                    //texto a partir del producto hacia atras
                    //Arreglo de $producto
                    if(!str_contains($contenido, $producto))
                        $producto = substr($producto, 0, strlen($producto) - 5);

                    $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));

                    for($j = 0; $j < 60; $j++){
                        //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = preg_replace('/[U]/', '', $revision);
                        $revision = trim($revision);

                        //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                        while(strlen($revision) <= 4 || is_numeric($revision) || str_contains($revision, $this->date) || str_contains($revision, "JGO") || str_contains($revision, "QINA") || (str_contains($revision, "COCA") && !str_contains($revision, "COCA COLA Z") && !str_contains($revision, "COCACOLA Z") ) || str_contains($revision, "GST") || str_contains($revision, "LECHE") || str_contains($revision, "NORMAL") || str_contains($revision, "DERECHO") || str_contains($revision, "CREMA (")){

                            if(str_contains($revision, $this->date))
                                break;

                            if(str_contains($revision,"EL 19"))
                                break;

                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                            //Limpieza por si lo que lee no es un producto
                            $revision = preg_replace('/[$]/', '', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[.]/', '', $revision);
                            $revision = preg_replace('/[@]/', '', $revision);
                            $revision = preg_replace('/[U]/', '', $revision);
                            $revision = trim($revision);

                            if(empty($revision))
                                break;
                        }
                        //echo $revision."\n";
                        //Validamos que no contenga la palabra clave de la FECHA ya que
                        //a partir de ahí empiezan los productos
                        if(!str_contains($revision, $this->date)){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                            //Otra limpieza por si hay un salto de linea intercalado
                            if(str_contains(substr($all_text, -2), "\n"))
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                            $fila++;
                        }
                        else
                            break;
                    }
                    //return $fila;
                //Fin evaluar la fila del producto

                //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                    //Generalmente a partir de la fecha es que estan los valores de los precios
                    //en estas facturas y tomamos el texto a partir de ahí hasta el final

                    $all_text = substr($content, strrpos($content, $this->date));
                    $all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));

                    for($j = 1; $j < 60; $j++){
                        //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                        if($j > 1)
                            $all_text = substr($all_text, stripos($all_text, "\n") + 1);

                        //Revisamos la linea
                        $revision = substr($all_text, 0, stripos($all_text, "\n"));
                        if(str_contains($revision, " ")){
                            if(substr_count($revision, " ") > 1)
                                $revision = trim(substr($revision, 0, stripos($revision, " ")));
                            else{
                                $revision = trim(substr($revision, stripos($revision, " ")));
                            }
                        }

                        $revision = preg_replace('/[.]/', '11111', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[$]/', '11111', $revision);

                        //Convalidamos que no hayan saltos de linea a travesados
                        while(str_contains(trim(substr($revision, 0, 3)), "\n") || !is_numeric($revision) || strlen($revision) <= 7){
                            if(str_contains($revision, " "))
                                $all_text = trim(substr($all_text, stripos($all_text, " ")));
                            else
                                $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));

                            //Re-Revisamos la linea
                            $revision = substr($all_text, 0, stripos($all_text, "\n"));
                            if(str_contains($revision, " ")){
                                if(substr_count($revision, " ") > 1)
                                    $revision = trim(substr($revision, 0, stripos($revision, " ")));
                                else{
                                    $revision = trim(substr($revision, stripos($revision, " ")));
                                }
                            }

                            $revision = preg_replace('/[.]/', '11111', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[$]/', '11111', $revision);

                            if(empty($all_text))
                                break;
                        }

                        //Evaluamos si ya estamos en la misma linea para culminar
                        if($fila == $j){
                            break;
                        }
                    }
                    //echo $all_text;
                //Fin evaluación posición del precio

                $linea_precio = substr($all_text, 0, stripos($all_text,"\n"));
                if(str_contains($linea_precio, "$"))
                    $linea_precio = substr($linea_precio, stripos($linea_precio,"$") + 1);

            }
        //Fin arreglo y toma del espacio donde aparece el precio digitado

        $precio = 0;
        $minp = 14;

        if($inline){
            $out = true;
            $linea_precio = str_replace("_", "", $linea_precio);

            while($out){
                $revision = trim(substr($linea_precio, 0, stripos($linea_precio, " ")));
                $linea_precio = trim(substr($linea_precio, stripos($linea_precio, " ")));

                $revision = str_replace(")", "", $revision);
                $revision = str_replace("%", "", $revision);
                $revision = str_replace("*", " ", $revision);

                if(str_contains($revision, ".")){
                    if(preg_match('/^([1-9]\d*|0)(\.\d+)?$/', $revision, $matches)){
                        $linea_precio = $matches[0];
                        $out = false;
                    }
                }

                if(!strpos($linea_precio, ' '))
                    $out = false;
            }
        }

        //Sacando el precio
        while($minp > 4) {
            $precio = substr($linea_precio, 0, $minp);
            $precio = preg_replace('/[,]/', '', $precio);
            if(is_numeric($precio)){
                break;
            }
            $minp--;
        }

        if(!is_numeric($precio)){
            $precio = 0;
            $this->state = "pendiente";
        }

        return $precio;
    }

    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = substr($content, stripos($content, $this->date));
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);

        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            /* if(str_contains($productos, "ORDEN"))
                $productos = substr($productos, stripos($productos, 'ORDEN'));

            if(str_contains($productos, "DESCRIPCION"))
                $productos = substr($productos, stripos($productos, 'DESCRIPCION')); */

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, "TOTAL"))
                $productos = substr($productos, 0, stripos($productos, 'TOTAL') + strlen('TOTAL'));

            /* if(str_contains($productos, "SUBTOTAL"))
                $productos = substr($productos, 0, strrpos($productos, 'SUBTOTAL')); */
        //Fin limpiado

        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(str_contains($revision, "@"))
                $revision = substr($productos, 0, stripos($productos, "@"));

            if(!is_numeric($revision)){

                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break;
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad

                //Eliminamos el precio si esta en la misma linea
                    $precio = 0;
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){
                        $revision = preg_replace('/['.$precio.']/', '', $revision);
                    }
                //Fin eliminido el precio

                $nombre = trim($revision);

                //Limpiamos texto basura
                if(strlen($nombre) <= 4)
                    $nombre = "";

                if($nombre && !str_contains($nombre, "TOTAL"))
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){

        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);

        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);

        //Sacamos el mesero
        $values->mesero = $this->mesero($content);

        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos($content, $values->ciudad);

        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        $values->extras = $this->productosExtra($content);

        //Sacamos del vector de productos extras los productos pernod
        $values->extras = $this->limpiandoExtra($values->productos, $values->extras);

        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
?>
