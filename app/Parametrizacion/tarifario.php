<?php
/*
*   Archivo en el que se hace la parametrizacion del tarifario aparte y se incluye luego en cada parametrizador del cdc.
*   Las variables enstan declaradas en cada cdc.
*   
*
*   Se asigna el valoer de la etiqueta del tarifario que esta en la base de datos
*/

use App\Models\Tarifario;
    //traer el ultimo registro y ver el mes_inicio y el mes_fin, si no esta entre ese intervalo restar uno al mes_inicio para que sea el tarifario anterior
    $tari = Tarifario::where('ciudad',$ciudad)->orderBy('mes_inicio', "desc")->first();
    $tari_an = date('Y', strtotime($tari->mes_inicio)); 

    if ($fact_mes >= date('m', strtotime($tari->mes_inicio)) && $fact_mes <= date('m', strtotime($tari->mes_fin))) {
        $this->etiqueta = $tari->etiqueta; 
        $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes_inicio','>=',$tari->mes_inicio)->where('mes_fin','<=',$tari->mes_fin)->whereYear('mes_inicio', '=', $tari_an)->where('ciudad',$ciudad)->get();
    
    }else {
        //resta un mes al mes de inicio para tomar el mes final del tarifario anterior
        $resta_mes = date('m', strtotime($tari->mes_inicio."- 1 month"));
        if($resta_mes < 10){
            $resta_mes = '0'.$resta_mes;
        }
        //trae el mes de inicion del tarifario anterior
        $mes_inicio_ant = Tarifario::select('mes_inicio','mes_fin','etiqueta')->whereMonth('mes_fin','=', "$resta_mes")->whereYear('mes_inicio', '=', $tari_an)->where('ciudad',$ciudad)->orderBy('mes_inicio', 'desc')->first();
        if (!empty($mes_inicio_ant)) {//verifica que exista tarifario anterior
            if ($fact_mes >= date('m', strtotime($mes_inicio_ant->mes_inicio)) && $fact_mes <= date('m', strtotime($mes_inicio_ant->mes_fin))){//compara si esta entre el tarifario anterior
                $tarifario_mes = Tarifario::select('FK_id_marca')->whereMonth('mes_fin','<=',"$resta_mes")->whereYear('mes_inicio', '=', $tari_an)->where('ciudad',$ciudad)->get();
                $this->etiqueta = $mes_inicio_ant->etiqueta;
            }
        }
        
        
    }
?>