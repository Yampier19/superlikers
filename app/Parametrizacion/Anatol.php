<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos;
use App\Models\Tarifario;

class Anatol
{
    public $state = "aceptado";
    public $date = "";
    public $referencia = "";
    public $month = "";
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new Anatol();
    }

    //sacamos el folio
    public function referenciaFolio($content){
        $folio = null;

        //EJEMPLO CON PALOMINOS RESTAURANTE UN CASO

        //Sacamos la referencia
        $ref_posicion = stripos($content, 'CHK') + strlen('CHK') + 1;
        $texto = substr($content, $ref_posicion);
        $texto = trim(substr($texto, 0, stripos($texto,"\n")));
        
        if(preg_match('/\d+/', $texto, $matches)){
            $folio = $matches[0];
        }
        else{

            if(str_contains($texto, "."))
                $texto = trim(substr($texto, stripos($texto,".") + 1));
            else{
                $palabras = trim(preg_replace("/[^A-Za-z: ?]/", "", $texto));
                $texto = str_replace($palabras, "", $texto);
            }

            $texto = trim(str_replace(".", "", $texto));

            $minr = 7;
            while($minr > 2) {
                $folio = trim(substr($texto, 0, $minr));
                if(is_numeric($folio)){
                    break; 
                }
                $minr--;
            }
        }

        //Si no posee solo números quiere decir que hubo un fallo
        if (!is_numeric($folio))
            $this->state = "pendiente";

        $this->referencia = $folio;

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){
        $mesero = null;

        if(str_contains($content, 'ANATOLE')){
            $pos_mesero = stripos($content, 'ANATOLE');
            $mesero = substr($content, $pos_mesero);
            $mesero = substr($mesero, stripos($mesero, "\n") + 1);

            //Limpiando la cadena
            while( !preg_match('!\d{4,}!', substr($mesero, 0, stripos($mesero, "\n")), $matches) ){
                $mesero = substr($mesero, stripos($mesero, "\n") + 1);

                if(empty($mesero))
                    break;
            }

            $mesero = substr($mesero, 0, stripos($mesero, "\n"));
            $mesero = trim(preg_replace("/[^A-Za-z ?]/", "", $mesero)); //Me quedo solo con el texto
        }
        else
            $mesero = "N/A";

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";

        return $mesero;
    }

    //sacamos la fecha
    public function fecha($content, $referencia){
        
        $fecha = date('Y-m-d H:i:s');

        //Segmento donde esta la linea de la fecha y hora
            $texto = substr($content, 0, strrpos($content, "\n"));
            //ejemplo n/j/Y H:i
            $texto = substr($texto, strrpos($texto, "\n") + 1);
            $texto = trim(str_replace("'", " ", $texto)); //limpiar si aparece el caracter '
            $this->date = $texto;

        //Sacamos la fecha que esta segmentada por /
            $fecha_texto = substr($texto, 0, stripos($texto, " "));
            $fecha_texto = str_replace('/', '-', $fecha_texto);
            $fecha = trim(preg_replace("/[^0-9- ?]/", "", $fecha_texto));

        //Sacamos la hora de la misma
            $horas = trim(substr($texto, stripos($texto, " ")));

            if(str_contains($horas, "PM") || str_contains($horas, "P M")  || str_contains($horas, "P.M")){
                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas));
                $this->horas = $horas;

                if(strlen($horas) == 4)
                    $horas = "0".$horas;

                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour != "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i");
                    }
                    catch(\Exception $e){}
                }
            }
            else{
                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas));
                $this->horas = $horas;
                
                if(strlen($horas) == 4)
                    $horas = "0".$horas;

                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour == "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i");
                    }
                    catch(\Exception $e){}
                }
            }

            //Revisamos la hora para que no tire fallo con la fecha
                $check_hora = DateTime::createFromFormat('H:i', $horas);
                if($check_hora && $check_hora->format('H:i') == $horas)
                    $horas = date("H:i", strtotime($horas));
                else
                    $horas = date("H:i");

        //Formateamos la fecha
            $fecha = $fecha." ".$horas;
            $fecha = str_replace("-", "/", $fecha);
            
        //Corrección y validación de la fecha
            $chech_date = DateTime::createFromFormat('n/j/Y H:i', $fecha);
            if($chech_date && $chech_date->format('n/j/Y H:i') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

        //valido si la fecha es superorio a la actual
            $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
            $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
            //COMPARO LAS FECHAS
            if ($fecha > $DateAndTime){
                //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
                $fecha = $DateAndTime;
                $this->month = null;
                $this->state = "pendiente";
            }
            $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
            
        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        switch ($marca) {
            case "ABSOLUT BLUE":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "ABSOLUT EXTRAKT":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "BEEFEATER DRY":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "BEEFEATER PINK":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    )
                );
                break;
            case "BEEFEATER BLACKBERRY":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "AVIATION",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "CHIVAS 13 TEQUILA":
                $referencia = array(
                    array(
                        "marca" => "SPICY COFFEE",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "SPICY COFFE",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "CHIVAS 13 SHERRY":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS XV":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "THE GLENLIVET FOUNDERS":
                $referencia = array(
                    array(
                        "marca" => "GLENLIVET",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "HAVANA 7":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    )
                );
                break;
            case "MARTELL VSOP":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "BALLATINE'S FINEST":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "ALTOS PLATA":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "THE GLENLIVET 12":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 18":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "MARTELL BS":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47":
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "NEGRONI",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "SPRITZ VENETO",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT WATERMELON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "JAMESON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
                break;
        }
        return $referencia;
        
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();
        
        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto
            
            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);
            
            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {
                    
                    $cantidad = 1;
                    $contenido = $content;

                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto["marca"];
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO
                    
                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["nombre"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO
                    
                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){

                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 6;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);

                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 6);

                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 6);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 6;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }

                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);

                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                if(empty($linea_marca))
                                    break;

                                $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            };

                        //CANTIDAD DEL PRODUCTO
                            $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad);
                        
                        //PRECIO DEL PRODUCTO
                            if($cantidad > 1){
                                $precio = 0;//$this->productoPrecio($linea_marca, $content, $new_content);
                            }
                            else
                                $precio = 0;//$this->productoPrecio($linea_marca, $content, $content);
                            
                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => substr($linea_marca, 0, stripos($linea_marca, "\n")),//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );
                        array_push($check_product, $data);
                        array_push($return_productos, $data);
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $formato, $bdproducto, $ciudad){
        $cantidades = 0;
        $minc = 6;
        $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));

        if(str_contains($linea_cantidad, "S2"))
            $linea_cantidad = trim(str_replace("S2", "", $linea_cantidad));
        elseif(str_contains($linea_cantidad, "S4"))
            $linea_cantidad = trim(str_replace("S4", "", $linea_cantidad));
        elseif(str_contains($linea_cantidad, "S1"))
            $linea_cantidad = trim(str_replace("S1", "", $linea_cantidad));
        elseif(str_contains($linea_cantidad, "SI"))
            $linea_cantidad = trim(str_replace("SI", "", $linea_cantidad));
        elseif(str_contains($linea_cantidad, "81"))
            $linea_cantidad = trim(str_replace("81", "", $linea_cantidad));

        while($minc > 0) {
            $cantidades = trim(substr($linea_cantidad, 0, $minc));
            if(is_numeric($cantidades)){
                break; 
            }
            $minc--;
        }

        if(!is_numeric($cantidades)){ 
            $cantidades = 1;
            $this->state = "pendiente";
        }

        if($cantidades >= 50){
            $this->state = "pendiente";
        }

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();
            
            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");
                        
                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01"); 
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }
                        
                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes); 

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }

        return $cantidades;
    }

    //Proceso de sacar precio del Producto
    public function productoPrecio($linea_marca, $content){
        //Revisemos si el precio esta en la misma linea del producto
        $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
        $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
        $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea_producto;
        //Arriba ya tenemos la linea del producto con su precio si hipoteticamente lo tiene
        
        //Arreglo y toma del espacio donde aparece el precio digitado
            $check_line = preg_replace('/[,]/', '1', $linea_producto);
            $check_line = preg_replace('/[.]/', '1', $check_line);
            
            //Reviso que exista un secuencia de 4 números consecutivos d{4,}
            preg_match('!\d{4,}!', $check_line, $matches);
            
            //$matches[0] -> hubo coincidencia | is_numeric(que esa coincidencia sea numerica)
            //$matches[0] >= 4 que sea mayor a 4 digitos
            if(isset($matches[0]) && is_numeric($matches[0]) && strlen(trim($matches[0])) >= 4){
                //TIENE EL PRECIO EN LA MISMA LINEA
                $linea_precio = substr($linea_marca, stripos($linea_marca,"$") + 1);
            }
            else{
                //NO TIENE EL PRECIO EN LA MISMA LINEA POR LO QUE DEBEMOS UBICARLO
                $fila = 0;
                $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto

                //Evaluamos la posición del producto para luego sacar la del precio
                    //texto a partir del producto hacia atras
                    $all_text = substr($content, 0, strrpos($content, $producto) + strlen($producto));

                    for($j = 0; $j < 30; $j++){
                        //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        
                        //Validamos que no contenga la palabra calve DESCRIPCION ya que 
                        //a partir de ahí empiezan los productos
                        if(!str_contains($revision, "DESCRIPCION")){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            
                            //Otra limpieza por si hay un salto de linea intercalado
                            if(str_contains(substr($all_text, -2), "\n"))
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                            $fila++;
                        }
                        else
                            break;
                    }
                //Fin evaluar la fila del producto

                //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                    //Generalmente a partir de IMPORTE es que estan los valores de los precios
                    //en estas facturas y tomamos el texto a partir de ahí hasta el final

                    $all_text = substr($content, strrpos($content, "IMPORTE"));
                    for($j = 1; $j < 30; $j++){
                        //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                        $all_text = substr($all_text, stripos($all_text, "\n") + 1);

                        //Convalidamos que no hayan saltos de linea a travesados
                        while(str_contains(substr($all_text, 0, 2), "\n")){
                            $all_text = substr($all_text, stripos($all_text, "\n") + 1);
                        }

                        //Evaluamos si ya estamos en la misma linea para culminar
                        if($fila == $j){
                            break;
                        }
                    }
                //Fin evaluación posición del precio
                
                $linea_precio = substr($all_text, 0, stripos($all_text,"\n"));
                if(str_contains($linea_precio, "$"))
                    $linea_precio = substr($all_text, stripos($all_text,"$") + 1);
            }
        //Fin arreglo y toma del espacio donde aparece el precio digitado
         
        $precio = 0;
        $minp = 14;
        
        while($minp > 4) {
            $precio = substr($linea_precio, 0, $minp);
            $precio = preg_replace('/[,]/', '', $precio);
            if(is_numeric($precio)){
                break;
            }
            $minp--;
        }

        if(!is_numeric($precio)){ 
            $precio = 0;
            $this->state = "pendiente";
            
        }
        
        
        return $precio;
    }
    
    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = substr($content, stripos($content, $this->referencia));
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);
        
        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            if(str_contains($productos, "TBL"))
                $productos = substr($productos, stripos($productos, 'TBL'));

            if(str_contains($productos, "GST"))
                $productos = substr($productos, stripos($productos, 'GST'));

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, $this->date))
                $productos = substr($productos, 0, stripos($productos, $this->date));
        //Fin limpiado

        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(!is_numeric($revision)){
                
                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break; 
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){ 
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad
                
                //Eliminamos el precio si esta en la misma linea
                    $precio = "";
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    //Elimina los precios intercalados
                    while(preg_match('!\d{4,}!', $revision, $matches)){
                        $precio = $matches[0];
                        $revision = trim(preg_replace('/['.$precio.']/', '', $revision));
                    }

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){ 
                        $revision = preg_replace('/['.$precio.']/', '', $revision);
                    }
                //Fin eliminido el precio
                
                $nombre = trim($revision);

                //Limpiamos texto basura
                    if(strlen($nombre) <= 4)
                        $nombre = "";
                    
                    if(str_contains($nombre, "WWW"))
                        $nombre = "";

                if($nombre && !str_contains($nombre, $this->date))
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){
        
        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);
        
        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);
        
        //Sacamos el mesero
        $values->mesero = $this->mesero($content);
        
        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos($content, $values->ciudad);
        
        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        $values->extras = $this->productosExtra($content);

        //Sacamos del vector de productos extras los productos pernod
        $values->extras = $this->limpiandoExtra($values->productos, $values->extras);
        
        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
?>