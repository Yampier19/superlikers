<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos;
use App\Models\Tarifario;

class MuiMui
{

    public $state = "aceptado";
    public $referencia = "";
    public $mesero = "";
    public $date = "";
    public $horas = "";
    public $month = "";
    public $cantidad = 1;
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new MuiMui();
    }

    //Sacamos la referencia del folio
    public function referenciaFolio($content){
        $folio = null;
        
        if(str_contains($content, 'CHK')){
            $ref_posicion = stripos($content, 'CHK') + strlen('CHK');
            $texto = substr($content, $ref_posicion);
            $texto = trim(substr($texto, 0, stripos($texto,"\n")));

            if(preg_match('/\d+/', $texto, $matches)){
                $folio = $matches[0];
            }
            else{

                if(str_contains($texto, "."))
                    $texto = trim(substr($texto, stripos($texto,".") + 1));
                else{
                    $palabras = trim(preg_replace("/[^A-Za-z: ?]/", "", $texto));
                    $texto = str_replace($palabras, "", $texto);
                }

                $texto = trim(str_replace(".", "", $texto));

                $minr = 7;
                while($minr > 2) {
                    $folio = trim(substr($texto, 0, $minr));
                    if(is_numeric($folio)){
                        break; 
                    }
                    $minr--;
                }
            }

            //Si no posee solo números quiere decir que hubo un fallo
            if (!is_numeric($folio))
                $this->state = "pendiente";
        }
        else
            $folio = "N/A";

        $this->referencia = $folio;

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){

        $mesero = null;

        $mesero = "N/A";

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";
        
        $this->mesero = $mesero;

        return $mesero;
    }

    //Sacamos la fecha con hora
    public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:i:s');
        $referencia = "CHK";

        //Segmento donde esta la linea de la fecha
            $date_texto = trim(substr($content, stripos($content, $referencia) + strlen($referencia)));
            $fecha_texto = $date_texto;
            $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

            while(str_contains($revision, "TBL") || str_contains($revision, "GST") || str_contains($revision, " MARCO") || strlen($revision) < 7){
                $fecha_texto = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);
                $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

                if(empty($revision))
                    break;
            }

            //aqui abajo ya tengo la linea de la fecha ejemplo: 5 MAY 21 H:i
            $this->date = $revision;
            
        //Sacamos la fecha
            $texto = trim(preg_replace("/[^A-Za-z: ?]/", "", $revision));
            $palabras = explode(" ", $texto);
            
            foreach($palabras as $palabra){
                if(strlen($palabra) > 3)
                    $revision = trim(str_replace($palabra, "", $revision));
            }
            
            if(str_contains($revision, $this->referencia))
                $revision = trim(str_replace($this->referencia, "", $revision));
            
            $fecha = $revision;
            $fecha = trim(str_replace(".", "", $fecha));
            $fecha = trim(str_replace(":", "", $fecha));
            $fecha = trim(str_replace("'", " ", $fecha));
            $fecha = trim(str_replace("PM", "", $fecha));
            $fecha = trim(str_replace("AM", "", $fecha));
            
            if(substr_count($fecha, " ") >= 3){
                $cantidad = substr_count($fecha, " ");
                while($cantidad > 2){
                    $cantidad--;
                    $fecha = trim(substr($fecha, 0, strrpos($fecha, " ")));
                }
            }

            $fecha = trim(str_replace(" ", "-", $fecha));
            
            //Ajuste del Mes
            $month = trim(preg_replace("/[^A-Za-z ?]/", "", $fecha));

            switch($month){
                case "JAN": $mes = "Jan"; break;
                case "FEB": $mes = "Feb"; break;
                case "MAR": $mes = "Mar"; break;
                case "APR": $mes = "Apr"; break;
                case "MAY": $mes = "May"; break;
                case "JUN": $mes = "Jun"; break;
                case "JUL": $mes = "Jul"; break;
                case "AUG": $mes = "Aug"; break;
                case "SEP": $mes = "Sep"; break;
                case "OCT": $mes = "Oct"; break;
                case "NOV": $mes = "Nov"; break;
                case "DEC": $mes = "Dec"; break;
                default: $mes = "Dec"; break;
            }

            $fecha = str_replace($month, $mes, $fecha);
            
            //Corregimos el año
                $year = substr($fecha, -2);
                try{
                    $year = DateTime::createFromFormat('y', $year);
                    if($year)
                        $year = $year->format('Y');
                    else
                        $year = "20".$year;
                }
                catch(\Exception $e){
                    $year = "20".$year;
                }
                //$year = "20".$year;
            
            $fecha = substr($fecha, 0, -2).$year;

        //Sacamos la hora de la misma
            $horas = trim($date_texto);
            $revision = trim(substr($horas, 0, stripos($horas, "\n")));
            
            while(substr_count($revision, ":") < 1){
                $date_texto = substr($date_texto, stripos($date_texto, "\n") + 1);
                $revision = trim(substr($date_texto, 0, stripos($date_texto, "\n")));

                if(empty($revision))
                    break;
            }
            
            if(str_contains($horas, "PM") || str_contains($horas, "P.M") || str_contains($horas, "P. M"))
                $tarde = true;
            else
                $tarde = false;

            $horas = $revision;
            $this->horas = $horas;
            
            $texto = trim(preg_replace("/[^A-Za-z ?]/", "", $revision));
            $palabras = explode(" ", $texto);
            
            foreach($palabras as $palabra){
                if(strlen($palabra) > 1)
                    $revision = trim(str_replace($palabra, "", $revision));
            }
            
            $horas = $revision;
            
            if(!str_contains($revision, " "))
                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas)); 
            else
                $horas = trim(substr($revision, 0, stripos($revision, " ")));
            
            while(str_contains($revision, " ")){
                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas)); 
                
                if(strlen($horas) > 3)
                    break;

                $revision = trim(substr($revision, stripos($revision, " ")));

                if(str_contains($revision, " "))
                    $horas = substr($revision, 0, stripos($revision, " "));
                else
                    $horas = $revision;
            }
            
            if(substr_count($horas, ":") > 1)
                $horas = substr($horas, 1);

            if($tarde){
                try{
                    $date = new DateTime($horas);
                    $date->modify("+12 hours");
                    $horas = $date->format("H:i");
                }
                catch(\Exception $e){}
            }

            //Revisamos la hora para que no tire fallo con la fecha
            $check_hora = DateTime::createFromFormat('H:i', $horas);
            if($check_hora && $check_hora->format('H:i') == $horas)
                $horas = date("H:i:s", strtotime($horas));
            else
                $horas = date("H:i:s");
            
        //Formateamos la fecha
            $fecha = $fecha." ".$horas;
            
        //Corrección y validación de la fecha
            $fecha = str_replace('/', '-', $fecha);
            $chech_date = DateTime::createFromFormat('j-M-Y H:i:s', $fecha);
            if($chech_date && $chech_date->format('j-M-Y H:i:s') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

            //valido si la fecha es superorio a la actual
            $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
            $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
            //COMPARO LAS FECHAS
            if ($fecha > $DateAndTime){
                //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
                $fecha = $DateAndTime;
                $this->month = null;
                $this->state = "pendiente";
            }
            $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
            
        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        //marca pues graba en teoria el nombre real que maneja en la factura
        //linea es que tipo de licor es
        //formato es si lo maneja como botella o como copa

        switch ($marca) {
            case "ABSOLUT BLUE": 
                $referencia = array(
                    array(
                        "marca" => "ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT EXTRAKT": 
                $referencia = array(
                    /* array(
                        "marca" => "SHOCHU FRUTOS ROJOS @ ABSOLUT EXTRACT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "SHOCHU FRUTOS ROJOS @",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ), */
                    array(
                        "marca" => "PETACON ABSOLUT EXTRAKT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT EXTRACT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER DRY": 
                $referencia = array(
                    /* array(
                        "marca" => "SHOCHU FRUTOS ROJOS\nBEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ), */
                    array(
                        "marca" => "NEGRONI BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "\nBEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BTL BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER PINK": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER BLACKBERRY": 
                $referencia = array(
                    array(
                        "marca" => "SHOCHU LICHI BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "SHOCHU LICHI",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BRAMBLE BEEFATER\nBLACKBERRY",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BRAMBLE BEEFATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "¿LONDRES? BEEFEATER\nBLACKBERRY",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "LONDRES? BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    /* array(
                        "marca" => "BLACKBERRY",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ), */
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 TEQUILA": 
                $referencia = array(
                    array(
                        "marca" => "CHIVAS 13 TEQULA @",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS 13 TE",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 SHERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS XV": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "THE GLENLIVET FOUNDERS": 
                $referencia = array(
                    array(
                        "marca" => "CBL GLENLIVET FOUNDER",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "DBL GLENLIVET FOUNDER",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "GLENLIVET FOUNDER",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "HAVANA 7": 
                $referencia = array(
                    array(
                        "marca" => "HAVANA CLUB 7",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "MARTELL VSOP": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                );
                break;
            case "BALLATINE'S FINEST": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ALTOS PLATA": 
                $referencia = array(
                    array(
                        "marca" => "SIERRA MADRE ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BTL ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "THE GLENLIVET 12": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 18": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "MARTELL BS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT WATERMELON": 
                $referencia = array(
                    array(
                        "marca" => "ABSOLUT SANDIA",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "JAMESON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
            break;
        }

        return $referencia;
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();
        
        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto
            
            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);
            
            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {
                    
                    $cantidad = 1;
                    $contenido = $content;

                    //QUITAMOS EL SALTO DE LINEA AL $producto["marca"] SI LO TIENE
                    $producto_marca = $producto["marca"];
                    if(str_contains($producto["marca"], "\n"))
                        $producto_marca = trim(str_replace("\n", "", $producto["marca"]));
                        
                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto_marca;
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO
                    
                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["nombre"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO
                    
                    //QUITAMOS EL SALTO DE LINEA AL $producto["marca"] SI LO TIENE
                        if(str_contains($producto["marca"], "\n"))
                            $producto["marca"] = trim(str_replace("\n", "", $producto["marca"]));
                        
                    //echo $cantidad." - ".$producto["marca"]."\n";
                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){
                        
                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;
                            
                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                            
                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 7);
                            
                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 7);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }

                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            
                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            if(str_contains($linea_marca, "\n")){
                                while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                    if(empty($linea_marca))
                                        break;

                                    $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                                };
                            }
                            
                        //AJUSTE DE LA VARIABLE DE CONTENIDO CUANDO UN PRODUCTO TIENE IMPRESO EL NOMBRE VARIAS VECES
                            $loop = 0;
                            $check = substr($contenido, 0, strrpos($contenido, "\n"));
                            if(str_contains($linea_marca, "\n")){
                                $pro_check = substr($linea_marca, 0, stripos($linea_marca, "\n"));
                            }
                            else
                                $pro_check = $linea_marca;
                                
                            if($cantidad > 1){
                                
                                $repeat = trim(substr($check, strrpos($check, "\n") + 1));
                                
                                if(!preg_match('~[0-9]+~', $repeat)){
                                    //while(str_contains($repeat, $producto["marca"]))
                                    while(str_contains($repeat, $producto["marca"]) && $repeat == $pro_check){
                                        $check = substr($check, 0, strrpos($check,"\n"));
                                        $repeat = substr($check, strrpos($check,"\n") + 1);
                                        
                                        if(empty($repeat)){
                                            break;
                                        }
                                        $loop++; 
                                    }
                                }
                            }

                        //CANTIDAD DEL PRODUCTO
                            if($cantidad > 1){
                                $cantidades = $this->productoCantidad($linea_marca, $loop, $producto["formato"], $product, $ciudad, $content, $new_content);
                            }
                            else
                                $cantidades = $this->productoCantidad($linea_marca, $loop, $producto["formato"], $product, $ciudad, $content, $content);
                        
                        //PRECIO DEL PRODUCTO
                            if($cantidad > 1){
                                $precio = $this->productoPrecio($linea_marca, $loop, $producto["marca"], $content, $new_content);
                            }
                            else
                                $precio = $this->productoPrecio($linea_marca, $loop, $producto["marca"],  $content, $content);
                        
                        //TERMINAR EL AJUSTE DE LA VARIABLE DE CONTENIDO Y CANTIDAD
                            $contenido = $check;
                            $cantidad -= $loop;
                        
                        
                        if(empty($linea_marca))
                            continue;
                         
                        if(str_contains($linea_marca, "\n")){
                            $linea_marca = substr($linea_marca, 0, stripos($linea_marca, "\n"));
                        }

                        if($codigo == 9878 && str_contains($linea_marca, "BLACKBERRY"))
                            continue;
                        
                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => $linea_marca,//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio*$this->cantidad,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );
                        array_push($check_product, $data);
                        array_push($return_productos, $data);
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $arreg, $formato, $bdproducto, $ciudad, $content, $contenido){
        $cantidades = 0;
        $minc = 6;

        if(str_contains($linea_marca, "\n")){
            $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));
        }
        else
            $linea_cantidad = $linea_marca;

        $check_line = substr($linea_cantidad, 0, 4);

        //Si la cantidad no esta en la misma linea debemos ubicarlo
        if(!preg_match('/\d+/', $check_line, $matches)){
            $fila = 0;

            if(str_contains($linea_marca, "\n")){
                $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto
            }
            else
                $producto = $linea_marca; //nombre del producto
            
            if(str_contains($contenido, 'DINE IN')){
                $ref_inicio = 'DINE IN';
            }
            elseif(str_contains($contenido, $this->date)){
                $ref_inicio = $this->date;
            }
            elseif(str_contains($contenido, 'GST')){
                $ref_inicio = 'GST';
            }
            else
                $ref_inicio = 'TBL';
            
            //Evaluamos la posición del producto para luego sacar la del precio
                //texto a partir del producto hacia atras
                if(!str_contains($contenido, $producto))
                    $producto = substr($producto, 0, strrpos($producto, " "));

                $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                
                for($j = 0; $j < 60; $j++){
                    //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                    $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                    
                    //Limpieza por si lo que lee no es un producto
                    $revision = preg_replace('/[$]/', '', $revision);
                    $revision = preg_replace('/[,]/', '', $revision);
                    $revision = preg_replace('/[.]/', '', $revision);
                    $revision = preg_replace('/[@]/', '', $revision);
                    $revision = trim($revision);

                    //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                    while(strlen($revision) <= 5 || is_numeric($revision) || str_contains($revision, $ref_inicio) /* || str_contains($revision, "PLATA") || str_contains($revision, "PICOSI") */ || $revision == "MARACUYA" || $revision == "BRASAS" || str_contains($revision, "TAMARINDO") /* || str_contains($revision, "TELOBO") */ || str_contains($revision, "YAKIT") || str_contains($revision, "ENSAMB") || str_contains($revision, "DINE IN") || str_contains($revision, "GST") || str_contains($revision, "TBL") || str_contains($revision, "MICHELADA ") || str_contains($revision, "BRASAS  ") || str_contains($revision, "BLACKBERRY") || str_contains($revision, "CONEJOS ") || str_contains($revision, "A CHILE AN") || str_contains($revision, "TANQUERAY F") || str_contains($revision, "PATRON S") || str_contains($revision, "PATRON R") || str_contains($revision, "COCO HI") || $revision == "CITRICOS" || $revision == "BOMBAY" || $revision == "7 LEGUAS BLANCO" || $revision == "MONTELOBOS" || $revision == "NORMAL" || $revision == "JAMAICA" || $revision == "SEVILLA" || $revision == "FRUTOS ROJOS" || $revision == "RESERVE" || $revision == "SILVER" || $revision == "PATRON SILVER" || $revision == "400 CONEJOS JOVEN" || $revision == "RASPBERRY RON" || $revision == "BACARDI BLANCO" || $revision == "GIN DE JUANITA" || str_contains($revision, $this->date)){
                        
                        if(str_contains($revision, $ref_inicio) || str_contains($revision, 'PAPALOTE') || str_contains($revision, '400 CONEJOS'))
                            break;

                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);

                        if(empty($revision))
                            break;
                    }

                    //Revision de ver que fila es el producto si viene dentro de SHOCHU
                    $c_revision = $revision;
                    if(str_contains($c_revision, "BEEFEATER") || str_contains($c_revision, "ALTOS PLATA") || str_contains($c_revision, "ABSOLUT EXTRACT") || str_contains($c_revision, "HAVANA CLUB") || str_contains($c_revision, "ABSOLUT AZUL") || str_contains($c_revision, "ABSOLUT SANDIA")){
                        
                        $checking = $all_text;

                        while(is_numeric($c_revision) || str_contains($c_revision, "PINA") || str_contains($c_revision, "TORONJA") || str_contains($c_revision, "MANGO") || str_contains($c_revision, "COCO") || str_contains($c_revision, "CITRI") || str_contains($c_revision, "PEPINO L") || str_contains($c_revision, "MANDARIA") || str_contains($c_revision, "PPINO ROM") || str_contains($c_revision, "KIWI") || str_contains($c_revision, "HIERBABU") || str_contains($c_revision, "FRESA") || str_contains($c_revision, "JAMAIC") || str_contains($c_revision, "MANDARINA") || str_contains($c_revision, "LICHI") || str_contains($c_revision, "GO PIC") || str_contains($c_revision, "TAMARIN") || str_contains($c_revision, "BEEFEATER") || str_contains($c_revision, "ALTOS PLATA") || str_contains($c_revision, "ABSOLUT EXTRACT") || str_contains($c_revision, "HAVANA CLUB") || str_contains($c_revision, "ABSOLUT AZUL") || str_contains($c_revision, "ABSOLUT SANDIA")){    
                            $checking = substr($checking, 0, strrpos($checking,"\n"));
                            $c_revision = substr($checking, strrpos($checking,"\n") + 1);
                            
                            if(str_contains($c_revision, "SHOCHU") || str_contains($c_revision, "SHODHU")){
                                $all_text = $checking;
                                $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                                break;
                            } 

                            if(empty($revision))
                                break;
                        }
                    }

                    //Validamos que no contenga la palabra clave de TOTAL ya que 
                    //a partir de ahí empiezan los productos
                    if(!str_contains($revision, $ref_inicio)){
                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        
                        //Otra limpieza por si hay un salto de linea intercalado
                        if(str_contains(substr($all_text, -2), "\n"))
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                        $fila++;
                    }
                    else
                        break;
                }
                //return $fila;
            //Fin evaluar la fila del producto
            
            //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                //Generalmente a partir de donde sale la REF_FOLIO es que estan los valores de los precios
                //en estas facturas y tomamos el texto a partir de ahí hasta el final
                $all_text = substr($content, stripos($content, $ref_inicio));
                $all_text = str_replace('7 LEGUAS', '', $all_text);
                $all_text = str_replace('400 CONE', '', $all_text);
                //$all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                
                for($j = 1; $j < 60; $j++){
                    //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                    //if($j > 1)
                        $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                    
                    //Revisamos la linea
                    $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                    if(str_contains($revision, " ")){
                        //if(substr_count($revision, " ") > 1)
                            $revision = trim(substr($revision, 0, stripos($revision, " ")));
                        /* else
                            $revision = trim(substr($revision, stripos($revision, " "))); */
                    }

                    $revision = str_replace('$', 'AAA', $revision);
                    $revision = str_replace(',', '', $revision);
                    $revision = str_replace('|', '', $revision);

                    //Convalidamos que no hayan saltos de linea a travesados
                    while(str_contains(substr($revision, 0, 2), "\n") || !is_numeric($revision) || strlen($revision) >= 2/*  || trim($revision) != 7 */){
                        if(str_contains($revision, " "))
                            $all_text = trim(substr($all_text, stripos($all_text, " ")));
                        else
                            $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                        
                        //Re-Revisamos la linea
                        $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                        if(str_contains($revision, " ")){
                            //if(substr_count($revision, " ") > 1)
                                $revision = trim(substr($revision, 0, stripos($revision, " ")));
                            /* else
                                $revision = trim(substr($revision, stripos($revision, " "))); */
                        }

                        $revision = str_replace('$', 'AAA', $revision);
                        $revision = str_replace(',', '', $revision);
                        $revision = str_replace('|', '', $revision);

                        if(empty($all_text))
                            break;
                    }

                    //Evaluamos si ya estamos en la misma linea para culminar
                    if($fila == $j){
                        break;
                    }
                }
            //Fin evaluación posición del precio
            
            $linea_cantidad = substr($all_text, 0, stripos($all_text,"\n"));
        }

        while($minc > 0) {
            $cantidades = trim(substr($linea_cantidad, 0, $minc));
            if(is_numeric($cantidades)){
                break; 
            }
            $minc--;
        }

        if(!is_numeric($cantidades)){ 
            $cantidades = 1;
            $this->state = "pendiente";
        }

        if($cantidades >= 50){
            $this->state = "pendiente";
        }

        $this->cantidad = $cantidades;

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();
            
            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");
                        
                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01"); 
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }
                        
                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes); 

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }

        return $cantidades;
    }

    //Proceso de sacar precio del Producto
    public function productoPrecio($linea_marca, $arreg, $titulo, $content, $contenido){
        //Revisemos si el precio esta en la misma linea del producto
        if(str_contains($linea_marca, "\n")){
            $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
            $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
            $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea_producto;
            //Arriba ya tenemos la linea del producto con su precio si hipoteticamente lo tiene
            $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"));
        }
        else
            $linea_producto = $linea_marca;

        //Arreglo y toma del espacio donde aparece el precio digitado
            $check_line = preg_replace('/[,]/', '1', $linea_producto);
            $check_line = preg_replace('/[.]/', '', $check_line);
            
            //Reviso que exista un secuencia de 4 números consecutivos d{4,}
            preg_match('!\d{4,}!', $check_line, $matches);
            $inline = true;

            //Revisamos si dos lineas por debajo de $linea_producto hay mas precios
                $ultimo_pro = false;
                //Linea 1
                $linea1 = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                $linea1 = substr($linea1, 0, stripos($linea1, "\n"));

                //Linea 2
                $linea2 = substr($linea_marca, stripos($linea_marca, $linea1) + strlen($linea1) + 1);
                $linea2 = substr($linea2, 0, stripos($linea2, "\n"));

                //Linea 3
                $linea3 = substr($linea_marca, stripos($linea_marca, $linea2) + strlen($linea2) + 1);
                $linea3 = substr($linea3, 0, stripos($linea3, "\n"));

                //Ultimas dos lineas
                $linea_ultimo = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea1." ".$linea2." ".$linea3;
                if(substr_count($linea_ultimo, ".") > 2 || str_contains($linea_ultimo, " IVA"))
                    $ultimo_pro = true;
                    
                //Ultimo check de que este dentro de su misma linea
                $last_check = substr($linea_marca, 0, stripos($linea_marca, "\n"));
                if(substr_count($last_check, ".") < 2 && substr_count($last_check, ".") >= 1)
                    $ultimo_pro = false;
                
                //PONDRO TODO COMO SI FUERA ULTIMO PARA QUE BUSQUEDA LINEA A LINEA
                $ultimo_pro = false;
            //$matches[0] -> hubo coincidencia | is_numeric(que esa coincidencia sea numerica)
            //$matches[0] >= 4 que sea mayor a 4 digitos
            if(isset($matches[0]) && is_numeric($matches[0]) && strlen(trim($matches[0])) >= 4 && $matches[0] != "0500" && !$ultimo_pro){
                //TIENE EL PRECIO EN LA MISMA LINEA
                if(str_contains($linea_producto, "$")){
                    $linea_precio = substr($linea_producto, stripos($linea_producto,"$") + 1);
                    $inline = false;
                }
                else
                    $linea_precio = preg_replace('/[,]/', '', $linea_producto);
            }
            else{
                //NO TIENE EL PRECIO EN LA MISMA LINEA POR LO QUE DEBEMOS UBICARLO
                $inline = true;
                $fila = 0;

                if(str_contains($linea_marca, "\n")){
                    $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto
                }
                else
                    $producto = $linea_marca; //nombre del producto

                if(str_contains($contenido, 'DINE IN')){
                    $ref_inicio = 'DINE IN';
                }
                elseif(str_contains($contenido, $this->date)){
                    $ref_inicio = $this->date;
                }
                elseif(str_contains($contenido, 'GST')){
                    $ref_inicio = 'GST';
                }
                else
                    $ref_inicio = 'TBL';

                //Evaluamos la posición del producto para luego sacar la del precio
                    //texto a partir del producto hacia atras
                    //Arreglo de $producto
                    if(!str_contains($contenido, $producto)){
                        if(str_contains($producto, ">"))
                            $producto = substr($producto, 0 , stripos($producto, ">"));
                        elseif(str_contains($producto, "ER R"))
                            $producto = trim(substr($producto, 0 , stripos($producto, " RES")));
                        else
                            $producto = substr($producto, 0, strlen($producto) - 5);
                    }
                    
                    $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                    
                    for($j = 0; $j < 60; $j++){
                        //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        
                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);
    
                        //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                        while(strlen($revision) <= 5 || is_numeric($revision) || str_contains($revision, $ref_inicio) /* || str_contains($revision, "PLATA") || str_contains($revision, "PICOSI") */ || $revision == "MARACUYA" || $revision == "BRASAS" || str_contains($revision, "TAMARINDO") /* || str_contains($revision, "TELOBO") */ || str_contains($revision, "YAKIT") || str_contains($revision, "ENSAMB") || str_contains($revision, "DINE IN") || str_contains($revision, "GST") || str_contains($revision, "TBL") /* || str_contains($revision, "MICHELADA ") */ || str_contains($revision, "BRASAS  ") || str_contains($revision, "BLACKBERRY") || str_contains($revision, "CONEJOS ") || str_contains($revision, "A CHILE AN") || str_contains($revision, "TANQUERAY F") || str_contains($revision, "PATRON S") || str_contains($revision, "PATRON R") || str_contains($revision, "COCO HI") || $revision == "CITRICOS" || $revision == "BOMBAY" || $revision == "MONTELOBOS" || $revision == "7 LEGUAS BLANCO" || $revision == "SEVILLA" || $revision == "NORMAL" || $revision == "JAMAICA" || $revision == "FRUTOS ROJOS" || $revision == "RESERVE" || $revision == "SILVER" || $revision == "PATRON SILVER" || $revision == "400 CONEJOS JOVEN" || $revision == "RASPBERRY RON" || $revision == "BACARDI BLANCO" || $revision == "GIN DE JUANITA" || str_contains($revision, $this->date)){
                    
                            if(str_contains($revision, $ref_inicio))
                                break;

                            if($revision == "RUSA" || str_contains($revision, 'PAPALOTE') || str_contains($revision, '400 CONEJOS'))
                                break;
    
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);
    
                            //Limpieza por si lo que lee no es un producto
                            $revision = preg_replace('/[$]/', '', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[.]/', '', $revision);
                            $revision = preg_replace('/[@]/', '', $revision);
                            $revision = trim($revision);
    
                            if(empty($revision))
                                break;
                        }
                        
                        //Revision de ver que fila es el producto si viene dentro de SHOCHU
                        $c_revision = $revision;
                        $c_revision = trim(preg_replace('/[@]/', '', $c_revision));
                        if(str_contains($c_revision, "BEEFEATER") || str_contains($c_revision, "ALTOS PLATA") || str_contains($c_revision, "ABSOLUT EXTRACT") || str_contains($c_revision, "HAVANA CLUB") || str_contains($c_revision, "ABSOLUT AZUL") || str_contains($c_revision, "ABSOLUT SANDIA")){
                            
                            $checking = $all_text;

                            while(is_numeric($c_revision) || str_contains($c_revision, "PINA") || str_contains($c_revision, "TORONJA") || str_contains($c_revision, "MANGO") || str_contains($c_revision, "COCO") || str_contains($c_revision, "CITRI") || str_contains($c_revision, "PEPINO L") || str_contains($c_revision, "MANDARIA") || str_contains($c_revision, "PPINO ROM") || str_contains($c_revision, "KIWI") || str_contains($c_revision, "HIERBABU") || str_contains($c_revision, "LICHI") || str_contains($c_revision, "JAMAIC") || str_contains($c_revision, "FRESA") || str_contains($c_revision, "MANDARINA") || str_contains($c_revision, "GO PIC") || str_contains($c_revision, "TAMARIN") || str_contains($c_revision, "BEEFEATER") || str_contains($c_revision, "ALTOS PLATA") || str_contains($c_revision, "ABSOLUT EXTRACT") || str_contains($c_revision, "HAVANA CLUB") || str_contains($c_revision, "ABSOLUT AZUL") || str_contains($c_revision, "ABSOLUT SANDIA")){    
                                $checking = substr($checking, 0, strrpos($checking,"\n"));
                                $c_revision = substr($checking, strrpos($checking,"\n") + 1);
                                $c_revision = trim(preg_replace('/[@]/', '', $c_revision));
                                
                                if(str_contains($c_revision, "SHOCHU") || str_contains($c_revision, "SHODHU")){
                                    $all_text = $checking;
                                    $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                                    break;
                                } 

                                if(empty($revision))
                                    break;
                            }
                        }
                        //echo $revision."\n";
                        //Validamos que no contenga la palabra clave de TOTAL ya que 
                        //a partir de ahí empiezan los productos
                        if(!str_contains($revision, $ref_inicio)){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            
                            //Otra limpieza por si hay un salto de linea intercalado
                            if(str_contains(substr($all_text, -2), "\n"))
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
    
                            $fila++;
                        }
                        else
                            break;
                    }
                    //echo "\n\n";
                    //return $fila;
                //Fin evaluar la fila del producto
                
                //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                    //Generalmente a partir de IMPORTE es que estan los valores de los precios
                    //en estas facturas y tomamos el texto a partir de ahí hasta el final
                    if(str_contains($contenido, $this->date) && strlen($this->date) > 3){
                        $ref_inicio = $this->date;
                    }
                    elseif(str_contains($contenido, 'DINE IN')){
                        $ref_inicio = 'DINE IN';
                    }
                    elseif(str_contains($contenido, 'GST')){
                        $ref_inicio = 'GST';
                    }
                    else
                        $ref_inicio = 'TBL';

                    $all_text = substr($content, strrpos($content, $ref_inicio));
                    $all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                    //echo $all_text."\n\n";
                    
                    for($j = 1; $j < 60; $j++){
                        //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                        if($j > 1)
                            $all_text = substr($all_text, stripos($all_text, "\n") + 1);

                        //Revisamos la linea
                        $pass = false;
                        $revision = substr($all_text, 0, stripos($all_text, "\n"));
                        if(str_contains($revision, " ")){
                            if(str_contains($revision, "440.00"))
                                $pass = true;
                            elseif(str_contains($revision, "90.00"))
                                $pass = true;
                            elseif(str_contains($revision, "39.00"))
                                $pass = true;
                            elseif(str_contains($revision, "305.00"))
                                $pass = true;
                            elseif(str_contains($revision, "320.00"))
                                $pass = true;
                            elseif(str_contains($revision, "98.00"))
                                $pass = true;
                            elseif(str_contains($revision, "180.00"))
                                $pass = true;
                            elseif(str_contains($revision, "170.00"))
                                $pass = true;
                            elseif(str_contains($revision, "190.00"))
                                $pass = true;
                            elseif(str_contains($revision, "204.00"))
                                $pass = true;
                            elseif(str_contains($revision, "237.00"))
                                $pass = true;
                            elseif(str_contains($revision, "487.00"))
                                $pass = true;
                            elseif(str_contains($revision, "635.00"))
                                $pass = true;
                            elseif(str_contains($revision, "196.00"))
                                $pass = true;
                            elseif(str_contains($revision, "660.00"))
                                $pass = true;
                            else
                                $pass = false;

                            /* if(substr_count($revision, " ") > 1)
                                $revision = trim(substr($revision, 0, stripos($revision, " ")));
                            else{
                                 */$revision = trim(substr($revision, stripos($revision, " ")));
                            //}
                        }

                        $revision = preg_replace('/[.]/', '111', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[$]/', '111', $revision);

                        //Convalidamos que no hayan saltos de linea a travesados
                        while(str_contains(trim(substr($revision, 0, 3)), "\n") || !is_numeric($revision) || strlen($revision) <= 5 || ($revision == "44011100" && !$pass) || ($revision == "3911100" && !$pass) || ($revision == "32011100" && !$pass) || ($revision == "30511100" && !$pass) || ($revision == "9011100" && !$pass) || ($revision == "9811100" && !$pass) || ($revision == "18011100" && !$pass) || ($revision == "17011100" && !$pass) || ($revision == "20411100" && !$pass) || ($revision == "19011100" && !$pass) || ($revision == "23711100" && !$pass) || ($revision == "48711100" && !$pass) || ($revision == "63511100" && !$pass) || ($revision == "19611100" && !$pass) || ($revision == "66011100" && !$pass)){
                            if(str_contains($revision, " "))
                                $all_text = trim(substr($all_text, stripos($all_text, " ")));
                            else
                                $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));

                            //Re-Revisamos la linea
                            $revision = substr($all_text, 0, stripos($all_text, "\n"));
                            $pass = false;
                            if(str_contains($revision, " ")){
                                if(str_contains($revision, "440.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "39.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "305.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "320.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "98.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "90.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "180.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "170.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "190.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "102.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "237.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "487.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "635.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "196.00"))
                                    $pass = true;
                                elseif(str_contains($revision, "660.00"))
                                    $pass = true;
                                else
                                    $pass = false;

                                /* if(substr_count($revision, " ") > 1)
                                    $revision = trim(substr($revision, 0, stripos($revision, " ")));
                                else{
                                    */$revision = trim(substr($revision, stripos($revision, " ")));
                                //}
                            }

                            $revision = preg_replace('/[.]/', '111', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[$]/', '111', $revision);
                            
                            if(empty($all_text))
                                break;
                        }
                        //echo $revision."\n";
                        //Evaluamos si ya estamos en la misma linea para culminar
                        if($fila == $j){
                            break;
                        }
                    }
                    //echo "\n\n";
                //Fin evaluación posición del precio
                
                $linea_precio = trim(substr($all_text, 0, stripos($all_text,"\n")));
                if(str_contains($linea_precio, "$"))
                    $linea_precio = trim(substr($linea_precio, stripos($linea_precio,"$") + 1));
            }
        //Fin arreglo y toma del espacio donde aparece el precio digitado
        
        $precio = 0;
        $minp = 14;
        
        if($inline){
            $out = true;
            while($out){
                $revision = trim(substr($linea_precio, 0, stripos($linea_precio, " ")));
                $linea_precio = trim(substr($linea_precio, stripos($linea_precio, " ")));
                
                if(str_contains($revision, ".")){
                    if(preg_match('/^([1-9]\d*|0)(\.\d+)?$/', $revision, $matches)){
                        $linea_precio = $matches[0];
                        $out = false;
                    }
                }

                if(!strpos($linea_precio, ' '))
                    $out = false;
            }
        }

        while($minp >= 2) {
            $precio = substr($linea_precio, 0, $minp);
            $precio = preg_replace('/[,]/', '', $precio);
            if(is_numeric($precio)){
                break;
            }
            $minp--;
        }

        if(!is_numeric($precio)){ 
            $precio = 0;
            $this->state = "pendiente";
        }

        return $precio;
    }

    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = substr($content, stripos($content, "CHK"));
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);

        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            if(str_contains($productos, "TBL"))
                $productos = substr($productos, stripos($productos, 'TBL'));

            if(str_contains($productos, "GST"))
                $productos = substr($productos, stripos($productos, 'GST'));

            if(str_contains($productos, $this->date))
                $productos = substr($productos, stripos($productos, $this->date));

            if(str_contains($productos, 'DINE IN'))
                $productos = substr($productos, stripos($productos, 'DINE IN'));

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, " IVA"))
                $productos = substr($productos, 0, strrpos($productos, ' IVA'));

            if(str_contains($productos, "SUBTOTAL"))
                $productos = substr($productos, 0, strrpos($productos, 'SUBTOTAL'));

            if(str_contains($productos, "TOTAL"))
                $productos = substr($productos, 0, strrpos($productos, 'TOTAL'));
        //Fin limpiado

        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(!is_numeric($revision)){
                
                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break; 
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){ 
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad
                
                //Eliminamos el precio si esta en la misma linea
                    $precio = 0;
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    //Elimina los precios intercalados
                    while(preg_match('!\d{4,}!', $revision, $matches)){
                        $precio = $matches[0];
                        $revision = trim(preg_replace('/['.$precio.']/', '', $revision));
                    }

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){ 
                        $revision = str_replace($precio, '', $revision);
                    }
                //Fin eliminido el precio
                
                $nombre = trim($revision);

                //Limpiamos texto basura
                if(strlen($nombre) <= 4)
                    $nombre = "";
            
                if(str_contains($nombre, "WWW"))
                    $nombre = "";

                if($nombre && !str_contains($nombre, "PLATA") && !str_contains($nombre, "IMPORTE") && !str_contains($nombre, " IVA") && !str_contains($nombre, "TOTAL"))
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){
        
        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);
        
        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);
        
        //Sacamos el mesero
        $values->mesero = $this->mesero($content);
        
        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos($content, $values->ciudad);
        
        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        //$values->extras = $this->productosExtra($content);
        
        //Sacamos del vector de productos extras los productos pernod
        //$values->extras = $this->limpiandoExtra($values->productos, $values->extras);
        
        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
