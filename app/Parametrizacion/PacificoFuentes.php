<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos; 
use App\Models\Tarifario;

class PacificoFuentes
{

    public $state = "aceptado";
    
    public $referencia = "";
    public $mesero = "";
    public $date = "";
    public $horas = "";
    public $month = "";
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new PacificoFuentes();
    }
    
    //Sacamos la referencia del folio
    public function referenciaFolio($content){
        $folio = null;
        
        if(str_contains($content, "FOLIO") || str_contains($content, "FOLO")){
            if(str_contains($content, "FOLIO"))
                $ref_posicion = stripos($content, 'FOLIO') + strlen('FOLIO');
            else
                $ref_posicion = stripos($content, 'FOLO') + strlen('FOLO');

            $texto = substr($content, $ref_posicion);
            $chetexto = $texto;
            $texto = trim(substr($texto, 0, stripos($texto,"\n")));

            if(!preg_match('~[0-9]+~', $texto)){
                $texto = trim(substr($chetexto, stripos($chetexto,"\n") + 1));
                $texto = trim(substr($texto, 0, stripos($texto,"\n")));
            }
            
            if(preg_match('/\d+/', $texto, $matches)){
                $folio = $matches[0];
            }
            else{
                $texto = trim(substr($texto, stripos($texto,":") + 1));

                $minr = 7;
                while($minr > 2) {
                    $folio = trim(substr($texto, 0, $minr));
                    if(is_numeric($folio)){
                        break; 
                    }
                    $minr--;
                }
            }
        }
        else{
            $folio = "N/A";
        }

        //Si no posee solo números quiere decir que hubo un fallo
        if (!is_numeric($folio))
            $this->state = "pendiente";

        $this->referencia = $folio;

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){

        $mesero = null;

        if(str_contains($content, "MONTO")){
            $pos_final = stripos($content, 'MONTO') - 1;
            $texto = substr($content, 0, $pos_final);

            //Echamos tres espacios atras y pegamos todo
            $texto = substr($texto, 0, strrpos($texto, "\n")); //EL PRIMERO
            $texto = substr($texto, 0, strrpos($texto, "\n")); //EL SEGUNDO
            $texto = substr($texto, 0, strrpos($texto, "\n")); //EL TERCERO

            //Sacamos la porción del mesero
            $pos_inicial = strlen($texto);
            $mesero = substr($content, $pos_inicial, $pos_final - $pos_inicial);
            $mesero = trim(str_replace("\n", " ", $mesero));

            //Limpiamos números o textos cortos intercalados
            $mesero = trim(preg_replace("/[^A-Za-z ?]/", "", $mesero)); //Me quedo solo con el texto
            $correccion = "";
            foreach (explode(" ",$mesero) as $palabra) {
                if(strlen($palabra) >= 3 && $palabra != "PAX")
                    $correccion .= $palabra." ";
            }

            $mesero = trim($correccion);
        }
        elseif(str_contains($content, "PAX") || str_contains($content, "PA.")){

            if(str_contains($content, "PAX"))
                $pos_inicial = stripos($content, 'PAX') + strlen('PAX');
            else
                $pos_inicial = stripos($content, 'PA.') + strlen('PA.');

            $texto = substr($content, $pos_inicial);
            $texto = substr($texto, stripos($texto, "\n") + 1);
            
            //Echamos tres espacios adelante y pegamos todo
            $texto = substr($texto, stripos($texto, "\n") + 1); //EL PRIMERO
            $texto = substr($texto, stripos($texto, "\n") + 1); //EL SEGUNDO
            $texto = substr($texto, stripos($texto, "\n") + 1); //EL TERCERO
            $texto = substr($texto, 0, stripos($texto, "\n")); //CORTARLO
            
            //Sacamos la porción del mesero
            $pos_final = stripos($content, $texto);
            $mesero = substr($content, $pos_inicial, $pos_final - $pos_inicial);
            $mesero = trim(str_replace("\n", " ", $mesero));

            //Limpiamos números o textos cortos intercalados
            $mesero = trim(preg_replace("/[^A-Za-z ?]/", "", $mesero)); //Me quedo solo con el texto
            $correccion = "";
            foreach (explode(" ",$mesero) as $palabra) {
                if(strlen($palabra) >= 3 && $palabra != "PAX")
                    $correccion .= $palabra." ";
            }

            $mesero = trim($correccion);
        }
        else{
            $mesero = "No legible";
            $this->state = "pendiente";
        }

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";
        
        $this->mesero = $mesero;

        return trim($mesero);
    }

    //Sacamos la fecha con hora
    /* public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:i:s');

        if(str_contains($content, "FECHA") || str_contains($content, "FECA")){
            //referenica de busqueda
                if(str_contains($content, "FECHA"))
                    $referencia = "FECHA";
                else
                    $referencia = "FECA";
                
            //Segmento donde esta la linea de la fecha
                $texto = substr($content, stripos($content, $referencia));
                $texto = trim(substr($texto, stripos($texto, "\n") + 1));
                $fecha_texto = trim(substr($texto, 0, stripos($texto, "\n")));
                
                //Formato d/m/Y
                $this->date = $fecha_texto; 

            //Sacamos la hora de la misma
                $horas = trim(substr($texto, stripos($texto, "\n") + 1)); 
                $horas = trim(substr($horas, 0, stripos($horas, "\n"))); 
                if(str_contains($horas, "P. M") || str_contains($horas, "PM")){
                    $horas = str_replace(".", ":", $horas);
                    $horas = preg_replace("/[^0-9:!?]/", "", $horas);
                    
                    while(substr_count($horas, ":") > 2){
                        $horas = substr($horas, 0, strlen($horas) - 1);

                        if(empty($horas))
                            break;
                    }

                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i:s");
                    }
                    catch(\Exception $e){}
                }
                else{
                    $horas = str_replace(".", ":", $horas);
                    $horas = preg_replace("/[^0-9:!?]/", "", $horas);
                    
                    if(substr_count($horas, ":") > 2){
                        $horas = substr($horas, 0, strlen($horas) - 1);
                    }
                }

                //Revisamos la hora para que no tire fallo con la fecha
                $check_hora = DateTime::createFromFormat('H:i:s', $horas);
                if($check_hora && $check_hora->format('H:i:s') == $horas)
                    $horas = date("H:i:s", strtotime($horas));
                else
                    $horas = date("H:i:s");

            //Formateamos la fecha
                $minf = 15;
                while($minf > 6) {
                    $fecha = substr($fecha_texto, 0, $minf);

                    $fecha_check = str_replace('/', '', $fecha);
                    if(is_numeric($fecha_check)){
                        break; 
                    }
                    $minf--;
                }
            
            $fecha = $fecha." ".$horas;
        }
        else{
            $fecha = date('d-m-Y H:i:s');
        }

        //Corrección y validación de la fecha
            $fecha = str_replace('/', '-', $fecha);
            $chech_date = DateTime::createFromFormat('d-m-Y H:i:s', $fecha);
            if($chech_date && $chech_date->format('d-m-Y H:i:s') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->state = "pendiente";
            }

        return $fecha;
    } */

    public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:i:s');

        //Segmento donde esta la linea de la fecha
            $date_texto = $content;//trim(substr($content, stripos($content, $referencia) + strlen($referencia)));
            $fecha_texto = $date_texto;
            $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

            while(substr_count($revision, "/") < 2){
                $fecha_texto = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);
                $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

                if(empty($revision))
                    break;
            }
            
            //aqui abajo ya tengo la linea de la fecha ejemplo: d/m/Y
            $this->date = $revision;
            
        //Sacamos la fecha
            $texto = trim(preg_replace("/[^A-Za-zÍ:,. ?]/", "", $revision));
            $palabras = explode(" ", $texto);

            foreach($palabras as $palabra)
                $revision = trim(str_replace($palabra, "", $revision));
                
            if(str_contains($revision, " "))
                $revision = substr($revision, 0, stripos($revision, " "));

            $fecha = str_replace("/", "-", $revision);
            $fecha = trim(preg_replace("/[^0-9- ?]/", "", $fecha));
            $fecha = str_replace("-", "/", $fecha);
            
        //Sacamos la hora de la misma
            $horas = trim($date_texto);
            $revision = trim(substr($horas, 0, stripos($horas, "\n")));
            
            while(substr_count($revision, ":") < 2){
                $date_texto = substr($date_texto, stripos($date_texto, "\n") + 1);
                $revision = trim(substr($date_texto, 0, stripos($date_texto, "\n")));

                if(empty($revision))
                    break;
            }
            
            $this->horas = $revision;

            if(str_contains($revision, $fecha)){
                $revision = substr($revision, stripos($revision, $fecha));
                $revision = trim(str_replace($fecha, "", $revision));
            }
                
            $horas = $revision;
            
            if(str_contains($horas, "PM") || str_contains($horas, "P.M") || str_contains($horas, "P. M")){
                if(str_contains($horas, " "))
                    $horas = substr($horas, 0, stripos($horas, " "));

                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas));
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour != "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i:s");
                    }
                    catch(\Exception $e){}
                }
            }
            else{
                if(str_contains($horas, " "))
                    $horas = substr($horas, 0, stripos($horas, " "));

                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas)); 
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour == "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i:s");
                    }
                    catch(\Exception $e){}
                }
            }

            //Revisamos la hora para que no tire fallo con la fecha
            $check_hora = DateTime::createFromFormat('H:i:s', $horas);
            if($check_hora && $check_hora->format('H:i:s') == $horas)
                $horas = date("H:i:s", strtotime($horas));
            else
                $horas = date("H:i:s");
            
        //Formateamos la fecha
            $minf = 15;
            $fecha_texto = $fecha;
            while($minf > 6) {
                $fecha = substr($fecha_texto, 0, $minf);

                $fecha_check = str_replace('/', '', $fecha);
                if(is_numeric($fecha_check)){
                    break; 
                }
                $minf--;
            }

            $fecha = $fecha." ".$horas;
            
        //Corrección y validación de la fecha
            $fecha = str_replace('/', '-', $fecha);
            $chech_date = DateTime::createFromFormat('d-m-Y H:i:s', $fecha);
            if($chech_date && $chech_date->format('d-m-Y H:i:s') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

            //valido si la fecha es superorio a la actual
            $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
            $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
            //COMPARO LAS FECHAS
            if ($fecha > $DateAndTime){
                //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
                $fecha = $DateAndTime;
                $this->month = null;
                $this->state = "pendiente";
            }
            $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
            
        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        //marca pues graba en teoria el nombre real que maneja en la factura
        //linea es que tipo de licor es
        //formato es si lo maneja como botella o como copa

        switch ($marca) {
            case "ABSOLUT BLUE": 
                $referencia = array(
                    array(
                        "marca" => "C. ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C.ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2X1 ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C 2X I ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2X | ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2X I ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C2X I ABSOLUT",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "2X1 ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "B. ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "B.ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT EXTRAKT": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER DRY": 
                $referencia = array(
                    array(
                        "marca" => "C.BEEFEATER+",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. BEEFEATER+",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. BEEFEATER--",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. BEEFEATER --",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CBEEFEATER+",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2X BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2X | BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2X I BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2XI BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2KI BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2XT BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "2X I BEEFEATER+",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "2X1 BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C. 2K I BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "B. BEEFEATER+",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BEEFEATER\n",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER PINK": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER BLACKBERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 TEQUILA": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 SHERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "CHIVAS XV": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "THE GLENLIVET FOUNDERS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "HAVANA 7": 
                $referencia = array(
                    array(
                        "marca" => "C. HAVANA C",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "C HAVANA C",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "B. HAVANA C",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "B HAVANA C",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "MARTELL VSOP": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "BALLATINE'S FINEST": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "ALTOS PLATA": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "THE GLENLIVET 12": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 18": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CHIVAS REGAL 18",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "MARTELL BS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    )
                );
                break;
            case "ABSOLUT WATERMELON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "PECERA WATERMELON",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "PECERA\nWATERMELON",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "JAMESON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
            break;
        }

        return $referencia;
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();
        
        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto
            
            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);
            
            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {
                    
                    $cantidad = 10;
                    $contenido = $content;

                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto["marca"];
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO
                    
                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["referencia"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO
                    
                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){

                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                            
                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 7);
                            
                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 7);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }
                        
                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);

                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                if(empty($linea_marca))
                                    break;

                                $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            };
                            
                        //CANTIDAD DEL PRODUCTO
                            if($cantidad > 1){
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $new_content);
                            }
                            else
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $content);
                
                        //PRECIO DEL PRODUCTO
                            if($cantidad > 1){
                                $precio = $this->productoPrecio($linea_marca, $content, $new_content);
                            }
                            else
                                $precio = $this->productoPrecio($linea_marca, $content, $content);
                            
                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => substr($linea_marca, 0, stripos($linea_marca, "\n")),//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );

                        if(strlen($data["nombre"]) > 1 ){
                            array_push($check_product, $data);
                            array_push($return_productos, $data);
                        }
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $formato, $bdproducto, $ciudad, $content, $contenido){
        $cantidades = 0;
        $minc = 6;
        $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));

        $check_line = substr($linea_cantidad, 0, 4);

        //Si la cantidad no esta en la misma linea debemos ubicarlo
        if(!preg_match('/\d+/', $check_line, $matches)){
            $fila = 0;
            $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto
            
            if(str_contains($contenido, 'MONTO'))
                $ref_inicio = 'MONTO';
            else
                $ref_inicio = trim(substr($this->mesero, strrpos($this->mesero, " ")));
            
            //Evaluamos la posición del producto para luego sacar la del precio
                //texto a partir del producto hacia atras
                if(!str_contains($contenido, $producto))
                    $producto = substr($producto, 0, strrpos($producto, " "));

                $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                
                for($j = 0; $j < 60; $j++){
                    //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                    $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                    
                    //Limpieza por si lo que lee no es un producto
                    $revision = preg_replace('/[$]/', '', $revision);
                    $revision = preg_replace('/[,]/', '', $revision);
                    $revision = preg_replace('/[.]/', '', $revision);
                    $revision = preg_replace('/[@]/', '', $revision);
                    $revision = trim($revision);

                    //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                    while(strlen($revision) <= 5 || is_numeric(str_replace(" ", "", $revision)) || str_contains($revision, $ref_inicio) || str_contains($revision, "AGUA MINERAL C") || str_contains($revision, "AGLA MINERAL C") || str_contains($revision, "AGUA MINERALC") || str_contains($revision, "AGUA MINERAL B") || str_contains($revision, "AGUA MINERAL A") || str_contains($revision, "AGUA MINERAL OP") || str_contains($revision, "نعمل") || str_contains($revision, "AGUA QUINA") || str_contains($revision, "AGLA QUINA") || str_contains($revision, "DIAMANTE") || str_contains($revision, "SAPIRE") || str_contains($revision, "SAPHIRE+") || str_contains($revision, "HIELO") || ( str_contains($revision, "SERVICIO") && !str_contains($revision, "SERVICIO CHELADA") && !str_contains($revision, "SERVICIO CLA") && !str_contains($revision, "ZA SERVICIO") ) || str_contains($revision, "BOTELLA") || str_contains($revision, "BLANCO") || str_contains($revision, "CAPEADO") || str_contains($revision, "CAMARON CON") || str_contains($revision, "CEBOLLA") || str_contains($revision, "ARRACHE") || str_contains($revision, "DOMR") || $revision == "PACIFICO" || str_contains($revision, "CLAS CO") || str_contains($revision, "BUFFALO") || str_contains($revision, "PREMIUM") || str_contains($revision, "TIER:") || str_contains($revision, "TIERRA") || str_contains($revision, "EMBOTE") || str_contains($revision, "MX") || str_contains($revision, "ML+") || str_contains($revision, "COCA COLA") || str_contains($revision, "CRIS") || str_contains($revision, "GUACA") /* || str_contains($revision, "MARLIN") */ || str_contains($revision, "FRANCESA") || str_contains($revision, "5 NL") || str_contains($revision, "NL+") || str_contains($revision, "5 ML") || str_contains($revision, "SIN AD") || str_contains($revision, "CITRICOS") || str_contains($revision, "2 AGUA") /* || str_contains($revision, "3 AGUA MI") */ || str_contains($revision, "EME OTELLADA") || str_contains($revision, "MICHELADO 2") || str_contains($revision, "JUNTOS") || str_contains($revision, "CORONITT") || str_contains($revision, "VACCARI NERO") || str_contains($revision, "PACIFICA ") || str_contains($revision, "SALSA AP") || str_contains($revision, "ROCKEFELLER") || str_contains($revision, "AJILLO+") || (str_contains($revision, "MALIBU") && $revision != "PECERA MALIBU" && $revision != "1 PECERA MALIBU") || str_contains($revision, "RON CH") || str_contains($revision, "JUNTO ") || str_contains($revision, "CORONA+") || str_contains($revision, "330 ML") || str_contains($revision, "CA VICTORIA") || str_contains($revision, "C A VICTORIA") || str_contains($revision, "PLATINO+") || str_contains($revision, "AÑOS +") || str_contains($revision, "AÑOS+") || str_contains($revision, "AGUA MINERALB") || str_contains($revision, "LAMBOR") || str_contains($revision, "CHEESE") || str_contains($revision, "CUCUMBE") /* || str_contains($revision, " PULPO") */ || str_contains($revision, "ARANDANO") || str_contains($revision, "LEMC NADE") || str_contains($revision, "N KRAKEN") || str_contains($revision, "N GOBER") || str_contains($revision, "CON EL") || str_contains($revision, "ZARANDEADO") || str_contains($revision, "SIN FR") || str_contains($revision, "SHARINS") || str_contains($revision, "POCO T") || str_contains($revision, "JUNTOO") || str_contains($revision, "CAMPECHA") || str_contains($revision, "AHUN ADO") || str_contains($revision, "AHUMADO") || str_contains($revision, "RASPBERRY") || str_contains($revision, "CABEAR") || str_contains($revision, "/PULPO") || str_contains($revision, "CHILTEP") || str_contains($revision, "AQUA ") || str_contains($revision, "JOVEV") || str_contains($revision, "NARANJMA") || str_contains($revision, "MACERADO") || str_contains($revision, "FINEST") || str_contains($revision, "EMPANI") || str_contains($revision, "NARANJA 250") || str_contains($revision, "ம") || str_contains($revision, "யை") || str_contains($revision, "BIEN CO") || str_contains($revision, "NO TAR") || str_contains($revision, "ALÉRGICO") || $revision == "AJILLO" || $revision == "COCACOLA LIGHT" || $revision == "COCA COLA LIGHT" || $revision == "ALFREDO" || $revision == "SEPARADOS" || $revision == "MANGO+" || $revision == "MANOTAS" || $revision == "IMPERIAL" || $revision == "CORONA" || $revision == "PESCADO" || $revision == "CAMARON" || $revision == "SALMON" || $revision == "MANZANA" || $revision == "COLADA" || $revision == "GRANDE" || $revision == "QUEEN+" || $revision == "CHOCOLATE" || $revision == "REFRESCO" || $revision == "MANZANA OP" || $revision == "DE PULPO" || $revision == "AGUA"){
                            
                        if(str_contains($revision, $ref_inicio))
                            break;

                        if($revision == "BULL" || $revision == "BURRO")
                            break;

                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);

                        if(empty($all_text) || substr_count($all_text, "\n") < 1)
                            break;
                    }
                    //echo $revision."\n";
                    //Validamos que no contenga la palabra clave de TOTAL ya que 
                    //a partir de ahí empiezan los productos
                    if(!str_contains($revision, $ref_inicio)){
                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        
                        //Otra limpieza por si hay un salto de linea intercalado
                        if(str_contains(substr($all_text, -2), "\n"))
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        
                        $fila++;
                    }
                    else
                        break;
                }
                //echo "\n\n";
                //return $fila;
            //Fin evaluar la fila del producto
            
            //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                //Generalmente a partir de donde sale la REF_FOLIO es que estan los valores de los precios
                //en estas facturas y tomamos el texto a partir de ahí hasta el final
                $all_text = substr($content, stripos($content, $ref_inicio));
                $all_text = str_replace("T", "1", $all_text);
                //$all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                
                for($j = 1; $j < 60; $j++){
                    //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                    //if($j > 1)
                        $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                    
                    //Revisamos la linea
                    $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                    if(str_contains($revision, " ")){
                        //if(substr_count($revision, " ") > 1)
                            $revision = trim(substr($revision, 0, stripos($revision, " ")));
                        /* else
                            $revision = trim(substr($revision, stripos($revision, " "))); */
                    }

                    $revision = str_replace('$', 'AAA', $revision);
                    $revision = str_replace(',', '', $revision);
                    $revision = str_replace('|', '', $revision);

                    //Convalidamos que no hayan saltos de linea a travesados
                    while(str_contains(substr($revision, 0, 2), "\n") || !is_numeric($revision) || strlen($revision) >= 2){
                        if(str_contains($revision, " "))
                            $all_text = trim(substr($all_text, stripos($all_text, " ")));
                        else
                            $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                        
                        //Re-Revisamos la linea
                        $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                        if(str_contains($revision, " ")){
                            //if(substr_count($revision, " ") > 1)
                                $revision = trim(substr($revision, 0, stripos($revision, " ")));
                            /* else
                                $revision = trim(substr($revision, stripos($revision, " "))); */
                        }

                        $revision = str_replace('$', 'AAA', $revision);
                        $revision = str_replace(',', '', $revision);
                        $revision = str_replace('|', '', $revision);

                        if(empty($all_text))
                            break;
                    }
                    //echo $revision."\n";
                    //Evaluamos si ya estamos en la misma linea para culminar
                    if($fila == $j){
                        break;
                    }
                }
                //echo "\n\n";
            //Fin evaluación posición del precio
            
            $linea_cantidad = substr($all_text, 0, stripos($all_text,"\n"));
        }
        
        if(str_contains($linea_cantidad, "C. 2")){
            $linea_cantidad = trim(str_replace("C. 2", "", $linea_cantidad));
        }

        while($minc > 0) {
            $cantidades = trim(substr($linea_cantidad, 0, $minc));
            if(is_numeric($cantidades)){
                break; 
            }
            $minc--;
        }

        if(!is_numeric($cantidades)){ 
            $cantidades = 1;
            $this->state = "pendiente";
        }

        if($cantidades >= 50){
            $this->state = "pendiente";
        }

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();
            
            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");
                        
                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01"); 
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }
                        
                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes); 

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }

        return $cantidades;
    }

    //Proceso de sacar precio del Producto
    public function productoPrecio($linea_marca, $content, $contenido){
        //Revisemos si el precio esta en la misma linea del producto
        $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
        $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
        $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea_producto;
        //Arriba ya tenemos la linea del producto con su precio si hipoteticamente lo tiene
        
        //Arreglo y toma del espacio donde aparece el precio digitado
            $check_line = preg_replace('/[,]/', '1', $linea_producto);
            $check_line = preg_replace('/[.]/', '1', $check_line);
            
            //Reviso que exista un secuencia de 4 números consecutivos d{4,}
            preg_match('!\d{4,}!', $check_line, $matches);
            $inline = true;
            
            //$matches[0] -> hubo coincidencia | is_numeric(que esa coincidencia sea numerica)
            //$matches[0] >= 4 que sea mayor a 4 digitos
            if(isset($matches[0]) && is_numeric($matches[0]) && strlen(trim($matches[0])) >= 4){
                //TIENE EL PRECIO EN LA MISMA LINEA
                if(str_contains($linea_marca, "$")){
                    $linea_precio = substr($linea_marca, stripos($linea_marca,"$") + 1);
                    $inline = false;
                }
                else
                    $linea_precio = preg_replace('/[,]/', '', $linea_producto);
            }
            else{
                //NO TIENE EL PRECIO EN LA MISMA LINEA POR LO QUE DEBEMOS UBICARLO
                $inline = false;
                $fila = 0;
                $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto
                
                if(str_contains($contenido, 'MONTO'))
                    $ref_inicio = 'MONTO';
                else
                    $ref_inicio = trim(substr($this->mesero, strrpos($this->mesero, " ")));
                
                //Evaluamos la posición del producto para luego sacar la del precio
                    //texto a partir del producto hacia atras
                    if(str_contains($producto, "+"))
                        $producto = trim(str_replace("+", "", $producto));

                    $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                    
                    for($j = 0; $j < 60; $j++){
                        //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        
                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);

                        //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                        while(strlen($revision) <= 5 || is_numeric(str_replace(" ", "", $revision)) || str_contains($revision, $ref_inicio) || str_contains($revision, "AGUA MINERAL C") || str_contains($revision, "AGLA MINERAL C") || str_contains($revision, "AGUA MINERALC") || str_contains($revision, "AGUA MINERAL B") || str_contains($revision, "AGUA MINERAL A") || str_contains($revision, "AGUA MINERAL OP") || str_contains($revision, "نعمل") || str_contains($revision, "AGUA QUINA") || str_contains($revision, "AGLA QUINA") || str_contains($revision, "DIAMANTE") || str_contains($revision, "SAPIRE") || str_contains($revision, "SAPHIRE+") || str_contains($revision, "HIELO") || ( str_contains($revision, "SERVICIO") && !str_contains($revision, "SERVICIO CHELADA") && !str_contains($revision, "SERVICIO CLA") && !str_contains($revision, "ZA SERVICIO") ) || str_contains($revision, "BOTELLA") || str_contains($revision, "BLANCO") || str_contains($revision, "CAPEADO") || str_contains($revision, "CAMARON CON") || str_contains($revision, "CEBOLLA") || str_contains($revision, "ARRACHE") || str_contains($revision, "DOMR") || $revision == "PACIFICO" || str_contains($revision, "CLAS CO") || str_contains($revision, "BUFFALO") || str_contains($revision, "PREMIUM") || str_contains($revision, "TIER:") || str_contains($revision, "TIERRA") || str_contains($revision, "EMBOTE") || str_contains($revision, "MX") || str_contains($revision, "ML+") || str_contains($revision, "COCA COLA") || str_contains($revision, "CRIS") || str_contains($revision, "GUACA") /* || str_contains($revision, "MARLIN") */ || str_contains($revision, "FRANCESA") || str_contains($revision, "5 NL") || str_contains($revision, "NL+") || str_contains($revision, "5 ML") || str_contains($revision, "SIN AD") || str_contains($revision, "CITRICOS") || str_contains($revision, "2 AGUA") /* || str_contains($revision, "3 AGUA MI") */ || str_contains($revision, "EME OTELLADA") || str_contains($revision, "MICHELADO 2") || str_contains($revision, "JUNTOS") || str_contains($revision, "CORONITT") || str_contains($revision, "VACCARI NERO") || str_contains($revision, "PACIFICA ") || str_contains($revision, "SALSA AP") || str_contains($revision, "ROCKEFELLER") || str_contains($revision, "AJILLO+") || (str_contains($revision, "MALIBU") && $revision != "PECERA MALIBU" && $revision != "1 PECERA MALIBU") || str_contains($revision, "RON CH") || str_contains($revision, "JUNTO ") || str_contains($revision, "CORONA+") || str_contains($revision, "330 ML") || str_contains($revision, "CA VICTORIA") || str_contains($revision, "C A VICTORIA") || str_contains($revision, "PLATINO+") || str_contains($revision, "AÑOS +") || str_contains($revision, "AÑOS+") || str_contains($revision, "AGUA MINERALB") || str_contains($revision, "LAMBOR") || str_contains($revision, "CHEESE") || str_contains($revision, "CUCUMBE") || str_contains($revision, "N KRAKEN") /* || str_contains($revision, " PULPO") */ || str_contains($revision, "ARANDANO") || str_contains($revision, "LEMC NADE") || str_contains($revision, "N GOBER") || str_contains($revision, "ZARANDEADO") || str_contains($revision, "CON EL") || str_contains($revision, "SIN FR") || str_contains($revision, "SHARINS") || str_contains($revision, "POCO T") || str_contains($revision, "JUNTOO") || str_contains($revision, "CAMPECHA") || str_contains($revision, "AHUN ADO") || str_contains($revision, "AHUMADO") || str_contains($revision, "RASPBERRY") || str_contains($revision, "CABEAR") || str_contains($revision, "/PULPO") || str_contains($revision, "CHILTEP") || str_contains($revision, "AQUA ") || str_contains($revision, "JOVEV") || str_contains($revision, "NARANJMA") || str_contains($revision, "MACERADO") || str_contains($revision, "FINEST") || str_contains($revision, "EMPANI") || str_contains($revision, "NARANJA 250") || str_contains($revision, "ம") || str_contains($revision, "யை") || str_contains($revision, "BIEN CO") || str_contains($revision, "NO TAR") || str_contains($revision, "ALÉRGICO") || $revision == "AJILLO" || $revision == "COCACOLA LIGHT" || $revision == "COCA COLA LIGHT" || $revision == "ALFREDO" || $revision == "SEPARADOS" || $revision == "MANGO+" || $revision == "MANOTAS" || $revision == "IMPERIAL" || $revision == "CORONA" || $revision == "PESCADO" || $revision == "CAMARON" || $revision == "SALMON" || $revision == "MANZANA" || $revision == "COLADA" || $revision == "GRANDE" || $revision == "QUEEN+" || $revision == "CHOCOLATE" || $revision == "REFRESCO" || $revision == "MANZANA OP" || $revision == "DE PULPO" || $revision == "AGUA"){
                        
                            if(str_contains($revision, $ref_inicio))
                                break;
                                
                            if($revision == "BULL" || $revision == "AGUA" || $revision == "BURRO")
                                break;

                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                            //Limpieza por si lo que lee no es un producto
                            $revision = preg_replace('/[$]/', '', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[.]/', '', $revision);
                            $revision = preg_replace('/[@]/', '', $revision);
                            $revision = trim($revision);

                            if(empty($all_text))
                                break;
                        }
                        //echo $revision."\n";
                        //Validamos que no contenga la palabra clave de TOTAL ya que 
                        //a partir de ahí empiezan los productos
                        if(!str_contains($revision, $ref_inicio)){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            
                            //Otra limpieza por si hay un salto de linea intercalado
                            if(str_contains(substr($all_text, -2), "\n"))
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            
                            $fila++;
                        }
                        else
                            break;
                    }
                    //echo "\n\n";
                    //return $fila;
                //Fin evaluar la fila del producto
                
                //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                    //Generalmente a partir de donde sale la REF_FOLIO es que estan los valores de los precios
                    //en estas facturas y tomamos el texto a partir de ahí hasta el final

                    $all_text = substr($content, stripos($content, $ref_inicio));
                    
                    for($j = 1; $j < 60; $j++){
                        //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                        $all_text = substr($all_text, stripos($all_text, "\n") + 1);
                        
                        //Revisamos la linea
                        $revision = substr($all_text, 0, stripos($all_text, "\n"));
                        if(str_contains($revision, "$")){
                            $revision = trim(substr($revision, stripos($revision, "$")));
                            $revision = str_replace(" ", "", $revision);
                        }

                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[$]/', '111', $revision);
                        $revision = preg_replace('/[|]/', '', $revision);

                        //Convalidamos que no hayan saltos de linea a travesados
                        while(str_contains(substr($revision, 0, 3), "\n") || !is_numeric($revision) || strlen($revision) <= 5/*  || $revision == "1112300" */){
                            $all_text = substr($all_text, stripos($all_text, "\n") + 1);
                            
                            //Re-Revisamos la linea
                            $revision = substr($all_text, 0, stripos($all_text, "\n"));
                            if(str_contains($revision, "$")){
                                $revision = trim(substr($revision, stripos($revision, "$")));
                                $revision = str_replace(" ", "", $revision);
                            }

                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[.]/', '', $revision);
                            $revision = preg_replace('/[$]/', '111', $revision);
                            $revision = preg_replace('/[|]/', '', $revision);

                            if(empty($revision))
                                break;
                        }
                        
                        //Evaluamos si ya estamos en la misma linea para culminar
                        if($fila == $j){
                            break;
                        }
                    }
                //Fin evaluación posición del precio
                
                $linea_precio = substr($all_text, 0, stripos($all_text,"\n"));
                if(str_contains($linea_precio, "$"))
                    $linea_precio = substr($linea_precio, stripos($linea_precio,"$") + 1);
                
            }
        //Fin arreglo y toma del espacio donde aparece el precio digitado
        
        $precio = 0;
        $minp = 14;
        
        if($inline){
            $out = true;
            while($out){
                $revision = trim(substr($linea_precio, 0, stripos($linea_precio, " ")));
                $linea_precio = trim(substr($linea_precio, stripos($linea_precio, " ")));
                
                if(str_contains($revision, ".")){
                    if(preg_match('/^([1-9]\d*|0)(\.\d+)?$/', $revision, $matches)){
                        $linea_precio = $matches[0];
                        $out = false;
                    }
                }

                if(!strpos($linea_precio, ' '))
                    $out = false;
            }
        }

        //Sacando el precio
        while($minp > 2) {
            $precio = substr($linea_precio, 0, $minp);
            $precio = preg_replace('/[,]/', '', $precio);
            $precio = preg_replace('/[|]/', '', $precio);
            $precio = trim($precio);

            if(is_numeric($precio)){
                break;
            }
            $minp--;
        }

        if(!is_numeric($precio)){ 
            $precio = 0;
            $this->state = "pendiente";
        }

        return $precio;
    }

    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = substr($content, stripos($content, $this->date));
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);
        
        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            if(str_contains($productos, "MONTO"))
                $productos = substr($productos, stripos($productos, 'MONTO'));

            if(str_contains($productos, trim(substr($this->mesero, strrpos($this->mesero, " ")))))
                $productos = substr($productos, stripos($productos, trim(substr($this->mesero, strrpos($this->mesero, " ")))));

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, "TOTAL"))
                $productos = substr($productos, 0, stripos($productos, 'TOTAL') + strlen('TOTAL'));

            if(str_contains($productos, "IVA"))
                $productos = substr($productos, 0, strrpos($productos, 'IVA') + strlen('IVA'));
            

        //Fin limpiado
        
        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(str_contains($revision, "@"))
                $revision = substr($productos, 0, stripos($productos, "@"));

            if(!is_numeric($revision)){
                
                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break; 
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){ 
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad
                
                //Eliminamos el precio si esta en la misma linea
                    $precio = 0;
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    //Elimina los precios intercalados
                    while(preg_match('/\d+/', $revision, $matches)){
                        $precio = $matches[0];
                        $revision = trim(preg_replace('/['.$precio.']/', '', $revision));
                    }

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){ 
                        $revision = preg_replace('/['.$precio.']/', '', $revision);
                    }
                //Fin eliminido el precio
                
                $nombre = trim($revision);

                //Limpiamos texto basura
                    if(strlen($nombre) <= 4)
                        $nombre = "";
                    
                    if(str_contains($nombre, "WWW"))
                        $nombre = "";

                if($nombre && !str_contains($nombre, "TOTAL") && !str_contains($nombre, "IVA"))
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){
        
        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);
        
        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);
        
        //Sacamos el mesero
        $values->mesero = $this->mesero($content);
        
        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos(str_replace(array('(', ')'), '', $content), $values->ciudad);
        
        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        $values->extras = $this->productosExtra($content);
        
        //Sacamos del vector de productos extras los productos pernod
        $values->extras = $this->limpiandoExtra($values->productos, $values->extras);
        
        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
?>