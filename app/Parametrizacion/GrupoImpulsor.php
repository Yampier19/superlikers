<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos;
use App\Models\Tarifario;

class GrupoImpulsor
{

    public $state = "aceptado";
    public $referencia = "";
    public $mesero = "";
    public $date = "";
    public $horas = "";
    public $month = "";
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new GrupoImpulsor();
    }

    //Sacamos la referencia del folio
    public function referenciaFolio($content){
        $folio = null;
        
        $folio = "N/A";

        $this->referencia = $folio;

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){

        $mesero = null;

        $mesero = "N/A";

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";
        
        $this->mesero = $mesero;

        return $mesero;
    }

    //Sacamos la fecha con hora
    public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:i:s');

        //Segmento donde esta la linea de la fecha
            $date_texto = trim(substr($content, stripos($content, $referencia) + strlen($referencia)));
            $fecha_texto = $date_texto;
            $revision = substr($fecha_texto, strrpos($fecha_texto, "\n") + 1);

            while(substr_count($revision, "/") < 2){
                $fecha_texto = substr($fecha_texto, 0, strrpos($fecha_texto, "\n"));
                $revision = substr($fecha_texto, strrpos($fecha_texto, "\n") + 1);

                if(empty($revision))
                    break;
            }
            
            //aqui abajo ya tengo la linea de la fecha ejemplo: d/m/Y
            $this->date = $revision;
       
        //Sacamos la fecha
            $texto = trim(preg_replace("/[^A-Za-z ?]/", "", $revision));
            $palabras = explode(" ", $texto);

            foreach($palabras as $palabra)
                $revision = trim(str_replace($palabra, "", $revision));

            if(str_contains($revision, " "))
                $revision = substr($revision, 0, stripos($revision, " "));

            $fecha = str_replace("/", "-", $revision);
            $fecha = trim(preg_replace("/[^0-9- ?]/", "", $fecha));
            $fecha = str_replace("-", "/", $fecha);
            
        //Sacamos la hora de la misma
            $horas = trim($date_texto);
            $revision = substr($horas, strrpos($horas, "\n") + 1);
            
            while(substr_count($revision, ":") < 2){
                $date_texto = substr($date_texto, 0, strrpos($date_texto, "\n"));
                $revision = substr($date_texto, strrpos($date_texto, "\n") + 1);

                if(empty($revision))
                    break;
            }
            
            if(str_contains($revision, $fecha)){
                $revision = substr($revision, stripos($revision, $fecha));
                $revision = trim(str_replace($fecha, "", $revision));
            }
                
            $horas = $revision;
            $this->horas = $horas;
            
            if(str_contains($horas, "PM") || str_contains($horas, "P.M") || str_contains($horas, "P. M")){
                if(str_contains($horas, " "))
                    $horas = substr($horas, 0, stripos($horas, " "));

                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas));
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour != "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i:s");
                    }
                    catch(\Exception $e){}
                }
            }
            else{
                if(str_contains($horas, " "))
                    $horas = substr($horas, 0, stripos($horas, " "));

                $horas = trim(preg_replace("/[^0-9:?]/", "", $horas)); 
                $first_hour = substr($horas, 0 ,stripos($horas, ":"));
                if($first_hour == "12"){
                    try{
                        $date = new DateTime($horas);
                        $date->modify("+12 hours");
                        $horas = $date->format("H:i:s");
                    }
                    catch(\Exception $e){}
                }
            }

            //Revisamos la hora para que no tire fallo con la fecha
            $check_hora = DateTime::createFromFormat('H:i:s', $horas);
            if($check_hora && $check_hora->format('H:i:s') == $horas)
                $horas = date("H:i:s", strtotime($horas));
            else
                $horas = date("H:i:s");
            
        //Formateamos la fecha
            $minf = 15;
            $fecha_texto = $fecha;
            while($minf > 6) {
                $fecha = substr($fecha_texto, 0, $minf);

                $fecha_check = str_replace('/', '', $fecha);
                if(is_numeric($fecha_check)){
                    break; 
                }
                $minf--;
            }

            $fecha = $fecha." ".$horas;
            
        //Corrección y validación de la fecha
            $fecha = str_replace('/', '-', $fecha);
            $chech_date = DateTime::createFromFormat('d-m-Y H:i:s', $fecha);
            if($chech_date && $chech_date->format('d-m-Y H:i:s') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

            //valido si la fecha es superorio a la actual
            $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
            $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
            //COMPARO LAS FECHAS
            if ($fecha > $DateAndTime){
                //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
                $fecha = $DateAndTime;
                $this->month = null;
                $this->state = "pendiente";
            }
            $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
            
        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        //marca pues graba en teoria el nombre real que maneja en la factura
        //linea es que tipo de licor es
        //formato es si lo maneja como botella o como copa

        switch ($marca) {
            case "ABSOLUT BLUE": 
                $referencia = array(
                    array(
                        "marca" => "VK COPA ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "VK ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "WK COPA ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT AZUL (COPA",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT AZUL (C",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "B.TABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "B.T ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BSTADSLLUT ALL",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "ABSOLUT AZUL (BOTELLA",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "ABSOLUT AZUL (B",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT EXTRAKT": 
                $referencia = array(
                    array(
                        "marca" => "VK BOTELLA ABSOLUT MANGO",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOTELLA ABSOLUT MANGO",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "ABSOLUT EXTRACK (BOTELLA",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "ABSOLUT EXTRACK (B",
                        "linea" => "VODKA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "VK COPA ABSOLUT MANGO",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA ABSOLUT MANGO",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT EXTRACK (COPA",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT EXTRACK (C",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER DRY": 
                $referencia = array(
                    array(
                        "marca" => "GB COPA BEFFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BEFFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BEEFEATER (COPA",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BEEFEATER (C",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BEEFEATER (BOTELLA",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BEEFEATER (B",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER PINK": 
                $referencia = array(
                    array(
                        "marca" => "GB COPA BEEFEATER PINK",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BEEFEATER PINK",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BEEFEATER PINK (C",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BEEFEATER PINK (BOTELLA",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BEEFEATER PINK (B",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER BLACKBERRY": 
                $referencia = array(
                    array(
                        "marca" => "GB COPA BEEFEATER BLACKBERRY",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BEEFEATER BLACKBERRY",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 TEQUILA": 
                $referencia = array(
                    array(
                        "marca" => "WK COPA CHIVAS 13 TEQUILA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => " COPA CHIVAS 13 TEQUILA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA CHIVAS 13 TEQUILA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOT. CHIVAS 13 TEQUILA",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOT CHIVAS 13 TEQUILA",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 SHERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS XV": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "THE GLENLIVET FOUNDERS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "HAVANA 7": 
                $referencia = array(
                    array(
                        "marca" => "RN COPA HAVANA 7",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => " COPA HAVANA 7",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "HAVANA 7 (C",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "RN BOTELLA HAVANA 7",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOTELLA HAVANA 7",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "HAVANA 7 AÑOS (BO",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "HAVANA 7 AFOS (BO",
                        "linea" => "RON",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "MARTELL VSOP": 
                $referencia = array(
                    array(
                        "marca" => "MT COPA MARTELL VS OP",
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "MT COPA MARTELL VS",
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA MARTELL VS OP",
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA MARTELL VSOP",
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CG MARTELL V.S.O.P",
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CG MARTELL",
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CG BOTELLA MARTELL V.S.O.P",
                        "linea" => "COÑAC",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "CG BOTELLA MARTELL",
                        "linea" => "COÑAC",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    ),
                );
                break;
            case "BALLATINE'S FINEST": 
                $referencia = array(
                    array(
                        "marca" => "B.TBALLANTINES FINETS",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "TBALLANTINES FINETS",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "B.TBALLANTINES",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "WK BOTELLA BALLANTINES",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOTELLA BALLANTINES",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "04 BALLATINES FINEST",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "75 WH BALLANTINES FINEST",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BALLANTINES FINEST (BOTELLA",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BALLANTINES FINEST (B",
                        "linea" => "WHISKY",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "WK COPA BALLANTINES",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BALLANTINES CORTESIA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BALLANTINES X 2",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA BALLANTINES",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "WK CUPA BALLANTINES 2X1",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CUPA BALLANTINES 2X1",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BALLANTINES FINEST (COPA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BALLANTINES FINEST (C",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ALTOS PLATA": 
                $referencia = array(
                    array(
                        "marca" => "TQ BOTELLA ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "BOTELLA ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "56 TQ ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => "34 TQ ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "TQ COPA ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "TO COPA ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA ALTOS PLATA X2",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COPA ALTOS PLATA",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "TQ ALTOS PLATA 2X1",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ALTOS PLATA 2X1",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ALTOS PLATA (C",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "$marca",
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "THE GLENLIVET 12": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "CHIVAS 18": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "MARTELL BS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT WATERMELON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "JAMESON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
            break;
        }

        return $referencia;
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();
        
        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto
            
            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);
            
            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {
                    
                    $cantidad = 1;
                    $contenido = $content;

                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto["marca"];
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO
                    
                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["nombre"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO
                    
                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){

                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);

                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 7);

                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 7);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 7;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }

                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);

                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                if(empty($linea_marca))
                                    break;

                                $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            };

                        //CANTIDAD DEL PRODUCTO
                            if($cantidad > 1){
                                $cantidades = $this->productoCantidad($linea_marca, $producto["marca"], $producto["formato"], $product, $ciudad, $content, $new_content);
                            }
                            else
                                $cantidades = $this->productoCantidad($linea_marca, $producto["marca"], $producto["formato"], $product, $ciudad, $content, $content);
                        
                        //PRECIO DEL PRODUCTO
                            $precio = 0;
                            
                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => substr($linea_marca, 0, stripos($linea_marca, "\n")),//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );
                        array_push($check_product, $data);
                        array_push($return_productos, $data);
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $nombre, $formato, $bdproducto, $ciudad, $content, $contenido){
        $cantidades = 0;
        $minc = 6;
        $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));

        //Ajuste de la cantidad para este tipo de factura
            $linea_cantidad = substr($linea_cantidad, stripos($linea_cantidad, $nombre) + strlen($nombre));
            $linea_cantidad = str_replace("TORRES 10", "", $linea_cantidad);
            $linea_cantidad = str_replace("DON JULIO 70", "", $linea_cantidad);
            $linea_cantidad = str_replace("BUCHANANS 12", "", $linea_cantidad);
            $linea_cantidad = str_replace("BUCHANANHS 12", "", $linea_cantidad);
            $linea_cantidad = str_replace("BUCHANANS 18", "", $linea_cantidad);
            $linea_cantidad = str_replace("LICOR 43", "", $linea_cantidad);
            $linea_cantidad = str_replace("BEYLIS 6. OZ", "", $linea_cantidad);
            $linea_cantidad = str_replace("BEYLIS 6.OZ", "", $linea_cantidad);
            $linea_cantidad = str_replace("HAVANA 7", "", $linea_cantidad);
            $linea_cantidad = str_replace("ESQUITE 1528", "", $linea_cantidad);
            $linea_cantidad = str_replace("REGAL 12", "", $linea_cantidad);
            $linea_cantidad = str_replace("STATE 12", "", $linea_cantidad);
            $linea_cantidad = str_replace("ARIO 23", "", $linea_cantidad);
            $linea_cantidad = str_replace("HAVANA 3", "", $linea_cantidad);
            $linea_cantidad = str_replace("X 2", "", $linea_cantidad);
            $linea_cantidad = str_replace("2 X 160", "", $linea_cantidad);
            $linea_cantidad = str_replace("X150", "", $linea_cantidad);
            $linea_cantidad = str_replace("C/GUSA", "", $linea_cantidad);
            $linea_cantidad = str_replace("C/GUS", "", $linea_cantidad);
            $linea_cantidad = str_replace("SPICED (BOTELLA", "", $linea_cantidad);
            $linea_cantidad = str_replace("CLARO (COP", "", $linea_cantidad);

            $palabras = trim(preg_replace("/[^A-Za-z: ?]/", "", $linea_cantidad));
            $palabras = explode(" ", $palabras);

            foreach($palabras as $palabra)
                $linea_cantidad = trim(str_replace($palabra, "", $linea_cantidad));
            
            if(str_contains($linea_cantidad, " ")){
                $linea_cantidad = trim(substr($linea_cantidad, strrpos($linea_cantidad, " ")));
            }
        //Fin Ajuste
        $linea_cantidad = trim(str_replace("-", "", $linea_cantidad));
        $linea_cantidad = trim(str_replace("*", "", $linea_cantidad));
        $linea_cantidad = trim(str_replace("#", "", $linea_cantidad));
        $linea_cantidad = trim(str_replace(".", "", $linea_cantidad));
            
        $check_line = substr($linea_cantidad, 0, 4);
        
        //Revisamos si dos lineas por debajo de $linea_producto hay mas precios
            $ultimo_pro = false;
            //Linea 1
            $linea1 = substr($linea_marca, stripos($linea_marca, "\n") + 1);
            $linea1 = substr($linea1, 0, stripos($linea1, "\n"));
            if(str_contains($linea1, " "))
                $linea1 = trim(substr($linea1, strrpos($linea1, " ")));

            //Linea 2
            $linea2 = substr($linea_marca, stripos($linea_marca, $linea1) + strlen($linea1) + 1);
            $linea2 = substr($linea2, 0, stripos($linea2, "\n"));
            if(str_contains($linea2, " "))
                $linea2 = trim(substr($linea2, strrpos($linea2, " ")));

            //Ultimas dos lineas
            $l_first = substr($linea_marca, 0, stripos($linea_marca, "\n"));
            if(str_contains($l_first, " "))
                $l_first = trim(substr($l_first, strrpos($l_first, " ")));

            $linea_ultimo = $l_first." ".$linea1." ".$linea2;
            
            if(substr_count($linea_ultimo, ".") > 1)
                $ultimo_pro = true;
            
        //Si la cantidad no esta en la misma linea debemos ubicarlo
        if(!preg_match('/\d+/', $check_line, $matches) || $ultimo_pro){
            $fila = 0;
            $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto
            
            if(str_contains($contenido, 'CRIPCI')){
                $ref_inicio = 'CRIPCI';
            }
            elseif(str_contains($contenido, 'CANTI')){
                $ref_inicio = 'CANTI';
            }
            else
                $ref_inicio = 'CHAPU';
            
            //Evaluamos la posición del producto para luego sacar la del precio
                //texto a partir del producto hacia atras
                if(!str_contains($contenido, $producto))
                    $producto = substr($producto, 0, strrpos($producto, " "));

                $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                
                for($j = 0; $j < 150; $j++){
                    //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                    $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                    
                    //Limpieza por si lo que lee no es un producto
                    $revision = preg_replace('/[$]/', '', $revision);
                    $revision = preg_replace('/[,]/', '', $revision);
                    $revision = preg_replace('/[.]/', '', $revision);
                    $revision = preg_replace('/[@]/', '', $revision);
                    $revision = trim($revision);

                    //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                    while(strlen($revision) <= 5 || is_numeric($revision) || str_contains($revision, $ref_inicio) || str_contains($revision, "CRIPCI") || str_contains($revision, "CANTI") || str_contains($revision, "CHAPU") || str_contains($revision, "09 BOT") || str_contains($revision, "10 COP") || str_contains($revision, "14 COP") || str_contains($revision, "14 TEQ") || str_contains($revision, "45 PER") || str_contains($revision, "86 PERN") || str_contains($revision, "6 PERI") || str_contains($revision, "22 BRA") || str_contains($revision, "35 CUB") || str_contains($revision, "87 JUN") || str_contains($revision, "600 PRO") || str_contains($revision, "60 SNA") || str_contains($revision, "16 VOD") || str_contains($revision, "02 WHI") || str_contains($revision, "LICORE") || str_contains($revision, " SLEV") || str_contains($revision, "HUMBER") || str_contains($revision, "STORVEN") || str_contains($revision, "RINCÓN") || str_contains($revision, "W-NW") || substr_count($revision, "/") > 1 || substr_count($revision, ":") > 1){
                        
                        if(str_contains($revision, $ref_inicio))
                            break;

                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);

                        if(empty($revision))
                            break;
                    }
                    //echo $revision."\n";
                    //Revision de que venga V.TRIP y arreglar el espaciado
                    /* if(str_contains($revision, "AGUA")){
                        $checking = substr($all_text, 0, strrpos($all_text,"\n"));
                        $revision = substr($checking, strrpos($checking,"\n") + 1);

                        if(str_contains($revision, "V.TRIP")){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        }
                    } */

                    //Validamos que no contenga la palabra clave de TOTAL ya que 
                    //a partir de ahí empiezan los productos
                    if(!str_contains($revision, $ref_inicio)){
                        $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                        
                        //Otra limpieza por si hay un salto de linea intercalado
                        if(str_contains(substr($all_text, -2), "\n"))
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                        $fila++;
                    }
                    else
                        break;
                }
                //echo "\n\n";
                //return $fila;
            //Fin evaluar la fila del producto
            
            //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                //Generalmente a partir de donde sale la REF_FOLIO es que estan los valores de los precios
                //en estas facturas y tomamos el texto a partir de ahí hasta el final
                $all_text = substr($content, stripos($content, $ref_inicio));
                //$all_text = trim(preg_replace('#[^0-9$ \n\.,]#', "", $all_text));
                
                //eliminamos de la cadena los nombres con su texto o valor
                $all_text = str_replace("TORRES 10", "", $all_text);
                $all_text = str_replace("DON JULIO 70", "", $all_text);
                $all_text = str_replace("BUCHANANS 12", "", $all_text);
                $all_text = str_replace("BUCHANANHS 12", "", $all_text);
                $all_text = str_replace("BUCHANANS 18", "", $all_text);
                $all_text = str_replace("LICOR 43", "", $all_text);
                $all_text = str_replace("BEYLIS 6. OZ", "", $all_text);
                $all_text = str_replace("BEYLIS 6.OZ", "", $all_text);
                $all_text = str_replace("HAVANA 7", "", $all_text);
                $all_text = str_replace("ESQUITE 1528", "", $all_text);
                $all_text = str_replace("REGAL 12", "", $all_text);
                $all_text = str_replace("STATE 12", "", $all_text);
                $all_text = str_replace("ARIO 23", "", $all_text);
                $all_text = str_replace("HAVANA 3", "", $all_text);
                $all_text = str_replace("X 2", "", $all_text);
                $all_text = str_replace("2 X 160", "", $all_text);
                $all_text = str_replace("X150", "", $all_text);
                $all_text = str_replace("C/GUSA", "", $all_text);
                $all_text = str_replace("C/GUS", "", $all_text);
                $all_text = str_replace("SPICED (BOTELLA", "", $all_text);
                $all_text = str_replace("CLARO (COP", "", $all_text);

                $all_text = trim(str_replace("-", "", $all_text));
                $all_text = trim(str_replace("*", "", $all_text));
                $all_text = trim(str_replace("#", "", $all_text));
                $all_text = trim(str_replace(".", "", $all_text));
                //return $all_text;
                for($j = 1; $j < 150; $j++){
                    //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                    //if($j > 1)
                        $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                    
                    //Revisamos la linea
                    $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                    if(str_contains($revision, " ")){
                        $revision = trim(substr($revision, strrpos($revision, " ")));
                    }

                    $revision = str_replace('$', 'AAA', $revision);
                    $revision = str_replace(',', '', $revision);
                    $revision = str_replace('|', '', $revision);

                    //Convalidamos que no hayan saltos de linea a travesados
                    while(str_contains(substr($revision, 0, 2), "\n") || !is_numeric($revision) || strlen($revision) > 3 || $revision == 11){
                        if(str_contains($revision, " "))
                            $all_text = trim(substr($all_text, stripos($all_text, " ")));
                        else
                            $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));
                        
                        //Re-Revisamos la linea
                        $revision = trim(substr($all_text, 0, stripos($all_text, "\n")));
                        if(str_contains($revision, " ")){
                            $revision = trim(substr($revision, strrpos($revision, " ")));
                        }

                        $revision = str_replace('$', 'AAA', $revision);
                        $revision = str_replace(',', '', $revision);
                        $revision = str_replace('|', '', $revision);

                        if(empty($all_text))
                            break;
                    }
                    //echo $revision."\n";
                    //Evaluamos si ya estamos en la misma linea para culminar
                    if($fila == $j){
                        break;
                    }
                }
                //echo "\n\n";
                //return $all_text;
            //Fin evaluación posición del precio

            $linea_cantidad = substr($all_text, 0, stripos($all_text,"\n"));
            
            if(str_contains($linea_cantidad, " ")){
                $linea_cantidad = trim(substr($linea_cantidad, strrpos($linea_cantidad, " ")));
            }
        }

        while($minc > 0) {
            $cantidades = trim(substr($linea_cantidad, 0, $minc));
            if(is_numeric($cantidades)){
                break; 
            }
            $minc--;
        }

        if(!is_numeric($cantidades)){ 
            $cantidades = 1;
            $this->state = "pendiente";
        }

        if($cantidades >= 50){
            $this->state = "pendiente";
        }

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();
            
            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");
                        
                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01"); 
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }
                        
                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes); 

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }

        return $cantidades;
    }

    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = $content;
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);

        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            if(str_contains($productos, "CHAPU"))
                $productos = substr($productos, stripos($productos, 'CHAPU'));

            if(str_contains($productos, "CANTI"))
                $productos = substr($productos, stripos($productos, 'CANTI'));

            if(str_contains($productos, "CRIPCI"))
                $productos = substr($productos, stripos($productos, 'CRIPCI'));

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, "CRVEC"))
                $productos = substr($productos, 0, strrpos($productos, 'CRVEC'));

            if(str_contains($productos, "ULTEPEC"))
                $productos = substr($productos, 0, strrpos($productos, 'ULTEPEC'));
        //Fin limpiado

        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(!is_numeric($revision)){
                
                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break; 
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){ 
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad
                
                //Eliminamos el precio si esta en la misma linea
                    $precio = 0;
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    //Elimina los precios intercalados
                    while(preg_match('!\d{4,}!', $revision, $matches)){
                        $precio = $matches[0];
                        $revision = trim(preg_replace('/['.$precio.']/', '', $revision));
                    }

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){ 
                        $revision = str_replace($precio, '', $revision);
                    }
                //Fin eliminido el precio
                
                $nombre = trim($revision);

                //Limpiamos texto basura
                if(strlen($nombre) <= 4)
                    $nombre = "";
            
                if(str_contains($nombre, "WWW"))
                    $nombre = "";

                if($nombre && $nombre != "VODKA" && $nombre != "COCTELES" && $nombre != "WHISKEY" && $nombre != "TEQUILA")
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){

        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);
        
        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);
        
        //Sacamos el mesero
        $values->mesero = $this->mesero($content);
        
        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos($content, $values->ciudad);
        
        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        $values->extras = $this->productosExtra($content);
        
        //Sacamos del vector de productos extras los productos pernod
        $values->extras = $this->limpiandoExtra($values->productos, $values->extras);
        
        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
