<?php

namespace App\Parametrizacion;

use Illuminate\Http\Response;

//Helper
use Datetime;
use stdClass;

//Modelos
use App\Models\Productos; 
use App\Models\Tarifario;

class Porfirio
{

    public $state = "aceptado";
    public $referencia = "";
    public $cantidad = "";
    public $date = "";
    public $horas = "";
    public $month = "";
    public $etiqueta = ""; //agrega la etiqueta del tarifario

    public static function instance()
    {
        return new Porfirio();
    }
    
    //Sacamos la referencia del folio
    public function referenciaFolio($content){
        $folio = null;
        

        if(str_contains($content, 'FOLI') || str_contains($content, 'FOL1') || str_contains($content, 'LIO #')){

            if(str_contains($content, 'FOLI'))
                $ref_posicion = stripos($content, 'FOLI') + strlen('FOLIO:');
            elseif(str_contains($content, 'FOL1'))
                $ref_posicion = stripos($content, 'FOL1') + strlen('FOLIO:');
            elseif(str_contains($content, 'LIO #'))
                $ref_posicion = stripos($content, 'LIO #') + strlen('LIO #:');
   
            $texto = substr($content, $ref_posicion);
            $new_text = $texto;
            $texto = trim(substr($texto, 0, stripos($texto,"\n")));
            
            if(preg_match('/\d+/', $texto, $matches)){
                $folio = $matches[0];
            }
            else{

                $palabras = trim(preg_replace("/[^A-Za-z:#. ?]/", "", $texto));
                $palabras = explode(" ", $palabras);

                foreach($palabras as $palabra)
                    $texto = trim(str_replace($palabra, "", $texto));

                if(empty($texto)){
                    $texto = trim(substr($new_text, stripos($new_text,"\n") + 1));
                    $texto = trim(substr($texto, 0, stripos($texto,"\n")));
                    $palabras = trim(preg_replace("/[^A-Za-z:#. ?]/", "", $texto));
                    $palabras = explode(" ", $palabras);

                    foreach($palabras as $palabra)
                        $texto = trim(str_replace($palabra, "", $texto));
                }

                $minr = 7;
                while($minr > 2) {
                    $folio = trim(substr($texto, 0, $minr));
                    if(is_numeric($folio)){
                        break; 
                    }
                    $minr--;
                }
            }

            //Si no posee solo números quiere decir que hubo un fallo
            if (!is_numeric($folio))
                $this->state = "pendiente";
        }
        else{
            $this->state = "pendiente";
            $folio = "N/A";
        }

        $this->referencia = $folio;

        return $folio;
    }

    //Sacamos el mesero
    public function mesero($content){

        $mesero = null;

        if(str_contains($content, 'ESER')){
            $pos_mesero = stripos($content, 'ESER') + strlen('ESERO');
            $mesero = substr($content, $pos_mesero);
            //$mesero = substr($mesero, stripos($mesero, "\n") + 1);
            
            $revision = trim(substr($mesero, 0, stripos($mesero, "\n")));
            //Limpieza por si lo que lee no es un producto
            $revision = preg_replace('/[$]/', '', $revision);
            $revision = preg_replace('/[,]/', '', $revision);
            $revision = preg_replace('/[.]/', '', $revision);
            $revision = trim($revision);

            while(strlen($revision) <= 4 || is_numeric($revision) || str_contains($revision, "MESER") || str_contains($revision, "RDEN") || str_contains($revision, "MESA") || str_contains($revision, "ESTAC") || str_contains($revision, "PERSON") || substr_count($revision, "/") > 1){
                $mesero = trim(substr($mesero, stripos($mesero, "\n") + 1));
                
                $revision = trim(substr($mesero, 0, stripos($mesero, "\n")));

                //Limpieza por si lo que lee no es un producto
                $revision = preg_replace('/[$]/', '', $revision);
                $revision = preg_replace('/[,]/', '', $revision);
                $revision = preg_replace('/[.]/', '', $revision);
                $revision = trim($revision);

                if(empty($revision))
                    break;
            }

            //Limpiamos números o textos cortos intercalados
            $mesero = trim(preg_replace("/[^A-Za-z ?]/", "", $revision)); //Me quedo solo con el texto
        }
        else
            $mesero = "N/A";

        //Si posee números quiere decir que hubo un fallo
        if (preg_match('~[0-9]+~', $mesero))
            $this->state = "pendiente";
        
        $this->mesero = $mesero;

        return $mesero;
    }

    //Sacamos la fecha con hora
    public function fecha($content, $referencia){

        $fecha = date('Y-m-d H:i:s');
        $referencia = "JALISCO";

        //Segmento donde esta la linea de la fecha
            $date_texto = $content;
            $fecha_texto = trim(substr($content, stripos($content, $referencia) + strlen($referencia)));
            $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

            while(str_contains($revision, "ESER") || str_contains($revision, "AJER") || str_contains($revision, "OLIO") || str_contains($revision, "IMPRE") || str_contains($revision, "MENSAL") || str_contains($revision, "RDEN") || str_contains($revision, "MESA") || str_contains($revision, "ENVIA") || str_contains($revision, "CALLE") || strlen($revision) < 7 || substr_count($revision, "-") > 1){
                $fecha_texto = substr($fecha_texto, stripos($fecha_texto, "\n") + 1);
                $revision = substr($fecha_texto, 0, stripos($fecha_texto, "\n"));

                if(empty($revision))
                    break;
            }
            
            //aqui abajo ya tengo la linea de la fecha ejemplo: d/m/Y
            $this->date = $revision;
            
        //Sacamos la fecha
            $texto = trim(preg_replace("/[^A-Za-z: ?]/", "", $revision));
            $palabras = explode(" ", $texto);
            
            foreach($palabras as $palabra){
                if(strlen($palabra) > 3)
                    $revision = trim(str_replace($palabra, "", $revision));
            }
            
            if(str_contains($revision, $this->referencia))
                $revision = trim(str_replace($this->referencia, "", $revision));
            
            $fecha = $revision;
            $fecha = trim(str_replace(".", "", $fecha));
            $fecha = trim(str_replace(":", "", $fecha));
            $fecha = trim(str_replace("'", " ", $fecha));
            $fecha = trim(str_replace("PM", "", $fecha));
            $fecha = trim(str_replace("AM", "", $fecha));

            if(substr_count($fecha, " ") >= 3){
                $cantidad = substr_count($fecha, " ");
                while($cantidad > 2){
                    $cantidad--;
                    $fecha = trim(substr($fecha, 0, strrpos($fecha, " ")));
                }
            }

            $fecha = trim(str_replace(" ", "-", $fecha));
            
            //Ajuste del Mes
            $month = trim(preg_replace("/[^A-Za-z ?]/", "", $fecha));
            
            switch($month){
                case "JAN": $mes = "Jan"; break;
                case "FEB": $mes = "Feb"; break;
                case "MAR": $mes = "Mar"; break;
                case "APR": $mes = "Apr"; break;
                case "MAY": $mes = "May"; break;
                case "JUN": $mes = "Jun"; break;
                case "JUL": $mes = "Jul"; break;
                case "AUG": $mes = "Aug"; break;
                case "SEP": $mes = "Sep"; break;
                case "OCT": $mes = "Oct"; break;
                case "NOV": $mes = "Nov"; break;
                case "DEC": $mes = "Dec"; break;
                default: $mes = "Dec"; break;
            }

            $fecha = str_replace($month, $mes, $fecha);
            
            //Corregimos el año
                $year = substr($fecha, -2);
                try{
                    $year = DateTime::createFromFormat('y', $year);
                    if($year)
                        $year = $year->format('Y');
                    else
                        $year = "20".$year;
                }
                catch(\Exception $e){
                    $year = "20".$year;
                }
                //$year = "20".$year;
            
            $fecha = substr($fecha, 0, -2).$year;
            
        //Sacamos la hora de la misma
            $horas = trim($date_texto);
            $revision = trim(substr($horas, 0, stripos($horas, "\n")));
            
            while(substr_count($revision, ":") < 2){
                $date_texto = substr($date_texto, stripos($date_texto, "\n") + 1);
                $revision = trim(substr($date_texto, 0, stripos($date_texto, "\n")));

                if(empty($revision))
                    break;
            }
            
            $this->horas = $revision;
            
            if(str_contains($revision, $fecha)){
                $revision = substr($revision, stripos($revision, $fecha));
                $revision = trim(str_replace($fecha, "", $revision));
            }
                
            $horas = $revision;
            
            if(str_contains($horas, " "))
                $horas = substr($horas, 0, stripos($horas, " "));
            $horas = trim(preg_replace("/[^0-9:?]/", "", $horas));
            
            //Revisamos la hora para que no tire fallo con la fecha
            $check_hora = DateTime::createFromFormat('H:i:s', $horas);
            if($check_hora && $check_hora->format('H:i:s') == $horas)
                $horas = date("H:i:s", strtotime($horas));
            else
                $horas = date("H:i:s");
            
        //Formateamos la fecha
            $fecha = $fecha." ".$horas;
            
        //Corrección y validación de la fecha
            $fecha = str_replace('/', '-', $fecha);
            $chech_date = DateTime::createFromFormat('d-M-Y H:i:s', $fecha);
            if($chech_date && $chech_date->format('d-M-Y H:i:s') == $fecha){
                $fecha = date("Y-m-d H:i:s", strtotime($fecha));
                $this->month = date("m", strtotime($fecha));
            }
            else{
                //Si cae aquí quiere decir que la fecha retenida no es correcta
                $fecha = date("Y-m-d H:i:s");
                $this->month = null;
                $this->state = "pendiente";
            }

            //valido si la fecha es superorio a la actual
            $DateAndTime = strtotime(date('Y-m-d H:i:s', time()));  //FECHA ACTUAL EN UNIX
            $fecha = strtotime($fecha); // FECHA TRAIDA EN UNIX
            //COMPARO LAS FECHAS
            if ($fecha > $DateAndTime){
                //SI ES MAYOR COLOCO LA FECHA ACTUAL Y LA DEJO PENDIENTE
                $fecha = $DateAndTime;
                $this->month = null;
                $this->state = "pendiente";
            }
            $fecha = date('Y-m-d H:i:s', $fecha); // RETORNO FECHA A FORMATO HUMANO
            
        return $fecha;
    }

    //Sacamos los productos con el switch y retornamos el array
    public function switchProductos($marca){
        $referencia = null;

        //marca pues graba en teoria el nombre real que maneja en la factura
        //linea es que tipo de licor es
        //formato es si lo maneja como botella o como copa

        switch ($marca) {
            case "ABSOLUT BLUE": 
                $referencia = array(
                    array(
                        "marca" => "COP ABSOLUT AZU",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COP ABSOLUT AZ",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "P ABSOLUT AZ",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "ABSOLUT AZUL",
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT EXTRAKT": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "BEEFEATER DRY": 
                $referencia = array(
                    array(
                        "marca" => "COP BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "P BEEFEATER",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BEEFEATER DIR",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "BEEFEATER PINK": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "BEEFEATER BLACKBERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 TEQUILA": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "CHIVAS 13 SHERRY": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "CHIVAS XV": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "THE GLENLIVET FOUNDERS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                ); 
                break;
            case "HAVANA 7": 
                $referencia = array(
                    array(
                        "marca" => "COP HAVANA CLUB",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "P HAVANA C",
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "RON",
                        "formato" => "copa",
                    ),
                ); 
                break;
            case "MARTELL VSOP": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "BALLATINE'S FINEST": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            case "ALTOS PLATA": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "THE GLENLIVET 12": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "CHIVAS 18": 
                $referencia = array(
                    array(
                        "marca" => "COP CHIVAS REGA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "P CHIVAS REGA",
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    ),
                );
                break;
            case "MARTELL BS": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "COÑAC",
                        "formato" => "copa",
                    )
                );
                break;
            case "AVION R44": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "TEQUILA",
                        "formato" => "copa",
                    )
                );
                break;
            case "MONKEY 47": 
                $referencia = array(
                    array(
                        "marca" => "COP MONKEY 47",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "COP MONKEY_47",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "CON YONKEY 47",
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                    array(
                        "marca" => "BOT MONKEY 47",
                        "linea" => "GINEBRA",
                        "formato" => "botella",
                    ),
                    array(
                        "marca" => $marca,
                        "linea" => "GINEBRA",
                        "formato" => "copa",
                    ),
                );
                break;
            case "ABSOLUT WATERMELON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "VODKA",
                        "formato" => "copa",
                    )
                );
                break;
            case "JAMESON": 
                $referencia = array(
                    array(
                        "marca" => $marca,
                        "linea" => "WHISKY",
                        "formato" => "copa",
                    )
                );
                break;
            default:
                $referencia = array(
                    array(
                        "marca" => "N/A NONE HERE",
                        "linea" => "",
                        "formato" => "",
                    )
                );
            break;
        }

        return $referencia;
    }

    //Sacamos los productos
    public function productos($content, $ciudad){
        $return_productos = [];
        $check_product = [];

        //TOMAMOS LOS PRODUCTOS VALIDOS DE ACUERDO AL MES DE LA FACTURA Y TARIFARIO
            $fact_mes = $this->month ? $this->month : date('m');
            // if(intval($fact_mes)%2 == 0)
            //     $fact_mes = "0". intval($fact_mes - 1);

            // $tarifario_mes = Tarifario::select('FK_id_marca')->where('mes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $tarifario_mes = [];
            
            //el codigo se cambio al archivo tarifario.php y se incluye 
            include('tarifario.php');

            //Revisamos el tarifario si vino vacio, quiere decir que puede ser del tarifario bimensual anterior para que revise en caso de ser una factura dentro de los 10 días.
            // if(count($tarifario_mes) <= 0)
            //     $tarifario_mes = Tarifario::select('FK_id_marca')->where('antmes',date('Y-'.$fact_mes.'-01'))->where('ciudad',$ciudad)->get();
            
            $array_tar = [];
            foreach($tarifario_mes as $tar){
                array_push($array_tar, $tar->FK_id_marca);
            }

            $productos = Productos::whereIn('id_productos', $array_tar)->get();//Productos::where('activo', 1)->get();
        
        //Recorremos productos a producto de la BD
        foreach($productos as $product){
            $marca = $product->marca; //nombre de la marca del producto
            $codigo = $product->referencia; //código de la marca del producto
            
            //Arreglamos como sale dicha marca en la factura por CDC
            $referencia = $this->switchProductos($marca);
            
            foreach ($referencia as $producto) {

                $palabra_coincide = str_contains($content, $producto["marca"]);
                if ($palabra_coincide) {
                    
                    $cantidad = 1;
                    $contenido = $content;

                    //REVISAMOS CUANTAS VECES SALE EL PRODUCTO
                        try{
                            $chequeo = $producto["marca"];
                            if(preg_match_all("/$chequeo/i", $content, $macthes))
                                $cantidad = count($macthes[0]);
                        }
                        catch(\Exception $e){
                            "";
                        }
                    //FIN REVISAMOS CUATAS VECES SALE EL PRODUCTO
                    
                    //NO REPITA EL MISMO PRODUCTO YA LEIDO
                        $repeat = false;
                        foreach($check_product as $objeto){
                            /* if($objeto["nombre"] == $producto["marca"]){
                                $repeat = true; break;
                            } */
                            if(str_contains($objeto["nombre"], $producto["marca"])){
                                $repeat = true; break;
                            }
                        }
                        if($repeat) continue;
                    //FIN NO REPETIR EL MISMO PRODUCTO
                    
                    //HACEMOS EL BARRIDO DE LOS PRODUCTOS DE ACUERDO A LA CANTIDAD DE VECES QUE SALE
                    for($i = 0; $i < $cantidad; $i++){

                        if($cantidad > 1){
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 5;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);

                            //Contenido para el proceso de sacar el precio
                            $new_content = substr($contenido, 0, $pos_marca_ref + strlen($producto["marca"]) + 5);

                            //Acomodar el contenido a lectura del texto
                            $contenido = substr($contenido, 0, $pos_marca_ref + 5);
                        }
                        else{
                            //ubicamos la posición de la referencia
                            $pos_marca_ref = strrpos($contenido, strval($producto["marca"])) - 5;

                            //sacamos la porción del producto apartir de la posición hasta el final
                            $linea_marca = substr($contenido, $pos_marca_ref);
                        }

                        //LIMPIAMOS LA PORCIÓN DONDE SALE EL PRODUCTO
                            $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);

                            //Chequiamos si consiguio un contenido pequeño para limpiar más
                            while(strlen(substr($linea_marca, 0, stripos($linea_marca, "\n"))) <= 5){
                                if(empty($linea_marca))
                                    break;

                                $linea_marca = substr($linea_marca, stripos($linea_marca, "\n") + 1);
                            };

                        //CANTIDAD DEL PRODUCTO
                            if($cantidad > 1){
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $new_content);
                            }
                            else
                                $cantidades = $this->productoCantidad($linea_marca, $producto["formato"], $product, $ciudad, $content, $content);
                    
                        //PRECIO DEL PRODUCTO
                            if($cantidad > 1){
                                $precio = $this->productoPrecio($linea_marca, $content, $new_content);
                            }
                            else
                                $precio = $this->productoPrecio($linea_marca, $content, $content);
                            
                        $data = array(
                            'referencia' => $codigo,
                            'nombre' => substr($linea_marca, 0, stripos($linea_marca, "\n")),//$producto["marca"],
                            'cantidad' => $cantidades,
                            'precio' => $precio,
                            'proveedor' => "Pernot",
                            'linea' => $producto["linea"],
                            'formato' => $producto["formato"],
                        );
                        array_push($check_product, $data);
                        array_push($return_productos, $data);
                    }
                }
            }
        }

        return $return_productos;
    }

    //Proceso de sacar cantidad del Producto
    public function productoCantidad($linea_marca, $formato, $bdproducto, $ciudad, $content, $contenido){
        $cantidades = 0;
        $minc = 6;
        $linea_cantidad = substr($linea_marca, 0 , stripos($linea_marca, "\n"));
        
        if(str_contains($linea_cantidad, "(") || str_contains($linea_cantidad, ")")){
            
            $linea_cantidad = str_replace("#", "*", $linea_cantidad);
            $linea_cantidad = str_replace("+", "*", $linea_cantidad);
            $linea_cantidad = str_replace("$", "*", $linea_cantidad);
            
            $linea_cantidad = substr($linea_cantidad, 0 , stripos($linea_cantidad, "*"));
            $linea_cantidad = trim( strrev($linea_cantidad) );
            
            while($minc > 0) {
                $cantidades = trim(substr($linea_cantidad, 0, $minc));
                if(is_numeric($cantidades)){
                    break; 
                }
                $minc--;
            }

            if(!is_numeric($cantidades)){ 
                $cantidades = 1;
                $this->state = "pendiente";
            }

            if($cantidades >= 50){
                if($cantidades >= 20)
                    $cantidades = substr($cantidades, 0 ,1);
                else
                    $this->state = "pendiente";
            }
        }
        else
            $cantidades = 1;

        $this->cantidad = $cantidades;

        //CORREGIMOS EL TARIFARIO
        if($formato == "botella"){
            //aqui se haria algo del tarifario con la $ciudad
            $multiplicador = Tarifario::where('FK_id_marca',$bdproducto->id_productos)
                                        ->where('ciudad',$ciudad)->first();
            
            if($multiplicador){
                try{
                    if($multiplicador->antmes){
                        //Evaluamos en que dia estamos del mes actual
                        $dia = date("d");
                        
                        //Mes Actual o Anterior para ajustar;
                            $mes = date("Y-m-01"); 
                            if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                                $mes = date('Y-m-d', strtotime("-1 months", strtotime($mes)));
                            }
                        
                        //Validamos la fecha de la factura actual para ver si la rechazamos o no
                            $mes = strtotime($mes);
                            //$date = strtotime($multiplicador->mes);
                            $date = strtotime($multiplicador->mes_inicio);

                            if($mes >= $date){
                                //100% tarifario actual pase lo que pase
                                $multiplicador = $multiplicador->copaxbotella;
                            }
                            else{
                                $mes = date("Y-m-01");
                                $mes = strtotime($mes); 

                                //if evaluar fecha de la factura para ver si se implementa tarifario nuevo o el viejo
                                if($this->month && $mes >= $date){
                                    if($this->month < date('m'))
                                        $multiplicador = $multiplicador->antcopaxbotella;
                                    else
                                        $multiplicador = $multiplicador->copaxbotella;
                                }
                                else
                                    $multiplicador = $multiplicador->antcopaxbotella;
                            }
                    }
                    else{
                        $multiplicador = $multiplicador->copaxbotella;
                    }
                }
                catch(\Exception $e){
                    $multiplicador = 1;
                }
                $cantidades *= $multiplicador;
                //condicional de mayor 120 quantitys y que automáticamente se vuelven pendientes 
                if ($cantidades >= 120) {
                    $this->state = "pendiente";
                }
            }
        }

        return $cantidades;
    }

    //Proceso de sacar precio del Producto
    public function productoPrecio($linea_marca, $content, $contenido){
        //Revisemos si el precio esta en la misma linea del producto
        $linea_producto = substr($linea_marca, stripos($linea_marca, "\n") + 1);
        $linea_producto = substr($linea_producto, 0, stripos($linea_producto, "\n"));
        $linea_producto = substr($linea_marca, 0, stripos($linea_marca, "\n"))." ".$linea_producto;
        //Arriba ya tenemos la linea del producto con su precio si hipoteticamente lo tiene
        
        //Arreglo y toma del espacio donde aparece el precio digitado
            $check_line = preg_replace('/[,]/', '1', $linea_producto);
            $check_line = preg_replace('/[.]/', '1', $check_line);
            
            //Reviso que exista un secuencia de 4 números consecutivos d{4,}
            preg_match('!\d{4,}!', $check_line, $matches);
            $inline = true;
            
            //$matches[0] -> hubo coincidencia | is_numeric(que esa coincidencia sea numerica)
            //$matches[0] >= 4 que sea mayor a 4 digitos
            if(isset($matches[0]) && is_numeric($matches[0]) && strlen(trim($matches[0])) >= 4 && $matches[0] != "01500" && $matches[0] != "01400"){
                //TIENE EL PRECIO EN LA MISMA LINEA
                /* if(str_contains($linea_producto, "$")){
                    $linea_precio = substr($linea_producto, stripos($linea_producto,"$") + 1);
                    $inline = false;
                }
                else */
                if(str_contains(".", $linea_producto))
                    $linea_precio = preg_replace('/[,]/', '', $linea_producto);
                else
                    $linea_precio = preg_replace('/[,]/', '.', $linea_producto);
            }
            else{
                //NO TIENE EL PRECIO EN LA MISMA LINEA POR LO QUE DEBEMOS UBICARLO
                $fila = 0;
                $producto = substr($linea_marca, 0 , stripos($linea_marca, "\n")); //nombre del producto

                if(str_contains($contenido, 'SCRIPCI')){
                    $ref_inicio = 'SCRIPCI';
                }
                elseif(str_contains($contenido, 'RDEN')){
                    $ref_inicio = 'RDEN';
                }
                elseif(str_contains($contenido, 'FOLI')){
                    $ref_inicio = 'FOLI';
                }
                else
                    $ref_inicio = 'PRESI';

                //Evaluamos la posición del producto para luego sacar la del precio
                    //texto a partir del producto hacia atras
                    //Arreglo de $producto
                    if(!str_contains($contenido, $producto)){
                        if(str_contains($producto, ">"))
                            $producto = substr($producto, 0, stripos($producto, ">"));
                        else
                            $producto = substr($producto, 0, strlen($producto) - 5);

                        if(str_contains($producto, " "))
                            $producto = substr($producto, 0, strrpos($producto, " "));
                    }
                    
                    $all_text = substr($contenido, 0, strrpos($contenido, $producto) + strlen($producto));
                    
                    for($j = 0; $j < 30; $j++){
                        //Nos paramos en el ultimo producto anterior al de la referencia por Pernod
                        $revision = substr($all_text, strrpos($all_text,"\n") + 1);
                        
                        //Limpieza por si lo que lee no es un producto
                        $revision = preg_replace('/[$]/', '', $revision);
                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[.]/', '', $revision);
                        $revision = preg_replace('/[@]/', '', $revision);
                        $revision = trim($revision);

                        //Reviso si no es un jugo, coca cola o quina ya que estos no tienen precio en las fact
                        while(strlen($revision) <= 5 || is_numeric($revision) || str_contains($revision, $ref_inicio) || str_contains($revision, "SCRIPCI") || str_contains($revision, "RDEN") || str_contains($revision, "PRESI")  || str_contains($revision, "TOTAL") || substr_count($revision, ":") > 1){
                            
                            if(str_contains($revision, $ref_inicio))
                                break;

                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            $revision = substr($all_text, strrpos($all_text,"\n") + 1);

                            //Limpieza por si lo que lee no es un producto
                            $revision = preg_replace('/[$]/', '', $revision);
                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[.]/', '', $revision);
                            $revision = preg_replace('/[@]/', '', $revision);
                            $revision = trim($revision);

                            if(empty($revision))
                                break;
                        }
                        //echo $revision."\n";
                        //Validamos que no contenga la palabra calve DESCRIPCION ya que 
                        //a partir de ahí empiezan los productos
                        if(!str_contains($revision, $ref_inicio)){
                            $all_text = substr($all_text, 0, strrpos($all_text,"\n"));
                            
                            //Otra limpieza por si hay un salto de linea intercalado
                            if(str_contains(substr($all_text, -2), "\n"))
                                $all_text = substr($all_text, 0, strrpos($all_text,"\n"));

                            $fila++;
                        }
                        else
                            break;
                    }
                    //echo "\n\n";
                    //return $fila;
                //Fin evaluar la fila del producto

                //Evaluamos la posición del precio para sacalo de acuerdo a la posición del producto
                    //Generalmente a partir de IMPORTE es que estan los valores de los precios
                    //en estas facturas y tomamos el texto a partir de ahí hasta el final
                    if(str_contains($contenido, 'SCRIPCI')){
                        $ref_inicio = 'SCRIPCI';
                    }
                    elseif(str_contains($contenido, 'RDEN')){
                        $ref_inicio = 'RDEN';
                    }
                    elseif(str_contains($contenido, 'FOLI')){
                        $ref_inicio = 'FOLI';
                    }
                    else
                        $ref_inicio = 'PRESI';

                    $all_text = substr($content, strrpos($content, $ref_inicio));
                    $all_text = trim(preg_replace('#[^0-9$* \n\.,]#', "", $all_text));
                    //return $all_text;

                    for($j = 1; $j < 30; $j++){
                        //Comenzamos en a partir de la primera fila de $precio y asi sucesivamente
                        if($j > 1)
                            $all_text = substr($all_text, stripos($all_text, "\n") + 1);

                        //Revisamos la linea
                        $revision = substr($all_text, 0, stripos($all_text, "\n"));
                        if(str_contains($revision, "$")){
                            $revision = trim(substr($revision, stripos($revision, "$")));
                        }

                        if(substr_count($revision, ".") > 1){
                            $revision = str_replace(". ", "", $revision);
                        }

                        if(str_contains($revision, " ")){
                            $revision = trim(substr($revision, stripos($revision, " ")));
                        }

                        $revision = preg_replace('/[,]/', '', $revision);
                        $revision = preg_replace('/[*]/', 'A', $revision);
                        $revision = preg_replace('/[.]/', '111', $revision);
                        $revision = preg_replace('/[$]/', '111', $revision);

                        //Convalidamos que no hayan saltos de linea a travesados
                        while(str_contains(substr($revision, 0, 3), "\n") || !is_numeric($revision) || strlen($revision) <= 5){
                            
                            if(str_contains($revision, " "))
                                $all_text = trim(substr($all_text, stripos($all_text, " ")));
                            else
                                $all_text = trim(substr($all_text, stripos($all_text, "\n") + 1));

                            //Re-Revisamos la linea
                            $revision = substr($all_text, 0, stripos($all_text, "\n"));
                            if(str_contains($revision, "$")){
                                $revision = trim(substr($revision, stripos($revision, "$")));
                            }

                            if(substr_count($revision, ".") > 1){
                                $revision = str_replace(". ", "", $revision);
                            }

                            if(str_contains($revision, " ")){
                                $revision = trim(substr($revision, stripos($revision, " ")));
                            }

                            $revision = preg_replace('/[,]/', '', $revision);
                            $revision = preg_replace('/[*]/', 'A', $revision);
                            $revision = preg_replace('/[.]/', '111', $revision);
                            $revision = preg_replace('/[$]/', '111', $revision);

                            if(empty($all_text))
                                break;
                        }
                        
                        //Evaluamos si ya estamos en la misma linea para culminar
                        if($fila == $j){
                            break;
                        }
                    }
                //Fin evaluación posición del precio
                
                $linea_precio = substr($all_text, 0, stripos($all_text,"\n"));
                if(str_contains($linea_precio, "$"))
                    $linea_precio = substr($linea_precio, stripos($linea_precio,"$") + 1);

                if(substr_count($linea_precio, ".") > 1){
                    $linea_precio = str_replace(". ", "", $linea_precio);
                }
            }
        //Fin arreglo y toma del espacio donde aparece el precio digitado
        
        $precio = 0;
        $minp = 14;
        
        $linea_precio = str_replace("*", " ", $linea_precio);
        $linea_precio = str_replace("#", " ", $linea_precio);
        $linea_precio = str_replace("(", " ", $linea_precio);
        $linea_precio = str_replace(")", " ", $linea_precio);
        $linea_precio = str_replace("+", " ", $linea_precio);
        $linea_precio = str_replace("$", " ", $linea_precio);
        
        if($inline){
            $out = true;
            while($out){
                $revision = trim(substr($linea_precio, 0, stripos($linea_precio, " ")));
                $linea_precio = trim(substr($linea_precio, stripos($linea_precio, " ")));
                
                if(str_contains($revision, ".")){
                    if(preg_match('/^([1-9]\d*|0)(\.\d+)?$/', $revision, $matches)){
                        $linea_precio = $matches[0];
                        $out = false;
                    }
                }

                if(!strpos($linea_precio, ' '))
                    $out = false;
            }
        }
        
        while($minp > 4) {
            $precio = substr($linea_precio, 0, $minp);
            $precio = preg_replace('/[,]/', '', $precio);
            if(is_numeric($precio)){
                break;
            }
            $minp--;
        }

        if(!is_numeric($precio)){ 
            $precio = 0;
            $this->state = "pendiente";
        }

        $precio *= $this->cantidad;

        return $precio;
    }

    //Sacamos los productos extras
    public function productosExtra($content){
        $return_productos = [];

        $productos = $content;//substr($content, stripos($content, 'PERSONAS'));
        $productos = preg_replace('/[,]/', '', $productos);
        $productos = preg_replace('/[.]/', '', $productos);

        //Limpiando el espaciado para solo tener la cadena o espacio de los productos
            //DELIMITANDO EL INICIO
            if(str_contains($productos, "PRESI"))
                $productos = substr($productos, stripos($productos, 'PRESI'));

            if(str_contains($productos, "FOLI"))
                $productos = substr($productos, stripos($productos, 'FOLI'));

            if(str_contains($productos, "RDEN"))
                $productos = substr($productos, stripos($productos, 'RDEN'));

            if(str_contains($productos, "SCRIPCI"))
                $productos = substr($productos, stripos($productos, 'SCRIPCI'));

            $productos = substr($productos, stripos($productos, "\n") + 1);

            //DELIMITANDO EL FINAL
            if(str_contains($productos, "ECTIV"))
                $productos = substr($productos, 0, strrpos($productos, 'ECTIV'));

            if(str_contains($productos, "CAMBI"))
                $productos = substr($productos, 0, strrpos($productos, 'CAMBI'));

            if(str_contains($productos, "TOTAL"))
                $productos = substr($productos, 0, strrpos($productos, 'TOTAL'));
        //Fin limpiado

        while($productos){
            $revision = substr($productos, 0, stripos($productos, "\n"));

            if(!is_numeric($revision)){
                
                //Eliminamos el espacio si tiene en la misma linea de la cantidad
                    $cantidades = 0;
                    $minc = 6;

                    while($minc > 0) {
                        $cantidades = trim(substr($revision, 0, $minc));
                        if(is_numeric($cantidades)){
                            break; 
                        }
                        $minc--;
                    }

                    if(is_numeric($cantidades)){ 
                        $revision = preg_replace('/['.$cantidades.']/', '', $revision);
                    }
                //Fin eliminada la cantidad
                
                //Eliminamos el precio si esta en la misma linea
                    $precio = 0;
                    $minp = 14;
                    $linea_precio = substr($revision, stripos($revision,"$") + 1);
                    $revision = preg_replace('/[$]/', '', $revision);

                    //Elimina los precios intercalados
                    while(preg_match('!\d{4,}!', $revision, $matches)){
                        $precio = $matches[0];
                        $revision = trim(preg_replace('/['.$precio.']/', '', $revision));
                    }

                    while($minp > 4) {
                        $precio = substr($linea_precio, 0, $minp);
                        $precio = preg_replace('/[,]/', '', $precio);
                        if(is_numeric($precio)){
                            break;
                        }
                        $minp--;
                    }

                    if(is_numeric($precio)){ 
                        $revision = str_replace($precio, '', $revision);
                    }
                //Fin eliminido el precio
                
                $nombre = trim($revision);

                //Limpiamos texto basura
                if(strlen($nombre) <= 4)
                    $nombre = "";
            
                if(str_contains($nombre, "WWW"))
                    $nombre = "";

                if($nombre && !str_contains($nombre, "TOTAL") && !str_contains($nombre, "CAMBI"))
                    array_push($return_productos, $nombre);
            }

            $productos = substr($productos, stripos($productos, "\n") + 1);
        }

        return $return_productos;
    }

    //Limpieza de duplicados en productos Extras
    public function limpiandoExtra($productos, $extras){

        $check_nombre = [];

        //Sacamos los productos pernod
        foreach($productos as $producto){
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto["nombre"]));
            array_push($check_nombre, $nombre);
        }

        //filtramos y sacamos los productos extra
        $extras = array_filter($extras, function($producto, $key) use ($check_nombre) {
            $nombre = trim(preg_replace("/[^A-Za-z ?]/", "", $producto));
            //Evaluamos que no este en los de Pernod
            if (!in_array($nombre, $check_nombre))
                return $producto;
        }, ARRAY_FILTER_USE_BOTH);

        return $extras;
    }

    //Lectura de Palominos
    public function params($values, $content){
        
        //Sacamos la referencia
        $values->referencia = $this->referenciaFolio($content);
        
        //Sacamos la fecha del invoice
        $values->fecha_factura = $this->fecha($content, $values->referencia);
        
        //Sacamos el mesero
        $values->mesero = $this->mesero($content);
        
        //Sacamos los productos de la factura bajo pernod
        $values->productos = $this->productos($content, $values->ciudad);

        //Sacamos todos los productos de la factura para lo nuevo que ellos desean
        $values->extras = $this->productosExtra($content);
        
        //Sacamos del vector de productos extras los productos pernod
        $values->extras = $this->limpiandoExtra($values->productos, $values->extras);
        
        //Tomamos el estado de la varibale de la clase ya que se pondra en pendiente si
        //algun dato de los recogidos falla
        $values->estado = $this->state;
        $values->tarifario = $this->etiqueta;

        return $values;
    }
}
?>