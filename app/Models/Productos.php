<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    use HasFactory;

    protected $table='productos';
    protected $primaryKey='id_productos';
    protected $fillable =[
        'id_productos',
        'marca',
        'referencia',
        'line',
        'activo',
    ];

    public function tarifarios(){
        return $this->hasMany('App\Models\Tarifario', 'FK_id_marca');
    }

    public function compras(){
        return $this->hasMany('App\Models\FacturaProducto', 'referencia', 'referencia');
    }
}
