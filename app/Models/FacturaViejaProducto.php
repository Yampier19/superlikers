<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturaViejaProducto extends Model
{
    use HasFactory;

    protected $table='factura_vieja_producto';
    protected $primaryKey='id_producto';
    protected $fillable =[
        'id_producto',
        'referencia', //nombre del producto
        'price',
        'quantity',
        'provider',
        'line',
        'formato',
        'FK_id_factura',
    ];

    public function factura(){
        return $this->belongsTo('App\Models\FacturaVieja', 'FK_id_factura');
    }
}
