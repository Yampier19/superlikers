<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;

    protected $table='facturas';
    protected $primaryKey='id_factura';
    protected $fillable =[
        'id_factura',
        'referencia',
        'cdc',
        'folio',
        'mesero',
        'campaign',
        'state',
        'moderation',
        'envio',
        'photo',
        'registro',
        'lectura',
        'tarifario',
        'user_upload',
        'FK_id_participante',
    ];

    public function participante(){
        return $this->belongsTo('App\Models\Participante', 'FK_id_participante');
    }

    public function productos(){
        return $this->hasMany('App\Models\FacturaProducto', 'FK_id_factura');
    }

    public function extras(){
        return $this->hasMany('App\Models\FacturaExtra', 'FK_id_factura');
    }
}
