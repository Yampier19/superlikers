<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurantes extends Model
{
    use HasFactory;

    protected $table='restaurantes';
    protected $primaryKey='id_restaurante';
    protected $fillable =[
        'id_restaurante',
        'nombre',
        'ciudad',
        'activo',
        'tipo',
    ];
}
