<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentRead extends Model
{
    use HasFactory;

    protected $table='content_read';
    protected $primaryKey='id';

    protected $fillable =[
        'id',
        'id_archivo',
        'archivo',
        'contenido',
    ];
}
