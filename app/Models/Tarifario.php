<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarifario extends Model
{
    use HasFactory;

    protected $table='tarifario';
    protected $primaryKey='id_tarifario';
    protected $fillable =[
        'id_tarifario',
        'ciudad',
        'competidor',
        'categoria',
        'copaxbotella',
        'antcopaxbotella',
        'mes_inicio',
        'mes_fin',
        'FK_id_marca',
        'etiqueta',
    ];

    public function productos(){
        return $this->belongsTo('App\Models\Productos', 'FK_id_marca');
    }
}
