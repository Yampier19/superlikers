<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturaExtra extends Model
{
    use HasFactory;

    protected $table='factura_extra';
    protected $primaryKey='id_extra';
    protected $fillable =[
        'id_extra',
        'nombre',
        'FK_id_factura',
    ];

    public function factura(){
        return $this->belongsTo('App\Models\Factura', 'FK_id_factura');
    }
}
