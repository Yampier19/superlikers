<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturaVieja extends Model
{
    use HasFactory;

    protected $table='facturas_viejas';
    protected $primaryKey='id_factura';
    protected $fillable =[
        'id_factura',
        'referencia',
        'cdc',
        'folio',
        'email',
        'mesero',
        'campaign',
        'moderation',
        'photo',
        'registro',
        'lectura',
        'user_upload',
        'FK_id_participante',
    ];

    public function productos(){
        return $this->hasMany('App\Models\FacturaViejaProducto', 'FK_id_factura');
    }
}
