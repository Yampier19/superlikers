<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    use HasFactory;

    protected $table='participante';
    protected $primaryKey='id_participante';
    protected $fillable =[
        'id_participante',
        'name',
        'id',
        'uid',
        'email',
    ];

    public function facturas(){
        return $this->hasMany('App\Models\Factura', 'FK_id_participante');
    }
}
