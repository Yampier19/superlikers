<?php

namespace App\Helper;

use Illuminate\Http\Response;
use Smalot\PdfParser\Parser;
use Smalot\PdfParser\XObject\Image;
use thiagoalessio\TesseractOCR\TesseractOCR;

//MODELOS
use App\Models\Restaurantes;
use App\Models\Factura;

//Helper
use Datetime;
use Google\Cloud\Vision\VisionClient;
use stdClass;

//Parametrizaciones
use App\Parametrizacion\Agaves;
use App\Parametrizacion\Almacen;
use App\Parametrizacion\Anatol;
use App\Parametrizacion\Argentilia;
use App\Parametrizacion\AsadorVasco;
use App\Parametrizacion\Azia;
use App\Parametrizacion\Balboa;
use App\Parametrizacion\Bar1528;
use App\Parametrizacion\BarraLaNacional;
use App\Parametrizacion\BarraSacrificios;
use App\Parametrizacion\BarrioLaBocha;
use App\Parametrizacion\BochaBQ;
use App\Parametrizacion\BochaChapalita;
use App\Parametrizacion\BochaNautica;
use App\Parametrizacion\BochaQueretaro;
use App\Parametrizacion\Brick;
use App\Parametrizacion\Tereza;
use App\Parametrizacion\BrunoQRO;
use App\Parametrizacion\BryanMonte;
use App\Parametrizacion\BuenaBarra;
use App\Parametrizacion\BuenaBarra2;
use App\Parametrizacion\BullMccabe;
use App\Parametrizacion\BurgerBar;
use App\Parametrizacion\CaccioGabilondo;
use App\Parametrizacion\Cantina20;
use App\Parametrizacion\Carranza;
use App\Parametrizacion\CasaBell;
use App\Parametrizacion\CasaHabano;
use App\Parametrizacion\CasaO;
use App\Parametrizacion\CasaPortuguesa;
use App\Parametrizacion\CerveceriaC;
use App\Parametrizacion\CigarPoint;
use App\Parametrizacion\Clover;
use App\Parametrizacion\CocinaAbierta;
use App\Parametrizacion\ComedorMilagros;
use App\Parametrizacion\Corajillo;
use App\Parametrizacion\CorazonAlcachofa;
use App\Parametrizacion\CorazonAlcachofaAndares;
use App\Parametrizacion\Costeñito;
use App\Parametrizacion\CusCus;
use App\Parametrizacion\Dante;
use App\Parametrizacion\DelBarril;
use App\Parametrizacion\DeliDeli;
use App\Parametrizacion\Divino106;
use App\Parametrizacion\DonAsadoPolanco;
use App\Parametrizacion\DonCapitanNacualpan;
use App\Parametrizacion\DonCapitanPolanco;
use App\Parametrizacion\DonCapitanSantaFe;
use App\Parametrizacion\EdificioAlcala;
use App\Parametrizacion\ElBayo;
use App\Parametrizacion\ElPatron;
use App\Parametrizacion\EstanciaArgentina;
use App\Parametrizacion\Frascati;
use App\Parametrizacion\Galaxy;
use App\Parametrizacion\GauchoTradicional;
use App\Parametrizacion\GrupoImpulsor;
use App\Parametrizacion\Guandola;
use App\Parametrizacion\Hanks;
use App\Parametrizacion\Harbors;
use App\Parametrizacion\Hatria;
use App\Parametrizacion\HijasTostada;
use App\Parametrizacion\Hotel1972;
use App\Parametrizacion\Hyatt;
use App\Parametrizacion\Ikebana;
use App\Parametrizacion\Jaibon;
use App\Parametrizacion\Japi;
use App\Parametrizacion\JardinParaiso;
use App\Parametrizacion\Kampai401;
use App\Parametrizacion\KampailPuebla;
use App\Parametrizacion\Karisma;
use App\Parametrizacion\KokoroAndares;
use App\Parametrizacion\KokoroSauPaulo;
use App\Parametrizacion\KokuLerma;
use App\Parametrizacion\LaAceituna;
use App\Parametrizacion\LaArtesanal;
use App\Parametrizacion\LaBikina;
use App\Parametrizacion\LaBikina2;
use App\Parametrizacion\LaInsurgente;
use App\Parametrizacion\LaLoggia;
use App\Parametrizacion\LaMadalena;
use App\Parametrizacion\LaPalapa;
use App\Parametrizacion\LaVikina;
use App\Parametrizacion\LosJuanes;
use App\Parametrizacion\LosLaureles;
use App\Parametrizacion\LosteriaBecco;
use App\Parametrizacion\Mallorquina;
use App\Parametrizacion\Mandala;
use App\Parametrizacion\MangosAng;
use App\Parametrizacion\Mariskeña;
use App\Parametrizacion\MarisqueriaRed;
use App\Parametrizacion\Matilde;
use App\Parametrizacion\MicaelaMar;
use App\Parametrizacion\MiCompaChava;
use App\Parametrizacion\MoritasGarza;
use App\Parametrizacion\GranBarra;
use App\Parametrizacion\MoritasInsurgentes;
use App\Parametrizacion\MoritasValle;
use App\Parametrizacion\Mortons;
use App\Parametrizacion\MuiMui;
use App\Parametrizacion\Mumma;
use App\Parametrizacion\PacificoFuentes;
use App\Parametrizacion\Palominos;
use App\Parametrizacion\Palominos2;
use App\Parametrizacion\PalominosInsurgentes;
use App\Parametrizacion\PalominosProvidencia;
use App\Parametrizacion\PalominosSur;
use App\Parametrizacion\ParaguasNorte;
use App\Parametrizacion\Peninsula;
use App\Parametrizacion\PescadorSanManuel;
use App\Parametrizacion\PescadorZabaleta;
use App\Parametrizacion\Porfirio;
use App\Parametrizacion\ResBeroa;
use App\Parametrizacion\RinconPolanco;
use App\Parametrizacion\RinconSantaFe;
use App\Parametrizacion\RusoBlanco;
use App\Parametrizacion\SalonRio;
use App\Parametrizacion\Sambuka;
use App\Parametrizacion\SanMaree;
use App\Parametrizacion\SeñorBamboo;
use App\Parametrizacion\Sinaloense;
use App\Parametrizacion\SraTanaka;
use App\Parametrizacion\TabernaLeon;
USE App\Parametrizacion\Tequilazo;
use App\Parametrizacion\TerrazaBrasil;
use App\Parametrizacion\TierraSol;
use App\Parametrizacion\Tribulus;
use App\Parametrizacion\VacaArgentina;
use App\Parametrizacion\VacaGourmet;
use App\Parametrizacion\Vini;
use App\Parametrizacion\Wallace;

class FacturaIA
{

    public static function instance()
    {
        return new FacturaIA();
    }

    public function readFactura($filename, $file){
        //$file AQUI ESTA EL ARCHIVO DE LA FOTO 
        
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $content = null;
        try{
            if ($extension =='pdf'){

                // instanciamos los metodos que analizan el documento
                $parseador = new Parser();
            
                //asignamos el archivo para el analisis de la lectura
                $documento = $parseador->parseFile($file);
                $paginas = $documento->getPages();

                foreach ($paginas as $indice => $pagina) {
                    $texto = $pagina->getText();
                    $content .= $texto;
                    
                    if($texto == ' ' || $texto == '  '){
                        $imagenes = $documento->getObjectsByType('XObject', 'Image');

                        foreach ($imagenes as $imagen) {
                            if ($imagen instanceof Image) {
                                $image = $imagen->getContent();

                                $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                $filenombre = substr(str_shuffle($caracteres_permitidos), 0, 8);
                                
                                //$archivo = storage_path('app/public/pdfImage.jpg');
                                $archivo = storage_path().'/app/public/'.$filenombre.".jpg";
                                file_put_contents($archivo, $image);
                                
                                //server path; /usr/local/bin/tesseract
                                //C:\Program Files\Tesseract-OCR/tesseract.exe
                                /* $text = (new TesseractOCR($archivo))
                                    ->executable('/usr/local/bin/tesseract')
                                    ->lang('eng', 'spa')
                                    ->run(); */

                                $key = storage_path().'/app/public/key.json';

                                $vision = new VisionClient(['keyFile' => json_decode(file_get_contents($key), true)]);
                                $image = fopen($archivo, 'r');
            
                                $ocr = $vision->image($image, ['TEXT_DETECTION']);
                                $result = $vision->annotate($ocr);
            
                                $result = (array) $result->info();
                                $text = $result["textAnnotations"][0]["description"];

                                //Eliminamos el archivo
                                unlink($archivo);
                            }
                            else{
                                $text = $imagen->getText();
                            }
                            
                            $content .= $text;
                        }

                        $extension = "jpg";
                    }
                }
            }
            elseif($extension !='pdf'){
                    //server path; /usr/local/bin/tesseract
                    //C:\Program Files\Tesseract-OCR/tesseract.exe
                    /* $text = (new TesseractOCR($file))
                        ->executable('C:\Program Files\Tesseract-OCR/tesseract.exe')
                        ->lang('eng', 'spa')
                        ->run(); */

                    $key = storage_path().'/app/public/key.json';

                    $vision = new VisionClient(['keyFile' => json_decode(file_get_contents($key), true)]);
                    $image = fopen($file, 'r');

                    $ocr = $vision->image($image, ['DOCUMENT_TEXT_DETECTION']);
                    $result = $vision->annotate($ocr);

                    $result = (array) $result->info();
                    $text = $result["textAnnotations"][0]["description"];

                    $content .= $text; 
            }

            //AQUI CONTIENE TODO LO QUE HAYA LEIDO en $CONTENT
            if($content !== ''){
                $values = new \stdClass;
                $values->contenido = $content; //contenido leido por el lector ocr o parser
            }
            else
                $values = null;

            return $values;
        }
        catch(\Exception $e) {
            //ALERTA DE ERROR DURANTE EL PROCESO DE LECTURA
            $values = new \stdClass;
            $values->error = 'Message Error: ' .$e->getMessage().' | '.$e->getLine();
            return $values;
        }
    }

    public function read($file){
        //$file AQUI ESTA EL ARCHIVO DE LA FOTO 
        
        $extension = $file->extension();
        $content = null;
        try{
            if ($extension =='pdf'){

                // instanciamos los metodos que analizan el documento
                $parseador = new Parser();
            
                //asignamos el archivo para el analisis de la lectura
                $documento = $parseador->parseFile($file);
                $paginas = $documento->getPages();

                foreach ($paginas as $indice => $pagina) {
                    $texto = $pagina->getText();
                    $content .= $texto;
                    
                    if($texto == ' ' || $texto == '  '){
                        $imagenes = $documento->getObjectsByType('XObject', 'Image');

                        foreach ($imagenes as $imagen) {
                            if ($imagen instanceof Image) {
                                $image = $imagen->getContent();

                                $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                $filenombre = substr(str_shuffle($caracteres_permitidos), 0, 8);
                                
                                //$archivo = storage_path('app/public/pdfImage.jpg');
                                $archivo = storage_path().'/app/public/'.$filenombre.".jpg";
                                file_put_contents($archivo, $image);
                                
                                //server path; /usr/local/bin/tesseract
                                //C:\Program Files\Tesseract-OCR/tesseract.exe
                                /* $text = (new TesseractOCR($archivo))
                                    ->executable('/usr/local/bin/tesseract')
                                    ->lang('eng', 'spa')
                                    ->run(); */

                                $key = storage_path().'/app/public/key.json';

                                $vision = new VisionClient(['keyFile' => json_decode(file_get_contents($key), true)]);
                                $image = fopen($archivo, 'r');
            
                                $ocr = $vision->image($image, ['TEXT_DETECTION']);
                                $result = $vision->annotate($ocr);
            
                                $result = (array) $result->info();
                                $text = $result["textAnnotations"][0]["description"];

                                //Eliminamos el archivo
                                unlink($archivo);
                            }
                            else{
                                $text = $imagen->getText();
                            }
                            
                            $content .= $text;
                        }

                        $extension = "jpg";
                    }
                }
            }
            elseif($extension =='jpeg' || $extension =='jpg' || $extension =='png'){
                    //server path; /usr/local/bin/tesseract
                    //C:\Program Files\Tesseract-OCR/tesseract.exe
                    /* $text = (new TesseractOCR($file))
                        ->executable('C:\Program Files\Tesseract-OCR/tesseract.exe')
                        ->lang('eng', 'spa')
                        ->run(); */

                    $key = storage_path().'/app/public/key.json';

                    $vision = new VisionClient(['keyFile' => json_decode(file_get_contents($key), true)]);
                    $image = fopen($file, 'r');

                    $ocr = $vision->image($image, ['DOCUMENT_TEXT_DETECTION']);
                    $result = $vision->annotate($ocr);

                    $result = (array) $result->info();

                    if(isset($result["textAnnotations"]))
                        $text = $result["textAnnotations"][0]["description"];
                    else
                        $text = "";

                    $content .= $text; 
            }

            //AQUI CONTIENE TODO LO QUE HAYA LEIDO en $CONTENT
            if($content !== ''){
                $values = new \stdClass;
                $values->contenido = $content; //contenido leido por el lector ocr o parser
            }
            else
                $values = null;

            return $values;
        }
        catch(\Exception $e) {
            //ALERTA DE ERROR DURANTE EL PROCESO DE LECTURA
            $values = new \stdClass;
            $values->error = 'Message Error: ' .$e->getMessage().' | '.$e->getLine();
            return $values;
        }
    }
    
    public function parametrizar($lectura){

        try{
            /* EJEMPLO DE LA VARIABLE DE LECTURA */
                $values = new \stdClass;
                $values->restaurante = "nombre del centro de consumo";
                $values->mesero = "No parametrizo"; //mesero o participante
                $values->referencia = "No parametrizo"; //codigo de la factura en la imagen
                $values->fecha_factura = date("Y-m-d H:i:s"); //día que marca la factura en que se imprimio
                $values->productos = []; //productos leidos pernot
                $values->extras = []; //productos extras 
                $values->contenido = $lectura->contenido; //texto leido por el visor o parser
                $values->tarifario = ""; //tarifario esto se sacara por la fecha impresa de la fact
            /* FIN EJEMPLO */

            //Identificamos el Centro de Consumo
            $restaurantes = Restaurantes::all();
            $default = false;
            foreach($restaurantes as $restaurante){
                if($default) break;

                $values->restaurante = $restaurante->nombre;
                $values->ciudad = $restaurante->ciudad;
                $values->encargado = $restaurante->activo;
                $restaurante->nombre = strtoupper($restaurante->nombre);

                //Arreglo los nombre de los restaurantes de acuerdo a como salen en factura
                $restaurante->nombre == "AGAVES" ? $restaurante->nombre = "BARRA(COMEDOR)" : "";
                $restaurante->nombre == "ALMACÉN DEL BIFE AGUASCALIENTES" ? $restaurante->nombre = "ALMACEN DEL BIFE" : "";
                $restaurante->nombre == "ANATOL" ? $restaurante->nombre = "ANATOLE" : "";
                $restaurante->nombre == "ARGENTILIA" ? $restaurante->nombre = "E ARGENTILIA" : "";
                $restaurante->nombre == "ASADOR VASCO PARQUE" ? $restaurante->nombre = "ASADOR VASCO PARQUE" : "";
                $restaurante->nombre == "AZIA" ? $restaurante->nombre = "AZIA" : "";
                $restaurante->nombre == "BALBOA LERMA" ? $restaurante->nombre = "BALBOA LERMA" : "";
                $restaurante->nombre == "1528 BAR (GRUPO IMPULSOR MAZARIEGOS)" ? $restaurante->nombre = "1528 BAR" : "";
                $restaurante->nombre == "BARRA LA NACIONAL" ? $restaurante->nombre = "BARRA LA NACIONAL" : "";
                $restaurante->nombre == "BARRA DE SACRIFICIOS" ? $restaurante->nombre = "BARRA DE SACRIFICIO" : "";
                $restaurante->nombre == "EL BARRIO DE LA BOCHA" ? $restaurante->nombre = "EL BARRIO LA BOCHA" : "";
                $restaurante->nombre == "EL BARRIO LA BOCHA" ? $restaurante->nombre = "EL BARRIO LA BOCHA" : "";
                $restaurante->nombre == "LA BOCHA CHAPALITA" ? $restaurante->nombre = "ADOR CHAPALITA" : "";
                $restaurante->nombre == "BRICK" ? $restaurante->nombre = "BRICK STEAKHOUSE" : "";
                $restaurante->nombre == "TEREZA" ? $restaurante->nombre = "TEREZA" : "";
                $restaurante->nombre == "BRUNO QRO" ? $restaurante->nombre = "HACIENDA CAMPANARIO QUERETARO" : "";
                $restaurante->nombre == "BRYAN´S MONTECRISTO" ? $restaurante->nombre = "BRYAN´S MONTECRISTO"/* "LA TRATTO S" */ : "";
                $restaurante->nombre == "BULL MCCABE" ? $restaurante->nombre = "BULL MCCABE" : "";
                $restaurante->nombre == "BURGER BAR FORUM" ? $restaurante->nombre = "BURGER BAR " : "";
                $restaurante->nombre == "BURGER BAR ROMA" ? $restaurante->nombre = "BURGER BAR " : "";
                $restaurante->nombre == "BURGER BAR TOREO" ? $restaurante->nombre = "BURGER BAR " : "";
                $restaurante->nombre == "CACCIO GABILONDO" ? $restaurante->nombre = "XXX TIEM" : "";
                $restaurante->nombre == "CANTINA LA 20" ? $restaurante->nombre = "20 GUADA" : "";
                $restaurante->nombre == "CARRANZA PRIME" ? $restaurante->nombre = "NZA PRIME" : "";
                $restaurante->nombre == "CASA BELL" ? $restaurante->nombre = "CASA BELL" : "";
                $restaurante->nombre == "LA CASA DEL HABANO CABO" ? $restaurante->nombre = "LA CASA DEL HABANO CABO" : "";
                $restaurante->nombre == "CASA Ó" ? $restaurante->nombre = "52027146" : "";
                $restaurante->nombre == "CASA PORTUGUESA" ? $restaurante->nombre = "EMILIO CASTELAR 1" : "";
                $restaurante->nombre == "CERVECERIA CHAPULTEPEC" ? $restaurante->nombre = "CERVECERIA CHAPULTEPEC" : "";
                $restaurante->nombre == "CIGAR POINT" ? $restaurante->nombre = "180601MTA" : "";
                $restaurante->nombre == "CLOVER (GRUPO IMPULSOR MAZARIEGOS)" ? $restaurante->nombre = "CLOVER" : "";
                $restaurante->nombre == "COCINA ABIERTA" ? $restaurante->nombre = "TEDDYS" : "";
                $restaurante->nombre == "COMEDOR DE LOS MILAGROS" ? $restaurante->nombre = "COMEDOR DE LOS MILAGROS" : "";
                $restaurante->nombre == "CORAJILLO" ? $restaurante->nombre = "CALLE 7B " : "";
                $restaurante->nombre == "CORAZÓN DE ALCACHOFA SAO PAULO" ? $restaurante->nombre = "SAO PAULO 2367 COLONIA" : "";
                $restaurante->nombre == "CORAZON DE ALCACHOFA ANDARES" ? $restaurante->nombre = "CORAZON DE ALCACHOFA ANDARES" : "";
                $restaurante->nombre == "COSTEÑITO PUEBLA" ? $restaurante->nombre = "COSTEÑITO " : "";
                $restaurante->nombre == "CUS CUS" ? $restaurante->nombre = "CUS CUS" : "";
                $restaurante->nombre == "DANTE" ? $restaurante->nombre = "DANTE BRASA Y FUEGOS" : "";
                $restaurante->nombre == "DEL BARRIL" ? $restaurante->nombre = "DEL BARRIL" : "";
                $restaurante->nombre == "DELI DELI" ? $restaurante->nombre = "DELI DELI" : "";
                $restaurante->nombre == "DIVINO 106" ? $restaurante->nombre = "DIVINO 106" : "";
                $restaurante->nombre == "DON ASADO POLANCO" ? $restaurante->nombre = "DURANGO 149 INT" : "";
                $restaurante->nombre == "DON CAPITAN NAUCALPAN" ? $restaurante->nombre = "DON CAPITAN S" : "";
                $restaurante->nombre == "DON CAPITÁN POLANCO" ? $restaurante->nombre = "DON CAPITÁN POLANCO" : "";
                $restaurante->nombre == "DON CAPITAN SANTA FE" ? $restaurante->nombre = "DE DON CAPITAN" : "";
                $restaurante->nombre == "EDIFICIO ALCALA" ? $restaurante->nombre = "EDIFICIO ALCALA" : "";
                $restaurante->nombre == "MARISCOS EL BAYO" ? $restaurante->nombre = "MARISCOS EL BAYO" : "";
                $restaurante->nombre == "EL PATRON" ? $restaurante->nombre = "ANTRO BANDA BAR" : "";
                $restaurante->nombre == "ESTANCIA ARGENTINA" ? $restaurante->nombre = "PLAYA DE ORO BOCA DEL RIO" : "";
                $restaurante->nombre == "FRASCATI" ? $restaurante->nombre = "FRASCATI SUR" : "";
                $restaurante->nombre == "GALAXY" ? $restaurante->nombre = "O GALAXY" : "";
                $restaurante->nombre == "GAUCHO TRADICIONAL PALMAS" ? $restaurante->nombre = "GAUCHO TRADICIONAL" : "";
                $restaurante->nombre == "GUANDOLA" ? $restaurante->nombre = "GUANDOLA" : "";
                $restaurante->nombre == "IMPULSOR MAZARIEGOS" ? $restaurante->nombre = "ULSOR MAZAR" : "";
                $restaurante->nombre == "HANKS" ? $restaurante->nombre = "HANKS" : "";
                $restaurante->nombre == "HARBOR´S" ? $restaurante->nombre = "MOCAMBO BOCA DEL RIO VERACRUZ" : "";
                $restaurante->nombre == "HATRIA" ? $restaurante->nombre = "HATRIA" : "";
                $restaurante->nombre == "HIJAS DE LA TOSTADA" ? $restaurante->nombre = "HIJAS DE LA TOSTADA" : "";
                $restaurante->nombre == "HOTEL 1972" ? $restaurante->nombre = "BAR SETENTA" : "";
                $restaurante->nombre == "HYATT ANDARES GDL" ? $restaurante->nombre = "ANDARES LO" : "";
                $restaurante->nombre == "IKEBANA" ? $restaurante->nombre = "IKEBANA" : "";
                $restaurante->nombre == "JAIBOL ANDAMAR" ? $restaurante->nombre = "JAIBOL ANDAMAR" : "";
                $restaurante->nombre == "JAPI" ? $restaurante->nombre = "JAPI " : "";
                $restaurante->nombre == "JARDIN PARAISO" ? $restaurante->nombre = "JARDIN PARAISO" : "";
                $restaurante->nombre == "KAMPAI 401" ? $restaurante->nombre = "KAMPAI 401" : "";
                $restaurante->nombre == "KAMPAIL PUEBLA" ? $restaurante->nombre = "009084" : "";
                $restaurante->nombre == "KARISMA" ? $restaurante->nombre = "KARISMA RESTAURANTE" : "";
                $restaurante->nombre == "KOKORO ANDARES" ? $restaurante->nombre = "KOKORO ANDARES" : "";
                $restaurante->nombre == "KOKORO SAO PAULO" ? $restaurante->nombre = "KOKORO SAO PAULO" : "";
                $restaurante->nombre == "KOKU LERMA" ? $restaurante->nombre = "KOKU LERMA" : "";
                $restaurante->nombre == "LA ACEITUNA" ? $restaurante->nombre = "OPERADORA NOVACO" : "";
                $restaurante->nombre == "LA ARTESANAL (GRUPO IMPULSOR MAZARIEGOS)" ? $restaurante->nombre = "CRISTOBAL DE LAS CASAS CHIAPAS" : "";
                $restaurante->nombre == "LA BIKINA" ? $restaurante->nombre = "LA BIKINA" : "";
                $restaurante->nombre == "LA INSURGENTE CHAPULTEPEC" ? $restaurante->nombre = "LA INSURGENTE" : "";
                $restaurante->nombre == "LA LOGGIA" ? $restaurante->nombre = "LA LOGGIA" : "";
                $restaurante->nombre == "LA MADALENA" ? $restaurante->nombre = "LA MADALENA" : "";
                $restaurante->nombre == "LA PALAPA" ? $restaurante->nombre = "LA PALAPA" : "";
                $restaurante->nombre == "LA VIKINA" ? $restaurante->nombre = "LA VIKINA" : "";
                $restaurante->nombre == "LOS JUANES" ? $restaurante->nombre = "OS JUANES CO" : "";
                $restaurante->nombre == "LOS LAURELES" ? $restaurante->nombre = "LOS LAURELES" : "";
                $restaurante->nombre == "L'OSTERIA DEL BECCO" ? $restaurante->nombre = "BECCO" : "";
                $restaurante->nombre == "MALLORQUINA SAN ÁNGEL" ? $restaurante->nombre = "GASTRO SAN ANGEL" : "";
                $restaurante->nombre == "MANDALA CLUB OAXACA" ? $restaurante->nombre = "ARMENTA Y LOP" : "";
                $restaurante->nombre == "MANGOS ANGELOPOLIS" ? $restaurante->nombre = "MANGOS PUE" : "";
                $restaurante->nombre == "MARISQUERIAS LA RED" ? $restaurante->nombre = "RIAS LA RED" : "";
                $restaurante->nombre == "LA BOCHA BQ" ? $restaurante->nombre = "BOCHA QUER" : "";
                $restaurante->nombre == "LA BOCHA NAUTICA" ? $restaurante->nombre = "BOCHA NÁUTICA" : "";
                $restaurante->nombre == "LA BOCHA QUERATERO" ? $restaurante->nombre = "BOCHA QUER" : "";
                $restaurante->nombre == "LA BUENA BARRA" ? $restaurante->nombre = "ARISTOTELES 124" : "";
                $restaurante->nombre == "BUENA BARRA MTY" ? $restaurante->nombre = "MISSOURI 600 INT" : "";
                $restaurante->nombre == "LA VACA ARGENTINA" ? $restaurante->nombre = "UERTO VALLARTA, JALISCO" : "";
                $restaurante->nombre == "MATILDE" ? $restaurante->nombre = "COLONIAS 221 GUADALAJARA" : "";
                $restaurante->nombre == "MARISKEÑA CAMPESTRE" ? $restaurante->nombre = "ULTRAINSUMOS S." : "";
                $restaurante->nombre == "MICAELA MAR Y LEÑA" ? $restaurante->nombre = "CALLE 47 #458" : "";
                $restaurante->nombre == "MI COMPA CHAVA" ? $restaurante->nombre = "MI COMPA CHAVA" : "";
                $restaurante->nombre == "MORITAS GARZA SADA" ? $restaurante->nombre = "MORITAS GARZA SADA" : "";
                $restaurante->nombre == "GRAN BARRA DOBLE B" ? $restaurante->nombre = "GRAN BARRA DOBLE B" : "";//
                $restaurante->nombre == "MORITAS INSURGENTES" ? $restaurante->nombre = "BOTANERO MORITAS INSURGENTES" : "";
                $restaurante->nombre == "MORITAS VALLE ORIENTE" ? $restaurante->nombre = "BOTANERO MORITAS VO" : "";
                $restaurante->nombre == "MORTON'S" ? $restaurante->nombre = "MORTON" : "";
                $restaurante->nombre == "MUI MUI" ? $restaurante->nombre = "MUI MUI" : "";
                $restaurante->nombre == "MUMMA ROOFTOP BAR" ? $restaurante->nombre = "MUMMA ROOFTOP BAR" : "";
                $restaurante->nombre == "PACÌFICO FUENTES DE SATELITE." ? $restaurante->nombre = "ANTOJERIA DEL MAR" : "";
                $restaurante->nombre == "PALOMINOS" ? $restaurante->nombre = "51 COLONIA LOS LAGOS" : "";
                $restaurante->nombre == "PALOMINOS INSURGENTES" ? $restaurante->nombre = "INSURGENTES SUR 734" : "";
                $restaurante->nombre == "PALOMINOS PROVIDENCIA" ? $restaurante->nombre = "VIDENCIA GUADALAJARA" : "";
                $restaurante->nombre == "PALOMINOS PUNTO SUR" ? $restaurante->nombre = "STROLISCO S.A" : "";
                $restaurante->nombre == "PARAGUAS NORTE" ? $restaurante->nombre = "PARAGUAS" : "";
                $restaurante->nombre == "PENINSULA" ? $restaurante->nombre = "INSULA A" : "";
                $restaurante->nombre == "PESCADORES SAN MANUEL" ? $restaurante->nombre = "AR LOS PESCADORES" : "";
                $restaurante->nombre == "PESCADORES ZAVALETA" ? $restaurante->nombre = "BARRA-CAFETERIA" : "";
                $restaurante->nombre == "PORFIRIO'S GUADALAJARA" ? $restaurante->nombre = "PORFIRIO" : "";
                $restaurante->nombre == "RESTAURANTE BEROA" ? $restaurante->nombre = "OPERADORA GOURMET AYB" : "";
                $restaurante->nombre == "RINCÓN ARGENTINO POLANCO" ? $restaurante->nombre = "COMANDA DE BAR" : "";
                $restaurante->nombre == "RINCÓN ARGENTINO SANTA FE" ? $restaurante->nombre = "RINCÓN ARGENTINO SANTA FE" : "";
                $restaurante->nombre == "RUSO BLANCO CLUB DE SOCIOS" ? $restaurante->nombre = "RUSO BLANCO CLUB DE SOCIOS" : "";
                $restaurante->nombre == "SALON RIOS" ? $restaurante->nombre = "SALON RIOS" : "";
                $restaurante->nombre == "SAMBUKA" ? $restaurante->nombre = "515 45 64" : "";
                $restaurante->nombre == "SAN MAREE" ? $restaurante->nombre = "SAN MAREE" : "";
                $restaurante->nombre == "SEÑOR BAMBOO" ? $restaurante->nombre = "R BAMBOO" : "";
                $restaurante->nombre == "SINALOENSE" ? $restaurante->nombre = "SINALOENSE" : "";
                $restaurante->nombre == "SRA TANAKA" ? $restaurante->nombre = "SRA TANAKA" : "";
                $restaurante->nombre == "TABERNA DEL LEÓN" ? $restaurante->nombre = "TABERNA DEL LEON" : "";
                $restaurante->nombre == "TEQUILAZOO (GRUPO IMPULSOR MAZARIEGOS)" ? $restaurante->nombre = "TEQUILAZOO" : "";
                $restaurante->nombre == "TERRAZA DO BRASIL" ? $restaurante->nombre = "TERRAZA DO BRASIL" : "";
                $restaurante->nombre == "TIERRA DEL SOL" ? $restaurante->nombre = "GRUPO CABSAN S" : "";
                $restaurante->nombre == "TRIBULUS" ? $restaurante->nombre = "TRIBULUS" : "";
                $restaurante->nombre == "VACA ARGENTINA GOURMETERIA" ? $restaurante->nombre = "VACA ARGENTINA GOURMETERIA" : "";
                $restaurante->nombre == "VINI" ? $restaurante->nombre = "VINI" : "";
                $restaurante->nombre == "WALLACE WHISKY BAR" ? $restaurante->nombre = "WALLACE WHISKY BAR" : "";
                //lo que se quiera arreglar
                
                if($restaurante->nombre == "DEL BARRIL"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "DEL BARRIL"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "DELBARRIL"))
                        $pasar = false;

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "BARRA(COMEDOR)"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "BARRA(COMEDOR)"))
                        $pasar = false;
                    
                    if(str_contains(mb_strtoupper($lectura->contenido), "KOKORO S"))
                        $pasar = true;

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "JARDIN PARAISO"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "PLAZA VILLA DE MADRID"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "CAFÉ PARAISO"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "CAFE PARAISO"))
                        $pasar = false;

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "AZIA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "AZIA")){
                        $revision = substr($lectura->contenido, 0, 150);
                        if(str_contains(mb_strtoupper($revision), "AZIA"))
                            $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "ARISTOTELES 124"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "ARISTOTELES 124"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "LA BRENA BARRA CDMX")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "MISSOURI 600 INT"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MISSOURI 600 INT"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MISSOURI 600 TNT")){
                        $pasar = false;
                    }/* 
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "LA BRENA BARRA CDMX")){
                        $pasar = false;
                    } */

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "RINCÓN ARGENTINO SANTA FE"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "HAMBURGUESAS RAPIDAS LOMAS SA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "L SABOR AUSTRAL S.A.")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "EY SABOR AUSTRAL S.A.")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "EL SAHOR AUSTRALSA")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "EL SABIA AUSTRAL")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "TABERNA DEL LEON"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "TABERNA DEL LEÓN"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TABERNA DEL LEON")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TEL: 5616 -")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "FPA ALTAMIRANO 46")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TA BONA\nLEON")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TABERNA DEL T")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TABERNA DEL SO")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TABERNA ELLER")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "STROLISCO S.A"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "STROLISCO S.A"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ALOMINO")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "LOMINOS")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "\nOMINOS")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PALMINOS")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MINO\n=")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "== AGREGADO")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "AGREGADO ==")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PALONNOS")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PALOMINES")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "== CUENTA NUEVA")){
                        $restaurante->nombre = "PALOMINO CORTO";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "INSURGENTES SUR 734"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "INSURGENTES SUR 734"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "INSURGENTES\nFELIPE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "RESTAURANT PALOHINOS\nGASTROPAS")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "ASADOR VASCO PARQUE"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "ASTRONOMICA EDER S.A"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ASADOR VASCO")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "OR VASCO\n")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "19SXA")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "GRUPO CABSAN S"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "GRUPO CABSAN S"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "GRUPO CAB SAN S")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TEL:9515")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "LA INSURGENTE"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "LA INSURGENTE"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "LA\nINSURGENTE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "INSURGENTE\n")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "RFC RACR85")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "MOCAMBO BOCA DEL RIO VERACRUZ"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MOCAMBO BOCA DEL RIO VERACRUZ"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MOCAMBO BOCA DE RIO VERACRUZ")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MOCAMBO BOCA DEL RIOERACRUZ")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MOCAMBO) BACA")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MOCAMBO BOCA DI RIO VERACRUZ")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARBORS")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "CLOVER"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MIGUEL HIDALGO #1B"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MIGUEL HIDALGO #18")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "CERVECERIA CHAPULTEPEC"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "ERIA CHAP"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ESTA CHAP")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "DON CAPITAN S"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "DON CAPITAN S"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "DCA040427")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "NAUCALPAN EDO MEX C.P.53150")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "NAUCALPAN EDO NEX C.P.53150")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "NAUCALPAN EDO NEX CO 8150")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "NAUCALPAN EDO HEX EP.53150")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "NAUCALPAN EDO EXCEP.33150")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "AR LOS PESCADORES"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "AR LOS PESCADORES"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "RES SAN MANUE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ARLOS PESCADORE")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "LA BIKINA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "BIKINA P"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "BIKIN P")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "BIKINA D")){
                        $restaurante->nombre = "LA BIKINA DE BOCA";
                        $values->restaurante = $restaurante->nombre;
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "HANKS"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "HARRYIS NEW"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRYTS NEW")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRYDS NEW")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRY'S NEW")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRY NOW")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRYS NEW")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRY NEW ORL")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRYIA NEW ORL")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HARRVIS NEW ORL")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HAY NEW ORL")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "NEW ORLEANE CAFE & OKTAYAR")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "NEW ORLEANS CAT\n")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "(415) 152 2645")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "(415) 152-2645")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "(415) 152645")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "(415) 152\n")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "415) 152-2045")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "(415) 152 2045")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "HIDALGO 12\nSAN")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "BOCHA QUER"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "BOCHA QUER"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "BOCINA QUER")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "442) 562-9117")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "442) 962-9117")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "962-9117")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "GAUCHO TRADICIONAL"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "GAUCHO TRADICIONAL"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "20429")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "JUNCAL 20")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "ULTRAINSUMOS S."){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "ULTRAINSUMOS S."))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "UL TRAINSUMOS S.")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "PARAGUAS"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "PARAGUAS"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PAYUR S")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PARAGUJAS")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "RFC:GPA14")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "U5123")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PARAUAD")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "LUIS DONALDO COLOSIO 1725, LOMAS")){
                        $pasar = false;
                    }

                    if(str_contains(mb_strtoupper($lectura->contenido), "PIANO"))
                        $pasar = true;

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "TERRAZA DO BRASIL"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "TERRAZA DO BRASIL"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TERRA DO BRASIL")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TERRAZA DO ORASIL")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "S\n2291003993")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "ANTOJERIA DEL MAR"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "ANTOJERIA DEL MAR"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ANTOJENA DEL MAR")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ARCO NA DEL MAR")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PACIFICO\nANTO")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PACIFICO\n\"APORIA")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "BRICK STEAKHOUSE"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "BRICK STEAKHOUSE"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "3324717066")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "TEREZA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "TEREZA"))
                    $pasar = false;
                    

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "KOKORO ANDARES"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "KOKORO ANDARES"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "XORO ANDARES")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "RFC:OHM200")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "OHM200715")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "200715024")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PUERTA DE HIERRO 4965 INT UP3-10")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "HIJAS DE LA TOSTADA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "HIJAS DE LA TOSTADA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PLAZA CITY CENTER LOCAL F2")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "SAO PAULO 2367 COLONIA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "SAO PAULO 2367 COLONIA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ALCACHOFASP")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "DIVINO 106"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "DIVINO 106"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "DIVINO. 106")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "DIVANO 106")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "BARRA DE SACRIFICIO"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "BARRA DE SACRIFICIO"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "BARRA SACRIFICIO")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "MUI MUI"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MUI MUI"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MUI MUL")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MUT MUT")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MUIT MUI")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MIT HUI")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "TEQUILAZOO"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "GENERAL UTRILLA S/N"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "TEQUILAZOO")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "1528 BAR"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MIGUEL HIDALGO 1D, ESQUINA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "MIGUEL HIDALGO 1D, E")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "EL BARRIO LA BOCHA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "76180 QUER"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "EL BARRIO DE LA BOCHA")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "EL BARRIB DE LA BOCHA")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "EL BARRID DE LA BOCHA")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "BARRA LA NACIONAL"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "CALLE 32 NO."))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "CALLE 32 N0.")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "CALLE 32 #")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "SRA TANAKA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "AV ROBLE 660"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "SRA TANAKA")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "MORITAS GARZA SADA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "LAS MORITAS DE SAN PEDRO"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), " 21690086")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "GRAN BARRA DOBLE B"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "GRAN BARRA DOBLE B")){
                        $pasar = false;
                    }
                    

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "LA PALAPA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "LA PALAPA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "LA PALAP")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PALAPA\n")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "CORAZON DE ALCACHOFA ANDARES"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "CORAZON DE ALCACHOFA ANDARES"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PUERTA DE HIERRO 4965 INT 112")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "VACA ARGENTINA GOURMETERIA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "CA GOUR"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "VACA GALE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "WACA GOUR")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "130716-")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "VACA COURNET")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "RUSO BLANCO CLUB DE SOCIOS"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "RUSO BLANCO"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "007CL6")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "TRIBULUS"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "40 GRADOS"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "8063PO")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "ALMACEN DEL BIFE"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "EL ALMACEN"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "06AM9")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "BALBOA LERMA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "BALBOA CONDESA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "22T2A")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "VINI"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "VINI VIDI VICI"))
                        $pasar = false;
                    if(str_contains(mb_strtoupper($lectura->contenido), "VINI THE"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "30M92")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "VIN, THE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "VIN THE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "VINI\nTHE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "VINO THE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "VING THE")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "FACTURACIONVINI")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "EDIFICIO ALCALA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "EDIFICIO ALCALA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "ICIOALC")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "DELI DELI"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "DELI GOURMET"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "OPERADORA GOURMET")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "DELI&DELI")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "22QUO")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "KOKU LERMA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "KOKU LERMA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "07-3344")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "RIO LERMA 94 COL")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "LA VIKINA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "VIKINA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "225KN7")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "MORTON"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MORTON"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "40-7897")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "WALLACE WHISKY BAR"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "WALLACE WHISKY"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "WALLACE WHISKY Y")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "BOCHA NÁUTICA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "BOCHA NÁUTICA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "BOCH N")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "BOCHA NAUTI")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "INSULA A"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "INSULA A"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PENINSULA\n")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "DON CAPITÁN POLANCO"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "DONCAPITANPOLANCO"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "DON CAPITAN POLANCO")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "56274")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "GUANDOLA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "GUANDOLA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "KABAH\n")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "HATRIA"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "HATRIA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "1299F9")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "JAIBOL ANDAMAR"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "EL JAIBON"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "34240")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "JAPI "){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "JAPI "))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "JAPI\nFACT")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "MARISCOS EL BAYO"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MARISCOS EL BAYO"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "AYO RUI")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "LA CASA DEL HABANO CABO"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "LOS CABOS SA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "404GMA")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "MUMMA ROOFTOP BAR"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "MUMMA"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "04A26")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif($restaurante->nombre == "PORFIRIO"){
                    $pasar = true;
                    if(str_contains(mb_strtoupper($lectura->contenido), "PORFIRIO"))
                        $pasar = false;
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "PORFIRID'S")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "S PUERTO VA")){
                        $pasar = false;
                    }
                    elseif(str_contains(mb_strtoupper($lectura->contenido), "O PORTO V")){
                        $pasar = false;
                    }

                    if($pasar) continue;
                }
                elseif(!str_contains(mb_strtoupper(str_replace(array(':'), '', $lectura->contenido)), $restaurante->nombre)) 
                    continue;
                
                switch ($restaurante->nombre) {
                    
                    case "BARRA(COMEDOR)": //AGAVES
                        $values =  Agaves::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ALMACEN DEL BIFE": //ALMACEN DEL BIFE
                        $values =  Almacen::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ANATOLE": //ANATOL
                        $values =  Anatol::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "E ARGENTILIA": //ANATOL
                        $values =  Argentilia::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ASADOR VASCO PARQUE": //ASADOR VASCO PARQUE
                        $values =  AsadorVasco::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "AZIA": //AZIA
                        $values =  Azia::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "BALBOA LERMA": //BALBOA LERMA
                        $values =  Balboa::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "1528 BAR": //1528 BAR (GRUPO IMPULSOR MAZARIEGOS)
                        $values =  Bar1528::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BARRA LA NACIONAL": //BARRA LA NACIONAL
                        $values =  BarraLaNacional::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BARRA DE SACRIFICIO": //BARRA DE SACRIFICIOS
                        $values =  BarraSacrificios::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "EL BARRIO LA BOCHA": //EL BARRIO DE LA BOCHA
                        $values =  BarrioLaBocha::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ADOR CHAPALITA": //LA BOCHA CHAPALITA
                        $values =  BochaChapalita::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BRICK STEAKHOUSE": //BRICK
                        $values =  Brick::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "TEREZA": //TEREZA
                        $values =  Tereza::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;  
                         
                    case "HACIENDA CAMPANARIO QUERETARO": //BRUNO QRO
                        $values =  BrunoQRO::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA TRATTO S": //BRYAN´S MONTECRISTO
                        $values =  BryanMonte::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BULL MCCABE": //BULL MCCABE
                        $values =  BullMccabe::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BURGER BAR ": //BURGER BAR FORUM | ROMA | TOREO
                        $values =  BurgerBar::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "XXX TIEM": //CACCIO GABILONDO
                        $values =  CaccioGabilondo::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "20 GUADA": //CANTINA LA 20
                        $values =  Cantina20::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "NZA PRIME": //CARRANZA PRIME
                        $values =  Carranza::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CASA BELL": //CASA BELL
                        $values =  CasaBell::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA CASA DEL HABANO CABO": //LA CASA DEL HABANO CABO
                        $values =  CasaHabano::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "52027146": //CASA Ó
                        $values =  CasaO::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "EMILIO CASTELAR 1": //CASA PORTUGUESA
                        $values =  CasaPortuguesa::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CERVECERIA CHAPULTEPEC": //CERVECERIA CHAPULTEPEC
                        $values =  CerveceriaC::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "180601MTA": //CIGAR POINT
                        $values =  CigarPoint::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CLOVER": //CLOVER (GRUPO IMPULSOR MAZARIEGOS)
                        $values =  Clover::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "TEDDYS": //COCINA ABIERTA
                        $values =  CocinaAbierta::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "COMEDOR DE LOS MILAGROS": //COMEDOR DE LOS MILAGROS
                        $values =  ComedorMilagros::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CALLE 7B ": //CORAJILLO
                        $values =  Corajillo::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "SAO PAULO 2367 COLONIA": //CORAZÓN DE ALCACHOFA SAO PAULO
                        $values =  CorazonAlcachofa::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CORAZON DE ALCACHOFA ANDARES": //CORAZON DE ALCACHOFA ANDARES
                        $values =  CorazonAlcachofaAndares::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "COSTEÑITO ": //COSTEÑITO PUEBLA
                        $values =  Costeñito::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CUS CUS": //CUS CUS
                        $values =  CusCus::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "DANTE BRASA Y FUEGOS": //DANTE
                        $values =  Dante::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "DEL BARRIL": //DEL BARRIL
                        $values =  DelBarril::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "DELI DELI": //DELI & DELI GOURMET
                        $values =  DeliDeli::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "DIVINO 106": //DIVINO 106
                        $values =  Divino106::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "DURANGO 149 INT": //DON ASADO POLANCO
                        $values =  DonAsadoPolanco::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "DON CAPITAN S": //DON CAPITAN NAUCALPAN
                        $values =  DonCapitanNacualpan::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "DON CAPITÁN POLANCO": //DON CAPITAN NAUCALPAN
                        $values =  DonCapitanPolanco::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                    
                    case "DE DON CAPITAN": //DON CAPITAN SANTA FE
                        $values =  DonCapitanSantaFe::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "EDIFICIO ALCALA": //EDIFICIO ALCALA
                        $values =  EdificioAlcala::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "MARISCOS EL BAYO": //MARISCOS EL BAYO
                        $values =  ElBayo::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "PLAYA DE ORO BOCA DEL RIO": //ESTANCIA ARGENTINA
                        $values =  EstanciaArgentina::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "FRASCATI SUR": //FRASCATI
                        $values =  Frascati::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "O GALAXY": //GALAXY
                        $values =  Galaxy::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "GAUCHO TRADICIONAL": //GAUCHO TRADICIONAL PALMAS
                        $values =  GauchoTradicional::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "GUANDOLA": //GUANDOLA
                        $values =  Guandola::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ULSOR MAZAR": //IMPULSOR MAZARIEGOS
                        $values =  GrupoImpulsor::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ANTRO BANDA BAR": //EL PATRON
                        $values =  ElPatron::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "MOCAMBO BOCA DEL RIO VERACRUZ": //HARBOR´S
                        $values =  Harbors::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "HANKS": //HANKS
                        $values =  Hanks::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "HATRIA": //HATRIA
                        $values =  Hatria::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "HIJAS DE LA TOSTADA": //HIJAS DE LA TOSTADA
                        $values =  HijasTostada::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BAR SETENTA": //HOTEL 1972
                        $values =  Hotel1972::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ANDARES LO": //HYATT ANDARES GDL
                        $values =  Hyatt::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "IKEBANA": //IKEBANA
                        $values =  Ikebana::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "JAIBOL ANDAMAR": //JAIBOL ANDAMAR
                        $values =  Jaibon::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "JAPI ": //JAPI
                        $values =  Japi::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "JARDIN PARAISO": //JARDIN PARAISO
                        $values =  JardinParaiso::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "KAMPAI 401": //KAMPAI 401
                        $values =  Kampai401::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "009084": //KAMPAIL PUEBLA
                        $values =  KampailPuebla::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "KARISMA RESTAURANTE": //KARISMA
                        $values =  Karisma::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "KOKORO ANDARES": //KOKORO ANDARES
                        $values =  KokoroAndares::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "KOKORO SAO PAULO": //KOKORO SAO PAULO
                        $values =  KokoroSauPaulo::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "KOKU LERMA": //KOKU LERMA
                        $values =  KokuLerma::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "OPERADORA NOVACO": //LA ACEITUNA
                        $values =  LaAceituna::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CRISTOBAL DE LAS CASAS CHIAPAS": //LA ARTESANAL
                        $values =  LaArtesanal::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA BIKINA": //LA BIKINA POLANCO
                        $values =  LaBikina::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA BIKINA DE BOCA": //LA BIKINA DE BOCA
                        $values =  LaBikina2::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA INSURGENTE": //LA INSURGENTE CHAPULTEPEC
                        $values =  LaInsurgente::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA LOGGIA": //LA LOGGIA
                        $values =  LaLoggia::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA MADALENA": //LA MADALENA
                        $values =  LaMadalena::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LA PALAPA": //LA PALAPA
                        $values =  LaPalapa::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "LA VIKINA": //LA VIKINA
                        $values =  LaVikina::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "OS JUANES CO": //LOS JUANES
                        $values =  LosJuanes::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "LOS LAURELES": //LOS LAURELES
                        $values =  LosLaureles::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BECCO": //L'OSTERIA DEL BECCO
                        $values =  LosteriaBecco::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "GASTRO SAN ANGEL": //MALLORQUINA SAN ÁNGEL
                        $values =  Mallorquina::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ARMENTA Y LOP": //MANDALA CLUB OAXACA
                        $values =  Mandala::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "MANGOS PUE": //MANGOS ANGELOPOLIS
                        $values =  MangosAng::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "RIAS LA RED": //MARISQUERIAS LA RED
                        $values =  MarisqueriaRed::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BOCHA QUER": //LA BOCHA QUERETARO
                        $values =  BochaBQ::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BOCHA NÁUTICA": //LA BOCHA NAUTICA
                        $values =  BochaNautica::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ARISTOTELES 124": //LA BUENA BARRA
                        $values =  BuenaBarra::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "MISSOURI 600 INT": //LA BUENA BARRA MTY
                        $values =  BuenaBarra2::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "UERTO VALLARTA, JALISCO": //LA VACA ARGENTINA
                        $values =  VacaArgentina::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "COLONIAS 221 GUADALAJARA": //MATILDE
                        $values =  Matilde::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "ULTRAINSUMOS S.": //MARISKEÑA CAMPESTRE
                        $values =  Mariskeña::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "CALLE 47 #458": //MICAELA MAR Y LEÑA
                        $values =  MicaelaMar::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "MI COMPA CHAVA": //MI COMPA CHAVA
                        $values =  MiCompaChava::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "MORITAS GARZA SADA": //MORITAS GARZA SADA
                        $values =  MoritasGarza::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                    case "GRAN BARRA DOBLE B": //GRAN BARRA DOBLE B
                        $values =  GranBarra::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                    case "BOTANERO MORITAS INSURGENTES": //MORITAS INSURGENTES
                        $values =  MoritasInsurgentes::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BOTANERO MORITAS VO": //MORITAS VALLE ORIENTE
                        $values =  MoritasValle::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "MORTON": //MORTON’S
                        $values =  Mortons::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "MUI MUI": //MUI MUI
                        $values =  MuiMui::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "MUMMA ROOFTOP BAR": //MUMMA ROOFTOP BAR
                        $values =  Mumma::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "ANTOJERIA DEL MAR": //PACÌFICO FUENTES DE SATELITE.
                        $values =  PacificoFuentes::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "51 COLONIA LOS LAGOS": //PALOMINOS DE CDMX EN RUISEÑOR
                        $values =  Palominos::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "PALOMINO CORTO": //PALOMINO CORTO
                        $values =  Palominos2::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "INSURGENTES SUR 734": //PALOMINOS DE CDMX EN INSURGENTES
                        $values =  PalominosInsurgentes::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "VIDENCIA GUADALAJARA": //PALOMINOS DE CDMX EN PROVIDENCIA
                        $values =  PalominosProvidencia::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "STROLISCO S.A": //PALOMINOS PUNTO SUR
                        $values =  PalominosSur::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "PARAGUAS": //PARAGUAS NORTE
                        $values =  ParaguasNorte::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "INSULA A": //PENINSULA
                        $values =  Peninsula::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "AR LOS PESCADORES": //PESCADORES SAN MANUEL
                        $values =  PescadorSanManuel::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "BARRA-CAFETERIA": //PESCADORES ZAVALETA
                        $values =  PescadorZabaleta::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "PORFIRIO": //PORFIRIO'S
                        $values =  Porfirio::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "OPERADORA GOURMET AYB": //RESTAURANTE BEROA
                        $values =  ResBeroa::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "COMANDA DE BAR": //RINCÓN ARGENTINO POLANCO
                        $values =  RinconPolanco::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "RINCÓN ARGENTINO SANTA FE": //RINCÓN ARGENTINO SANTA FE
                        $values =  RinconSantaFe::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "RUSO BLANCO CLUB DE SOCIOS": //RUSO BLANCO CLUB DE SOCIOS
                        $values =  RusoBlanco::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "SALON RIOS": //SALON RIOS
                        $values =  SalonRio::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "515 45 64": //SAMBUKA
                        $values =  Sambuka::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "SAN MAREE": //SAN MAREE
                        $values =  SanMaree::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "R BAMBOO": //SEÑOR BAMBOO
                        $values =  SeñorBamboo::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "SINALOENSE": //SINALOENSE
                        $values =  Sinaloense::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "SRA TANAKA": //SRA TANAKA
                        $values =  SraTanaka::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "TABERNA DEL LEON": //TABERNA DEL LEON
                        $values =  TabernaLeon::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "TEQUILAZOO": //TEQUILAZOO (GRUPO IMPULSOR MAZARIEGOS)
                        $values =  Tequilazo::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "TERRAZA DO BRASIL": //TERRAZA DO BRASIL
                        $values =  TerrazaBrasil::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "GRUPO CABSAN S": //TIERRA DEL SOL
                        $values =  TierraSol::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "TRIBULUS": //TRIBULUS
                        $values =  Tribulus::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "VACA ARGENTINA GOURMETERIA": //VACA ARGENTINA GOURMETERIA
                        $values =  VacaGourmet::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;

                    case "VINI": //VINI THE BAR
                        $values =  Vini::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                        
                    case "WALLACE WHISKY BAR": //WALLACE WHISKY BAR
                        $values =  Wallace::instance()->params($values, mb_strtoupper($lectura->contenido)); 
                        $default = true; break;
                }
            }

            //EVALUAMOS SI NO APARECE PRODUCTO
            if(empty($values->productos) /* && $values->estado != "pendiente" */)
                $values->estado = "pendiente";

            if($values->referencia == "No parametrizo")
                $values->restaurante = null;

            //Sacamos la fecha el mes para el tema de categoriar la factura en category
                $meses = array(
                    "01" => "Enero", "02" => "Febrero", "03" => "Marzo", "04" => "Abril", 
                    "05" => "Mayo", "06" => "Junio", "07" => "Julio", "08" => "Agosto",
                    "09" => "Septiembre", "10" => "Octubre", "11" => "Noviembre", "12" => "Diciembre",
                );
                $month = date("m",strtotime($values->fecha_factura));

                try{ 
                    $values->category = $meses[$month]; 
                }catch(\Exception $e){
                    $month = date('m'); 
                    $values->category = $meses[$month];
                }

            
            //Tiempo valido el mes actual hasta los primeros 10 días del siguiente mes / para validar fact
                isset($lectura->upload) ? "" : $lectura->upload = date('Y-m-d');

                $time = strtotime($lectura->upload);
                //Evaluamos en que dia estamos del mes actual
                    $dia = date("d",$time);//date("d");
            
                //Mes Actual o Anterior para ajustar;
                    $mes1 = date('Y-m-01', $time);//date("Y-m-01"); 
                    if($dia < 10){ //ajuste por si estamos aun dentro de los 10 días
                        $mes1 = date('Y-m-d', strtotime("-1 months", strtotime($mes1)));
                    }
                    
                //Sumamos un mes
                    $mes2 = date('Y-m-d', strtotime("+1 months", strtotime($mes1)));

                //Agregamos 10 días
                    $mes2 = date('Y-m-d', strtotime($mes2. ' + 9 days'));
                
                //Validamos la fecha de la factura actual para ver si la rechazamos o no
                    $mes1 = strtotime($mes1);
                    $mes2 = strtotime($mes2);
                    $date = strtotime($values->fecha_factura);
                    
                    if($date < $mes1){
                        $values->estado = "rechazado";
                    }
                    elseif($date > $mes2){
                        $values->estado = "pendiente";
                    }
            
            //Validar si la leemos nosotros o no para dejar en pendiente
                if(!$values->encargado)
                    $values->estado = "pendiente";
    
            //Validamos si la referencia ya esta cargado con el CDC indicado
                if(isset($lectura->reparam)){
                    if($lectura->reparam == "no"){
                        if($values->referencia != "N/A" && $values->referencia != "No parametrizo" /* && $values->estado == "aceptado" */){
                            //$check_fact = Factura::where('cdc', $values->restaurante)->where('folio', $values->referencia)->first();
                            $check_fact = Factura::where('cdc', $values->restaurante)->
                                                   where('folio', $values->referencia)->first();
                                                   //whereDate('registro', date("Y-m-d", strtotime($values->fecha_factura)))->first();

                            if($check_fact){
                                $values->estado = "rechazado";
                            }
                        }
                        /* elseif($values->referencia != "N/A"){
                            $check_fact = Factura::where('cdc', $values->restaurante)->where('registro', $values->fecha_factura)->first();

                            if($check_fact){
                                $values->estado = "folio repetido";
                            }
                        } */
                    }
                }

            //Sacamos el tarifario que fue de acuerdo a la fecha impresa
                //  $tarifa = array(
                //      "01" => "ene_feb", "02" => "ene_feb", 
                //      "03" => "mar_abr", "04" => "mar_abr", 
                //      "05" => "may_jun", "06" => "may_jun", 
                //      "07" => "jul_ago", "08" => "jul_ago",
                //      "09" => "sep_oct", "10" => "sep_oct", 
                //      "11" => "nov_dic", "12" => "nov_dic",
                //  );
                //  $mes_fact = date("m", strtotime($values->fecha_factura));

                 try{ 
                   // $values->tarifario = $tarifa[$mes_fact]; 
                   //asigno el valor que esta en la base de datos
                    $values->tarifario = $values->tarifario; 
                
                    
                 }catch(\Exception $e){
                     //$mes_fact = date('m'); 
                     $values->tarifario = 'ti';
                 }

            //coloco la etiqueta del tarifario que se encuentra en la base de datos

            return $values;

        }
        catch(\Exception $e) {
            //ALERTA DE ERROR DURANTE EL PROCESO DE LECTURA
            $values = new \stdClass;
            $values->error = 'Message Error: ' .$e->getMessage().' | '.$e->getLine();
            return $values;
        }
    }
}
?>