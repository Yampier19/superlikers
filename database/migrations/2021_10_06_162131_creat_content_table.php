<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('content_read', function (Blueprint $table) {
            Schema::create('content_read', function (Blueprint $table) {
                $table->id('id');
                $table->string('id_archivo'); //ID GENERICO INVENTADO
                $table->string('archivo'); //LINK DE LA IMAGEN
                $table->text('contenido'); //LECTURA DEL GOOGLE VISION O PARSER
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_read');
    }
}
