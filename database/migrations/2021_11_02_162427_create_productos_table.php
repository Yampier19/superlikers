<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            Schema::create('productos', function (Blueprint $table) {
                $table->id('id_productos');
                $table->string('marca');
                $table->string('referencia');
                $table->string('line');
                $table->boolean('activo')->default(1);
                $table->timestamps();
            });
        });

        DB::table('productos')->insert(array(
            array(
                'marca' => 'ABSOLUT BLUE',
                'referencia' => '9876',
                'line' => 'VODKA',
            ),
            array(
                'marca' => 'ABSOLUT EXTRAKT',
                'referencia' => '9877',
                'line' => 'VODKA',
            ),
            array(
                'marca' => 'BEEFEATER DRY',
                'referencia' => '9878',
                'line' => 'GINEBRA',
            ),
            array(
                'marca' => 'BEEFEATER PINK',
                'referencia' => '9879',
                'line' => 'GINEBRA',
            ),
            array(
                'marca' => 'BEEFEATER BLACKBERRY',
                'referencia' => '9880',
                'line' => 'GINEBRA',
            ),
            array(
                'marca' => 'CHIVAS 13 TEQUILA',
                'referencia' => '9881',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'CHIVAS 13 SHERRY',
                'referencia' => '9882',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'CHIVAS XV',
                'referencia' => '9883',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'THE GLENLIVET FOUNDERS',
                'referencia' => '9884',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'HAVANA 7',
                'referencia' => '9885',
                'line' => 'RON',
            ),
            array(
                'marca' => 'MARTELL VSOP',
                'referencia' => '9886',
                'line' => 'COÑAC',
            ),
            array(
                'marca' => "BALLATINE'S FINEST",
                'referencia' => '9887',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'ALTOS PLATA',
                'referencia' => '9888',
                'line' => 'TEQUILA',
            ),
            array(
                'marca' => 'THE GLENLIVET 12',
                'referencia' => '9889',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'CHIVAS 18',
                'referencia' => '9890',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'MARTELL BS',
                'referencia' => '9891',
                'line' => 'COÑAC',
            ),
            array(
                'marca' => 'AVION R44',
                'referencia' => '9892',
                'line' => 'TEQUILA',
            ),
            array(
                'marca' => 'MONKEY 47',
                'referencia' => '9893',
                'line' => 'GINEBRA',
            ),
            array(
                'marca' => 'JAMESON',
                'referencia' => '9894',
                'line' => 'WHISKY',
            ),
            array(
                'marca' => 'ABSOLUT WATERMELON',
                'referencia' => '9895',
                'line' => 'VODKA',
            ),
            array(
                'marca' => 'ABSOLUT ELYX',
                'referencia' => '9896',
                'line' => 'VODKA',
            ),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
