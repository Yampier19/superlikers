<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMesToTarifario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tarifario', function (Blueprint $table) {
            $table->dropColumn('mes');
            $table->dropColumn('antmes');

            $table->date('mes_inicio');
            $table->date('mes_fin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tarifario', function (Blueprint $table) {
            //
        });
    }
}
