<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTarifarioTable extends Migration
{
    
    public function up()
    {
        Schema::table('tarifario', function (Blueprint $table) {
            Schema::create('tarifario', function (Blueprint $table) {
                $table->id('id_tarifario');
                $table->string('ciudad');
                $table->string('competidor')->default("PERNOD RICARD");
                $table->string('categoria');
                $table->bigInteger('copaxbotella')->default(11);
                $table->bigInteger('antcopaxbotella')->default(11)->nullable();
                $table->date('mes')->default(date("Y-m-d"));
                $table->date('antmes')->nullable();
                $table->timestamps();
                $table->bigInteger('FK_id_marca')->nullable()->unsigned(); //id_producto o marca

                $table->foreign('FK_id_marca')->references('id_productos')->on('productos');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('tarifario');
    }
}
