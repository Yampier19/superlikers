<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facturas', function (Blueprint $table) {
            Schema::create('facturas', function (Blueprint $table) {
                $table->id('id_factura');
                $table->string('programa')->default('pernod'); //programa de la factura, pernod (mesero) o pernod_off (vendedores)
                $table->string('referencia')->index();
                $table->string('cdc')->nullable(); //centro de consumo
                $table->string('folio')->nullable(); //Folio en la factura, su ID
                $table->string('mesero')->nullable(); //Nombre impreso en la factura
                $table->string('campaign'); //ti
                $table->string('state')->default('no param'); //estado de nuestro server empezara no param 
                $table->string('moderation'); //pending son las que necestamos
                $table->boolean('envio')->default(false); //estado de si se cargo en superlikers o no
                $table->string('photo');
                $table->timestamp('registro');
                $table->text('lectura')->nullable(); //LECTURA DEL GOOGLE VISION
                $table->string('tarifario')->default('N/A');
                $table->timestamp('user_upload')->nullable();
                $table->timestamps();
                $table->bigInteger('FK_id_participante')->nullable()->unsigned(); //distinct_id

                $table->foreign('FK_id_participante')->references('id_participante')->on('participante');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
