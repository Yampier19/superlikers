<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('factura_extra', function (Blueprint $table) {
            Schema::create('factura_extra', function (Blueprint $table) {
                $table->id('id_extra');
                $table->string('nombre');
                $table->timestamps();
                $table->bigInteger('FK_id_factura')->nullable()->unsigned();

                $table->foreign('FK_id_factura')->references('id_factura')->on('facturas');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_extra');
    }
}
