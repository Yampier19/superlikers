<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('factura_producto', function (Blueprint $table) {
            Schema::create('factura_producto', function (Blueprint $table) {
                $table->id('id_producto');
                $table->string('referencia'); //nombre
                $table->decimal('price',13,3);
                $table->decimal('quantity',13,2);
                $table->string('provider')->nullable();
                $table->string('line');
                $table->string('formato')->default('copa');
                $table->timestamps();
                $table->bigInteger('FK_id_factura')->nullable()->unsigned();

                $table->foreign('FK_id_factura')->references('id_factura')->on('facturas');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_producto');
    }
}
