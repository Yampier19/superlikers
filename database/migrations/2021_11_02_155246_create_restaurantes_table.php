<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateRestaurantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurantes', function (Blueprint $table) {
            Schema::create('restaurantes', function (Blueprint $table) {
                $table->id('id_restaurante');
                $table->string('nombre');
                $table->string('ciudad')->nullable();
                $table->boolean('activo')->default(1);
                $table->string('tipo')->default("ticket");
                $table->timestamps();
            });
        });

        DB::table('restaurantes')->insert(array(
            array(
                'nombre' => 'PALOMINOS PROVIDENCIA'
            ),
            array(
                'nombre' => 'TABERNA DEL LEÓN'
            ),
            array(
                'nombre' => 'PALOMINOS'
            ),
            array(
                'nombre' => 'LA BOCHA BQ'
            ),
            array(
                'nombre' => 'MATILDE'
            ),
            array(
                'nombre' => 'LA BOCHA NAUTICA'
            ),
            array(
                'nombre' => 'LA VACA ARGENTINA'
            ),
            array(
                'nombre' => 'LA BUENA BARRA'
            ),
            array(
                'nombre' => 'TERRAZA DO BRASIL'
            ),
            array(
                'nombre' => 'PALOMINOS INSURGENTES'
            ),
            array(
                'nombre' => 'DON CAPITAN NAUCALPAN'
            ),
            array(
                'nombre' => 'RINCÓN ARGENTINO POLANCO'
            ),
            array(
                'nombre' => 'MARISQUERIAS LA RED'
            ),
            array(
                'nombre' => 'ANATOL'
            ),
            array(
                'nombre' => 'BARRA LA NACIONAL'
            ),
            array(
                'nombre' => 'GAUCHO TRADICIONAL PALMAS'
            ),
            array(
                'nombre' => 'TIERRA DEL SOL'
            ),
            array(
                'nombre' => 'KAMPAIL PUEBLA'
            ),
            array(
                'nombre' => 'BRUNO QRO'
            ),
            array(
                'nombre' => 'HYATT ANDARES GDL'
            ),
            array(
                'nombre' => 'PACÌFICO FUENTES DE SATELITE.'
            ),
            array(
                'nombre' => 'LA ACEITUNA'
            ),
            array(
                'nombre' => 'COCINA ABIERTA'
            ),
            array(
                'nombre' => 'BRICK'
            ),
            array(
                'nombre' => 'CORAZÓN DE ALCACHOFA SAO PAULO'
            ),
            array(
                'nombre' => 'DON CAPITAN SANTA FE'
            ),
            array(
                'nombre' => 'ASADOR VASCO PARQUE'
            ),
            array(
                'nombre' => 'SAMBUKA'
            ),
            array(
                'nombre' => 'HIJAS DE LA TOSTADA'
            ),
            array(
                'nombre' => 'MUI MUI'
            ),
            array(
                'nombre' => 'COMEDOR DE LOS MILAGROS'
            ),
            array(
                'nombre' => 'EL PATRON'
            ),
            array(
                'nombre' => 'KOKORO ANDARES'
            ),
            array(
                'nombre' => 'EL BARRIO LA BOCHA'
            ),
            array(
                'nombre' => 'AZIA'
            ),
            array(
                'nombre' => 'DIVINO 106'
            ),
            array(
                'nombre' => 'FRASCATI'
            ),
            array(
                'nombre' => 'MORITAS GARZA SADA'
            ),
            array(
                'nombre' => 'RINCÓN ARGENTINO SANTA FE'
            ),
            array(
                'nombre' => 'SRA TANAKA'
            ),
            array(
                'nombre' => 'LA LOPEZ'
            ),
            array(
                'nombre' => 'BURGER BAR ROMA'
            ),
            array(
                'nombre' => 'LA INSURGENTE CHAPULTEPEC'
            ),
            array(
                'nombre' => 'LOS JUANES'
            ),
            array(
                'nombre' => 'LA MADALENA'
            ),
            array(
                'nombre' => 'LOS LAURELES'
            ),
            array(
                'nombre' => 'DANTE'
            ),
            array(
                'nombre' => 'MICAELA MAR Y LEÑA'
            ),
            array(
                'nombre' => 'CAUDILLOS TUXLA'
            ),
            array(
                'nombre' => 'LA RIVIERA DEL SUR'
            ),
            array(
                'nombre' => 'TEQUILAZOO (GRUPO IMPULSOR MAZARIEGOS)'
            ),
            array(
                'nombre' => '1528 BAR (GRUPO IMPULSOR MAZARIEGOS)'
            ),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurantes');
    }
}
