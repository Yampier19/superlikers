<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasViejasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facturas_viejas', function (Blueprint $table) {
            Schema::create('facturas_viejas', function (Blueprint $table) {
                $table->id('id_factura');
                $table->string('referencia')->index();
                $table->string('cdc')->nullable(); //centro de consumo
                $table->string('folio')->nullable(); //Folio en la factura, su ID
                $table->string('email')->nullable(); //email del usuario del mesero
                $table->string('mesero')->nullable(); //Nombre impreso en la factura
                $table->string('campaign'); //ti
                $table->string('photo');
                $table->timestamp('registro');
                $table->text('lectura')->nullable(); //LECTURA DEL GOOGLE VISION
                $table->timestamp('user_upload')->nullable();
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas_viejas');
    }
}
