<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('participante', function (Blueprint $table) {
            Schema::create('participante', function (Blueprint $table) {
                $table->id('id_participante');
                $table->string('programa')->default('pernod'); //programa de la factura, pernod (mesero) o pernod_off (vendedores)
                $table->string('name');
                $table->string('id');
                $table->string('uid');
                $table->string('email');
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participante');
    }
}
