<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Controladores
use App\Http\Controllers\FacturaController;
use App\Http\Controllers\FacturaViejaController;
use App\Http\Controllers\ParametricController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/invoice')->group(function(){
    
    /////////////////////////////SUPERLIKERS//////////////////////////////
    //Entradas de Superlikers para recibir lo que tengan almacenado
    Route::post('/entries',[FacturaController::class,'entries']);
    
    //Enviando Factura a registrar venta
    Route::post('/retail',[FacturaController::class,'retailInvoice']);

    //Cambiando factura a aceptada o rechazada
    Route::post('/estado',[FacturaController::class,'changeStateInvoice']);

    ///////////////////////////ENDPOINTS PARA PRODUCCIÓN///////////////////
    //Entradas de Superlikers que luego son almacenadas en la BD
    Route::post('/retrieveEntries',[FacturaController::class,'retrieveEntries']);

    //Envio de OCR y Parametrización al JOB
    Route::post('/sendOCR',[FacturaController::class,'sendOCRParam']);

    //Re Envio de OCR y Parametrización al JOB de facturas que no parametrizaron solo se leyeron
    Route::post('/resendOCR',[FacturaController::class,'reSendOCRParam']);

    //Envio de Facturas ya parametrizadas a Superlikers
    Route::post('/registrarInvoice',[FacturaController::class,'sendInvoice']);

    //Enviar factura a reparametrizar
    Route::get('/parametrizar/{id}',[FacturaController::class,'paramInvoice']);

    //Enviar factura individual a registrar
    Route::post('/register',[FacturaController::class,'registrarInvoice']);

    ///////////////////////////TESTING/////////////////////////////////////////////

    //Enviador de invoices a OCR
    Route::post('/lector500',[FacturaController::class,'invoiceLector500']);

    //Testing Lectura + Parametrización
    Route::post('/testing',[FacturaController::class,'testOCRParam']);

    //Lectura y Save de Invoice
    Route::post('/lector',[FacturaController::class,'invoiceLector']);
    Route::post('/save',[FacturaController::class,'saveinvoice']);

    ///////////////////////////TESTING DEVOLUCIONES/////////////////////////////////////////////

    //Entradas de Superlikers para recibir lo que tengan almacenado
    Route::post('/entriesdev',[FacturaController::class,'entriesDev']);
    Route::post('/individual',[FacturaController::class,'individual']);
    Route::post('/devolucion',[FacturaController::class,'devolucion']);
    Route::post('/recarga',[FacturaController::class,'recarga']);

    //testeo de información de participante por endpoint para determinar programa de la factura
    Route::post('/participante',[FacturaController::class,'participanteData']);
});

//RUTAS DE PARAMETRIZACIÓN VIEJAS
Route::prefix('/old')->group(function(){
    
    /////////////////////////////SUPERLIKERS//////////////////////////////
    //Entradas de Superlikers para recibir lo que tengan almacenado
    Route::post('/entries',[FacturaViejaController::class,'entries']);
    
    //Entradas de Superlikers que luego son almacenadas en la BD
    Route::post('/retrieveEntries',[FacturaViejaController::class,'retrieveEntries']);

    //Envio de OCR y Parametrización al JOB
    Route::post('/sendOCR',[FacturaViejaController::class,'sendOCRParam']);

    //Enviar factura a reparametrizar
    Route::get('/parametrizar/{id}',[FacturaViejaController::class,'paramInvoice']);
    
    //Re Envio de OCR y Parametrización al JOB de facturas que no parametrizaron solo se leyeron
    Route::post('/resendOCR',[FacturaViejaController::class,'reSendOCRParam']);
});

//RUTAS PARA LA PARAMETRIZACIÓN Y HACER PRUEBAS
Route::prefix('/pruebas')->group(function(){
    Route::post('/lector',[ParametricController::class,'invoiceLector']);
    Route::get('/parametrizar/{id?}',[ParametricController::class,'parametrizar']);

    //Ver la informacion de factura enviada
    Route::get('/facturas',[ParametricController::class,'facturas']);
    Route::get('/factura/{id?}',[ParametricController::class,'factura']);
    Route::get('/getImage/{filename?}', [ParametricController::class, 'getImage']);
});