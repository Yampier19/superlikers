<?php

use Illuminate\Support\Facades\Route;

//CONTROLADORES
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\CentroController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\FacturaPanelController;
use App\Http\Controllers\ParticipanteController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TarifarioController;
use App\Http\Controllers\newFacturas;

use App\Http\Controllers\CommentsController;

//RUTAS DE LOGIN | LOGOUT
Route::get('/',[LoginController::class,'form_login'])->name('login'); //mostrar el login o dashboard
Route::get('login',[LoginController::class,'form_login']);//->name('login'); //mostrar el login
Route::post('auth',[LoginController::class,'login'])->name('login.auth'); //metodo de login
Route::get('logout',[LoginController::class,'logout'])->name('login.logout'); //metodo de logout

// RUTAS RESET PASSWORD
// Route::prefix('/password')->group(function(){
//     Route::get('/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
//     Route::post('/email/{tipo?}',[PasswordResetController::class,'create'])->name('password.email');
//     Route::get('/find/{token}/{tipo}', [PasswordResetController::class,'find'])->name('password.find');
//     Route::post('/reset', [PasswordResetController::class, 'reset'])->name('password.update');
// });

//RUTAS DEL DASHBOARD
Route::middleware(['auth', 'state'])->get('home',[DashboardController::class,'index'])->name('home');
Route::prefix('/portal')->group(function(){
    //Route::get('/getImage/{filename?}', [DashboardController::class, 'getImage'])->name('admin.icon');
    Route::middleware(['auth', 'state'])->get('',[DashboardController::class,'index'])->name('portal.index');
});

//RUTA DE LOS CENTROS DE CONSUMO
// Route::prefix('/cdc')->group(function(){
//     Route::middleware(['auth', 'state'])->get('',[CentroController::class,'index'])->name('consumos.index');
//     Route::middleware(['auth', 'state'])->get('/edit/{id?}',[CentroController::class,'editar'])->name('consumos.edit');

//     //CRUD
//     Route::middleware(['auth', 'state'])->post('/store',[CentroController::class,'store'])->name('consumos.store');
//     Route::middleware(['auth', 'state'])->post('/update/{id}',[CentroController::class,'update'])->name('consumos.update');
// });

//Nuevas Vistas
Route::prefix('/newViews')->group(function(){

    Route::prefix('/facturas')->group(function(){
        Route::middleware(['auth', 'state'])->get('edit/{id}',[newFacturas::class,'edit'])->name('nvfacturas.edit');
    });


});
//PRODUCTOS PERNOD
// Route::prefix('/productos')->group(function(){
//     Route::middleware(['auth', 'state'])->get('',[ProductoController::class,'index'])->name('productos.index');
//     Route::middleware(['auth', 'state'])->get('/edit/{id?}',[ProductoController::class,'editar'])->name('productos.edit');

//     //CRUD
//     Route::middleware(['auth', 'state'])->post('/store',[ProductoController::class,'store'])->name('productos.store');
//     Route::middleware(['auth', 'state'])->post('/update/{id}',[ProductoController::class,'update'])->name('productos.update');
// });

// RUTAS MESEROS
// Route::prefix('/usuarios')->group(function(){
//     Route::middleware(['auth', 'state'])->get('/panel',[ParticipanteController::class,'index'])->name('usuarios.index');
//     Route::middleware(['auth', 'state'])->get('/listado',[ParticipanteController::class,'registrados'])->name('usuarios.list_register');
//     Route::middleware(['auth', 'state'])->get('/facturas/{email?}/{filtro?}/{filtro2?}',[ParticipanteController::class,'facturas'])->name('usuarios.list_tracking');
// });


// RUTAS FACTURAS
Route::prefix('/facturas')->group(function(){
    //Vistas
    Route::middleware(['auth', 'state'])->get('/panel',[FacturaPanelController::class,'index'])->name('facturas.index');
    Route::middleware(['auth', 'state'])->get('/listado/{filtro?}/{filtro2?}',[FacturaPanelController::class,'listado'])->name('facturas.listado');
    Route::middleware(['auth', 'state'])->get('/list/ocr',[FacturaPanelController::class,'ocr'])->name('facturas.ocr');
    Route::middleware(['auth', 'state'])->get('/list/submit',[FacturaPanelController::class,'enviadas'])->name('facturas.submit');
    Route::middleware(['auth', 'state'])->get('/list/nosubmit/{filtro?}',[FacturaPanelController::class,'no_enviadas'])->name('facturas.nosubmit');
    Route::middleware(['auth', 'state'])->get('/list/slope/{filtro?}',[FacturaPanelController::class,'pendientes'])->name('facturas.slope');
    Route::middleware(['auth', 'state'])->get('/list/aceptado/{filtro?}',[FacturaPanelController::class,'aceptadas'])->name('facturas.accepted');
    //Route::middleware(['auth', 'state'])->get('/edit/{id?}',[FacturaPanelController::class,'editar'])->name('facturas.edit');

    //Añadir comentarios
    Route::middleware(['auth', 'state'])->get('/add/coments/{fecha}',[CommentsController::class,'addComents'])->name('facturas.addComents');

    //Buscar Folio
    // Route::middleware(['auth', 'state'])->get('/search/folio/{id}',[FacturaPanelController::class,'searchFolio'])->name('facturas.searchFolio');


    //Facturas por Centro de Consumo
    Route::middleware(['auth', 'state'])->get('/restaurante/{cdc}/{filtro?}/{filtro2?}',[FacturaPanelController::class,'cdc_facturas'])->name('facturas.cdc');

    //Productos de la factura
    Route::middleware(['auth', 'state'])->post('/productos/{id?}',[FacturaPanelController::class,'productos'])->name('facturas.productos');

    //CRUD
    Route::middleware(['auth', 'state'])->post('/parametrizar/{id}',[FacturaPanelController::class,'parametrizar'])->name('facturas.param');
    Route::middleware(['auth', 'state'])->post('/retrievesParam',[FacturaPanelController::class,'sendParam'])->name('facturas.sendparam');
    Route::middleware(['auth', 'state'])->post('/sendInvoices',[FacturaPanelController::class,'sendInvoice'])->name('facturas.sendRegistro');
    //Route::middleware(['auth', 'state'])->post('/retrieves',[FacturaPanelController::class,'retrieveInvoices'])->name('facturas.retrieve');
    Route::middleware(['auth', 'state'])->post('/update/{id}',[FacturaPanelController::class,'update'])->name('facturas.update');
});

// RUTAS CRON JOBS
Route::prefix('/jobs')->group(function(){
    //Vistas
    Route::middleware(['auth', 'state'])->get('/panel',[JobController::class,'index'])->name('jobs.index');
    Route::middleware(['auth', 'state'])->get('/work/{filtro?}',[JobController::class,'pending'])->name('jobs.work');
    Route::middleware(['auth', 'state'])->get('/fail/{filtro?}',[JobController::class,'failed'])->name('jobs.fail');

    //CRUD
    Route::middleware(['auth', 'state'])->post('/restaurar/{uuid}',[JobController::class,'restoreJob'])->name('jobs.restore');
    Route::middleware(['auth', 'state'])->post('/restaurarAll',[JobController::class,'restoreAll'])->name('jobs.restoreAll');
    Route::middleware(['auth', 'state'])->post('/delete/job/{id}',[JobController::class,'deleteJob'])->name('jobs.delete');
    Route::middleware(['auth', 'state'])->post('/delete/failed/{id}',[JobController::class,'deleteFailed'])->name('fail.delete');
});

//RUTAS DEL TARIFARIO
// Route::prefix('/tarifario')->group(function(){
//     Route::middleware(['auth', 'state'])->get('/panel/{filtro?}',[TarifarioController::class,'index'])->name('tarifarios.index');
//     Route::middleware(['auth', 'state'])->get('/edit/{id?}',[TarifarioController::class,'editar'])->name('tarifarios.edit');
//     Route::middleware(['auth', 'state'])->get('/editall/{ciudad?}',[TarifarioController::class,'editarall'])->name('tarifarios.editall');

//     //CRUD
//     Route::middleware(['auth', 'state'])->post('/store',[TarifarioController::class,'store'])->name('tarifario.store');
//     Route::middleware(['auth', 'state'])->post('/delete/{id}',[TarifarioController::class,'delete'])->name('tarifario.delete');
//     Route::middleware(['auth', 'state'])->post('/update/{id}',[TarifarioController::class,'update'])->name('tarifario.update');
//     Route::middleware(['auth', 'state'])->post('/updateall',[TarifarioController::class,'updateall'])->name('tarifario.updateall');
// });

// Route::prefix('/reportes')->group(function(){
//     Route::middleware(['auth', 'state'])->get('/panel',[ReportController::class,'index'])->name('reportes.index');
//     Route::middleware(['auth', 'state'])->post('/export',[ReportController::class,'exportData'])->name('reportes.export');
// });
