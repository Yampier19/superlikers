@extends('layouts.app')

@section('content')


<div class="container">
    <div class="fs-3 fw-bold">Buscando Folio {{$id}}</div>
    <div class="row my-5">
        @forelse($folios as $folio)

            <div class="col-md-4 col-12">

                <div class="bg-white p-3">
                    <h5 class="text-center">{{$folio->cdc}}</h5>
                    <p>Id Factura: <b>{{$folio->id_factura}}</b></p>

                    <p>Folio: <span class="text-info">{{$folio->folio}}</span></p>

                    <p>Estado: {{$folio->state}}</p>

                    <p>Fecha usuario subida <span style="font-size:12px">{{$folio->user_upload}}</span></p>

                    <p>Fecha de creación <span style="font-size:12px">{{$folio->created_at}}</span></p>

                    <p>Ultima fecha de edición <span style="font-size:12px">{{$folio->updated_at}}</span></p>

                    {{-- <a href="{{route('facturas.edit',$folio->id_factura)}}" class="btn btn-primary w-100" target="_blank">Ver Factura</a> --}}

                </div>


            </div>
        @empty

        <div class="text-center">
            <script src="https://cdn.lordicon.com/xdjxvujz.js"></script>
<lord-icon
    src="https://cdn.lordicon.com/tdrtiskw.json"
    trigger="loop"
    colors="primary:#3080e8,secondary:#e8308c"
    style="width:250px;height:250px">
</lord-icon>
<h2>No se encontraron facturas con este folio</h2>
        </div>
        @endforelse
    </div>
</div>



@endsection
