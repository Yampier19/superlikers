@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">

            <div class="col-md-12 mx-auto">
                <a href="{{ route('productos.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Volver al panel de productos
                </a>
            </div>

            <!-- Edición de CDC -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white my-auto">Editar Datos del Producto {{ $producto->marca }}</h5>
                </div>

                <form action="{{ route('productos.update',$producto->id_productos) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-4">

                        <h5 class="text-center mb-4">Datos a Modificar, tenga en cuenta que el SKU es un dato sensible a la hora de registrar en Superlikers.</h5>
                        
                        <div class="col-md-6">
                            <label for="">Referencia o SKU del producto:</label>
                            <input type="number" name="referencia" id="referencia" value="{{ old('referencia', $producto->referencia) }}"
                            class="form-control @error('referencia') is-invalid @enderror" required>
                            @error('referencia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <label for="name_user">Estado Actual del Producto:</label>
                            <select name="estado" id="estado" class="form-control @error('estado') is-invalid @enderror" required>
                                <option value="1" {{ $producto->activo ? "selected" : "" }}>Activado</option>
                                <option value="0" {{ $producto->activo ? "" : "selected" }}>Desactivado</option>
                            </select>
                            @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6 mt-3">
                            <div class="col-12">
                                <label for="">Linea del Producto:</label>
                                <select name="sel_line" id="editLine" class="form-control @error('line') is-invalid @enderror" required>
                                    <option value="{{ $producto->line }}" selected>{{ $producto->line }}</option>
                                    @foreach($lineas as $linia)
                                        @if($linia->line != $producto->line)
                                            <option value="{{ $linia->line }}">{{ $linia->line }}</option>
                                        @endif
                                    @endforeach
                                    <option value="OTRO">Otro...</option>
                                </select>
                                @error('line')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-12 mt-3 d-none" id="divLine">
                                <label for="">Agrega el nombre de la linea nueva a editar:</label>
                                <input type="text" class="form-control" id="typeLine">
                            </div>
                            <input type="hidden" name="line" id="sendLine" value="{{ $producto->line }}">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button type="submit" class="form-control text-white btn-danger" style="cursor: pointer">Editar Centro de Consumo</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        $('#editLine').change(function(){
            var line = $(this).val();

            if(line == "OTRO"){
                $('#divLine').removeClass('d-none');
                $('#typeLine').attr('required', true);
                $('#sendLine').val(null);
            }
            else{
                $('#divLine').addClass('d-none');
                $('#typeLine').removeAttr('required');
                $('#sendLine').val(line);
            }

            console.log($('#sendLine').val());
        });

        $('#typeLine').change(function(){
            var line = $(this).val();
            
            $('#sendLine').val(line);
            console.log($('#sendLine').val());
        });
    });
</script>