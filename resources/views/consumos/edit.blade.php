@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">

            <div class="col-md-12 mx-auto">
                <a href="{{ route('consumos.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Volver al panel de centros de consumo
                </a>
            </div>

            <!-- Edición de CDC -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white my-auto">Editar Datos del CDC - {{ $restaurante->nombre }}</h5>
                </div>

                <form action="{{ route('consumos.update',$restaurante->id_restaurante) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-4">

                        <h5 class="text-center mb-4">Datos a Modificar, tome en cuenta que modificar la ciudad alterara futuras lecturas de acuerdo al tarifario.</h5>
                        
                        <div class="col-md-6 mb-3">
                            <div class="col-12">
                                <label for="">Ciudad del CDC:</label>
                                <select name="sel_ciudad" id="editCiudad" class="form-control @error('ciudad') is-invalid @enderror" required>
                                    <option value="{{ $restaurante->ciudad }}" selected disabled>{{ $restaurante->ciudad }}</option>
                                    <option value="OTRO">Otro...</option>
                                    @foreach($ciudades as $ciudad)
                                        @if($ciudad != $restaurante->ciudad)
                                            <option value="{{ $ciudad->ciudad }}">{{ $ciudad->ciudad }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('ciudad')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-12 mt-3 d-none" id="divCiudad">
                                <label for="">Agrega el nombre de la ciudad nueva a editar:</label>
                                <input type="text" class="form-control" id="typeCiudad">
                            </div>
                            <input type="hidden" name="ciudad" id="sendCiudad" value="{{ $restaurante->ciudad }}">
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="name_user">Tipo de factura que maneja el CDC:</label>
                            <select name="tipo" class="form-control @error('tipo') is-invalid @enderror" required>
                                <option value="ticket" {{ $restaurante->tipo == "ticket" ? "selected" : "" }}>
                                    Ticket Individual
                                </option>
                                <option value="tira" {{ $restaurante->tipo == "tira" ? "selected" : "" }}>
                                    Tira de Venta
                                </option>
                                <option value="comanda" {{ $restaurante->tipo == "comanda" ? "selected" : "" }}>
                                    Comanda
                                </option>
                            </select>
                            @error('tipo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="name_user">Estado Actual del Centro de Consumo:</label>
                            <select name="estado" id="estado" class="form-control @error('estado') is-invalid @enderror" required>
                                <option value="1" {{ $restaurante->activo ? "selected" : "" }}>Activado</option>
                                <option value="0" {{ $restaurante->activo ? "" : "selected" }}>Desactivado</option>
                            </select>
                            @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button type="submit" class="form-control text-white btn-danger" style="cursor: pointer">Editar Centro de Consumo</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        $('#editCiudad').change(function(){
            var ciudad = $(this).val();

            if(ciudad == "OTRO"){
                $('#divCiudad').removeClass('d-none');
                $('#typeCiudad').attr('required', true);
                $('#sendCiudad').val(null);
            }
            else{
                $('#divCiudad').addClass('d-none');
                $('#typeCiudad').removeAttr('required');
                $('#sendCiudad').val(ciudad);
            }

            console.log($('#sendCiudad').val());
        });

        $('#typeCiudad').change(function(){
            var ciudad = $(this).val();
            
            $('#sendCiudad').val(ciudad);
            console.log($('#sendCiudad').val());
        });
    });
</script>