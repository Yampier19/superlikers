@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger text-white">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row mb-5 mx-auto">

            <div class="col-md-12 mx-auto">
                <a href="{{ route('tarifarios.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Volver al panel de tarifarios
                </a>
            </div>

            <!-- Edición de Cuestinarios -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white my-auto">Editar Tarifario de la Ciudad: {{ $ciudad }}</h5>
                </div>

                <form action="{{ route('tarifario.updateall') }}" method="post" enctype="multipart/form-data">

                    @csrf

                    <!-- CIUDAD -->
                    <input type="hidden" name="ciudad" value="{{ $ciudad }}">

                    <!-- TARIFARIO -->
                    <div class="row col-md-12 my-3" id="productosPernod">
                        <div class="col">
                            <label for="" style="color: #51A2A7;">Tarifario Actual de cada Marca Pernod que se encuentra en esta ciudad</label>
                            <br>
                            <ul>
                            @foreach($tarifarios as $tarifario)
                                <li>
                                    <span>{{ $tarifario->FK_id_marca ." - ". $tarifario->productos->marca }}</span>
                                    <span>{{ " | Multiplicador: ".$tarifario->copaxbotella." (".date('F Y', strtotime($tarifario->mes)).")" }}</span>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- FIN TARIFARIO -->

                    <hr>
                    <div class="row col-md-12 my-3">
                        <div class="col">
                            <label style="font-size: 12px; color: #51A2A7;  margin-bottom: 0px;">Agregar / Editar Tarifario de los Productos Pernod<br><br>Selecciona si deseas Hacerlo?</label>
                            <select name="selectPernod" id="inputSelect" class="form-control" required="required">
                                <option value="" selected disabled>Seleccionar...</option>
                                <option value="1">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>

                    <hr>

                    <div class="row col-md-12 my-3 divOculto" style="display: none">
                        <div class="col-10">
                            <label style="color: #51A2A7;">Selecciona la marca pernod y añadir para agregar una nueva.</label>
                            <select name="select" id="inputAdd" class="form-control">
                                <option value="" selected disabled>Seleccionar...</option>
                                <option value="1">Marca Pernod</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <span id="addPernod" class="btn btn-sm btn-secondary">Añadir</span>
                        </div>
                    </div>

                    <!-- PRODUCTOS PERNOD AGREGADOS -->
                        <div style="display: none" class="divOculto" id="div2">
                            <label for="" style="color: #51A2A7;">Tarifario de la Marca Pernod</label>
                            <div class="row field_wrapper2 mx-auto">
                                <?php $cantidadPD = 0; ?>
                                @foreach($tarifarios as $tarifario)
                                    <?php $cantidadPD++; ?>
                                    <div class="row col-md-12 mt-1" id="proPernod-{{ $cantidadPD }}">
                                        <div class="col">
                                            <div class="row d-flex justify-content-end mt-1">

                                                <div class="form-group col">
                                                    <label for="">Referencia - Producto:</label>
                                                    <select name="pernod[{{ $cantidadPD }}][ref]" class="form-control" required>
                                                        @foreach($marcas as $marca)
                                                            <option value="{{ $marca->id_productos }}" {{ $marca->id_productos == $tarifario->FK_id_marca ? "selected" : "" }}>
                                                                {{ $marca->marca }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col">
                                                    <label for="">Multiplicador - Fecha (Actual/Futuro):</label>
                                                    <span>{{ $tarifario->copaxbotella." - (".date('F Y', strtotime($tarifario->mes)).")" }}</span>
                                                </div>
                                                
                                                <div class="form-group col">
                                                    <label for="">Multiplicador Nuevo:</label>
                                                    <input name="pernod[{{ $cantidadPD }}][multi]" min=0 type="number" step=".01" class="form-control validateMulti" required>
                                                </div>
                                                
                                                <div class="form-group col">
                                                    <label for="">Fecha de Inicio (Nueva):</label>
                                                    <input type="hidden" class="actualFecha" value="{{ $tarifario->mes }}">
                                                    <input name="pernod[{{ $cantidadPD }}][mes]" type="month" class="form-control validateFecha" required>
                                                </div>
                                                
                                                <div class="form-group col">
                                                    <a href="javascript:void(2);" class="remove_button2" title="Remove field 2"><i class="fas fa-trash text-danger mt-3 mr-2"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <hr>
                        </div>
                    <!-- FIN PRODUCTOS PERNOD AGREGADOS -->

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button type="submit" class="form-control text-white btn-danger" style="cursor: pointer">Editar Datos</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<!-- CANTIDAD DE PRODUCTOS -->
<input type="hidden" id="proPernod" value="{{ $cantidadPD }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        $("#inputSelect").change(function(){
            var selectValue = $(this).val();
            switch (selectValue) {
                case "1":
                    $(".divOculto").show();
                    $("#productosPernod").hide();
                    break;

                default: 
                    $(".divOculto").hide();
                    $("#productosPernod").show();
                    break;
            }
        });

        $("#addPernod").click(function(){
            var selectValue = $('#inputAdd').val();
            
            switch (selectValue) {
                case "1": addPernod(); break;
            } 
        });

        //Ajuste de las cantidades si no cumplen con el minimo de ser mayor a 1
            $('.validateMulti').change(function(){
                var cantidad = $(this).val();
                if(cantidad < 1) $(this).val(1);
            });

        //Ajuste de la fecha
            $('.validateFecha').change(function(){
                var fecha = new Date($(this).val());
                var fecha2 = new Date($(this).prev().val());
                
                if(fecha.getTime() <= fecha2.getTime()){
                    $(this).val(null);
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'La fecha del nuevo tarifario debe superar al mes y año del tarifario actual!',
                    })
                }
            });
    });
</script>

<!-- PRODUCTO PERNOD -->
<script type="text/javascript">

    var pPD = 0; //INCIAL DE PRODUCTOS PERNOD

    $(document).ready(function() {
        pPD = $('#proPernod').val(); //INICIAL DE PRODUCTOS PERNOD
    });

    function addPernod(){
        pPD++; //Increment field counter
        console.log("pPD: "+pPD);

        var wrapper2 = $('.field_wrapper2'); //Input field wrapper
        var fieldHTML2 = '<div class="row col-md-12 mt-1" id="proPernod-'+pPD+'">'
                            +'<div class="col">'
                                +'<div class="row d-flex justify-content-end mt-1">'
                                    +'<div class="form-group col">'
                                        +'<label for="">Referencia - Producto:</label>'
                                        +'<select name="pernod['+pPD+'][ref]" class="form-control" required>'
                                            @foreach($marcas as $marca)
                                                +'<option value="{{ $marca->id_productos }}">{{ $marca->marca }}</option>'
                                            @endforeach
                                        +'</select>'
                                    +'</div>'
                                    +'<div class="form-group col">'
                                        +'<label for="">Multiplicador Nuevo:</label>'
                                        +'<input name="pernod['+pPD+'][multi]" min=0 type="number" step=".01" class="form-control validateMulti" required>'
                                    +'</div>'
                                    +'<div class="form-group col">'
                                        +'<label for="">Fecha de Inicio (Nueva):</label>'
                                        +'<input name="pernod['+pPD+'][mes]" type="month" class="form-control validateDate" required>'
                                    +'</div>'
                                    +'<div class="form-group col">'
                                        +'<a href="javascript:void(2);" class="remove_button2" title="Remove field 2"><i class="fas fa-trash text-danger mt-3 mr-2"></i></a>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>';
        $(wrapper2).append(fieldHTML2);
    }

    $(document).ready(function() {
        var wrapper2 = $('.field_wrapper2'); //Input field wrapper
        $(wrapper2).on('click', '.remove_button2', function(e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').parent('div').parent('div').parent('div').remove(); //Remove field html
            //pPD--; //Decrement field counter
        });

        //Validaciones de Cantidades
        $(wrapper2).on('change', '.validateMulti', function(e) { //Once remove button is clicked
            e.preventDefault();
            var cantidad = $(this).val();
            if(cantidad < 1) $(this).val(1);
        });

        //Validaciones de Fecha
        $(wrapper2).on('change', '.validateDate', function(e) { //Once remove button is clicked
            e.preventDefault();
            var fecha = new Date($(this).val()+"-01");

            var fecha2 = new Date();
            var mes_actual = fecha2.getMonth() + 1; mes_actual < 10 ? mes_actual = "0"+mes_actual : "";
            var year_actual = fecha2.getFullYear();
            var fecha2 = new Date(year_actual+"-"+mes_actual+"-01");
            
            if(fecha.getTime() < fecha2.getTime()){
                $(this).val(null);
                Swal.fire({
                    icon: 'warning',
                    title: 'Oops...',
                    text: 'La fecha del tarifario a futuro no puede ser menor al mes y año actual!',
                })
            }
        });
    });
</script>