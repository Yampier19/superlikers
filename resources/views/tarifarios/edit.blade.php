@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">

            <div class="col-md-12 mx-auto">
                <a href="{{ route('tarifarios.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Volver al panel de tarifarios
                </a>
            </div>

            <!-- Edición de Tarifario -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white my-auto">Editar Datos de Tarifario en {{ $tarifario->ciudad }}</h5>
                </div>

                <form action="{{ route('tarifario.update',$tarifario->id_tarifario) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Producto de la Marca:</label>
                            <select name="marca" id="selectMarca" class="form-control @error('marca') is-invalid @enderror">
                                @foreach($marcas as $marca)
                                    <option value="{{ $marca->id_productos }}" {{ old('marca', $tarifario->FK_id_marca) == $marca->id_productos ? "selected" : "" }}>
                                        {{ $marca->marca }}
                                    </option>
                                @endforeach
                            </select>
                            @error('marca')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-4">

                        <h4 class="text-center">Tarifario Actual o Futuro</h4>

                        <div class="col-md-6">
                            <label for="name_user">Modificador Actual/Futuro (botella x copa):</label>
                            <input name="modActual" id="modActual" type="number" min=0 value="{{ old('modActual', $tarifario->copaxbotella) }}"
                            class="form-control @error('modActual') is-invalid @enderror" required>
                            @error('modActual')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Inicio del Modificador Actual/Futuro {{ $editable ? "(No editable)" : "" }}:</label>
                            <input name="mes" id="mesActual" type="month" value="{{ old('mes', date('Y-m', strtotime($tarifario->mes)) ) }}"
                            class="form-control @error('mes') is-invalid @enderror" {{ $editable ? "" : "readonly" }} required>
                            @error('mes')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <hr>

                    @if($tarifario->antmes)
                        <div class="row col-md-12 my-4">
                            
                            <h4 class="text-center">Tarifario Antiguo</h4>

                            <div class="col-md-6">
                                <label for="name_user">Modificador Antiguo (botella x copa) {{ $editable ? "(No editable)" : "" }}:</label>
                                <input name="modAntiguo" id="modAntiguo" type="number" min=0 value="{{ old('modAntiguo', $tarifario->antcopaxbotella) }}"
                                class="form-control @error('modAntiguo') is-invalid @enderror" {{ $editable ? "" : "readonly" }} required>
                                @error('modAntiguo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="name_user">Fecha de Inicio del Modificador Antiguo (no editable): {{ $tarifario->antmes ? date('F Y', strtotime($tarifario->antmes)) : "" }}</label>
                            </div>
                        </div>
                    @else
                        <input type="hidden" name="modAntiguo" value="0">
                    @endif

                    <div class="row col-md-12 my-4 addTarifario" style="display: none">
                        
                        <h4 class="text-center">Agregar Nuevo Tarifario</h4>

                        <div class="col-md-6">
                            <label for="name_user">Modificador Nuevo (botella x copa):</label>
                            <input name="modNuevo" id="modNuevo" type="number" min=0 value="{{ old('modNuevo') }}"
                            class="form-control @error('modNuevo') is-invalid @enderror">
                            @error('modNuevo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Inicio del Nuevo Tarifario:</label>
                            <input name="mesNuevo" id="mesNuevo" type="month" value="{{ old('mesNuevo') }}"
                            class="form-control @error('mesNuevo') is-invalid @enderror">
                            @error('mesNuevo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <hr>

                    <div class="row col-md-12 my-4">
                        <div class="col">
                            <label style="font-size: 12px; color: #51A2A7;  margin-bottom: 0px;">Agregar Nuevo Tarifario<br><br>Selecciona si deseas Hacerlo?</label>
                            <select name="selectPernod" id="inputSelect" class="form-control" required="required">
                                <option value="" selected disabled>Seleccionar...</option>
                                <option value="1">Si</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>

                    <hr>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button type="submit" class="form-control text-white btn-danger" style="cursor: pointer">Editar Tarifario</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        //Agregar nuevo tarifario
            $("#inputSelect").change(function(){
                var selectValue = $(this).val();
                switch (selectValue) {
                    case "1":
                        $(".addTarifario").show();
                        $('#modNuevo').attr('required', true);
                        $('#mesNuevo').attr('required', true);
                        break;

                    default: 
                        $(".addTarifario").hide();
                        $('#modNuevo').removeAttr('required');
                        $('#mesNuevo').removeAttr('required');
                        break;
                }
            });

        //Ajuste de las cantidades si no cumplen con el minimo de ser mayor a 1
            $('#modActual').change(function(){
                var cantidad = $(this).val();
                if(cantidad < 1) $(this).val(1);
            });

            $('#modAntiguo').change(function(){
                var cantidad = $(this).val();
                if(cantidad < 1) $(this).val(1);
            });

            $('#modNuevo').change(function(){
                var cantidad = $(this).val();
                if(cantidad < 1) $(this).val(1);
            });

        //Evaluación de las fechas con script
            $('#mesActual').change(function(){
                var fecha = new Date($(this).val()+"-01");
                var fecha2 = new Date();
                
                if(fecha.getTime() < fecha2.getTime()){
                    $(this).val(null);
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'La fecha del tarifario a futuro no puede ser menor al mes y año actual!',
                    })
                }
            });

            $('#mesNuevo').change(function(){
                var fecha = new Date($(this).val());
                var fecha2 = new Date($('#mesActual').val());

                if(fecha.getTime() <= fecha2.getTime()){
                    $(this).val(null);
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'La fecha del nuevo tarifario debe superar al mes y año del tarifario actual!',
                    })
                }
            });
    });
</script>