@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif




  <!-- Modal -->
  <div class="modal fade" id="comentsModal" tabindex="-1" aria-labelledby="comentsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="comentsModalLabel">Comentarios</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            @if ($factura->coments == null || $factura->coments == "" || $factura->coments == " ")
                <p>No hay Comentarios para esta factura</p>
            @else
                @php
                echo  str_replace('+', '<br>-', $factura->coments)
                @endphp

            @endif

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

        <div class="row mb-5 mx-auto">

            <div class="col-md-12 mx-auto">
                <a href="{{ route('facturas.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Volver al panel de facturas
                </a>
            </div>

            <!-- Edición de Cuestinarios -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white my-auto">{{ !$factura->envio ? "Editar" : "" }} Datos de la Factura         <!-- Button trigger modal -->
                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#comentsModal">
                            Comentarios
                          </button></h5>
                </div>

                <form action="{{ route('facturas.update',$factura->id_factura) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="cargada">Fecha Registrada en Superlikers:</label>
                            <input name="" id="" type="text" value="{{ $factura->user_upload }}" readonly
                            class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="tarifario">Tarifario de la Factura</label>
                            <input name="" id="" type="text" value="{{ $factura->tarifario }}" readonly
                            class="form-control">
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Centro de Consumo CDC:</label>
                            <select name="cdc" id="selectCDC" class="form-control @error('cdc') is-invalid @enderror">
                                <option value="" selected disabled>Selecciona Centro de Consumo...</option>
                                @foreach($restaurantes as $restaurante)
                                    <option value="{{ $restaurante->nombre }}" id="{{ $restaurante->ciudad }}"
                                        {{ old('cdc', $restaurante->nombre) == $factura->cdc ? "selected" : "" }}>
                                        {{ $restaurante->nombre }}
                                    </option>
                                @endforeach
                            </select>
                            <input type="hidden" name="ciudad" id="ciudadSelected">
                            @error('cdc')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">

                            <label for="name_user">Folio (referencia impresa), N/A si no posee:</label>
                            <div class="row align-items-center">

                                <div class="col-6">

                                    <input name="folio" id="" type="text" value="{{ old('folio', $factura->folio) }}"
                                    class="form-control @error('folio') is-invalid @enderror" required>
                                    @error('folio')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <a href="{{route('facturas.searchFolio',$factura->folio)}}" target="_blank" class="btn btn-primary w-100">Buscar Folios Iguales</a>
                                </div>
                            </div>



                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Mesero (coloca N/A si no posee):</label>
                            <input name="mesero" id="" type="text" value="{{ old('mesero', $factura->mesero) }}"
                            class="form-control @error('mesero') is-invalid @enderror" required>
                            @error('mesero')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Estado de la Factura:</label>
                            <select name="state" id="selectState" class="form-control @error('state') is-invalid @enderror" required>
                                <option value="" selected disabled>Selecciona Centro de Consumo...</option>
                                <option value="aceptado" {{ old('state', 'aceptado') == $factura->state ? "selected" : "" }}>
                                    aceptado
                                </option>
                                <option value="pendiente" {{ old('state', 'pendiente') == $factura->state ? "selected" : "" }}>
                                    pendiente
                                </option>
                                <option value="folio repetido" {{ old('state', 'folio repetido') == $factura->state ? "selected" : "" }}>
                                    folio repetido
                                </option>
                                <option value="rechazado" {{ old('state', 'rechazado') == $factura->state ? "selected" : "" }}>
                                    rechazado
                                </option>
                            </select>
                            @error('state')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Fecha Impresa:</label>
                            <input name="registro" id="" type="date"
                            value="{{ old('registro', date('Y-m-d', strtotime($factura->registro))) }}"
                            class="form-control @error('registro') is-invalid @enderror" required>
                            @error('registro')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Hora Impresa (si no tiene coloca 00:00:00):</label>
                            <input name="hora" id="" type="time" step="1"
                            value="{{ old('hora', date('H:i:s', strtotime($factura->registro))) }}"
                            class="form-control @error('hora') is-invalid @enderror" required>
                            @error('hora')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
<div class="row text-center align-items-center ">
    <div class="col-6 my-3">

        <a href="{{ $factura->photo }}" target="_blank" type="button" class="bg-primary btn btn-sm text-white w-100 p-3">
            Abrir Imagen de Factura
        </a>

</div>

<div class=" col-6 my-3 ">

<textarea rows="5" class="p-3 w-100 rounded border-secondary">{{ $factura->lectura }}</textarea>

</div>
</div>


                    <!-- PRODUCOS PERNOD SI TIENE -->
                        @if($factura->productos->count() > 0)
                            <div class="row col-md-12 my-3" id="productosPernod">
                                <div class="col">
                                    <label for="" style="color: #51A2A7;">Productos de la Marca Pernod</label>
                                    <br>
                                    <ul>
                                    <!-- PRODUCTOS EXISTENTES -->
                                    @foreach($factura->productos as $product)


                                        <li>
                                            <span>{{ $product->line ." - ". $product->referencia }}</span>
                                            <span> / <b>Precio:</b> ${{number_format($product->price, 0)}}</span>
                                            <span> / <b>Cantidad:</b> {{number_format($product->quantity, 0)}}</span>
                                            <span> / <b>Formato:</b> {{ $product->formato }}</span>

                                            @if($product->quantity >= 40 )
                                            <span class="text-danger"> / <b>Alerta:</b>  Quantitys Altos por favor revise  </span>


                                            <div class="modal fade" id="modal{{$product->id_producto}}" tabindex="-1" aria-labelledby="label{{$product->id_producto}}" aria-hidden="true">
                                                <div class="modal-dialog">
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <h5 class="modal-title" id="label{{$product->id_producto}}">Quantity Alto</h5>
                                                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h1 class="text-danger">Alerta</h1>
                                                      El producto {{ $product->line ." - ". $product->referencia }} tiene un quantity Alto <b>{{number_format($product->quantity, 0)}}</b>, Por favor Revice
                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Ok, Voy a revisar</button>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                            <script>

var modalQ = new bootstrap.Modal(document.getElementById('modal{{$product->id_producto}}'), {
  keyboard: false
});


modalQ.show();

                                            </script>

                                            @endif
                                        </li>
                                    @endforeach
                                    </ul>
                                    <!-- PRODUCTOS EXISTENTES -->
                                </div>
                            </div>
                        @endif
                    <!-- FIN PERNOD -->

                    <!-- PRODUCOS ADICIONALES SI TIENE -->
                        @if($factura->extras->count() > 0)
                            <div class="row col-md-12 my-3" id="productosAdicional">
                                <div class="col">
                                    <label for="" style="color: #51A2A7;">Productos de Adicionales de la Factura</label>
                                    <br>
                                    <ul>
                                    <!-- PRODUCTOS EXISTENTES -->
                                    @foreach($factura->extras as $extra)
                                        <li>{{ $extra->nombre }}</li>
                                    @endforeach
                                    </ul>
                                    <!-- PRODUCTOS EXISTENTES -->
                                </div>
                            </div>
                        @endif
                    <!-- FIN ADICIONALES -->

                    <hr>
                    @if(!$factura->envio)
                        <div class="row col-md-12 my-3">
                            <div class="col">
                                <label style="font-size: 12px; color: #51A2A7;  margin-bottom: 0px;">Agregar / Editar Productos Pernod<br><br>Selecciona si deseas Hacerlo?</label>
                                <select name="selectPernod" id="inputSelect" class="form-control" required="required">
                                    <option value="" selected disabled>Seleccionar...</option>
                                    <option value="1">Si</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    @endif

                    <hr>

                    <div class="row col-md-12 my-3 divOculto" style="display: none">
                        <div class="col-10">
                            <label style="color: #51A2A7;">Seleccione el tipo de producto que deseas añadir, si hace falta</label>
                            <select name="select" id="inputAdd" class="form-control">
                                <option value="" selected disabled>Seleccionar...</option>
                                <option value="1">Producto Pernod</option>
                                <option value="2">Producto Extra</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <span id="addPernod" class="btn btn-sm btn-secondary">Añadir</span>
                        </div>
                    </div>

                    <!-- PRODUCTOS PERNOD AGREGADOS -->
                        <div style="display: none" class="divOculto" id="div2">
                            <label for="" style="color: #51A2A7;">Productos de la Marca Pernod</label>
                            <div class="row field_wrapper2 mx-auto">
                                <?php $cantidadPD = 0; ?>
                                @foreach($factura->productos as $product)
                                    <?php $cantidadPD++; ?>
                                    <div class="row col-md-12 mt-1" id="proPernod-{{ $cantidadPD }}">
                                        <div class="col">
                                            <div class="row d-flex justify-content-end mt-1">

                                                <div class="form-group col">
                                                    <label for="">Referencia - Producto:</label>
                                                    <select name="pernod[{{ $cantidadPD }}][ref]" class="form-control" required>
                                                        @foreach($marcas as $marca)
                                                            <option value="{{ $marca->referencia }}" {{ $marca->referencia == $product->referencia ? "selected" : "" }}>
                                                                {{ $marca->marca }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col">
                                                    <label for="">Precio:</label>
                                                    <input name="pernod[{{ $cantidadPD }}][precio]" type="number" class="form-control" step=".001" value="{{ $product->price }}" required>
                                                </div>

                                                <?php
                                                    $quantities = 0;
                                                    if($product->formato == "jarra")
                                                        $quantities = $product->quantity / 3;
                                                    elseif($product->formato == "botella"){

                                                        if($product->quantity % 11 == 0)
                                                            $quantities = $product->quantity / 11;
                                                        elseif($product->quantity % 7 == 0)
                                                            $quantities = $product->quantity / 7;
                                                        else
                                                            $quantities = floor($product->quantity / 6.6);

                                                    }
                                                    else
                                                        $quantities = $product->quantity;
                                                ?>

                                                <div class="form-group col">
                                                    <label for="">Cantidad:</label>
                                                    <input name="pernod[{{ $cantidadPD }}][cantidad]" type="number" class="form-control" step=".001" value="{{ $quantities }}" required>
                                                </div>

                                                <div class="form-group col">
                                                    <label class="d-block" for="">Formato:</label>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" value="copa" @if ($product->formato == "copa") checked @endif
                                                               name="pernod[{{ $cantidadPD }}][forma]" id="pernod[{{ $cantidadPD }}][forma]0">
                                                        <label class="form-check-label">Copa</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" value="botella" @if ($product->formato == "botella") checked @endif
                                                               name="pernod[{{ $cantidadPD }}][forma]" id="pernod[{{ $cantidadPD }}][forma]1">
                                                        <label class="form-check-label">Botella</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" value="jarra" @if ($product->formato == "jarra") checked @endif
                                                               name="pernod[{{ $cantidadPD }}][forma]" id="pernod[{{ $cantidadPD }}][forma]2">
                                                        <label class="form-check-label">Jarra</label>
                                                    </div>
                                                </div>

                                                <div class="form-group col">
                                                    <a href="javascript:void(2);" class="remove_button2" title="Remove field 2"><i class="fas fa-trash text-danger mt-3 mr-2"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <hr>
                        </div>
                    <!-- FIN PRODUCTOS PERNOD AGREGADOS -->

                    <!-- PRODUCTOS ADICIONALES AGREGADOS -->
                        <div style="display: none" class="divOculto" id="div3">
                            <label for="" style="color: #51A2A7;">Productos Adicionales de la Factura</label>
                            <div class="row field_wrapper3 mx-auto">
                                <?php $cantidadEX = 0; ?>
                                @foreach($factura->extras as $extra)
                                    <?php $cantidadEX++; ?>
                                    <div class="row col-md-12 mt-1" id="proExtra-{{ $cantidadEX }}">
                                        <div class="col">
                                            <div class="row d-flex justify-content-end mt-1">
                                                <div class="form-group col">
                                                    <label for="">Nombre del Producto:</label>
                                                    <input name="extra[{{ $cantidadEX }}]" type="text" class="form-control" value="{{ $extra->nombre }}" required>
                                                </div>
                                                <div class="form-group col">
                                                    <a href="javascript:void(3);" class="remove_button3" title="Remove field 3"><i class="fas fa-trash text-danger mt-3 mr-2"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <hr>
                        </div>
                    <!-- FIN PRODUCTOS ADICIONALES AGREGADOS -->

                    @if(!$factura->envio)
                        <div class="row col-md-12 my-3">
                            <div class="col-md-6 mx-auto">
                                <button type="submit" class="form-control text-white btn-danger" style="cursor: pointer">Editar Datos</button>
                            </div>
                        </div>
                    @else
                        <div class="row col-md-12 my-3">
                            <div class="col-md-6 mx-auto">
                                <button type="button" class="form-control text-white btn-danger" style="cursor: pointer" disabled>Factura ya Registrada</button>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">





<!-- CANTIDAD DE PRODUCTOS -->
<input type="hidden" id="proPernod" value="{{ $cantidadPD }}">
<input type="hidden" id="proExtra" value="{{ $cantidadEX }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        //CAMBIAMOS LA CIUDAD DEL INPUT CIUDAD
        var optionCiudad = $( "#selectCDC option:selected" ).attr('id');
        $('#ciudadSelected').val(optionCiudad);
        $("#selectCDC").change(function(){
            var optionCiudad = $( "#selectCDC option:selected" ).attr('id');
            $('#ciudadSelected').val(optionCiudad);
        });

        $("#inputSelect").change(function(){
            var selectValue = $(this).val();
            switch (selectValue) {
                case "1":
                    $(".divOculto").show();
                    $("#productosPernod").hide();
                    $("#productosAdicional").hide();
                    break;

                default:
                    $(".divOculto").hide();
                    $("#productosPernod").show();
                    $("#productosAdicional").show();
                    break;
            }
        });

        $("#addPernod").click(function(){
            var selectValue = $('#inputAdd').val();

            switch (selectValue) {
                case "1": addPernod(); break;
                case "2": addEspecial(); break;
            }
        });
    });
</script>

<!-- PRODUCTO PERNOD -->
<script type="text/javascript">

    var pPD = 0; //INCIAL DE PRODUCTOS PERNOD

    $(document).ready(function() {
        pPD = $('#proPernod').val(); //INICIAL DE PRODUCTOS PERNOD
    });

    function addPernod(){
        pPD++; //Increment field counter
        console.log("pPD: "+pPD);

        var wrapper2 = $('.field_wrapper2'); //Input field wrapper
        var fieldHTML2 = '<div class="row col-md-12 mt-1" id="proPernod-'+pPD+'">'
                            +'<div class="col">'
                                +'<div class="row d-flex justify-content-end mt-1">'

                                    +'<div class="form-group col">'
                                        +'<label for="">Referencia - Producto:</label>'
                                        +'<select name="pernod['+pPD+'][ref]" class="form-control" required>'
                                            @foreach($marcas as $marca)
                                                +'<option value="{{ $marca->referencia }}">{{ $marca->marca }}</option>'
                                            @endforeach
                                        +'</select>'
                                    +'</div>'

                                    +'<div class="form-group col">'
                                        +'<label for="">Precio:</label>'
                                        +'<input name="pernod['+pPD+'][precio]" type="number" class="form-control" step=".001" required>'
                                    +'</div>'

                                    +'<div class="form-group col">'
                                        +'<label for="">Cantidad:</label>'
                                        +'<input name="pernod['+pPD+'][cantidad]" type="number" class="form-control" step=".001" required>'
                                    +'</div>'

                                    +'<div class="form-group col">'
                                        +'<label class="d-block" for="">Formato:</label>'
                                        +'<div class="form-check form-check-inline">'
                                            +'<input class="form-check-input" type="radio" value="copa" checked name="pernod['+pPD+'][forma]" id="pernod['+pPD+'][forma]0">'
                                            +'<label class="form-check-label">Copa</label>'
                                        +'</div>'
                                        +'<div class="form-check form-check-inline">'
                                            +'<input class="form-check-input" type="radio" value="botella" name="pernod['+pPD+'][forma]" id="pernod['+pPD+'][forma]1">'
                                            +'<label class="form-check-label">Botella</label>'
                                        +'</div>'
                                        +'<div class="form-check form-check-inline">'
                                            +'<input class="form-check-input" type="radio" value="jarra" name="pernod['+pPD+'][forma]" id="pernod['+pPD+'][forma]2">'
                                            +'<label class="form-check-label">Jarra</label>'
                                        +'</div>'
                                    +'</div>'

                                    +'<div class="form-group col">'
                                        +'<a href="javascript:void(2);" class="remove_button2" title="Remove field 2"><i class="fas fa-trash text-danger mt-3 mr-2"></i></a>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>';
        $(wrapper2).append(fieldHTML2);
    }

    $(document).ready(function() {
        var wrapper2 = $('.field_wrapper2'); //Input field wrapper
        $(wrapper2).on('click', '.remove_button2', function(e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').parent('div').parent('div').parent('div').remove(); //Remove field html
            //pPD--; //Decrement field counter
        });
    });
</script>





<!-- PRODUCTO EXTRA -->
<script type="text/javascript">

    var pEX = 0; //INCIAL DE PRODUCTOS EXTRA

    $(document).ready(function() {
        pEX = $('#proExtra').val(); //INICIAL DE PRODUCTOS EXTRA
    });

    function addEspecial(){
        pEX++; //Increment field counter
        console.log("pPD: "+pEX);

        var wrapper3 = $('.field_wrapper3'); //Input field wrapper
        var fieldHTML3 = '<div class="row col-md-12 mt-1" id="proExtra-'+pEX+'">'
                            +'<div class="col">'
                                +'<div class="row d-flex justify-content-end mt-1">'
                                    +'<div class="form-group col">'
                                        +'<label for="">Nombre del Producto:</label>'
                                        +'<input name="extra['+pEX+']" type="text" class="form-control" required>'
                                    +'</div>'
                                    +'<div class="form-group col">'
                                        +'<a href="javascript:void(3);" class="remove_button3" title="Remove field 3"><i class="fas fa-trash text-danger mt-3 mr-2"></i></a>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>';
        $(wrapper3).append(fieldHTML3);
    }

    $(document).ready(function() {
        var wrapper3 = $('.field_wrapper3'); //Input field wrapper
        $(wrapper3).on('click', '.remove_button3', function(e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').parent('div').parent('div').parent('div').remove(); //Remove field html
            //pEX--; //Decrement field counter
        });
    });
</script>
