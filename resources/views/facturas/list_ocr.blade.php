@extends('layouts.app')
@section('content')

@if(Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if(Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if(Session::has('param'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('param') }}
    </div>
@endif

@if(Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<div class="row">
    <div class="col p-3">
        <h3>Listado de facturas por OCR</h3>
    </div>
</div>

<!-- BOTONES DE OPCIONES -->
{{-- <div class="d-block">
    <button class="bg-secondary btn btn-sm text-white" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Recoger lote de facturas de superlikers
    </button>
</div> --}}

<div class="d-block">
    <form method="post" class="mb-0" action="{{ route('facturas.sendparam') }}">
        @csrf
        <button type="submit" class="bg-secondary btn btn-sm text-white"
            {{ !$jobs->isEmpty() ? "disabled" : "" }}>
            Enviar las facturas a parametrizar ( OCR )
        </button>
    </form>

    @if(!$jobs->isEmpty())
        <h5 class="mb-5 mt-2 text-dark">
            Hay facturas en proceso de parametrización por debajo ({{ $jobs->count() }}), espere a que culminen para
            solicitar nuevas facturas a parametrizar.
        </h5>
    @endif()
</div>
<!-- FIN BOTONES -->

<!-- PAGINACIÓN -->
{{ $facturas->links() }}

<table class="table mx-auto" id="example">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Programa</th>
            <th scope="col">Referencia</th>
            <th scope="col">Fecha Impresa</th>
            <th scope="col">Fecha Subida (SL)</th>
            <th scope="col">CDC</th>
            <th scope="col">Folio</th>
            <th scope="col">Estado</th>
            <th scope="col">Imagen</th>
            <th scope="col">Participante</th>
            <th scope="col">Parametrizar</th>
        </tr>
    </thead>
    <tbody>
        @foreach($facturas as $invoice)
            <tr>
                <td class="font-weight-bold">{{ $invoice->id_factura }}</td>
                <td class="font-weight-bold">{{ $invoice->programa }}</td>
                <td class="font-weight-bold">{{ $invoice->referencia }}</td>
                <td class="text-center">{{ $invoice->registro }}</td>
                <td class="text-center">{{ $invoice->user_upload }}</td>
                <td>{{ $invoice->cdc ? $invoice->cdc : "N/A" }}</td>
                <td>{{ $invoice->folio }}</td>
                <td>{{ $invoice->state }}</td>
                <td class="text-white align-middle">
                    <a href="{{ $invoice->photo }}" target="blank" class="my-auto">
                        <button class="bg-secondary btn btn-sm text-white">
                            Ver Imagen
                        </button>
                    </a>
                </td>
                {{-- <td class="text-center ">{{ $invoice->participante->email }}</td> --}}
                <td class="text-center "></td>
                <td class="text-center">
                    <form method="post" class=""
                        action="{{ route('facturas.param', $invoice->id_factura) }}">
                        @csrf
                        <button type="submit" class="bg-danger btn btn-sm text-white">
                            Enviar
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

{{-- <!-- MODAL DE SOLICITAR LOTE DE FACTURAS -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                    Traer lote de facturas de Superlikers
                </h5>
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close" id="closemodal"
                    style="background: #053F72; color: white">
                    <span aria-hidden="true">&times;</span>
                    <script>
                        $('#closemodal').click(function () {
                            $('#exampleModal').modal('hide');
                        });

                    </script>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('facturas.retrieve') }}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <label for="">
                            esto traera facturas no almacenadas y en estado no pending desde 7 días hacia atras, hasta
                            el día de hoy.
                        </label>
                        <div class="col">
                            <label for="">¿Cantidad de facturas? (Máximo 1000 facturas):</label>
                            <input name="cantidad" id="factCant" type="number" min=0 max=1000 value="1"
                                class="form-control @error('¿Cantidad') is-invalid @enderror">
                            @error('¿Cantidad')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6"
                    style="background-color: #053F72; border-radius: 40px; color: white">
                    SOLICITAR LOTES
                </button>
            </div>
            </form>
        </div>
    </div>
</div> --}}
<!-- FIN MODAL -->

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
{{-- <script>
    $(document).ready(function () {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }

        $("#factCant").change(function () {
            var value = $(this).val();

            if (value > 1000)
                $(this).val(1000);
            else if (value <= 0)
                $(this).val(1);
        });
    });

</script> --}}
