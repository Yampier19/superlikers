@extends('layouts.app')
@section('content')

<div class="row">
  <div class="col p-3">
      <h3>Listado de facturas enviadas</h3>
  </div>
</div>

<!-- PAGINACIÓN -->
{{ $facturas->links() }}

<div class="table-responsive">
<table class="table mx-auto" id="example">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Programa</th>
        <th scope="col">Referencia</th>
        <th scope="col">Fecha Impresa</th>
        <th scope="col">Fecha Subida (SL)</th>
        <th scope="col">CDC</th>
        <th scope="col">Folio</th>
        <th scope="col">Estado</th>
        <th scope="col">Imagen</th>
        <th scope="col">Participante</th>
        <th scope="col">Productos</th>
      </tr>
    </thead>
    <tbody>
      @foreach($facturas as $invoice)
        <tr>
          <td class="font-weight-bold">{{ $invoice->id_factura }}</td>
          <td class="font-weight-bold">{{ $invoice->programa }}</td>
          <td class="font-weight-bold">{{ $invoice->referencia }}</td>
          <td class="text-center">{{ $invoice->registro }}</td>
          <td class="text-center">{{ $invoice->user_upload }}</td>
          <td>{{ $invoice->cdc ? $invoice->cdc : "N/A" }}</td>
          <td>{{ $invoice->folio }}</td>
          <td>{{ $invoice->state }}</td>
          <td class="text-white align-middle">
            <a href="{{ $invoice->photo }}" target="blank" class="my-auto">
              <button class="bg-secondary btn btn-sm text-white">
                Ver Imagen
              </button>
            </a>
          </td>
          <td class="text-center">{{ $invoice->participante->email }}</td>
          <td class="text-center">
            <button class="bg-danger btn btn-sm text-white facturaSelected" id="{{ $invoice->id_factura }}" 
              onclick="productos(<?php echo $invoice->id_factura ?>)">
              Productos
            </button>
          </td>
        </tr>
      @endforeach
    </tbody>
</table>
</div>

<!-- MODAL DE VER PRODUCTOS DE LA FACTURA -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                Productos de la factura - Código (<span id="factID"></span>)
              </h5>
              <button type="button" class="close btn" data-dismiss="modal" aria-label="Close" id="closemodal" style="background: #053F72; color: white">
                  <span aria-hidden="true">&times;</span>
                  <script>
                      $('#closemodal').click(function() {
                          $('#exampleModal').modal('hide');
                      });
                  </script>
              </button>
          </div>
          <div class="modal-body">
            <!-- PRODUCTOS PERNOT -->
            <h6>Productos de la Marca Pernod</h6>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Referencia</th>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Linea</th>
                  <th scope="col">Formato</th>
                </tr>
              </thead>
              <tbody id="productoPernot">
                <tr>
                  <td>9878 - Beefeater Dry</td>
                  <td>1</td>
                  <td>150.000</td>
                  <td>GINEBRA</td>
                  <td>copa</td>
                </tr>
              </tbody>
            </table>

            <!-- PRODUCTOS EXTRAS -->
            <h6>Productos extras de la factura</h6>
            <ul id="productoExtra">
              <li>Vaso Limon</li>
              <li>Chelada</li>
            </ul>
          </div>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<input type="hidden" id="urlModal" value="{{ route('facturas.productos') }}">

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
  $(document).ready(function() {
      if($('#showerror').val() == 1){
        $('.invalid-feedback').css('display','block');
        $('#exampleModal').modal('show');
      }
  });

  function productos(id_factura){
        
    var id = id_factura;
    var url = $('#urlModal').val();
    var html = "";
    var html2 = "";

    $.ajax({
        url: url+'/'+id,
        data: {
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        type: 'POST',
        success: function(response){
            if(response.pernots){
                let pernots = response.pernots;
                let extras = response.extras;
                console.log(pernots);
                console.log(extras);
                
                //Arreglamos el Modal
                $('#factID').text(id);
                $('#productoPernot').empty();
                $('#productoExtra').empty();

                //Productos Pernod
                if (pernots.length === 0){
                    html = "<td colspan='4'>No posee productos pernod la factura</td>";
                }
                else{
                    pernots.forEach(function(element){
                        html += "<tr>";
                        html += "<td>"+element.referencia+"</td>";
                        html += "<td>"+element.quantity+"</td>";
                        html += "<td>"+element.price+"</td>";
                        html += "<td>"+element.line+"</td>";
                        html += "<td>"+element.formato+"</td>";
                        html += "</tr>";
                    });
                }

                //Productos extras
                if (extras.length === 0){
                    html2 = "<li>No posee productos extras la factura...</li>";
                }
                else{
                    html2 = "";
                    extras.forEach(function(element){
                        html2 += "<li>"+element.nombre+"</li>";
                    });
                }

                $("#productoPernot").append(html);
                $("#productoExtra").append(html2);
                $('#exampleModal').modal('show');
            }
            else{
                Swal.fire(
                    response.message,
                    'Presiona el boton para cerrar el modal',
                    'warning'
                )
            }
        }
    });
  }
</script>