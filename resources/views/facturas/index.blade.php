@extends('layouts.app')
@section('content')

@if (Session::has('delete'))
  <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
      {{ Session::get('delete') }}
  </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('param'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('param') }}
  </div>
@endif

@if (Session::has('edit'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('edit') }}
  </div>
@endif

<div class="row">
    <div class="col p-3">
        <h3>Panel de facturas</h3>
    </div>
</div>

<div class="row">
  <div class="col-lg-8">
    <div class="row">
      
      <div class="col-xl-12">
        <div class="row">
          <div class="col-md-4">
            <div class="card">
              <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                  <i class="fas fa-landmark opacity-10 " aria-hidden="true"></i>
                </div>
              </div>
              <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Facturas OCR</h6>
                <span class="text-xs">Solicitar Facturas a superlikers y parametrizar lotes</span>
                <hr class="horizontal dark my-3">
                <a href="{{ route('facturas.ocr') }}">
                  <h6 class="mb-0">Ver más</h6>
                </a>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                  <i class="fas fa-landmark opacity-10" aria-hidden="true"></i>
                </div>
              </div>
              <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Enviadas</h6>
                <span class="text-xs">Facturas que fueron registradas exitosamente a superlikers</span>
                <hr class="horizontal dark my-3">
                <a href="{{ route('facturas.submit') }}">
                  <h6 class="mb-0">Ver más</h6>
                </a>
              </div>
            </div>
          </div>

          <div class="col-md-4 mt-md-0 mt-4">
            <div class="card">
              <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                  <i class="fab fa-paypal opacity-10" aria-hidden="true"></i>
                </div>
              </div>
              <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Pendientes</h6>
                <span class="text-xs">Facturas que fallo la parametrización o cayó a estado diferente a aceptado por defecto</span>
                <hr class="horizontal dark my-3">
                <a href="{{ route('facturas.slope') }}">
                  <h6 class="mb-0">Ver más</h6>
                </a>
              </div>
            </div>
          </div>

          <div class="col-md-4 mt-4">
            <div class="card">
              <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                  <i class="fab fa-paypal opacity-10" aria-hidden="true"></i>
                </div>
              </div>
              <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Aceptadas</h6>
                <span class="text-xs">Facturas que estan en estado aceptado</span>
                <hr class="horizontal dark my-3">
                <a href="{{ route('facturas.accepted') }}">
                  <h6 class="mb-0">Ver más</h6>
                </a>
              </div>
            </div>
          </div>

          <div class="col-md-4 mt-4">
            <div class="card">
              <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                  <i class="fab fa-paypal opacity-10" aria-hidden="true"></i>
                </div>
              </div>
              <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">No enviadas</h6>
                <span class="text-xs">Preparadas para enviarse, tambien para solicitar que se envie a registrar</span>
                <hr class="horizontal dark my-3">
                <a href="{{ route('facturas.nosubmit') }}">
                  <h6 class="mb-0">Ver más</h6>
                </a>
              </div>
            </div>
          </div>

        </div>
      </div>
      
    </div>

    <!-- OTRAS HERRAMIENTAS -->
    {{-- <div class="col-md-12 mb-lg-0 mb-4">
      <div class="card mt-4">
        <div class="card-header pb-0 p-3">
          <div class="row">
            <div class="col-6 d-flex align-items-center">
              <h6 class="mb-0">Otras herramientas</h6>
            </div>
          </div>
        </div>
        <div class="card-body p-3">
          <div class="row">
            <div class="col-md-6 mb-md-0 mb-4">
              <a href="{{ route('consumos.index') }}">
                <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                  <img class="w-10 me-3 mb-0" src="https://cdn.shopify.com/s/files/1/2062/4119/files/180return_icon_fe97b8a7-8e13-4ba9-899e-13320242b343_240x240.png?v=1567543656" alt="logo">
                  <h6 class="mb-0">Centro de consumos</h6>
                </div>
              </a>
            </div>
            <div class="col-md-6">
              <a href="{{ route('reportes.index') }}">
                <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                  <img class="w-10 me-3 mb-0" src="https://aiglobaldevelopers.com/images/user/login.png" alt="logo">
                  <h6 class="mb-0">Ir a reportes</h6>
                </div>
              </a> 
            </div>
          </div>
        </div> 
      </div>
    </div> --}}
  </div>

  <!-- BLOQUE DE FACTURAS SUBIDAS, SON SOLO 5 FACTURAS VISIBLES, EN ESTE CASO LAS ULTIMAS -->
  <div class="col-lg-4">
    <div class="card h-100">
      <!-- ENVIAR A VER TODAS -->
      <div class="card-header pb-0 p-3">
        <div class="row">
          <div class="col-6 d-flex align-items-center">
            <h6 class="mb-0">Facturas</h6>
          </div>
          <div class="col-6 text-end">
            <a href="{{ route('facturas.listado') }}" >
              <button class="btn btn-outline-primary btn-sm mb-0">Ver todas</button>
            </a>
          </div>
        </div>
      </div>

      <div class="card-body p-3 pb-0">
        <ul class="list-group">
          @forelse($facturas as $factura)
          <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
            <div class="d-flex flex-column">
              <h6 class="mb-1 text-dark font-weight-bold text-sm">{{ date_format(date_create($factura->registro), "F j, Y") }}</h6>
              <span class="text-xs">{{ $factura->cdc ? $factura->cdc : "NO PARAMETRIZO" }} </span>
            </div>
            <div class="d-flex align-items-center text-sm">
              {{ $factura->state }}
              <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4 facturaSelected" id="{{ $factura->id_factura }}"
                onclick="productos(<?php echo $factura->id_factura ?>)">
                <i class="fas fa-box text-lg me-1 w-100" aria-hidden="true"></i> 
                Productos
              </button>
            </div>
          </li>
          @empty
            <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
              <div class="d-flex flex-column">
                <h6 class="mb-1 text-dark font-weight-bold text-sm">{{ date_format(date_create(date('Y-m-d')), "F j, Y") }}</h6>
                <span class="text-xs">No hay facturas registradas</span>
              </div>
              <div class="d-flex align-items-center text-sm">
                N/A
                <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i class="fas fa-box text-lg me-1" aria-hidden="true"></i> ...</button>
              </div>
            </li>
          @endforelse
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DE VER PRODUCTOS DE LA FACTURA -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                Productos de la factura - Código (<span id="factID"></span>)
              </h5>
              <button type="button" class="close btn" data-dismiss="modal" aria-label="Close" id="closemodal" style="background: #053F72; color: white">
                  <span aria-hidden="true">&times;</span>
                  <script>
                      $('#closemodal').click(function() {
                          $('#exampleModal').modal('hide');
                      });
                  </script>
              </button>
          </div>
          <div class="modal-body">
            <!-- PRODUCTOS PERNOT -->
            <h6>Productos de la Marca Pernod</h6>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Referencia</th>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Linea</th>
                  <th scope="col">Formato</th>
                </tr>
              </thead>
              <tbody id="productoPernot">
                <tr>
                  <td>9878 - Beefeater Dry</td>
                  <td>1</td>
                  <td>150.000</td>
                  <td>GINEBRA</td>
                  <td>copa</td>
                </tr>
              </tbody>
            </table>

            <!-- PRODUCTOS EXTRAS -->
            <h6>Productos extras de la factura</h6>
            <ul id="productoExtra">
              <li>Vaso Limon</li>
              <li>Chelada</li>
            </ul>
          </div>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<input type="hidden" id="urlModal" value="{{ route('facturas.productos') }}">
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
  function productos(id_factura){
        
    var id = id_factura;
    var url = $('#urlModal').val();
    var html = "";
    var html2 = "";

    $.ajax({
        url: url+'/'+id,
        data: {
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        type: 'POST',
        success: function(response){
            if(response.pernots){
                let pernots = response.pernots;
                let extras = response.extras;
                console.log(pernots);
                console.log(extras);
                
                //Arreglamos el Modal
                $('#factID').text(id);
                $('#productoPernot').empty();
                $('#productoExtra').empty();

                //Productos Pernod
                if (pernots.length === 0){
                    html = "<td colspan='4'>No posee productos pernod la factura</td>";
                }
                else{
                    pernots.forEach(function(element){
                        html += "<tr>";
                        html += "<td>"+element.referencia+"</td>";
                        html += "<td>"+element.quantity+"</td>";
                        html += "<td>"+element.price+"</td>";
                        html += "<td>"+element.line+"</td>";
                        html += "<td>"+element.formato+"</td>";
                        html += "</tr>";
                    });
                }

                //Productos extras
                if (extras.length === 0){
                    html2 = "<li>No posee productos extras la factura...</li>";
                }
                else{
                    html2 = "";
                    extras.forEach(function(element){
                        html2 += "<li>"+element.nombre+"</li>";
                    });
                }

                $("#productoPernot").append(html);
                $("#productoExtra").append(html2);
                $('#exampleModal').modal('show');
            }
            else{
                Swal.fire(
                    response.message,
                    'Presiona el boton para cerrar el modal',
                    'warning'
                )
            }
        }
    });
  }
</script>