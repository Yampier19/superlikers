@extends('layouts.app')
@section('content')

@if (Session::has('delete'))
  <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
      {{ Session::get('delete') }}
  </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('param'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('param') }}
  </div>
@endif

@if (Session::has('edit'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('edit') }}
  </div>
@endif

<div class="row">
  <div class="col p-3">
      <h3>Listado de facturas para registrar en superlikers</h3>
  </div>
</div>

<!-- BOTONES DE OPCIONES -->
  <div class="d-block">
    <button class="bg-secondary btn btn-sm text-white" data-bs-toggle="modal" data-bs-target="#filtrarModal">
      Filtrar tabla por fecha en que se tomo la factura.
    </button>
  </div>

  @if($filtro)
    <div class="d-block">
      <a href="{{ route('facturas.nosubmit') }}">
        <button class="bg-secondary btn btn-sm text-white">
          Limpiar filtro
        </button>
      </a>
    </div>
  @endif()

  <div class="d-block">
    <button class="bg-danger btn btn-sm text-white" data-bs-toggle="modal" data-bs-target="#registrarModal"
      {{ !$jobs->isEmpty() ? "disabled" : "" }}>
      Enviar las facturas a registrar ( Subir a la API de superlikers )
    </button>

    @if(!$jobs->isEmpty())
      <h5 class="mb-5 mt-2 text-dark">
        Hay facturas en proceso de registro por debajo ({{$jobs->count()}}), espere a que culminen para solicitar nuevas facturas a registrar.
      </h5>
    @endif()
  </div>

  <div class="row align-items-center">
    <div class="col-md-4">
        <div class="form-floating mb-3">
            <input type="date" class="form-control" id="fechaComents" placeholder="Fecha">
            <label for="fechaConments">Fecha</label>
        </div>
    </div>

    <div class="col-md-4">
        <button class="btn btn-primary" onclick="comentarios()">Generar comentarios</button>
    </div>

</div>
<!-- FIN BOTONES -->

<!-- PAGINACIÓN -->
{{ $facturas->links() }}

<div class="">
<table class="table overflow-scroll" id="example">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Programa</th>
        <th scope="col">Referencia</th>
        <th scope="col">Fecha Impresa</th>
        <th scope="col">Fecha Subida (SL)</th>
        <th scope="col">Día Parametrizada</th>
        <th scope="col">CDC</th>
        <th scope="col">Folio</th>
        <th scope="col">Estado</th>
        <th scope="col">Imagen</th>
        <th scope="col">Participante</th>
        <th scope="col">Productos</th>
        {{-- <th scope="col">Opciones</th> --}}
      </tr>
    </thead>
    <tbody>
      @foreach($facturas as $invoice)
        <tr>
          <td class="font-weight-bold">{{ $invoice->id_factura }}</td>
          <td class="font-weight-bold">{{ $invoice->programa }}</td>
          <td class="font-weight-bold">{{ $invoice->referencia }}</td>
          <td class="text-center">{{ $invoice->registro }}</td>
          <td class="text-center">{{ $invoice->user_upload }}</td>
          <td class="text-center">{{ $invoice->created_at }}</td>
          <td>{{ $invoice->cdc ? $invoice->cdc : "N/A" }}</td>
          <td>{{ $invoice->folio }}</td>
          <td>{{ $invoice->state }}</td>
          <td class="text-white align-middle">
            <a href="{{ $invoice->photo }}" target="blank" class="my-auto">
              <button class="bg-secondary btn btn-sm text-white">
                Ver Imagen
              </button>
            </a>
          </td>
          <td class="text-center">{{ $invoice->participante->email }}</td>
          <td class="text-center">
            <button class="bg-danger btn btn-sm text-white facturaSelected" id="{{ $invoice->id_factura }}"
              onclick="productos(<?php echo $invoice->id_factura ?>)">
              Productos
            </button>
          </td>
          {{-- <td class="text-center">
            <a target="blank" href="{{ route('facturas.edit', $invoice->id_factura) }}">
                <button class="bg-secondary btn btn-sm text-white">
                    Editar
                </button>
            </a>
          </td> --}}
        </tr>
      @endforeach
    </tbody>
</table>
</div>

<!-- MODAL DE VER PRODUCTOS DE LA FACTURA -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                Productos de la factura - Código (<span id="factID"></span>)
              </h5>
              <button type="button" class="close btn" data-dismiss="modal" aria-label="Close" id="closemodal" style="background: #053F72; color: white">
                  <span aria-hidden="true">&times;</span>
                  <script>
                      $('#closemodal').click(function() {
                          $('#exampleModal').modal('hide');
                      });
                  </script>
              </button>
          </div>
          <div class="modal-body">
            <!-- PRODUCTOS PERNOT -->
            <h6>Productos de la Marca Pernod</h6>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Referencia</th>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Linea</th>
                  <th scope="col">Formato</th>
                </tr>
              </thead>
              <tbody id="productoPernot">
                <tr>
                  <td>9878 - Beefeater Dry</td>
                  <td>1</td>
                  <td>150.000</td>
                  <td>GINEBRA</td>
                  <td>copa</td>
                </tr>
              </tbody>
            </table>

            <!-- PRODUCTOS EXTRAS -->
            <h6>Productos extras de la factura</h6>
            <ul id="productoExtra">
              <li>Vaso Limon</li>
              <li>Chelada</li>
            </ul>
          </div>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<!-- MODAL DE PARA FILTRAR POR LA FECHA -->
<div class="modal fade" id="filtrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
            <div class="modal-header text-center">
                <h6 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                  Selecciona la fecha a partir de la cual deseas recoger lote de facturas
                </h6>
            </div>
            <div class="modal-body">
              <div class="form-row">
                  <div class="col">
                      <label for="">Fecha:</label>
                      <input name="factFecha" id="factFecha" type="date" class="form-control">
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button class="btn mx-auto col-md-6 selectFact" style="background-color: #053F72; border-radius: 40px; color: white">
                    FILTRAR LOTE
                </button>
            </div>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<!-- MODAL PARA ENVIAR A REGISTRAR LAS FACTURAS DENTRO DE UN RANGO DE TIEMPO ESTABLECIDO -->
<div class="modal fade" id="registrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
            <div class="modal-header text-center">
                <h6 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                  Selecciona las fechas a partir de las cuales deseas subir facturas a superlikers
                </h6>
            </div>
            <form action="{{ route('facturas.sendRegistro') }}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="modal-body">
                <div class="form-row">
                    <div class="col my-3">
                        <label for="">Fecha Inicial:</label>
                        <input name="factInicial" id="factInicial" type="date" class="form-control" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col my-3">
                        <label for="">Fecha Final (Opcional):</label>
                        <input name="factFinal" id="factFinal" type="date" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col my-3">
                        <label for="">Enviar por fecha de lectura o fecha en la cual el usuario la cargo a superlikers:</label>
                        <select name="tipo" id="tipo" class="form-control" required>
                            <option value="" selected disabled>Selecciona Centro de Consumo...</option>
                            <option value="lectura">Fecha de lectura</option>
                            <option value="usuario">Fecha de registro del Usuario</option>
                        </select>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="submit" class="btn mx-auto col-md-6 selectFact"
                          style="background-color: #053F72; border-radius: 40px; color: white">
                      REGISTRAR LOTE
                  </button>
              </div>
            </form>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<input type="hidden" id="urlModal" value="{{ route('facturas.productos') }}">
<input type="hidden" id="filtroModal" value="{{ route('facturas.nosubmit') }}">

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<script>
    function comentarios(){

        let inputs = $('#fechaComents');

        let asociado = $(inputs).val();

        let html = `<iframe class='w-100 h-100' src='/facturas/add/coments/${asociado}' title='Comentarios'></iframe>`

        $('.comentsModal').html(html);

        let modalQ = new bootstrap.Modal(document.getElementById('comentsModal'), {
        keyboard: false
        });

        modalQ.show();


    }
</script>

<div class="modal fade" id="comentsModal" tabindex="-1" aria-labelledby="comentsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
      <div class="modal-content ">
        <div class="modal-header">
          <h5 class="modal-title" id="comentsModalLabel">Comentarios</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body comentsModal">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          
        </div>
      </div>
    </div>
  </div>

<script>
  $(document).ready(function() {
      if($('#showerror').val() == 1){
        $('.invalid-feedback').css('display','block');
        $('#exampleModal').modal('show');
      }

      $('.selectFact').click(function(){
        var fecha = $('#factFecha').val();
        var url = $('#filtroModal').val();

        if(fecha)
          location.href = url+"/"+fecha;
      });
  });

  function productos(id_factura){

    var id = id_factura;
    var url = $('#urlModal').val();
    var html = "";
    var html2 = "";

    $.ajax({
        url: url+'/'+id,
        data: {
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        type: 'POST',
        success: function(response){
            if(response.pernots){
                let pernots = response.pernots;
                let extras = response.extras;
                console.log(pernots);
                console.log(extras);

                //Arreglamos el Modal
                $('#factID').text(id);
                $('#productoPernot').empty();
                $('#productoExtra').empty();

                //Productos Pernod
                if (pernots.length === 0){
                    html = "<td colspan='4'>No posee productos pernod la factura</td>";
                }
                else{
                    pernots.forEach(function(element){
                        html += "<tr>";
                        html += "<td>"+element.referencia+"</td>";
                        html += "<td>"+element.quantity+"</td>";
                        html += "<td>"+element.price+"</td>";
                        html += "<td>"+element.line+"</td>";
                        html += "<td>"+element.formato+"</td>";
                        html += "</tr>";
                    });
                }

                //Productos extras
                if (extras.length === 0){
                    html2 = "<li>No posee productos extras la factura...</li>";
                }
                else{
                    html2 = "";
                    extras.forEach(function(element){
                        html2 += "<li>"+element.nombre+"</li>";
                    });
                }

                $("#productoPernot").append(html);
                $("#productoExtra").append(html2);
                $('#exampleModal').modal('show');
            }
            else{
                Swal.fire(
                    response.message,
                    'Presiona el boton para cerrar el modal',
                    'warning'
                )
            }
        }
    });
  }
</script>
