@extends('layouts.app')
@section('content')

@if (Session::has('delete'))
  <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
      {{ Session::get('delete') }}
  </div>
@endif

@if (Session::has('param'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('param') }}
  </div>
@endif

<div class="row mb-5">
  <div class="col p-3">
      <h3>Usuarios Registrados</h3>
  </div>
</div>

<!-- PAGINACIÓN -->
{{ $usuarios->links() }}

<div class="table-responsive">
  <table class="table mx-auto" id="example">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nombre</th>
        <th scope="col">Email / UID</th>
        <th scope="col">Número de Facturas Procesadas</th>
        <th scope="col">Opciones</th>
      </tr>
    </thead>
    <tbody>
    @foreach($usuarios as $usuario)
        <tr>
          <td class="font-weight-bold">{{ $usuario->id }}</td>
          <td class="font-weight-bold">{{ $usuario->name }}</td>
          <td class="">{{ $usuario->email }}</td>
          <td class="text-center">
            {{ $usuario->total }} <i class="fas fa-file-invoice" aria-hidden="true"></i>
          </td>
          <td class="text-white align-middle">
            <a href="{{ route('usuarios.list_tracking', $usuario->email) }}" class="my-auto">
              <button class="bg-secondary btn btn-sm text-white">
                Ver Facturas
              </button>
            </a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection