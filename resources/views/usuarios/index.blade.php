@extends('layouts.app')
@section('content')

@if (Session::has('delete'))
  <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
      {{ Session::get('delete') }}
  </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('param'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('param') }}
  </div>
@endif

@if (Session::has('edit'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('edit') }}
  </div>
@endif

<div class="row">
    <div class="col p-3">
        <h3>Panel de usuarios</h3>
    </div>
</div>

<div class="row">
    <div class="col-lg-8">
      <div class="row">
        
        <div class="col-xl-12">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header mx-4 p-3 text-center">
                  <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                    <i class="fas fa-landmark opacity-10 " aria-hidden="true"></i>
                  </div>
                </div>
                <div class="card-body pt-0 p-3 text-center">
                  <h6 class="text-center mb-0">Usuarios Registrados</h6>
                  <span class="text-xs">Listado de todos los usuarios registrados con factura tomada en superlikers</span>
                  <hr class="horizontal dark my-3">
                  {{-- <a href="{{ route('usuarios.list_register') }}">
                    <h6 class="mb-0">Ver más</h6>
                  </a> --}}
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="card">
                <div class="card-header mx-4 p-3 text-center">
                  <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                    <i class="fas fa-landmark opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
                <div class="card-body pt-0 p-3 text-center">
                  <h6 class="text-center mb-0">Facturas de Usuario</h6>
                  <span class="text-xs">Todas las facturas enviadas a superlikers por parte de un usuario</span>
                  <hr class="horizontal dark my-3">
                  <a href="{{ route('usuarios.list_tracking') }}">
                    <h6 class="mb-0">Ver más</h6>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>

      <!-- LINK DE REDIRECCIÓN -->
      <div class="col-md-12 mb-lg-0 mb-4">
        <div class="card mt-4">
          <div class="card-header pb-0 p-3">
            <div class="row">
              <div class="col-6 d-flex align-items-center">
                <h6 class="mb-0">Otras herramientas</h6>
              </div>
            </div>
          </div>
          <div class="card-body p-3">
            <div class="row">
              <div class="col-md-6 mb-md-0 mb-4">
                <a href="{{ route('consumos.index') }}">
                  <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                    <img class="w-10 me-3 mb-0" src="https://cdn.shopify.com/s/files/1/2062/4119/files/180return_icon_fe97b8a7-8e13-4ba9-899e-13320242b343_240x240.png?v=1567543656" alt="logo">
                    <h6 class="mb-0">Centro de consumos</h6>
                  </div>
                </a>
              </div>
              <div class="col-md-6">
                <a href="{{ route('reportes.index') }}">
                  <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                    <img class="w-10 me-3 mb-0" src="https://aiglobaldevelopers.com/images/user/login.png" alt="logo">
                    <h6 class="mb-0">Ir a reportes</h6>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- BLOQUE DE LA DERECHA -->
    <div class="col-lg-4">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <div class="row">
            <div class="col-12 d-flex align-items-center text-center">
              <h6 class="mb-0">Usuarios con mayor número de facturas</h6>
            </div>
          </div>
        </div>
        <div class="card-body p-3 pb-0">
          <ul class="list-group">
            @forelse($usuarios as $usuario)
              <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                <div class="d-flex flex-column">
                  <h6 class="mb-1 text-dark font-weight-bold text-sm">{{ $usuario->email }}</h6>
                  <?php $nombre = substr($usuario->name, 0, strrpos($usuario->name, " ")); ?>
                  <span class="text-xs">{{ $nombre }}</span>
                </div>
                <div class="d-flex align-items-center text-sm">
                  <span class="font-weight-bold">{{ $usuario->total }}</span>
                  <a href="{{ route('usuarios.list_tracking', $usuario->email) }}">
                    <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4 facturaSelected" id="{{ $usuario->email }}">
                      <i class="fas fa-file-invoice text-lg me-1 w-100" aria-hidden="true"></i> 
                    </button>
                  </a>
                </div>
              </li>
            @empty
              <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                <div class="d-flex flex-column">
                  <h6 class="mb-1 text-dark font-weight-bold text-sm">{{ date_format(date_create(date('Y-m-d')), "F j, Y") }}</h6>
                  <span class="text-xs">No hay usuarios/facturas registradas</span>
                </div>
                <div class="d-flex align-items-center text-sm">
                  N/A
                  <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i class="fas fa-file-invoice text-lg me-1" aria-hidden="true"></i> 0</button>
                </div>
              </li>
            @endforelse
          </ul>
        </div>
      </div>
    </div>
    <!-- FIN BLOQUE DE LA DERECHA -->
</div>

@endsection