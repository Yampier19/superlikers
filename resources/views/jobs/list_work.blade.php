@extends('layouts.app')
@section('content')

@if (Session::has('delete'))
  <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
      {{ Session::get('delete') }}
  </div>
@endif

@if (Session::has('param'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('param') }}
  </div>
@endif

<div class="row">
  <div class="col p-3">
      <h3>Trabajos Pendientes o Procesandose en segundo plano por superlikers</h3>
  </div>
</div>

<!-- BOTONES DE OPCIONES -->
  <div class="d-block">
    <button class="bg-secondary btn btn-sm text-white" data-bs-toggle="modal" data-bs-target="#filtrarModal">
      Filtrar tabla
    </button>
  </div>
    
  @if($filtro)
    <div class="d-block">
      <a href="{{ route('jobs.work') }}">
        <button class="bg-secondary btn btn-sm text-white">
          Limpiar filtro
        </button>
      </a>
    </div>
  @endif()
<!-- FIN BOTONES -->

<div class="table-responsive">
<table class="table mx-auto" id="example">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Trabajo</th>
        <th scope="col">Intentos</th>
        <th scope="col">Registro</th>
        <th scope="col">Payload (COD Job)</th>
        <th scope="col">Opción</th>
      </tr>
    </thead>
    <tbody>
      @foreach($jobs as $job)
        <tr>
          <td class="font-weight-bold">{{ $job->id }}</td>
          <td class="font-weight-bold">
            @switch($job->queue)
              @case("paramJob") Parametrizando @break
              @case("reParamJob") Re-Parametrizado de una vieja @break
              @case("registrarInv") Registrando en Superlikers @break
              @default No referenciado
            @endswitch
          </td>
          <td class="text-center">{{ $job->attempts }}</td>
          <td class="text-center">{{ $job->created_at }}</td>
          <td class="text-center">{{ $job->payload }}</td>
          <td class="text-center">
            <form method="post" class="reParamFact" action="{{ route('jobs.delete', $job->id) }}">
              @csrf
              <button type="button" class="btn btn-sm text-white formSubmit" style="background-color: #053F72 !important">
                Eliminar Job
              </button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
</table>
</div>

<!-- MODAL DE PARA FILTRAR POR TIPO DE JOB -->
<div class="modal fade" id="filtrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
            <div class="modal-header text-center">
                <h6 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                  Selecciona el tipo de trabajo que quieres filtrar
                </h6>
            </div>
            <div class="modal-body">
              <div class="form-row">
                  <div class="col">
                      <label for="">Tipo de Trabajo:</label>
                      <select name="factJob" id="factJob" class="form-control">
                          <option value="" selected disabled>Selecciona el tipo de trabajo...</option>
                          <option value="paramJob">Parametrizaciones</option>
                          <option value="reParamJob">Re-Parametrización de Antiguas</option>
                          <option value="registrarInv">Registrandose en Superlikers</option>
                      </select>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button class="btn mx-auto col-md-6 selectJob" style="background-color: #053F72; border-radius: 40px; color: white">
                    FILTRAR TRABAJOS
                </button>
            </div>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<input type="hidden" id="filtroModal" value="{{ route('jobs.work') }}">

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
  $(document).ready(function() {
      if($('#showerror').val() == 1){
        $('.invalid-feedback').css('display','block');
        $('#exampleModal').modal('show');
      }

      $('.selectJob').click(function(){
        var job = $('#factJob').val();
        var url = $('#filtroModal').val();

        if(job)
          location.href = url+"/"+job;
      });
  });
</script>