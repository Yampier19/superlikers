@extends('layouts.app')
@section('content')

@if (Session::has('delete'))
  <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
      {{ Session::get('delete') }}
  </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('param'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('param') }}
  </div>
@endif

@if (Session::has('edit'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('edit') }}
  </div>
@endif

<div class="row">
    <div class="col p-3">
        <h3>Panel de Trabajos / Jobs en segundo Plano</h3>
    </div>
</div>

<div class="row">
  <div class="col">
    <div class="row">
      
      <div class="col-xl-12">
        <div class="row">
          <div class="col-md-4">
            <div class="card">
              <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                  <i class="fas fa-landmark opacity-10 " aria-hidden="true"></i>
                </div>
              </div>
              <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Jobs Pendientes</h6>
                <span class="text-xs">Trabajos que estan aun en proceso realizandose</span>
                <hr class="horizontal dark my-3">
                <a href="{{ route('jobs.work') }}">
                  <h6 class="mb-0">Ver más</h6>
                </a>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-header mx-4 p-3 text-center">
                <div class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                  <i class="fas fa-landmark opacity-10" aria-hidden="true"></i>
                </div>
              </div>
              <div class="card-body pt-0 p-3 text-center">
                <h6 class="text-center mb-0">Jobs Fallidos</h6>
                <span class="text-xs">Trabajos que incurrieron en error durante su proceso</span>
                <hr class="horizontal dark my-3">
                <a href="{{ route('jobs.fail') }}">
                  <h6 class="mb-0">Ver más</h6>
                </a>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card">
              <div class="card-body p-3 pb-0">
                  <!-- JOBS PENDIENTES -->
                  <div class="d-flex align-items-center text-sm m-auto">
                    <button class="btn btn-link text-dark text-sm mb-0 px-0 m-auto">
                      <i class="fas fa-check-circle text-success text-lg me-1 w-100" aria-hidden="true"></i> 
                      Jobs Pendientes <br>
                      <span class="font-weight-bold">( {{ $jobs->count() }} )</span>
                    </button>
                  </div>

                  <hr>
                  
                  <!-- JOBS FALLADOS -->
                  <div class="d-flex align-items-center text-sm m-auto">
                    <button class="btn btn-link text-dark text-sm mb-0 px-0 m-auto">
                      <i class="fas fa-exclamation-triangle text-danger text-lg me-1 w-100" aria-hidden="true"></i> 
                      Jobs Fallados <br>
                      <span class="font-weight-bold">( {{ $fail->count() }} )</span>
                    </button>
                  </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      
    </div>

    <!-- OTRAS HERRAMIENTAS -->
    {{-- <div class="col-md-12 mb-lg-0 mb-4">
      <div class="card mt-4">
        <div class="card-header pb-0 p-3">
          <div class="row">
            <div class="col-6 d-flex align-items-center">
              <h6 class="mb-0">Otras herramientas</h6>
            </div>
          </div>
        </div>
        <div class="card-body p-3">
          <div class="row">
            <div class="col-md-6 mb-md-0 mb-4">
              <a href="{{ route('consumos.index') }}">
                <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                  <img class="w-10 me-3 mb-0" src="https://cdn.shopify.com/s/files/1/2062/4119/files/180return_icon_fe97b8a7-8e13-4ba9-899e-13320242b343_240x240.png?v=1567543656" alt="logo">
                  <h6 class="mb-0">Centro de consumos</h6>
                </div>
              </a>
            </div>
            <div class="col-md-6">
              <a href="{{ route('facturas.index') }}">
                <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                  <img class="w-10 me-3 mb-0" src="https://aiglobaldevelopers.com/images/user/login.png" alt="logo">
                  <h6 class="mb-0">Ir a facturas</h6>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div> --}}
  </div>
</div>

@endsection