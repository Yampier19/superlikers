@extends('layouts.app')
@section('content')

@if (Session::has('delete'))
  <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
      {{ Session::get('delete') }}
  </div>
@endif

@if (Session::has('param'))
  <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
      {{ Session::get('param') }}
  </div>
@endif

<div class="row">
  <div class="col p-3">
      <h3>Trabajos Fallidos en segundo plano en el server</h3>
  </div>
</div>

<!-- BOTONES DE OPCIONES -->
  <div class="d-block">
    <button class="bg-secondary btn btn-sm text-white" data-bs-toggle="modal" data-bs-target="#filtrarModal">
      Filtrar tabla
    </button>
  </div>
    
  @if($filtro)
    <div class="d-block">
      <a href="{{ route('jobs.fail') }}">
        <button class="bg-secondary btn btn-sm text-white">
          Limpiar filtro
        </button>
      </a>
    </div>
  @endif()

  @if(!$fails->isEmpty())
    <div class="d-block">
        <form method="post" class="mb-0" action="{{ route('jobs.restoreAll') }}">
          @csrf
          <button type="submit" class="bg-danger btn btn-sm text-white">
            Recuperar todos los failed jobs
          </button>
        </form>
    </div>
  @endif()
<!-- FIN BOTONES -->

<div class="table-responsive">
<table class="table mx-auto" id="example">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Trabajo</th>
        <th scope="col">UUID</th>
        <th scope="col">Conexión</th>
        <th scope="col">Error</th>
        <th scope="col">Opción</th>
        <th scope="col">Eliminar</th>
      </tr>
    </thead>
    <tbody>
      @foreach($fails as $fail)
        <tr>
          <td class="font-weight-bold">{{ $fail->id }}</td>
          <td class="font-weight-bold">
            @switch($fail->queue)
              @case("paramJob") Parametrizando @break
              @case("reParamJob") Re-Parametrizado de una vieja @break
              @case("registrarInv") Registrando en Superlikers @break
              @default No referenciado
            @endswitch
          </td>
          <td class="text-center">{{ $fail->uuid }}</td>
          <td class="text-center">{{ $fail->connection }}</td>
          <td class="text-center">{{ substr($fail->exception, 0, stripos($fail->exception, "(")) }}</td>
          <td class="text-center">
            <form method="post" class="reParamFact" action="{{ route('jobs.restore', $fail->id) }}">
              @csrf
              <button type="submit" class="btn btn-danger btn-sm text-white formSubmit">
                Recuperar Job
              </button>
            </form>
          </td>
          <td class="text-center">
            <form method="post" class="reParamFact" action="{{ route('fail.delete', $fail->id) }}">
              @csrf
              <button type="button" class="btn btn-sm text-white formSubmit" style="background-color: #053F72 !important">
                Eliminar
              </button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
</table>
</div>

<!-- MODAL DE PARA FILTRAR POR TIPO DE JOB -->
<div class="modal fade" id="filtrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
            <div class="modal-header text-center">
                <h6 class="modal-title" id="exampleModalLabel" style="color: #053F72; font-weight: bold">
                  Selecciona el tipo de trabajo fallido que quieres filtrar
                </h6>
            </div>
            <div class="modal-body">
              <div class="form-row">
                  <div class="col">
                      <label for="">Tipo de Trabajo:</label>
                      <select name="factJob" id="factJob" class="form-control">
                          <option value="" selected disabled>Selecciona el tipo de trabajo...</option>
                          <option value="paramJob">Parametrizaciones</option>
                          <option value="reParamJob">Re-Parametrización de Antiguas</option>
                          <option value="registrarInv">Registrandose en Superlikers</option>
                      </select>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button class="btn mx-auto col-md-6 selectJob" style="background-color: #053F72; border-radius: 40px; color: white">
                    FILTRAR TRABAJOS
                </button>
            </div>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<input type="hidden" id="filtroModal" value="{{ route('jobs.fail') }}">

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
  $(document).ready(function() {
      if($('#showerror').val() == 1){
        $('.invalid-feedback').css('display','block');
        $('#exampleModal').modal('show');
      }

      $('.selectJob').click(function(){
        var failed = $('#factJob').val();
        var url = $('#filtroModal').val();

        if(failed)
          location.href = url+"/"+failed;
      });
  });
</script>