@include('layouts.components.head')

    <!-- Sidebar -->
        @include('layouts.components.sidebar')
    <!-- End Sidebar -->

    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    
        <!-- Navbar -->
            @include('layouts.components.navbar')
        <!-- End Navbar -->
        <div class="container-fluid py-4">
          @yield('content')
        </div>
    </main>

    <!-- SidebarRight -->
    @include('layouts.components.paint')
    <!-- End SidebarRight -->

@include('layouts.components.footer')